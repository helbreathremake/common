﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath.Common.Assets;

namespace Helbreath.Common
{
    public static class ItemHelper
    {
        public static Dictionary<int, Item> LoadItems(string configFilePath)
        {

            Dictionary<int, Item> items = new Dictionary<int, Item>();

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Item"))
                        if (!items.ContainsKey(Int32.Parse(reader["Id"])))
                        {
                            Item item = Item.ParseXml(reader);
                            items.Add(item.ItemId, item);
                        }
            reader.Close();

            return items;
        }

        public static int MixColours(int colour1, int colour2)
        {
            Color c1 = Color.FromArgb(colour1);
            Color c2 = Color.FromArgb(colour2);

            int outR = (c1.R + c2.R) >> 1;

            int outG = (c1.G + c2.G) >> 1;

            int outB = (c1.B + c2.B) >> 1;

            Color c = Color.FromArgb(outR, outG, outB);

            return c.ToArgb();
        }

        public static string GetAbbreviatedName(ItemStat stat)
        {
            switch (stat)
            {
                case ItemStat.PoisonResistance: return "PR";
                case ItemStat.PhysicalResistance: return "DR";
                case ItemStat.PhysicalAbsorption: return "PA";
                case ItemStat.MagicAbsorption: return "MA";
                case ItemStat.MagicResistance: return "MR";
                case ItemStat.HPRecovery: return "HP Recover";
                case ItemStat.MPRecovery: return "MP Recover";
                case ItemStat.SPRecovery: return "SP Recover";
                case ItemStat.EarthAbsorption: return "Earth Abs.";
                case ItemStat.AirAbsorption: return "Air Abs.";
                case ItemStat.WaterAbsorption: return "Water Abs.";
                case ItemStat.FireAbsorption: return "Fire Abs.";
                case ItemStat.ManaConverting: return "Mana Conv.";
                case ItemStat.CriticalChance: return "Crit Increase";
                case ItemStat.HPIncrease: return "Max HP+";
                case ItemStat.MPIncrease: return "Max MP+";
                case ItemStat.SPIncrease: return "Max SP+";
                case ItemStat.Critical: return "Critical";
                case ItemStat.Poison: return "Poison";
                case ItemStat.Righteous: return "Righteous";
                case ItemStat.Sharp: return "Sharp";
                case ItemStat.Ancient: return "Ancient";
                case ItemStat.HittingProbability: return "Hit Prob.";
                case ItemStat.Agile: return "Agile";
                case ItemStat.DamageBonus: return "Damage+";
                case ItemStat.Legendary: return "Legendary";
                case ItemStat.ComboDamage: return "Combo Dmg.";
                case ItemStat.Vampiric: return "Vampiric";
                case ItemStat.Zealous: return "Zealous";
                case ItemStat.Piercing: return "Piercing";
                case ItemStat.MaxCrits: return "Max Crits+";
                case ItemStat.Exhaustive: return "Exhaust";
                case ItemStat.CastingProbability: return "Cast Prob.";
                case ItemStat.Experience: return "Experience";
                case ItemStat.Gold: return "Gold";
                case ItemStat.Endurance: return "Endurance";
                case ItemStat.ManaSave: return "Mana Save";
                case ItemStat.Luck: return "Luck";
                case ItemStat.Strength: return "Strength";
                case ItemStat.Dexterity: return "Dexterity";
                case ItemStat.Magic: return "Magic";
                case ItemStat.Intelligence: return "Intelligence";
                case ItemStat.Vitality: return "Vitality";
                case ItemStat.Agility: return "Agility";






                default: return stat.ToString();
            }
        }

        public static string GetAbbreviatedName(ItemCategory category)
        {
            return category.ToString();
        }
    }

}
