﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public class MerchantItem
    {
        public int MerchantId;
        public int Id;
        public string DatabaseId;
        public Item Item;
        public bool Unlimited;

        public MerchantItem(int merchantId)
        {
            this.Id = ++Globals.MerchantItemCounter; // ensures unique id stored in memory
            this.DatabaseId = Guid.NewGuid().ToString(); // database id
            this.MerchantId = merchantId;
        }

        public MerchantItem(int merchantId, Item item, bool unlimited = false)
        {
            this.MerchantId = merchantId;
            this.Id = ++Globals.MerchantItemCounter; // ensures unique id stored in memory
            this.DatabaseId = Guid.NewGuid().ToString(); // database id
            this.Item = item;
            this.Unlimited = unlimited;
        }

        public MerchantItem(int merchantId, Item item, int id, bool unlimited = false)
        {
            this.MerchantId = merchantId;
            this.Id = id; // ensures unique id stored in memory
            this.DatabaseId = Guid.NewGuid().ToString(); // database id
            this.Item = item;
            this.Unlimited = unlimited;
        }

        public void ParseData(DataRow row)
        {
            // parse data row
            Item item = World.ItemConfiguration[(int)row["ItemId"]].Copy((int)row["Count"]);
            item.Endurance = (int)row["Endurance"];
            item.Colour = (int)row["Colour"];
            item.SpecialEffect1 = (int)row["SpecialEffect1"];
            item.SpecialEffect2 = (int)row["SpecialEffect2"];
            item.SpecialEffect3 = (int)row["SpecialEffect3"];
            item.Quality = (ItemQuality)(int)row["Quality"];
            item.Level = (int)row["Level"];
            string[] stats = row["Stats"].ToString().Split(' ');
            for (int i = 0; i < stats.Length / 2; i++)
                item.Stats.Add((ItemStat)Int32.Parse(stats[i * 2]), Int32.Parse(stats[(i * 2) + 1]));
            bool unlimited = (bool)row["Unlimited"];

            // set properties
            this.DatabaseId = row["Id"].ToString();
            this.Item = item;
            this.Unlimited = unlimited;
            this.MerchantId = (int)row["MerchantId"];
        }

        public static MerchantItem Parse(DataRow row)
        {
            MerchantItem item = new MerchantItem((int)row["MerchantId"]);
            item.ParseData(row);
            return item;
        }
    }
}
