﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets;

namespace Helbreath.Common
{
    public class Inventory : IEnumerable<Item>
    {
        private List<Item> inventory;

        //STAT Multipliers
        private int PhysicalAbsorptionMultiplier;

        //Basic Inventory Info
        public int InventoryCount; //add to dialogs
        public int InventoryEquipedCount;
        public int EquipmentCount; //add to dialogs
        public int isEquipedCount;
        public int UpgradeItemCount;
        public int UpgradeIngredientCount;
        public int UpgradeItemIndex = -1;
        public int UpgradeIngredientIndex = -1;
        //public int[] UpgradeIngredientIndexes = new int[Globals.MaxiumUpgradeIngredients]; 
        public int ArrowsIndex = -1;
        public Item Arrows;
        public bool HasArrows;
        public int GoldItemIndex = -1;
        public Item GoldItem;

        //Possible Stat Bonuses from items - All, Types.ItemStat included
        public int MagicResistanceBonus;
        public int ManaSaveBonus; 
        public int PhysicalDamageBonus; 
        public int LuckBonus; //Added Correctly, currently used to survive killing blow, increase itemrarity/stat value on drops
        public int MagicDamageBonus; 
        public int AirAbsorptionBonus; 
        public int EarthAbsorptionBonus;
        public int FireAbsorptionBonus; 
        public int WaterAbsorptionBonus; 
        public int PoisonResistanceBonus; 
        public int HitChanceBonus; 
        public int HPRecoveryBonus; 
        public int MPRecoveryBonus; 
        public int SPRecoveryBonus; 
        public int MagicAbsorptionBonus; 
             
        //PHYSICAL ABSORPTION
        public int PhysicalAbsorptionGeneral;
        public int PhysicalAbsorptionHead; 
        public int PhysicalAbsorptionArms; 
        public int PhysicalAbsorptionBody;
        public int PhysicalAbsorptionLegs;
        public int PhysicalAbsorptionShield;
        public int PhysicalAbsorptionHeadBonus;
        public int PhysicalAbsorptionArmsBonus;
        public int PhysicalAbsorptionBodyBonus;
        public int PhysicalAbsorptionLegsBonus;
        public int PhysicalAbsorptionShieldBonus;
        public int PhysicalResistance;

        public int StrengthBonus; //TODO add to all places
        public int DexterityBonus; //TODO add to all places
        public int IntelligenceBonus; //TODO add to all places
        public int MagicBonus; //TODO add to all places
        public int CastProbabilityBonus;
        public int GoldBonus;
        public int ExperienceBonus; 
        public int MaxHPBonus;
        public int MaxMPBonus; 
        public int MaxSPBonus;
        public int MaxCritsBonus;
        public int VampiricBonus;  
        public int ZealousBonus;
        public int PiercingBonus;
        public int ExhaustiveBonus; 
        public int ManaConvertingBonus;
        public int CriticalIncreaseChanceBonus; 
        public int CriticalDamageBonus; 
        public int PoisonousBonus; 
        public int AgileBonus; //NOT USED - implemented on item.cs
        public int ComboDamageBonus; 
        public int VitalityBonus; //TODO add to all places
        public int AgilityBonus; //TODO add to all places
        public int SpiritAbsorptionBonus;
        public int UnholyAbsorptionBonus;
        public int KinesisAbsorptionBonus; 
        public bool RighteousBonus;
        public bool SharpDamageBonus; //Not used, implemented in Item.cs for weapons //TODO possibly move to allow for sharp objects
        public bool AncientDamageBonus;  //Not used, implemented in Item.cs for weapons //TODO possibly move to allow for ancient objects
        public bool LegendaryBonus; //Not used, implemented in Item.cs for weapons //TODO possibly move to allow for legendary objects

        public bool CompleteFireProtection;
        public bool CompleteAirProtection; 
        public bool CompleteEarthProtection; 
        public bool CompleteWaterProtection; 
        public bool CompleteUnholyProtection; 
        public bool CompleteKinesisProtection; 
        public bool CompleteSpiritProtection; 
        public bool CompleteMagicProtection;

        public bool ItemSet1HasItems;
        public bool ItemSet2HasItems;
        public bool ItemSet3HasItems;
        public bool ItemSet4HasItems;
        public bool ItemSet5HasItems;
         
        //Hero Set  
        public bool Hero;
        public bool HeroWarrior; 
        public bool HeroMage; 
        public bool HeroBattleMage; 
        public bool HeroArcher; 
        public bool HeroLeggings; //add to dialogs?
        public bool HeroHauberk; //add to dialogs?
        public bool HeroCape; //add to dialogs?
        public bool HeroPlate; //add to dialogs?
        public bool HeroHelm; //add to dialogs?
        public bool HeroRobe; //add to dialogs?
        public bool HeroCap; //add to dialogs?
        public bool HeroBattleCap; //add to dialogs?
        public bool HeroBattlePlate; //add to dialogs?
        public bool HeroLeather; //add to dialogs?
        public bool HeroHood; //add to dialogs?

        //Godly Set
        public bool Godly; //Remove? and use SetBonuses?
        public bool GodlyWarrior;
        public bool GodlyMage; 
        public bool GodlyBattleMage;
        public bool GodlyArcher; 
        public bool GodlyLeggings; //add to dialogs
        public bool GodlyHauberk; //add to dialogs
        public bool GodlyCape;//add to dialogs
        public bool GodlyPlate; //add to dialogs
        public bool GodlyHelm; //add to dialogs
        public bool GodlyRobe; //add to dialogs
        public bool GodlyCap; //add to dialogs
        public bool GodlyBattleCap; //add to dialogs
        public bool GodlyBattlePlate; //add to dialogs
        public bool GodlyLeather; //add to dialogs
        public bool GodlyHood; //add to dialogs

        public List<Item> DepleteEnduranceMagicAbsorption = new List<Item>(Globals.MaximumTotalItems);   //TODO recheck how this works
        public List<ItemSet> SetBonuses = new List<ItemSet>(Enum.GetValues(typeof(ItemSet)).Length); //add to dialogs?

        public Dictionary<ItemSpecialAbilityType, int> SpecialAbilitiesActive = new Dictionary<ItemSpecialAbilityType,int>();
        public List<ItemSpecialAbilityType> SpecialAbilitiesPassive = new List<ItemSpecialAbilityType>();
        public int ActivationTime;

        public Item this[int index] { get { return inventory[index]; } set { inventory[index] = value; } }
        public int Count { get { return inventory.Count; } }
        public List<Item> List { get { return inventory; } }

        public Inventory(int maxItems)
        {
            this.inventory = new List<Item>(maxItems);
            Update();
        }

        public void Add(Item item)
        {
            inventory.Add(item);
        }

        public void Clear()
        {
            inventory.Clear();
        }

        private void ClearInventoryCounts()
        {
            //Bonus values to 0
            MagicResistanceBonus =
            ManaSaveBonus =
            PhysicalDamageBonus =
            LuckBonus =
            MagicDamageBonus =
            AirAbsorptionBonus =
            EarthAbsorptionBonus =
            FireAbsorptionBonus =
            WaterAbsorptionBonus =
            PoisonResistanceBonus =
            HitChanceBonus =
            HPRecoveryBonus =
            MPRecoveryBonus =
            SPRecoveryBonus =
            MagicAbsorptionBonus =
            StrengthBonus =
            DexterityBonus =
            IntelligenceBonus =
            MagicBonus = 
            CastProbabilityBonus =
            GoldBonus =
            ExperienceBonus =
            MaxHPBonus =
            MaxMPBonus = 
            MaxSPBonus =
            MaxCritsBonus =
            VampiricBonus =
            ZealousBonus = 
            PiercingBonus =
            ExhaustiveBonus =
            ManaConvertingBonus =
            CriticalIncreaseChanceBonus =
            CriticalDamageBonus =
            PoisonousBonus =
            AgileBonus =
            ComboDamageBonus =
            VitalityBonus =
            AgilityBonus =
            SpiritAbsorptionBonus =
            UnholyAbsorptionBonus =
            KinesisAbsorptionBonus =
            InventoryCount =
            EquipmentCount =
            InventoryEquipedCount =
            isEquipedCount =
            ActivationTime =
            UpgradeIngredientCount =
            UpgradeItemCount =

            //PHYSICAL ABSORPTION
            PhysicalAbsorptionGeneral =
            PhysicalAbsorptionHead =
            PhysicalAbsorptionArms =
            PhysicalAbsorptionBody =
            PhysicalAbsorptionLegs =
            PhysicalAbsorptionShield =
            PhysicalAbsorptionHeadBonus =
            PhysicalAbsorptionArmsBonus =
            PhysicalAbsorptionBodyBonus =
            PhysicalAbsorptionLegsBonus =
            PhysicalAbsorptionShieldBonus =
            PhysicalResistance =
            0;

            //Set Bool Values to False;
            CompleteFireProtection =
            CompleteAirProtection =
            CompleteEarthProtection =
            CompleteWaterProtection =
            CompleteUnholyProtection = 
            CompleteKinesisProtection = 
            CompleteSpiritProtection = 
            CompleteMagicProtection = 
            RighteousBonus =
            SharpDamageBonus =
            AncientDamageBonus =
            LegendaryBonus =
            HasArrows =

            //Sets
            Hero =
            HeroWarrior =
            HeroMage =
            HeroBattleMage =
            HeroArcher =    
            HeroLeggings =
            HeroHauberk =
            HeroCape =
            HeroPlate =
            HeroHelm =
            HeroRobe =
            HeroCap =
            HeroBattleCap =
            HeroBattlePlate =
            HeroLeather =
            HeroHood =
            GodlyLeggings =
            GodlyHauberk =
            GodlyCape =
            GodlyPlate =
            GodlyHelm =
            GodlyRobe =
            GodlyCap =
            GodlyBattleCap =
            GodlyBattlePlate =
            GodlyLeather= 
            GodlyHood =
            Godly =
            GodlyWarrior =
            GodlyMage =
            GodlyBattleMage =
            GodlyArcher = 
      
            ItemSet1HasItems =
            ItemSet2HasItems =
            ItemSet3HasItems =
            ItemSet4HasItems =
            ItemSet5HasItems =
            false;

            //Clear Lists
            DepleteEnduranceMagicAbsorption.Clear(); //Add to dialogs?
            SetBonuses.Clear();
            SpecialAbilitiesActive.Clear();
            SpecialAbilitiesPassive.Clear();

            //Items = null
            GoldItem =
            Arrows =
            null;

            //Item Index Locations
            ArrowsIndex = -1;
            GoldItemIndex = -1;
            UpgradeItemIndex = -1;
            UpgradeIngredientIndex = -1;

            //for (int i = 0; i < UpgradeIngredientIndexes.Length; i++)
            //{
            //    UpgradeIngredientIndexes[i] = -1;
            //}

        }

        public void Update()
        {
            ClearInventoryCounts();

            if (inventory != null && inventory.Count > 0)
            {
                for (int i = 0; i < Globals.MaximumTotalItems; i++)//Both Equiped and InventoryV1 slot (Double counts Equiped items)
                {
                    if (i < Globals.MaximumEquipment) //Only Equiped Item slots
                    {
                        if (inventory[i] != null)
                        {
                            Item item = inventory[i];

                            EquipmentCount++; //List anything in Equipment
                            if (item.IsEquipped) { isEquipedCount++; } //if item is set to equipped, count that item

                            if (item.EffectType == ItemEffectType.AddEffect)  //TODO Change to be easier to understand?
                            {
                                switch (item.Effect1)
                                {
                                    case 1: MagicResistanceBonus += item.Effect2; break;
                                    case 2: ManaSaveBonus += item.Effect2; break;
                                    case 3: PhysicalDamageBonus += item.Effect2; break;
                                    case 4: PhysicalResistance += item.Effect2; break;
                                    case 5: LuckBonus += item.Effect2; break;
                                    case 6: MagicDamageBonus += item.Effect2; break;
                                    case 7: AirAbsorptionBonus += item.Effect2; break;
                                    case 8: EarthAbsorptionBonus += item.Effect2; break;
                                    case 9: FireAbsorptionBonus += item.Effect2; break;
                                    case 10: WaterAbsorptionBonus += item.Effect2; break;
                                    case 11: PoisonResistanceBonus += item.Effect2; break;
                                    case 12: HitChanceBonus += item.Effect2; break;
                                    case 13: HPRecoveryBonus += item.Effect2; break;
                                    case 14: HitChanceBonus += item.Effect2; break;
                                    case 15: MagicAbsorptionBonus += item.Effect2; break;
                                    case 16: StrengthBonus += item.Level; break;
                                    case 17: DexterityBonus += item.Level; break;
                                    case 18: IntelligenceBonus += item.Level; break;
                                    case 19: MagicBonus += item.Level; break;
                                    case 20: PhysicalAbsorptionGeneral += item.Effect2; break;
                                }
                            }

                            if (item.EffectType == ItemEffectType.Angel)
                            {
                                switch ((AngelType)item.Effect1)
                                {
                                    case AngelType.Str: StrengthBonus += item.Level; break;
                                    case AngelType.Dex: DexterityBonus += item.Level; break;
                                    case AngelType.Int: IntelligenceBonus += item.Level; break;
                                    case AngelType.Mag: MagicBonus += item.Level; break;
                                    case AngelType.Vit: VitalityBonus += item.Level; break;
                                    case AngelType.Agi: AgilityBonus += item.Level; break;
                                }
                            }

                            //Base PA, DR, MA + Bonus PA from item stats
                            switch (item.EquipType)
                            {
                                case EquipType.Arms:
                                    {
                                        PhysicalAbsorptionArms += item.PhysicalAbsorption;
                                        PhysicalResistance += item.PhysicalResistance;
                                        MagicAbsorptionBonus += item.MagicAbsorption;
                                        if (item.Stats.ContainsKey(ItemStat.PhysicalAbsorption)) PhysicalAbsorptionArmsBonus += item.Stats[ItemStat.PhysicalAbsorption];
                                        break;
                                    }
                                case EquipType.FullBody:
                                case EquipType.Body:
                                    {
                                        PhysicalAbsorptionBody += item.PhysicalAbsorption;
                                        PhysicalResistance += item.PhysicalResistance;
                                        MagicAbsorptionBonus += item.MagicAbsorption;
                                        if (item.Stats.ContainsKey(ItemStat.PhysicalAbsorption)) PhysicalAbsorptionBodyBonus += item.Stats[ItemStat.PhysicalAbsorption];
                                        break;
                                    }
                                case EquipType.Head:
                                    {
                                        PhysicalAbsorptionHead += item.PhysicalAbsorption;
                                        PhysicalResistance += item.PhysicalResistance;
                                        MagicAbsorptionBonus += item.MagicAbsorption;
                                        if (item.Stats.ContainsKey(ItemStat.PhysicalAbsorption)) PhysicalAbsorptionHeadBonus += item.Stats[ItemStat.PhysicalAbsorption];
                                        break;
                                    }
                                case EquipType.Legs:
                                case EquipType.Feet:
                                    {
                                        PhysicalAbsorptionLegs += item.PhysicalAbsorption;
                                        PhysicalResistance += item.PhysicalResistance;
                                        MagicAbsorptionBonus += item.MagicAbsorption;
                                        if (item.Stats.ContainsKey(ItemStat.PhysicalAbsorption)) PhysicalAbsorptionLegsBonus += item.Stats[ItemStat.PhysicalAbsorption];
                                        break;
                                    }
                                case EquipType.LeftHand:
                                    {
                                        PhysicalAbsorptionShield += item.PhysicalAbsorption;
                                        PhysicalResistance += item.PhysicalResistance;
                                        MagicAbsorptionBonus += item.MagicAbsorption;
                                        if (item.Stats.ContainsKey(ItemStat.PhysicalAbsorption)) PhysicalAbsorptionShieldBonus += item.Stats[ItemStat.PhysicalAbsorption];
                                        break;
                                    }
                                case EquipType.Back:
                                    {
                                        PhysicalAbsorptionGeneral += item.PhysicalAbsorption;
                                        PhysicalResistance += item.PhysicalResistance;
                                        MagicAbsorptionBonus += item.MagicAbsorption;
                                        if (item.Stats.ContainsKey(ItemStat.PhysicalAbsorption)) PhysicalAbsorptionGeneral += item.Stats[ItemStat.PhysicalAbsorption];
                                        break;
                                    }
                                case EquipType.DualHand: //Weapons
                                case EquipType.LeftFinger: //Rings
                                case EquipType.Neck: //Necklaces
                                case EquipType.RightFinger: //Rings
                                case EquipType.RightHand: //Weapons
                                case EquipType.Utility: break; //Angel/Gems
                                default: break;
                            }

                            //SpecialAbilities
                            if (item.SpecialAbilityType != null && item.SpecialAbilityType != ItemSpecialAbilityType.None)
                            {
                                switch (item.SpecialAbilityType)
                                {
                                    //Passive Abilities
                                    case ItemSpecialAbilityType.Blood: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Blood)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.Blood); HitChanceBonus += 50; break; //TODO make use of dictionary values?
                                    case ItemSpecialAbilityType.Berserk: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Berserk)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.Berserk); break;
                                    case ItemSpecialAbilityType.Dark: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Dark)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.Dark); break;
                                    case ItemSpecialAbilityType.Light: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Light)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.Light); break;
                                    case ItemSpecialAbilityType.DemonSlayer: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.DemonSlayer)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.DemonSlayer); break;
                                    case ItemSpecialAbilityType.Kloness: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Kloness)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.Kloness); break;
                                    case ItemSpecialAbilityType.BowLine: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.BowLine)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.BowLine); break;//TODO Not Implemented
                                    case ItemSpecialAbilityType.Storm: if (!SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Storm)) SpecialAbilitiesPassive.Add(ItemSpecialAbilityType.Storm); break;//TODO Not Implemented
                                    case ItemSpecialAbilityType.Unknown: break; //?? WHat is this for ??//TODO Not Implemented

                                    //Active Abilities
                                    case ItemSpecialAbilityType.MerienArmour:
                                        {
                                            if (!SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienArmour)) { SpecialAbilitiesActive.Add(ItemSpecialAbilityType.MerienArmour, item.SpecialEffect1); }
                                            else { SpecialAbilitiesActive[ItemSpecialAbilityType.MerienArmour] += item.SpecialEffect1; ActivationTime += item.SpecialEffect1; }
                                            break;
                                        }
                                    case ItemSpecialAbilityType.MerienShield:
                                        {
                                            if (!SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienShield)) { SpecialAbilitiesActive.Add(ItemSpecialAbilityType.MerienShield, item.SpecialEffect1); }
                                            else { SpecialAbilitiesActive[ItemSpecialAbilityType.MerienShield] += item.SpecialEffect1; ActivationTime += item.SpecialEffect1; }
                                            break;
                                        }
                                    case ItemSpecialAbilityType.IceWeapon:
                                        {
                                            if (!SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.IceWeapon)) { SpecialAbilitiesActive.Add(ItemSpecialAbilityType.IceWeapon, item.SpecialEffect1); }
                                            else { SpecialAbilitiesActive[ItemSpecialAbilityType.IceWeapon] += item.SpecialEffect1; ActivationTime += item.SpecialEffect1; }
                                            break;
                                        }
                                    case ItemSpecialAbilityType.MedusaWeapon:
                                        {
                                            if (!SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MedusaWeapon)) { SpecialAbilitiesActive.Add(ItemSpecialAbilityType.MedusaWeapon, item.SpecialEffect1); }
                                            else { SpecialAbilitiesActive[ItemSpecialAbilityType.MedusaWeapon] += item.SpecialEffect1; ActivationTime += item.SpecialEffect1; }
                                            break;
                                        }
                                    case ItemSpecialAbilityType.XelimaWeapon:
                                        {
                                            if (!SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.XelimaWeapon)) { SpecialAbilitiesActive.Add(ItemSpecialAbilityType.XelimaWeapon, item.SpecialEffect1); }
                                            else { SpecialAbilitiesActive[ItemSpecialAbilityType.XelimaWeapon] += item.SpecialEffect1; ActivationTime += item.SpecialEffect1; }
                                            break;
                                        }
                                    default: break;
                                }
                            }

                            // mana save
                            if (item.EffectType == ItemEffectType.AttackManaSave) ManaSaveBonus += item.Effect4;

                            // Additional Hit Chance Based off of Type (used to only apply during criticals)
                            if (item.RelatedSkill == SkillType.Archery) HitChanceBonus += 30;
                            if (item.RelatedSkill == SkillType.LongSword) HitChanceBonus += 30;
                            if (item.RelatedSkill == SkillType.Hammer) HitChanceBonus += 20;
                            if (item.RelatedSkill == SkillType.Staff) HitChanceBonus += 50;

                            //Base cast probability for Hats & Robes
                            if (item.Category == ItemCategory.Hats) CastProbabilityBonus += 5;
                            if (item.Category == ItemCategory.Robes) CastProbabilityBonus += 20;

                            //ADD Bonus Stat Values from item stats
                            if (item.Stats.ContainsKey(ItemStat.MagicAbsorption)) MagicAbsorptionBonus += item.Stats[ItemStat.MagicAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.CastingProbability)) CastProbabilityBonus += item.Stats[ItemStat.CastingProbability] * 3;
                            if (item.Stats.ContainsKey(ItemStat.PhysicalResistance)) PhysicalResistance += item.Stats[ItemStat.PhysicalResistance] * 5;
                            if (item.Stats.ContainsKey(ItemStat.HittingProbability)) HitChanceBonus += item.Stats[ItemStat.HittingProbability] * 5;
                            if (item.Stats.ContainsKey(ItemStat.HPRecovery)) HPRecoveryBonus += item.Stats[ItemStat.HPRecovery] * 5;
                            if (item.Stats.ContainsKey(ItemStat.MPRecovery)) MPRecoveryBonus += item.Stats[ItemStat.MPRecovery] * 5;
                            if (item.Stats.ContainsKey(ItemStat.SPRecovery)) SPRecoveryBonus += item.Stats[ItemStat.SPRecovery] * 5;
                            if (item.Stats.ContainsKey(ItemStat.PoisonResistance)) PoisonResistanceBonus += item.Stats[ItemStat.PoisonResistance] * 5;
                            if (item.Stats.ContainsKey(ItemStat.Gold)) GoldBonus += item.Stats[ItemStat.Gold] * 3;
                            if (item.Stats.ContainsKey(ItemStat.DamageBonus)) { PhysicalDamageBonus += item.Stats[ItemStat.DamageBonus]; MagicDamageBonus += item.Stats[ItemStat.DamageBonus]; }
                            if (item.Stats.ContainsKey(ItemStat.HPIncrease)) MaxHPBonus += item.Stats[ItemStat.HPIncrease];
                            if (item.Stats.ContainsKey(ItemStat.MPIncrease)) MaxMPBonus += item.Stats[ItemStat.MPIncrease];
                            if (item.Stats.ContainsKey(ItemStat.SPIncrease)) MaxSPBonus += item.Stats[ItemStat.SPIncrease];
                            if (item.Stats.ContainsKey(ItemStat.MaxCrits)) MaxCritsBonus += item.Stats[ItemStat.MaxCrits];
                            if (item.Stats.ContainsKey(ItemStat.EarthAbsorption)) EarthAbsorptionBonus += item.Stats[ItemStat.EarthAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.AirAbsorption)) AirAbsorptionBonus += item.Stats[ItemStat.AirAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.FireAbsorption)) FireAbsorptionBonus += item.Stats[ItemStat.FireAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.WaterAbsorption)) WaterAbsorptionBonus += item.Stats[ItemStat.WaterAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.Experience)) ExperienceBonus += item.Stats[ItemStat.Experience] * 3;
                            if (item.Stats.ContainsKey(ItemStat.Vampiric)) VampiricBonus += item.Stats[ItemStat.Vampiric];
                            if (item.Stats.ContainsKey(ItemStat.Zealous)) ZealousBonus += item.Stats[ItemStat.Zealous];
                            if (item.Stats.ContainsKey(ItemStat.Exhaustive)) ExhaustiveBonus += item.Stats[ItemStat.Exhaustive];
                            if (item.Stats.ContainsKey(ItemStat.Piercing)) PiercingBonus += item.Stats[ItemStat.Piercing];
                            if (item.Stats.ContainsKey(ItemStat.ManaConverting)) ManaConvertingBonus += item.Stats[ItemStat.ManaConverting];
                            if (item.Stats.ContainsKey(ItemStat.CriticalChance)) CriticalIncreaseChanceBonus += item.Stats[ItemStat.CriticalChance];
                            if (item.Stats.ContainsKey(ItemStat.Critical)) CriticalDamageBonus += item.Stats[ItemStat.Critical];
                            if (item.Stats.ContainsKey(ItemStat.Poison)) PoisonousBonus += item.Stats[ItemStat.Poison] * 5;
                            if (item.Stats.ContainsKey(ItemStat.Agile)) AgileBonus += item.Stats[ItemStat.Agile]; //TODO Not Implemented
                            if (item.Stats.ContainsKey(ItemStat.ComboDamage)) ComboDamageBonus += item.Stats[ItemStat.ComboDamage] * 5;
                            if (item.Stats.ContainsKey(ItemStat.MagicResistance)) MagicResistanceBonus += item.Stats[ItemStat.MagicResistance] * 5;
                            if (item.Stats.ContainsKey(ItemStat.ManaSave)) ManaSaveBonus += item.Stats[ItemStat.ManaSave]; //TODO check            
                            if (item.Stats.ContainsKey(ItemStat.Luck)) LuckBonus += item.Stats[ItemStat.Luck];
                            if (item.Stats.ContainsKey(ItemStat.Strength)) StrengthBonus += item.Stats[ItemStat.Strength];
                            if (item.Stats.ContainsKey(ItemStat.Dexterity)) DexterityBonus += item.Stats[ItemStat.Dexterity];
                            if (item.Stats.ContainsKey(ItemStat.Intelligence)) IntelligenceBonus += item.Stats[ItemStat.Intelligence];
                            if (item.Stats.ContainsKey(ItemStat.Magic)) MagicBonus += item.Stats[ItemStat.Magic];
                            if (item.Stats.ContainsKey(ItemStat.Vitality)) VitalityBonus += item.Stats[ItemStat.Vitality];
                            if (item.Stats.ContainsKey(ItemStat.Agility)) AgilityBonus += item.Stats[ItemStat.Agility];
                            if (item.Stats.ContainsKey(ItemStat.SpiritAbsorption)) SpiritAbsorptionBonus += item.Stats[ItemStat.SpiritAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.UnholyAbsorption)) UnholyAbsorptionBonus += item.Stats[ItemStat.UnholyAbsorption];
                            if (item.Stats.ContainsKey(ItemStat.KinesisAbsorption)) KinesisAbsorptionBonus += item.Stats[ItemStat.KinesisAbsorption];

                            //True/False Bonuses
                            if (!RighteousBonus && item.Stats.ContainsKey(ItemStat.Righteous)) RighteousBonus = true;
                            if (!SharpDamageBonus && item.Stats.ContainsKey(ItemStat.Sharp)) SharpDamageBonus = true;
                            if (!AncientDamageBonus && item.Stats.ContainsKey(ItemStat.Ancient)) AncientDamageBonus = true;
                            if (!LegendaryBonus && item.Stats.ContainsKey(ItemStat.Legendary)) LegendaryBonus = true;
                            if (!CompleteAirProtection && item.Stats.ContainsKey(ItemStat.CompleteAirProtection)) CompleteAirProtection = true; ;
                            if (!CompleteEarthProtection && item.Stats.ContainsKey(ItemStat.CompleteEarthProtection)) CompleteEarthProtection = true;
                            if (!CompleteFireProtection && item.Stats.ContainsKey(ItemStat.CompleteFireProtection)) CompleteFireProtection = true;
                            if (!CompleteWaterProtection && item.Stats.ContainsKey(ItemStat.CompleteWaterProtection)) CompleteWaterProtection = true;
                            if (!CompleteUnholyProtection && item.Stats.ContainsKey(ItemStat.CompleteUnholyProtection)) CompleteUnholyProtection = true;
                            if (!CompleteKinesisProtection && item.Stats.ContainsKey(ItemStat.CompleteKinesisProtection)) CompleteKinesisProtection = true;
                            if (!CompleteSpiritProtection && item.Stats.ContainsKey(ItemStat.CompleteSpiritProtection)) CompleteSpiritProtection = true;
                            if (!CompleteMagicProtection && item.Stats.ContainsKey(ItemStat.CompleteMagicProtection)) CompleteMagicProtection = true;
                            if (item.Stats.ContainsKey(ItemStat.MagicAbsorptionDepleteEndurance)) DepleteEnduranceMagicAbsorption.Add(item);  //Add check that item has MA ?

                            //ITEMSET
                            //Hero Set
                            if (!HeroBattleCap && item.Set == ItemSet.HeroBattleCap) HeroBattleCap = true;
                            if (!HeroBattlePlate && item.Set == ItemSet.HeroBattlePlate) HeroBattlePlate = true;
                            if (!HeroCap && item.Set == ItemSet.HeroCap) HeroCap = true;
                            if (!HeroRobe && item.Set == ItemSet.HeroRobe) HeroRobe = true;
                            if (!HeroHood && item.Set == ItemSet.HeroHood) HeroHood = true;
                            if (!HeroLeather && item.Set == ItemSet.HeroLeather) HeroLeather = true;
                            if (!HeroHelm && item.Set == ItemSet.HeroHelm) HeroHelm = true;
                            if (!HeroPlate && item.Set == ItemSet.HeroPlate) HeroPlate = true;
                            if (!HeroLeggings && item.Set == ItemSet.HeroLeggings) HeroLeggings = true;
                            if (!HeroHauberk && item.Set == ItemSet.HeroHauberk) HeroHauberk = true;
                            if (!HeroCape && item.Set == ItemSet.HeroCape) HeroCape = true;

                            //Godly Set
                            if (!GodlyBattleCap && item.Set == ItemSet.GodlyBattleCap) GodlyBattleCap = true;
                            if (!GodlyBattlePlate && item.Set == ItemSet.GodlyBattlePlate) GodlyBattlePlate = true;
                            if (!GodlyCap && item.Set == ItemSet.GodlyCap) GodlyCap = true;
                            if (!GodlyRobe && item.Set == ItemSet.GodlyRobe) GodlyRobe = true;
                            if (!GodlyHood && item.Set == ItemSet.GodlyHood) GodlyHood = true;
                            if (!GodlyLeather && item.Set == ItemSet.GodlyLeather) GodlyLeather = true;
                            if (!GodlyHelm && item.Set == ItemSet.GodlyHelm) GodlyHelm = true;
                            if (!GodlyPlate && item.Set == ItemSet.GodlyPlate) GodlyPlate = true;
                            if (!GodlyLeggings && item.Set == ItemSet.GodlyLeggings) GodlyLeggings = true;
                            if (!GodlyHauberk && item.Set == ItemSet.GodlyHauberk) GodlyHauberk = true;
                            if (!GodlyCape && item.Set == ItemSet.GodlyCape) GodlyCape = true; 
                        }
                    }

                    if (i >= Globals.MaximumEquipment) //Only InventoryV1 Items
                    {
                        if (inventory[i] != null)
                        {
                            Item item = inventory[i];

                            InventoryCount++;
                            if (item.IsEquipped) { InventoryEquipedCount++; }

                            if (item.IsUpgradeIngredient) { UpgradeIngredientIndex = i; UpgradeIngredientCount++; } 

                            if (item.IsUpgradeItem)
                            {
                                if (item.IsBroken) { inventory[i].IsUpgradeItem = false; }
                                else if (item.Level == Globals.MaximumItemLevel) { inventory[i].IsUpgradeItem = false; }
                                else { UpgradeItemIndex = i; UpgradeItemCount++; }
                            } 

                            if (item.SetNumber > 0)
                            {
                                switch (inventory[i].SetNumber)
                                {
                                    case 1: ItemSet1HasItems = true; break;
                                    case 2: ItemSet2HasItems = true; break;
                                    case 3: ItemSet3HasItems = true; break;
                                    case 4: ItemSet4HasItems = true; break;
                                    case 5: ItemSet5HasItems = true; break;
                                    default: break;
                                }
                            }

                            if (item.Type == ItemType.Arrow) //TODO - Allow for multiple arrow locations for different type of arrows when implemeted
                            {
                                Arrows = inventory[i];
                                ArrowsIndex = i;
                                HasArrows = true;
                            }
                            else if (item.IsGold)
                            {
                                GoldItem = inventory[i];
                                GoldItemIndex = i;
                            }

                            //Update InventoryV1 Bonuses of non-equipable items
                            if (item.EquipType == EquipType.None)
                            {

                            }
                        }
                    }
                }

                //If all set items are true, enable set bonus
                if (HeroCape) { SetBonuses.Add(ItemSet.Hero); Hero = true; } //TODO Add implementation 
                if (HeroLeggings && HeroHauberk)
                {
                    if (HeroPlate && HeroHelm) { SetBonuses.Add(ItemSet.HeroWarrior); HeroWarrior = true; HitChanceBonus += 100; PhysicalDamageBonus += 5; } 
                    if (HeroRobe && HeroCap) { SetBonuses.Add(ItemSet.HeroMage); HeroMage = true; HitChanceBonus += 100; MagicDamageBonus += 3; } 
                    if (HeroBattlePlate && HeroBattleCap) { SetBonuses.Add(ItemSet.HeroBattleMage); HeroBattleMage = true; HitChanceBonus += 100; MagicDamageBonus += 3; } 
                    if (HeroLeather && HeroHood) { SetBonuses.Add(ItemSet.HeroArcher); HeroArcher = true; HitChanceBonus += 100; PhysicalDamageBonus += 5; } 
                }

                if (GodlyCape) { SetBonuses.Add(ItemSet.Godly); Godly = true; } //TODO Add implementation 
                if (GodlyLeggings && GodlyHauberk)
                {
                    if (GodlyPlate && GodlyHelm) { SetBonuses.Add(ItemSet.GodlyWarrior); GodlyWarrior = true; HitChanceBonus += 150; PhysicalDamageBonus += 10; }  
                    if (GodlyRobe && GodlyCap) { SetBonuses.Add(ItemSet.GodlyMage); GodlyMage = true; HitChanceBonus += 150; MagicDamageBonus += 6; } 
                    if (GodlyBattlePlate && GodlyBattleCap) { SetBonuses.Add(ItemSet.GodlyBattleMage); GodlyBattleMage = true; HitChanceBonus += 150; MagicDamageBonus += 6; } 
                    if (GodlyLeather && GodlyHood) { SetBonuses.Add(ItemSet.GodlyArcher); GodlyArcher = true; HitChanceBonus += 150; PhysicalDamageBonus += 10; } 
                }
            }
        }

        public IEnumerator<Item> GetEnumerator()
        {
            return inventory.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return inventory.GetEnumerator();
        }


        public bool ChangeUpgradeItem(int itemIndex)
        {
            bool itemPlaced = false;
            if (itemIndex >= Globals.MaximumEquipment && itemIndex < Globals.MaximumTotalItems && inventory[itemIndex] != null)
            {
                Item item = inventory[itemIndex];
                if (item.UpgradeType != ItemUpgradeType.None && !item.IsBroken) 
                {
                    if (item.IsUpgradeIngredient || item.IsUpgradeItem) //If already added,  just remove
                    {
                        inventory[itemIndex].IsUpgradeItem = false;
                        inventory[itemIndex].IsUpgradeIngredient = false;
                        itemPlaced = true;
                    }
                    else //if not added, add
                    {
                        switch (item.UpgradeType)
                        {
                            case ItemUpgradeType.Hero: { return false; }
                            case ItemUpgradeType.Ingredient:
                                if (UpgradeItemCount == 0 ||inventory[UpgradeItemIndex].UpgradeType == ItemUpgradeType.Xelima || inventory[UpgradeItemIndex].UpgradeType == ItemUpgradeType.Merien)
                                {
                                    if (UpgradeIngredientCount > 0)
                                    {
                                        for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) //Remove all    
                                        {
                                            if (inventory[i] != null && inventory[i].IsUpgradeIngredient) inventory[i].IsUpgradeIngredient = false;
                                        }
                                    }
                                    inventory[itemIndex].IsUpgradeIngredient = true; itemPlaced = true;
                                }
                                break;
                            case ItemUpgradeType.Merien:
                            case ItemUpgradeType.Xelima:         
                                if (UpgradeItemCount > 0)
                                {
                                    for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) //Remove all      
                                    {
                                        if (inventory[i] != null && inventory[i].IsUpgradeItem) inventory[i].IsUpgradeItem = false;     
                                    }
                                }
                                inventory[itemIndex].IsUpgradeItem = true; itemPlaced = true;
                                break;
                            case ItemUpgradeType.Majestic:
                                if (UpgradeIngredientCount > 0)
                                {
                                    for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) //Remove all    
                                    {
                                        if (inventory[i] != null && inventory[i].IsUpgradeIngredient) inventory[i].IsUpgradeIngredient = false;
                                    }
                                }

                                if (UpgradeItemCount > 0)
                                {
                                    for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) //Remove all
                                    {
                                        if (inventory[i] != null && inventory[i].IsUpgradeItem) inventory[i].IsUpgradeItem = false;
                                    }

                                }
                                inventory[itemIndex].IsUpgradeItem = true; itemPlaced = true;
                                break;
                            case ItemUpgradeType.Additive:
                            case ItemUpgradeType.None:
                            default: break;
                        }
                    }
                    Update();
                }
            }
            return itemPlaced;
        }

        public void ClearUpgradeItems()
        {
            for (int i = 0; i < Globals.MaximumTotalItems; i++)
            {
                if (inventory[i] != null)
                {
                    if (inventory[i].IsUpgradeItem) inventory[i].IsUpgradeItem = false;
                    if (inventory[i].IsUpgradeIngredient) inventory[i].IsUpgradeIngredient = false;
                }
            }

            Update();
        }

        /// <summary>
        /// Quick access to the Helmet currently equipped.
        /// Head = 0
        /// </summary>
        public Item Helmet { get { return inventory[(int)EquipType.Head]; } }

        /// <summary>
        /// Quick access to the Body Armour currently equipped.
        /// Body = 1
        /// </summary>
        public Item BodyArmour { get { return inventory[(int)EquipType.Body]; } }

        /// <summary>
        /// Quick access to the Hauberk currently equipped.
        /// Arms = 2
        /// </summary>
        public Item Hauberk { get { return inventory[(int)EquipType.Arms]; } }

        /// <summary>
        /// Quick access to the Leggings currently equipped.
        /// Legs = 3
        /// </summary>
        public Item Leggings { get { return inventory[(int)EquipType.Legs]; } }

        /// <summary>
        /// Quick access to the Cape currently equipped.
        /// Back = 4
        /// </summary>
        public Item Cape { get { return inventory[(int)EquipType.Back]; } }

        /// <summary>
        /// Quick access to the Boots currently equipped.
        /// Feet = 5
        /// </summary>
        public Item Boots { get { return inventory[(int)EquipType.Feet]; } }

        /// <summary>
        /// Quick access to the Necklace currently equipped.
        /// Neck = 6
        /// </summary>
        public Item Necklace { get { return inventory[(int)EquipType.Neck]; } }

        /// <summary>
        /// Quick access to the LeftRing currently equipped (wasn't used)
        /// LeftFinger = 7
        /// </summary>
        public Item LeftRing { get { return inventory[(int)EquipType.LeftFinger]; } }

        /// <summary>
        /// Quick access to the RightRing currently equipped 
        /// RightFinger = 8
        /// </summary>
        public Item RightRing { get { return inventory[(int)EquipType.RightFinger]; } }

        /// <summary>
        /// Quick access to the Shield currently equipped.
        /// LeftHand = 9
        /// </summary>
        public Item Shield { get { return inventory[(int)EquipType.LeftHand]; } }

        /// <summary>
        /// Quick access to the Weapon currently equipped.
        /// RightHand = 10
        /// </summary>
        public Item Weapon { get { return inventory[(int)EquipType.RightHand]; } }


        /// <summary>
        /// Quick access to the Angel/Gem currently equipped
        /// Utility = 11 
        /// </summary>
        public Item Angel { get { return inventory[(int)EquipType.Utility]; } }

        /// <summary>
        /// Quick access to DualHand Weapon currently equipped.
        /// DualHand = 12
        /// </summary>
        public Item DualHandedWeapon { get { return (inventory[(int)EquipType.RightHand].EquipType == EquipType.DualHand ? inventory[(int)EquipType.RightHand] : null); } }

        /// <summary>
        /// Quick access to the Costume currently equipped.
        /// FullBody = 13
        /// </summary>
        public Item Costume { get { return (inventory[(int)EquipType.Body].EquipType == EquipType.FullBody ? inventory[(int)EquipType.Body] : null); } }

        //SpecialAbilitiesPassive
        public bool Blood { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Blood); } }
        public bool Berserk { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Berserk); } }
        public bool Dark { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Dark); } }
        public bool Light { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Light); } }
        public bool DemonSlayer { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.DemonSlayer); } }
        public bool Kloness { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Kloness); } }
        public bool BowLine { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.BowLine); } }
        public bool Storm { get { return SpecialAbilitiesPassive.Contains(ItemSpecialAbilityType.Storm); } }

        //SpecialAbilitiesActive
        public bool MerienArmour { get { return SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienArmour); } }
        public bool MerienShield { get { return SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienShield); } }
        public bool IceWeapon { get { return SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.IceWeapon); } }
        public bool MedusaWeapon { get { return SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MedusaWeapon); } }
        public bool XelimaWeapon { get { return SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.XelimaWeapon); } }

        public int TotalPhysicalAbsorptionGeneral { get { return PhysicalAbsorptionGeneral; } }
        public int TotalPhysicalAbsorptionHead { get { return PhysicalAbsorptionHead + PhysicalAbsorptionHeadBonus; } }
        public int TotalPhysicalAbsorptionArms { get { return PhysicalAbsorptionArms + PhysicalAbsorptionArmsBonus; } }
        public int TotalPhysicalAbsorptionBody { get { return PhysicalAbsorptionBody + PhysicalAbsorptionBodyBonus; } }
        public int TotalPhysicalAbsorptionLegs { get { return PhysicalAbsorptionLegs + PhysicalAbsorptionLegsBonus; } }
        public int TotalPhysicalAbsorptionShield { get { return PhysicalAbsorptionShield + PhysicalAbsorptionShieldBonus; } }
    }
}
