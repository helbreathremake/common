﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common
{
    public enum PlayType
    {
        PvE = 0,
        PvP = 1
    }

    public enum ItemSpecialWeaponPrimaryType
    {
        None = 0,
        Critical = 1,
        Poison = 2,
        Righteous = 3,
        Agile = 5,
        Light = 6,
        Sharp = 7,
        Endurance = 8,
        Ancient = 9,
        CastingProbability = 10,
        ManaConverting = 11,
        CriticalChance = 12,
        //new
        Legendary = 13,
        

        //abbreviations for createitem command
        C,
        P,
        CP,
        MC,
        CC
    }

    public enum ItemSpecialWeaponSecondaryType
    {
        None = 0,
        PoisonResistance = 1,
        HittingProbability = 2,
        PhysicalResistance = 3,
        HPRecovery = 4,
        SPRecovery = 5,
        MPRecovery = 6,
        MagicResistance = 7,
        PhysicalAbsorption = 8,
        MagicAbsorption = 9, 
        ConsecutiveDamage = 10,
        Experience = 11,
        Gold = 12,
        

        //abbreviations for createitem command
        DR,
        HP,
        HPR,
        SPR,
        MPR,
        MR,
        PA,
        MA,
        PR
    }

    public enum ItemStat // v2 system
    {
        None = 0,

        //Armour
        PoisonResistance = 1,
        PhysicalResistance = 2,
        PhysicalAbsorption = 3,
        MagicAbsorption = 4, 
        MagicResistance = 5,
        HPRecovery = 6,
        SPRecovery = 7,
        MPRecovery = 8,
        EarthAbsorption = 9,
        AirAbsorption = 10,
        FireAbsorption = 11,
        WaterAbsorption = 12,
        ManaConverting = 13,
        CriticalChance = 14,
        HPIncrease = 15,
        MPIncrease = 16,
        SPIncrease = 17,
        UnholyAbsorption = 18,
        SpiritAbsorption = 19,
        KinesisAbsorption = 20,
        ManaSave = 21,
        MaxCrits = 22,


        //Weapons
        Critical = 40,
        Poison = 41,
        Righteous = 42,
        Sharp = 43,
        Endurance = 44,
        Ancient = 45,
        HittingProbability = 46,
        Agile = 47,
        DamageBonus = 48,
        Legendary = 49,
        ComboDamage = 50,
        Vampiric = 51,
        Zealous = 52,
        Piercing = 53,
        Exhaustive = 54,

        // both
        CastingProbability = 80,
        Experience = 81,
        Gold = 82,
        Luck = 83,

        //Special Stats
        CompleteFireProtection = 200,
        CompleteAirProtection,
        CompleteEarthProtection,
        CompleteWaterProtection,
        CompleteUnholyProtection,
        CompleteSpiritProtection,
        CompleteKinesisProtection,
        CompleteMagicProtection,
        MagicAbsorptionDepleteEndurance, //Reduces endurace of item when hit by magic
        Strength,
        Dexterity,
        Magic,
        Intelligence,
        Vitality,
        Agility,
    }

    public enum ItemUpgradeType
    {
        None,
        Majestic,
        Xelima,
        Merien,
        Additive, //Not used, possible for event rewards such as how hbworld used team capes to +255 or add two of same items to combine into new item / level
        Hero,
        Ingredient,
    }

    public enum SkillType
    {
        None = -1,
        Mining = 0,
        Fishing = 1,
        Farming = 2,
        MagicResistance = 3,
        Magic = 4,
        Hand = 5,
        Archery = 6,
        ShortSword = 7,
        LongSword = 8,
        Fencing = 9,
        Axe = 10,
        Shield = 11,
        Alchemy = 12,
        Manufacturing = 13,
        Hammer = 14,
        Crafting = 15,
        PretendCorpse = 19,
        Staff = 21,
        PoisonResistance = 23,
        BattleStaff = 24
    }

    public enum TitleType
    {
        DistanceTravelled = 1,
        ItemsCollected = 2,
        RareItemsCollected = 3,
        MonstersKilled = 4,
        PlayersKilled = 5,
        MonsterDeaths = 6,
        PlayerDeaths = 7,
        MonsterDamageDealt = 8,
        PlayerDamageDealt = 9,
        QuestsCompleted = 10,
        HighestMeleeDamage = 11,
        HighestMagicDamage = 12,
        HighestValueItem = 13,
        PublicEventsGold = 14,
        PublicEventsSilver = 15,
        PublicEventsBronze = 16
    }

    public enum OwnerRelationShip
    {
        Enemy,
        Neutral,
        Friendly
    }

    public enum OwnerSide
    {
        None = -1,
        Neutral = 0,
        Aresden = 1,
        Elvine = 2,
        Wild = 10
    }

    public enum OwnerSideStatus
    {
        Combatant = 1,
        Civilian = 2,
        Criminal = 3,
        Summon = 4
    }

    public enum ObjectType
    {
        None = 0,
        Owner = 1,
        DeadOwner = 2,
        DynamicObject = 3
    }

    public enum AttackType
    {
        Normal = 1,
        Critical = 2
    }

    public enum MovementType
    {
        Undefined,
        None,
        Guard,
        Random,
        RandomArea,
        Follow
    }

    public enum MotionType
    {
        Undefined = -1,
        Idle = 0,
        Move = 1,
        Run = 2,
        Attack =3,       
        Magic = 4,
        PickUp = 5,
        TakeDamage = 6,
        Fly = 7,
        Dash = 8,
        Bow = 9,
        Die = 10,
        BowAttack = 15, // special case for attacking with bow
        AttackStationary = 16, // special case for towers

        None = 100,
        Dead = 101,
        DeadFadeOut = 16, // special case for fading out spirte after death
    }

    public enum AIDifficulty
    {
        Easy,
        Normal,
        Hard,
        Heroic,
        Godly
    }

    public enum AIEquipment
    {
        Random,
        Civilian,  //clothes
        Light,     // low level armour, no helm
        Medium,    // normal armour
        Heavy,      // knight/dark knight
        Hero
    }

    public enum AIClass
    {
        Random,     // randomise during GenerateHuman
        Warrior,    //plates,wings,horns,2h or 1h+shield
        Mage,       //robes,hats, staves/wands + traps
        BattleMage, //plates,hats,hoses,battlestaffs + traps
        Archer      //leathers,caps(hoods),bows + traps
    }

    public enum AIBehaviour
    {
        Random,     // randomized between all other types
        Defender,   // tanking, shields, short chasing, self preservation
        Supporter,  // buffing, ally target duplicating, escaping, paralysing
        Healer,     // ally preservation, self preservation, healing, curing, cancelling
        Berserker   // damage dealing, berserking, target bias, long chasing
    }

    public enum AIState
    {
        None,
        Retreat
    }

    public enum AIActionCategory
    {
        Healing,
        Buff,
        Debuff,
        Protect,
        Offensive,
        Misc
    }

    public enum MotionDirection
    {
        None = 0,
        North = 1,
        NorthEast = 2,
        East = 3,
        SouthEast = 4,
        South = 5,
        SouthWest = 6,
        West = 7,
        NorthWest = 8
    }

    public enum MotionTurn
    {
        Right = 1,
        Left = 2
    }

    public enum DynamicObjectType
    {
        Fire = 1,
        Fish = 2,
        FishObject = 3,
        Rock = 4,
        Gem = 5,
        AresdenFlag = 6,
        ElvineFlag = 7,
        IceStorm = 8,
        SpikeField = 9,
        PoisonCloudBegin = 10,
        PoisonCloudLoop = 11,
        PoisonCloudEnd = 12,
        FireCrusade = 13,
        FireBow = 14,
        Portal = 20
    }

    public enum DynamicObjectShape
    {
        Wall = 1,
        Field = 2
    }

    public enum OwnerType
    {
        None = -1,
        Player = 0,
        Npc = 1
    }

    public enum OwnerSize
    {
        Small = 0,
        Medium = 1,
        Large = 2
    }

    public enum MonsterDifficulty
    {
        None = -1,
        Normal = 0,
        Boss = 1, //Hellclaw, Tigerworm - Always second drops
        LargeBoss = 2, //Abaddon, Wyvern, Fire-Wyvern - 9 Squares and Always second drops
    }

    public enum EquipType
    {
        None = -1,
        Head = 0,
        Arms = 2,
        Body = 1,
        Legs = 3,
        Feet = 5,
        Neck = 6,
        LeftFinger = 7,
        RightFinger = 8,
        LeftHand = 9,
        RightHand = 10,
        DualHand = 12,
        Back = 4,
        FullBody = 13,
        Utility = 11 // angel, gems
    }

    public enum ItemCategory
    {
        None = 0,
        // v2 new
        Ammunition,
        Weapons,
        Bows,
        Staffs,
        BattleStaffs,
        Armour,
        Shields,
        Robes,
        Hats,
        Clothes,
        Capes,
        Rings,
        Necklaces,
        Angels,
        Upgrades,
        Potions,
        Food,
        Dyes,
        Manuals,
        Seeds,
        Hero,

        //Weapon = 1,
        //Bow = 3,
        //Arrow = 4,
        //Shield = 5,
        //Armour = 6,
        //Stave = 8,
        //Clothing = 11,
        //Accessory = 12, // (boots, shoes, capes and dark mage robes?)
        //Accessory2 = 13, // (hero robes and capes + regular robes?)
        Outfit = 15, // i.e santasuit
        Commodity = 21, //seed bags, potions, tools, gold packets
        Loot = 31, // tablet pieces, monster parts, fish,
        Depletable = 42, // manuals, potions, flags, maps, lottery tickets, guild tickets, scrolls, dyes
        Fishing = 43, // fishing rod
        Valuable = 46, // wares, rings, necklaces, gems, ingots, ores, flowerpot + bouquette,
    }

    // used to differentiate displays on dialog boxes
    public enum ItemDisplayGroup
    {
        All = 0,
        Ungrouped = 1,
        Weapons = 2,
        Armour = 3,
        Consumables = 4
    }

    public enum ItemSet
    {
        //Set Names
        None,
        Hero,
        HeroWarrior,
        HeroMage,
        HeroBattleMage,
        HeroArcher,
        Godly,
        GodlyWarrior,
        GodlyMage,
        GodlyBattleMage,
        GodlyArcher,

        //Set Pieces
        //Hero Set Pieces
        HeroLeggings,
        HeroHauberk,
        HeroCape,
        HeroPlate,
        HeroHelm,
        HeroRobe,
        HeroCap,
        HeroBattleCap,
        HeroBattlePlate,
        HeroLeather,
        HeroHood,

        //Godly Set Pieces
        GodlyLeggings,
        GodlyHauberk,
        GodlyCape,
        GodlyPlate,
        GodlyHelm,
        GodlyRobe,
        GodlyCap,
        GodlyBattleCap,
        GodlyBattlePlate,
        GodlyLeather,
        GodlyHood
    }

    public enum ItemRarity
    {
        None = 0, // if no rarity specified, it will never drop
        VeryCommon = 1,
        Common = 2,
        Uncommon = 3,
        Rare = 4,
        VeryRare = 5,
        UltraRare = 6
    }

    public enum AngelType
    {
        Str = 1,
        Dex = 2,
        Int = 3,
        Mag = 4,
        Vit = 5,
        Agi = 6
    }

    public enum LootDrop
    {
        Primary,
        Secondary
    }

    public enum EconomyType
    {
        Classic,
        Virtual
    }

    public enum ItemType
    {
        None = 0,
        Equip = 1,
        Apply = 2,
        Deplete = 3,
        Install = 4,
        Consume = 5,
        Arrow = 6,
        Eat = 7,
        Skill = 8,
        Perm = 9,
        EnableDialogBox = 10, // ? used to open upgrade dialogbox / ancient stone crafting ect.
        DepleteDestination = 11,
        Material = 12,
        //Upgrade = 13
    }

    public enum ItemEffectType
    {
        None = 0,
        Attack = 1,
        Defence = 2,
        AttackBow = 3,
        HP = 4,
        MP = 5,
        SP = 6,
        Food = 7,
        Get = 8,
        LearnSkill = 9,
        ShowLocation = 10,
        Magic = 11,
        ChangeAttribute = 12,
        AttackManaSave = 13,
        AddEffect = 14,
        MagicAbsorption = 15,
        Flag = 16,
        Dye = 17,
        LearnMagic = 18,
        AttackMaxHPDown = 19, // obsolete. blood weapons are Attack with SpecialEffectType="Blood"
        AttackDefence = 20,
        MaterialAttribute = 21, // ??
        UnlimitedSP = 22,
        Lottery = 23,
        AttackActivation = 24, // activation only, since some special ability types are passive (Kloness, Blood, Berserk etc)
        DefenceActivation = 25,
        Zemstone = 26,
        ConstructionKit = 27,
        Unfreeze = 28,
        DefenceAntiMine = 29, // ??
        FarmSeed = 30,
        Slate = 31,
        ArmourDye = 32,
        GuildAdmission = 33,
        GuildDismissal = 34,
        Summon = 35, //
        AttackMagic = 36, // battlestaffs
        Xelima = 37,
        Merien = 38,
        Angel = 39,
        Manufacture,
        Alchemy,
        Craft,
    }

    public enum ItemQuality
    {
        None = 0,
        Flimsy = 1,
        Sturdy = 2,
        Reinforced = 3,
        Studded = 4
    }

    public enum TradeState
    {
        None,
        Request,
        Accepted,
        Rejected,
        Cancelled,
        Completed
    }

    public enum ItemEffectTypeChangeAttribute
    {
        HairColour = 1,
        HairStyle = 2,
        Skin = 3,
        Gender = 4,
        Underwear = 5
    }

    public enum ItemSpecialAbilityType
    {
        None = 0,
        XelimaWeapon = 1,
        IceWeapon = 2,
        MedusaWeapon = 3,
        DemonSlayer = 7,
        Dark = 8, // dark executor
        Light = 9, // lighting blade
        Blood = 10,
        Berserk = 11,
        Storm = 12,
        Kloness = 15,
        MerienArmour = 50,
        Unknown = 51, // ??
        MerienShield = 52,
        BowLine = 53,
    }

    public enum ItemSpecialAbilityStatus
    {
        Activated = 1,
        Equipped = 2,
        Expired = 3,
        Unequipped = 4,
        Angel = 5
    }

    public enum SelectionMode
    {
        None,
        ApplyItem
    }

    public enum NpcType
    {
        None,
        CrusadeArrowTower = 36,
        CrusadeCannonTower = 37,
        CrusadeManaCollector = 38,
        CrusadeDetector = 39,
        CrusadeEnergyShield = 40,
        CrusadeGrandMagicGenerator = 41,
        CrusadeManaStone = 42,
        HeldenianWarBeetle = 43,
        HeldenianGodHandKnight = 44,
        HeldenianGodHandKnightMounted = 45,
        HeldenianTempleKnight = 46,
        HeldenianBattleGolem = 47,
        HeldenianCatapult = 51,
        HeldenianBattleTank = 86,
        HeldenianCannonTower = 87,
        HeldenianArrowTower = 89,
        HeldenianGate = 91,
        Crops = 64
    }

    public enum MerchantType
    {
        None = 0,
        General = 1,
        Blacksmith = 2,
        Warehouse = 3,
        MagicShop = 4,
    }

    public enum NpcPerk
    {
        None = 0,
        Clairvoyant = 1,
        DestructionOfMagic = 2,
        AntiPhysical = 3,
        AntiMagic = 4,
        Poisonous = 5,
        CriticalPoisonous = 6,
        Explosive = 7,
        CriticalExplosive = 8
    }

    public enum MagicCategory
    {
        Utility = 0,
        Offensive = 1,
        Defensive = 2,
        Support = 3
    }

    public enum MagicType
    {
        None = 0,
        DamageSingle = 1,
        HPUpSingle = 2,
        DamageArea = 3,
        SPDownSingle = 4,
        SPDownArea = 5,
        SPUpSingle = 6,
        SPUpArea = 7,
        Teleport = 8,
        Summon = 9,
        CreateItem = 10,
        Protect = 11,
        Paralyze = 12,
        Invisibility = 13,
        CreateDynamic = 14, // firefield etc
        Possession = 15,
        Confuse = 16,
        Poison = 17,
        Berserk = 18,
        DamageLine = 19,
        Polymorph = 20,
        DamageAreaNoSingle = 21,
        Tremor = 22,
        Ice = 23,
        DamageSingleSPDown = 25,
        IceLine = 26,
        DamageAreaNoSingleSPDown = 27, 
        ArmourBreak = 28,
        Cancellation = 29,
        DamageLineSPDown = 30,
        Inhibition = 31, 
        Resurrection = 32,
        Scan = 33,
        Kinesis = 34,
        ExploitCorpse = 35,
        TurnUndead = 36,
        UnlimitedSP = 37,
        Spiral = 38,
        GhostSpiral = 39,
        Barbs = 40
    }

    public enum ConditionGroup
    {
        Speed = 0,
        Protection = 1,
        Damage = 2,
        Regen = 3,
        Defence = 4,
        Infliction = 5,
        Confusion = 6
    }

    public enum MagicAttribute
    {
        None = 0,
        Earth = 1,
        Air = 2,
        Fire = 3,
        Water = 4,
        Unholy = 6,
        Kinesis = 7,
        Spirit = 8,
        All = 9
    }

    public enum MagicTarget
    {
        /// <summary>
        /// Uses effect 1,2,3 in the magic configuration.
        /// </summary>
        Single,
        /// <summary>
        /// Uses effect 4,5,6 in the magic configuration.
        /// </summary>
        Area,
        /// <summary>
        /// Uses effect 7,8,9 in the magic configuration.
        /// </summary>
        Dynamic
    }

    public enum WeatherType
    {
        Clear = 0,
        Rain = 1,
        RainMedium = 2,
        RainHeavy = 3,
        Snow = 4,
        SnowMedium = 5,
        SnowHeavy = 6
    }

    public enum TimeOfDay
    {
        Day = 1,
        Night = 2,
        NightChristmas = 3
    }

    public enum GenderType
    {
        None = 0,
        Male = 1,
        Female = 2
    }

    public enum SkinType
    {
        White = 0,
        Black = 1,
        Yellow = 2
    }

    public enum MineralType
    {
        RockEasy = 1,
        RockMedium = 2,
        RockHard = 3,
        RockExtreme = 4,
        GemEasy = 5,
        GemHard = 6
    }

    public enum FishType
    {
        Fish,
        Item
    }

    public enum BuildingType
    {
        None,
        Warehouse,
        Shop,
        Blacksmith,
        AresdenCityhall,
        ElvineCityhall,
        Church,
        Guildhall,
        WizardTower,
        CommandHall
    }

    public enum WorldEventType
    {
        Crusade,
        Heldenian,
        Apocalypse,
        CaptureTheFlag,
        KingOfTheHill
    }

    public enum PublicEventType
    {
        None = 0,
        Defence = 1,
        Ambush = 2,
        Constructor = 3,
        Elimination = 4,
        Escort = 5
    }

    public enum PublicEventResult
    {
        Succeeded = 1,
        Failed = 2
    }

    public enum PublicEventRating
    {
        None = 0,
        Bronze = 1,
        Silver = 2,
        Gold = 3
    }

    public enum PublicEventDifficulty
    {
        None = 0,
        Easy = 1,
        Normal = 2,
        Hard = 3,
        Heroic = 4,
        Godly = 5,
        Random = 10
    }

    public enum PublicEventState
    {
        None = 0,
        Created = 1,
        Started = 2,
        Ended = 3
    }

    public enum CrusadeDuty
    {
        None = 0,
        Fighter = 1,
        Constructor = 2,
        Commander = 3
    }

    public enum HeldenianType
    {
        Battlefield = 1,
        CastleSeige = 2
    }

    public enum PortalType
    {
        Apocalypse,
        ClearAllMobs
    }

    public enum VitalType
    {
        None = 0,
        Health = 1,
        Mana = 2,
        Stamina = 3,
        Hunger = 4,
    }

    public enum DamageType
    {
        Melee = 0,
        Magic = 1,
        Ranged = 2,
        Environment = 5,
        Poison = 6,
        Spirit = 7
    }

    // TODO - attack type. e.g. > 20 is "critical attack".
    public enum WeaponType
    {
        None,
        Hand,
        Dagger,
        ShortSword,
        Fencing,
        StormBlade,
        LongSword,
        Axe,
        Hammer,
        Wand,
        LongBow,
        ShortBow,
        BattleStaff,
        Throwing,
    }

    public enum MarketOrderType
    {
        Buy = 0,
        Sell = 1
    }

    public enum PartyRequestType
    {
        Withdraw = 0,
        Join = 1,
        Dismiss = 2
    }

    public enum QuestType
    {
        Slayer,
        Assassination,
        Delivery,
        Escort,
        Guard,
        GoToLocation,
        BuildStructure,
        SupplyBuildStructure,
        StrategicStrike,
        SendToBattle,
        PlaceFlag
    }

    public enum QuestRewardType
    {
        Gold,
        Item,
        Majestics,
        Contribution
    }

    public enum DefaultState
    {
        Loading,
        MainMenu,
        AccountCreation,
        Login,
        CharacterSelect,
        Playing,
        Waiting
    }

    public enum GameMouseState
    {
        Normal = 0,
        Pickup = 101, // animation
        Pickup1 = 1,
        Pickup2 = 2,
        Aggressive = 3,
        Magic = 4,
        MagicAggressive = 5,
        Highlight = 6,
        HighlightAggressive = 7,
        Loading = 8,
        Selection = 102, // animation
        Selection1 = 9,
        Selection2 = 10,
        Selection3 = 11
    }

    public enum AnimationType
    {
        Idle = 0,
        IdleAttack = 1,
        Walking = 2,
        WalkingAttack = 3,
        Running = 4,
        Bowing = 5,
        Attacking = 6,
        Archery = 7,
        Casting = 8,
        Pickup = 9,
        TakeDamage = 10,
        Dying = 11,
        Effect = 12,
        None = 15
    }

    public enum AnimationDirection
    {
        North = 0,
        NorthEast = 1,
        East = 2,
        SouthEast = 3,
        South = 4,
        SouthWest = 5,
        West = 6,
        NorthWest = 7,
        None = 15
    }
    public enum SpriteType
    {
        None,
        Human,
        Monster,
        Armour,
        EquipmentPack,
        HairAndUndies,
        Shields,
        Weapons,
        Bows,
        Tiles,
        Effect,
        Interface
    }

    public enum FontType 
    {
        DialogSize8Bold,
        DialogsSmallSize8,
        DialogsSmallerSize7,
        GeneralSize10,
        GeneralSize10Bold,
        DamageSmallSize11,
        DamageMediumSize13,
        DamageLargeSize14Bold,
        MagicMedieval14,
        MagicMedieval18,
        MagicMedieval24,
        MagicMedieval40,
        DisplayNameSize13Spacing1,
    }

    public enum GameFontType
    {
        Bold,
        Regular,
        Small
    }

    public enum MenuItemType
    {
        Image,
        Text,
        Input,
        Button,
        SmallButton,
        Title,
        Character,
        UnusedCharacter,
        LockedCharacter,
        Gender,
        SliderBar,
        ScrollBar,
        CreateHotKey,
    }

    public enum GraphicsDetail
    {
        Low = 0,
        Medium = 1,
        High = 2
    }

    public enum UserInterfaceType
    {
        Classic,
        World
    }

    public enum DialogBoxState
    {
        Normal,
        Compact,
        Statistics,
        Quest,
        Horizontal,
        Vertical,
        Buy,
        Sell,
        Repair,
        IngredientUpgrade,
        MajesticUpgrade,
        HeroUpgrade,
        Success,
        Fail,
        Learn,
        Large,
    }

    public enum ConfigurationState
    {
        Normal,
        Changed,
        Reload,
    }

    public enum MenuItemState
    {
        Normal,
        Press,
        Hold,
        Delete,
        Action,
        Reset,
        Expanded,
    }

    public enum SettingsType
    {
        None,
        Music,
        Sound,
        Transparency,
        MiniMap,
        Wisper,
        Shout,
        Numbers,
        Delete,
        DeleteButton,
        CreateButton,
        Press,
        Hold,
        Action,
        Reset,
    }

    public enum AdvancedSettingsType
    {
        TeamColor,
        Lalala,
        Falalal,
    }

    public enum DialogBoxKind
    {
        MenuDialogBoxType,
        GameDialogBoxType,
    }

    public enum DraggedType
    {
        None,
        Inventory,
        Warehouse,
        MagicIcon,
    }

    public enum MenuDialogBoxType
    {
        None,
        Login,
        NewAccount,
        Settings,
        Keyboard,
        Advanced,
        SelectCharacter,
        CreateCharacter,
        DeleteCharacter,
        ChangePassword,
    }

    public enum GameDialogBoxType
    {
        None, //Empty
        MiniMap,  //MiniMapDialogBox.cs
        InventoryV1, // = InventoryV1DialogBox.cs
        SpellBook, //SpellBookDialogBox.cs
        Skill, // = SkillDialogBox.cs
        Chat,  // = ChatDialogBox.cs
        System, // = SystemDialogBox.cs
        LevelUpV1, // = LevelUpV1DialogBox.cs
        Party, // = PartyDialogBox.cs
        Quest, // = QuestDialogBox.cs
        Manufacture, // =ManufactureDialogBox.cs
        Alchemy, // = AlchemyDialogBox.cs - DialogBox not created yet

        // v2 dialogs by kinger
        HPIconPanel, // = HPIconPanelDialogBox.cs
        MPIconPanel, // = MPIconPanelDialogBox.cs
        SPIconPanel,  // = SPIconPanelDialogBox.cs
        HungerIconPanel,  // = HungerIconPanelDialogBox.cs
        LocationIconPanel, // = LocationIconPanelDialogBox.cs
        CharacterIconPanel, // = CharacterIconPanelDialogBox.cs
        ChatIconPanel, // = ChatIconPanelDialogBox.cs
        WeaponIconPanel, // = WeaponIconPanelDialogBox.cs
        //IconPanelEvent, //Currently Not used
        InventoryIconPanel, // InventoryIconPanelDialogBox.cs
        SpellBookIconPanel, // SpellBookIconPanelDialogBox.cs
        SkillsIconPanel, // StatsIconPanelDialogBox.cs -- Currently Used to trigger Skills
        SystemIconPanel, // = SytemIconPanelDialogBox.cs
        LockIconPanel, // = LockIconPanelDialogBox.cs
        CharacterV2, // = CharacterV2DialogBox.cs
        //SpellBookV2,   // = SpellBookV2DialogBox.cs -dont use anymore
        HotBar,  // = HotBarDialogBox.cs
        HotBarMagic, // = HotBarMagicDialogBox.cs
        Merchant, // = MerchantDialogBox.cs
        TradeV2, // = TradeV2DialogBox.cs
        PartyBar, // = PartyBarDialogBox.cs
        UpgradeV2, // = UpgradeV2DialogBox.cs
        WarehouseV2, // = WarehouseV2DialogBox.cs
        MagicShop, // = MagicShopDialogBox.cs
        CreateSet,
        ClearSet,
        Stats,
        Slate,
        Craft,
        Information,
        LevelUpIconPanel,

        //Messagebox types
        TradeV2Confirmation, //Currently a MessageBox trigger
        PartyRequest, //Currently a MessageBox trigger
        HUDResetConfirmation, //Currently a MessageBox trigger
        Informational, //Currently a MessageBox trigger
    }

    public enum PopupType
    {
        Inventoy,
        Warehouse,
        Magic,
    }

    public enum Stat
    {
        Strength = 0x01,
        Dexterity = 0x02,
        Intelligence = 0x03,
        Vitality = 0x04,
        Magic = 0x05,
        Agility = 0x06
    }

    public enum PixelShader
    {
        Berserk = 1,
        Weapon = 2,
    }

    public enum Resolution
    {
        /// <summary>
        /// 640 x 480
        /// </summary>
        Classic = 0,
        /// <summary>
        /// 800 x 600
        /// </summary>
        Standard = 1,
        /// <summary>
        /// 1024 x 768
        /// </summary>
        Large = 2
    }

    public enum ResolutionSetting
    {
        Width = 0,
        Height = 1,
        CellsWide = 2,
        CellsHeigh = 3,
        CenterX = 4,
        CenterY = 5,
        GameBoardWidth = 6,
        GameBoardHeight = 7,
        GameBoardShiftX = 8,
        GameBoardShiftY = 9
    }

    public enum HotKeyActions
    {
        None,
        HelpScreen,
        QuickSlotOne,
        QuickSlotOneSet,
        QuickSlotTwo,
        QuickSlotTwoSet,
        QuickCast,
        CharacterScreen,
        InventoryScreen,
        MagicScreen,
        SkillScreen,
        ChatHistoryScreen,
        TransparencyMode,
        SystemMenuScreen,
        AttackMode,
        UseHPPotion,
        UseMPPotion,
        SafeMode,
        RepeatMessage,
        ScreenShot,
        ActivateAbility,
        ActivateMedusa,
        ActivateIce,
        ActivateXelima,
        ActivateMerienArmour,
        ActivateMerienShield,
        Attack,
        ForceAttack,
        AutoAttack,
        MiniMap,
        WisperMode,
        MusicMode,
        DashAttack, //allow player to dash attack
        CriticalAttack, //let player use critical attack
        AutoRun, //set player in run mode always
        Run, // only run while button is pressed down
        Logout,
        FullScreenMode,
        DetailMode,
        OverlayMode, 
        DebugMode, //display debug information
        CancelAction, //stop casting current spell/exit chat box
        ChatMode, //enable typing in message box, chat box
        ZoomIn, //zoomin on minimap
        ZoomOut, //zoomout of minimap
        MagicScreenOne, //brings up first page in spell book
        MagicScreenTwo, //brings up second page in spell book
        MagicScreenThree,
        MagicScreenFour,
        MagicScreenFive,
        MagicScreenSix,
        MagicScreenSeven,
        MagicScreenEight,
        MagicScreenNine,
        MagicScreenTen,
        AutoAttackPlayer,
        TeamColorMode, //Display players with capes/boots colored based on if they are either party members/guild members/elvines/aresdens
        TileOverLayMode, //
        HotBar1,
        HotBar2,
        HotBar3,
        HotBar4,
        HotBar5,
        HotBar6,
        HotBar7,
        HotBar8,
        HotBar9,
        HotBar0,
        ZoomInScreen,
        ZoomOutScreen,
        Set1,
        Set2,
        Set3,
        Set4,
        Set5,
        InformationScreen,
        JoinParty,
    }

    public enum HotBarType
    {
        Spell,
        Skill
    }

    public enum SpriteEffectType
    {
        Desaturate // glow
    }

    public enum PartyAction
    {
        None = 0,
        Create = 1,
        Join = 2,
        Update = 3,
        ChangeMode = 4,
        Leave = 5,
        Reject = 6,
        Request = 7,
        Info = 8
    }

    public enum PartyRejectReason
    {
        InParty = 0,
        InCombatMode = 1,
        InWrongTown = 2,
        RequestPending = 3,
        Rejected = 4,
        PartyFull = 5
    }

    public enum UpgradeFailReason
    {
        Incompatible = 0,
        MaxLevel = 1,
        ChanceFailed = 2,
        ItemBroken = 3,
        ToFewMajestics = 4,
        NotUpgradable = 5,
        NoItem = 6,
        NoIngredient = 7,
        NotAnUpgradeIngredient = 8,
        EquippedItem = 9,
        NotImplemented = 10,
    }

    public enum PartyMode
    {
        Hunting = 0,
        Raiding = 1
    }

    public enum BackgroundType //Used to deterime plane on background animation.
    {
        Foreground = 0,
        Midground = 1,
        Background = 2,
        Stationary = 3,
        Smoke = 4,
        Smokebehind = 5,
        Weapon = 6,
        Legs = 7,
        Head = 8,
        Body = 9,
        Grass = 10,
    }

    public enum DrawEffectType
    {
        None,
        Hit,
        Miss, // 14
        Arrow, // 2
        Critical, // 20-27
        Burst,
        Burst2, // 8
        DustKick, // 14
        GoldDrop, // 4
        AdvancedMagic, // 57
        LightningMagic,
        LightningMagicReverse,
        Kinesis,
        KinesisReverse,
        MagicMissile,
        MagicMissileExplosion,
        MagicMissileLarge,
        MagicMissileLargeExplosion,
        MagicMissileLargeExplosion2,
        EnergyBolt,
        EnergyBoltLarge,
        EnergyBoltExplosion,
        FireBurst,
        FireBall,
        FireBallExplosion,
        FireBallExplosionLarge,
        FireBallExplosionLarge2,
        FireStrike,
        FireStrikeLarge,
        EnergyArrow,
        EnergyArrowLarge,
        EnergyArrowExplosion,
        Lightning,
        LightningBolt,
        LightningStrike,
        ChillWind,
        ChillWindLarge,
        Wind,
        WindLarge,
        EnergyStrike,
        EnergyStrikeTrail,
        EnergyStrikeExplosion,
        Blizzard,
        BlizzardShard,
        BlizzardShard2,
        IceCloud,
        BlizzardShardTrail2,
        BlizzardShardTrail3,
        Ice,
        IceExplosion,
        IceStrike,
        IceStrikeLarge,
        IceStrikeTrail,
        IceStrikeTrailLarge,
        Icicle, // type modifiers - 41,42,43,44,45,46
        EarthwormStrike,
        Worm,
        BloodyShockWave,
        BloodyShockWaveTrail,
        BloodyShock,
        EarthShockWave,
        EarthShock,
        Meteor,
        MeteorTrail,
        MeteorExplosion, // 63
        MeteorExplosion2, // 61
        AbaddonMeteor,
        AbaddonMeteor2,
        AbaddonMeteor3,
        AbaddonMeteorExplosion,
        Debuff,
        Firework1,
        Firework2,
        Fireworks,
        Flash,
        Protect,
        Bubble,
        Stars,
        SpiralDown,
        SpiralUp,
        Defence,
        Defence2,
        Twist,
        Aura,
        Cancellation,
        IllusionMovement,
        Illusion,
        Inhibition,
        Resurrect,
        HeroMage,
        HeroWarrior,
        Twinkle, // shiney items // 54, 55
        AngelTwinkle,
        StormBlade,
        EnergyStaff,
        FireStaff,
        IceStaff,
        EarthStaff,
        MeteorStaff,
        RainSplash,
        GhostSpiral,
        Ghost1,
        Ghost2,
        Ghost3,
        Ghost4,
        Ghost5,
        Ghost6,
    }

    public enum ItemColor
    {

    }

    public enum GameColor //Add all values to one main color Cache both int form and color form 
    {
        None, // = -2509462, //new Color(120, 120, 120));
        Custom,
        Default,

        // GENERAL
        Normal, // = -1,
        Exploited, // = -8355712,
        Shadow, // = -16777216,
        Berserk, // = -23296,
        Haste,// = -986896,

        // WEAPONS
        Agile, // = -7566181,
        Light,  //= -7566181,
        Strong, // = -7566181,
        Poison, // = -7885945,
        Critical, // = -1068752,
        Sharp, // = -10193980,
        Righteous, // = -2302498,
        Ancient,// = -4425028,
        Legendary, // = -10027009,
        Blood, // = -57826,
        Golden, // = -2509483,
        Vampire, // = -2509483,
        Zealot, // = -6723841,
        Exhaust, // = -6723841,
        Crits, // = -24064,
        Piercing, // = -8372224,
        Absorption, // = -128,

        // ARMOURS
        Manufactured, // = -6447742,
        Health, // = -54016,
        Mana, // = -16755201,
        Staminar, // = -16711872,

        //QUALITY
        Flimsy, // = -24064,
        Sturdy, // = -6723841,
        Reinforced, // = -2509483,
        Studded, // = -10027009,

        //Damage Colors
        DamageMelee, //Color(230, 230, 25)
        DamageRanged, //Color(255, 255, 255);
        DamageMagic, //Color(25, 168, 230)
        DamagePoison, // Color(61, 158, 0)
        DamageEnvironmental, //Color(255, 149, 0)
        DamageSpirit, //Color(153, 153, 153)

        //Vitals
        HP, //Color(173, 0, 0)
        MP, //Color(0, 114, 196)
        SP,  //Color(20, 124, 0)
        Hunger, //Color(204, 142, 0)

        //Chat
        Command, // = -2509471, //new Color(255, 255, 255));
        Local, // = -2509472, //new Color(255, 255, 255));
        Whisper, // = -2509473, //new Color(150, 150, 170));
        Global, // = -2509474, //new Color(255, 130, 130));
        Town, // = -2509475, //new Color(130, 130, 255));
        GameMaster, // = -2509476, //(135, 55, 220) //new Color(153, 102, 255));
        Guild, // = -2509477, //new Color(130, 255, 130));
        Party, // = -2509478, //new Color(230, 230, 130));

        //Relationship
        Friendly, // = -2509479, //new Color(30, 255, 30));
        Enemy, // = -2509460, //Color.Red);
        Neutral, // = -2509461, //new Color(50, 50, 255));
        //None, // = -2509462, //new Color(120, 120, 120));
        Wild, // = -2509463, //new Color(120, 120, 120)); 

        //Town
        Aresden, // = -2509464, // Aresden (255, 45, 0) //new Color(255, 60, 60));
        Elvine, // = -2509465, //Elvine (0 85, 255) * maybe G = 45? //new Color(60, 60, 255));

        //Dialog
        Orange, // = -2509466, //new Color(255, 162, 0));
        Green, // = -2509467, //new Color(30, 255, 30));
        LevelUp,
        MajesticUp, //new Color(255, 255, 20);
        //Yellow ? WeaponIconDialog box
    }

    public enum MessageBoxResponse
    {
        Yes,
        No
    }
}
