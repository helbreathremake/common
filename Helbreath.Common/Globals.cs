﻿using System;
using System.Collections.Generic;

namespace Helbreath.Common
{
    public static class Globals
    {
        // general
        public static int MaximumLevel = 180;
        public static int MaximumStat = 200;
        public static long[] ExperienceTable; // calculated during game server start up based on maximum level
        public static int ExperienceMultiplier = 1; // default, set in configs
        public static int[] SkillExperienceTable; // calculated during game server start up
        public static int TravellerLimit = 19;
        public static int EnemyKillModifier = 1;
        public static bool PlayersDropItems = false;
        public static Dictionary<int, GenderType> NewPlayerItems; // collected from config
        public static int MerchantItemCounter = 0; // stores current merchant itemId
        public readonly static EconomyType EconomyType = EconomyType.Virtual;
        public readonly static int CreateGuildCost = 2000;
        public readonly static int CreateGuildLevel = 100;
        public readonly static int CreateGuildCharisma = 10;
        public readonly static int MaximumSkills = 60;
        public readonly static int MaximumSpells = 100;
        public readonly static int MaximumEquipment = 12; //
        public readonly static int MaximumInventoryItems = 90; // was 50 now 90 bag slots
        public readonly static int MaximumTotalItems = MaximumEquipment + MaximumInventoryItems; //102 b/c Equiped items reside in same List<Items> as inventory items.
        public readonly static int MaximumWarehouseTabItems = 99;
        public readonly static int MaximumSets = 6; // 
        public readonly static int MaximumGuildMembers = 50;
        public readonly static int MaximumPartyMembers = 8;
        public readonly static int MaximumReputation = 10000;
        public readonly static int MinimumReputation = -10000;
        public readonly static int MagicFlyDamage = 50; //50;
        public readonly static int MagicFlyDamageFightzone = 80; //80;
        public readonly static int MeleeFlyDamage = 40; //40;
        public readonly static int MeleeFlyDamageFightzone = 80; //80;
        public readonly static int MaximumMapPlayers = 200;
        public readonly static int MaximumTotalNpcs = 20000;
        public readonly static int MaximumMapNpcs = 20000;
        public readonly static int MaximumNpcPerks = 3; // Note - cannot go higher than 6 without increasing network buffer
        public readonly static int MaximumMapDynamicObjects = 200;
        public readonly static int MaximumItemsPerTile = 12;
        public readonly static int MaximumSummons = 5;
        public readonly static int MaximumWarContribution = 30000;
        public readonly static int MaximumEnduranceStripable = 2000;
        public readonly static int MaximumItemLevel = 20;
        //public readonly static int MaxiumUpgradeIngredients = 10;

        // percentages
        public static int PrimaryDropRate = 100;
        public static int SecondaryDropRate = 50;
        public readonly static int MinimumSkillLevel = 100;
        public readonly static int MinimumHitChance = 10;
        public readonly static int MaximumHitChance = 90;
        public readonly static int MaximumPhysicalAbsorption = 80;
        public readonly static int MaximumMagicalAbsorption = 80;
        public readonly static int MaximumManaSave = 90;
        public readonly static int MaximumFreezeProtection = 100;
        public readonly static int MaximumPoisonProtection = 100;
        public readonly static int MaximumLuck = 50;
        public readonly static int MaximumStripChance = 50;

        // seconds
        public static int HungerTime = 60;
        public static int HPRegenTime = 10;
        public static int MPRegenTime = 10;
        public static int SPRegenTime = 10;
        public static int PoisonTime = 5; // default 12
        public static int ExperienceRollTime = 10;
        public static int MobSpawnRate = 3;
        public static int NpcSummonDuration = 90;
        public static int SpecialAbilityTime = 300; //Amount of Seconds until next use
        public readonly static int SkillTime = 6;
        public readonly static int ItemClearTime = 300;

        // addons
        public static bool TitlesEnabled = false;

        // bytes
        public readonly static int MaximumDataBuffer = 10240;

        // strings
        public readonly static string WarehouseKeeperName = "Howard";
        public readonly static string TravellerTownName = "default";
        public readonly static string AresdenTownName = "aresden";
        public readonly static string ElvineTownName = "elvine";
        public readonly static string MiddlelandName = "middleland";
        public readonly static string HeldenianBattleFieldName = "BtField";
        public readonly static string HeldenianRampartName = "HRampart";
        public readonly static string HeldenianCastleName = "GodH";
        public readonly static string TravellerRevivalZone = "default";
        public readonly static string AresdenRevivalZone = "cityhall_1";
        public readonly static string AresdenFarmName = "arefarm";
        public readonly static string ElvineFarmName = "elvfarm";
        public readonly static string ElvineRevivalZone = "cityhall_2";
        public readonly static string NeutralZoneName = "bisle";
        public readonly static string UnboundItem = "00000000-0000-0000-0000-000000000000";

        // casting probability modifiers based on magic circle 1-10
        public readonly static int[] MagicCastingProbability = { 0, 300, 250, 200, 150, 100, 80, 70, 60, 50, 40 };
        public readonly static int[] MagicCastingLevelPenalty = { 0, 5, 5, 8, 8, 10, 14, 28, 32, 36, 40 };

        // character movement modifiers
        public readonly static int[] MoveDirectionX = { 0, 0, 1, 1, 1, 0, -1, -1, -1 };
        public readonly static int[] MoveDirectionY = { 0, -1, -1, 0, 1, 1, 1, 0, -1 };
        public readonly static int[] EmptyPositionX = { 0, 1, 1, 0, -1, -1, -1, 0, 1, 2, 2, 2, 2, 1, 0, -1, -2, -2, -2, -2, -2, -1,  0,  1,  2,  2, 2, 2, 2, 2, 1, 0, -1, -2, -3, -3, -3, -3, -3, -3, -3, -2, -1,  0,  1,  2,  3,  3,  3, 3, 3, 3, 3 };
        public readonly static int[] EmptyPositionY = { 0, 0, 1, 1, 1, 0, -1, -1, -1, -1, 0, 1, 2, 2, 2, 2, 2,   1,  0, -1, -2, -2, -2, -2, -2, -1, 0, 1, 2, 3, 3, 3,  3,  3,  3,  2,  1,  0, -1, -2, -3, -3, -3, -3, -3, -3, -3, -2, -1, 0, 1, 2, 3 };

        public static int[][][][][] MapPanCells; //[Resolution[Direction[OffscreenCells[Cells[X,Y]]]]]

        public readonly static int[,] FarmingSkillTable = 
                {
                    {42, 34, 27, 21, 16, 12,  9,  7,  6},  //20
                    {43, 40, 33, 27, 22, 18, 15, 13, 10},  //30
                    {44, 41, 38, 32, 27, 23, 20, 18, 13},  //40
                    {45, 42, 39, 36, 31, 27, 24, 22, 15},  //50
                    {46, 43, 40, 37, 34, 30, 27, 25, 16},  //60
                    {47, 44, 41, 38, 35, 32, 29, 27, 20},  //70
                    {48, 45, 42, 39, 36, 33, 30, 28, 23},  //80
                    {49, 46, 43, 40, 37, 34, 31, 28, 25},  //90
                    {50, 47, 44, 41, 38, 35, 32, 29, 26}  //100
                };

        public readonly static int[,] FarmingDropTable = 
                {
                   {40,  0,  0,  0,  0,  0,  0,  0,  0},  //20
                   {41, 38,  0,  0,  0,  0,  0,  0,  0},  //30
                   {43, 40, 36,  0,  0,  0,  0,  0,  0},  //40
                   {46, 42, 38, 35,  0,  0,  0,  0,  0},  //50
                   {50, 45, 41, 37, 33,  0,  0,  0,  0},  //60
                   {55, 49, 44, 40, 35, 31,  0,  0,  0},  //70
                   {61, 54, 48, 43, 38, 33, 30,  0,  0},  //80
                   {68, 60, 53, 47, 42, 37, 32, 28,  0},  //90
                   {76, 67, 59, 52, 46, 41, 35, 29, 24}  //100
                };


        // client
        public static readonly bool Debug = true;
        public static readonly bool DebugOwner = true;
        public static readonly int CellWidth = 32;
        public static readonly int CellHeight = 32;
        public static int OffScreenCells = 9;
        public static readonly int CriticalDamage = 10000;
        public static readonly int LargeDamage = 500;
        public static readonly int MediumDamage = 100;
        public static readonly int MaximumChatHistory = 80;
        public static readonly int MaximumEventHistory = 6;
        public static readonly int MaximumWeatherObjects = 500;

        //Resolution globals for centered screen position
        //Width, Height, CellsWide, CellsHigh, CenterX, CenterY, GameBoardWidth, GameBoardHeight, GameBoardShiftX, GameBoardShiftY
        public readonly static int[,] Resolutions =
      {
            //{ 640  ,480 ,21 ,15 ,10 ,7 ,672 ,480 ,16 ,0 }, // 640x480
            { 800  ,600 ,25 ,19 ,12 ,9 ,800 ,608 ,0 ,4 }, // 800x600
            { 1280 ,720 ,40 ,23 ,20 ,12 ,1280 ,736 ,0 ,8 }, // 1280x720
            { 1280 ,800 ,40 ,25 ,20 ,12 ,1280 ,810 ,0 ,8 }, // 1280x720
    };

        // Draw orders for equipment on the Character dialog box
        // back, legs, arms, feet, body, fullbody, left, right, dual, neck, rfinger, lfinger, head 
        public readonly static int[] EquipmentDrawOrder = { 12, 4, 3, 5, 2, 13, 7, 8, 9, 6, 10, 11, 1 };

        // Offsets for the equipment on the character dialog box
        // none, head, body, arms, legs, feet, neck, lefthand, righthand, dualhand, leftfinger, rightfinger, back, fullbody
        public readonly static int[,] EquipmentDrawOffsetX = 
        { 
            { 0,0,0,0,0,0,0,0,0,0,0,0,0,0 }, // none
            { 0,72,171,171,171,171,35,90,57,57,32,98,41,171  }, // male
            { 0,72,171,171,171,171,35,84,57,57,32, 98,45,171}  // female
        };
        public readonly static int[,] EquipmentDrawOffsetY = 
        { 
            { 0,0,0,0,0,0,0,0,0,0,0,0,0,0 }, // none
            { 0, 135,290,290,290,290,120,170,186,186,193,182,137,290}, // male
            { 0,139,290,290,290,290,120,175,186,186,193,182,143,290}  // female
        };
    }
}
