﻿using System;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;
using Helbreath.Common.Assets.Objects.Dynamic;
using Helbreath.Common.Events;
using Helbreath.Common.Assets.Quests;
using System.Threading;

namespace Helbreath.Common
{
    public class World
    {
        // GAMESERVER EVENTS
        public event SendDataHandler SendData;
        public event LogHandler MessageLogged;
        public event GuildHandler GuildCreated;
        public event GuildHandler GuildDisbanded;
        public event WorldEventHandler WorldEventEnded;
        public event ItemPurchaseHandler ItemPurchased;
        public event ItemSoldHandler ItemSold;

        // CONFIGURATIONS
        public static Dictionary<int, Item> ItemConfiguration;
        public static Dictionary<string, Npc> NpcConfiguration;
        public static Dictionary<string, LootTable> PrimaryLootConfiguration;
        public static Dictionary<string, LootTable> SecondaryLootConfiguration;
        public static Dictionary<int, Magic> MagicConfiguration;
        public static Dictionary<int, ManufactureItem> ManufacturingConfiguration;
        public static Dictionary<int, AlchemyItem> AlchemyConfiguration;
        public static Dictionary<int, CraftingItem> CraftingConfiguration;
        public static Dictionary<int, string> SummonConfiguration;
        public static Dictionary<int, Quest> QuestConfiguration;
        public static Apocalypse ApocalypseConfiguration;
        public static Crusade CrusadeConfiguration;
        public static Heldenian HeldenianConfiguration;
        public static CaptureTheFlag CaptureTheFlagConfiguration;
        public static Dictionary<WorldEventType, WorldEventResult> WorldEventResults;
        public static Dictionary<int, Dictionary<int, MerchantItem>> MerchantConfiguration;
        public string News;

        // NPCS
        private bool[] npcIndexes; // available indexes. client limitations
        private Dictionary<int, Npc> npcs;
        public readonly object NpcLock = new object();
        private DateTime npcTimer;
        private DateTime npcActionTimer;
        private DateTime spawnTimer;

        // PLAYERS
        private Dictionary<int, Character> players;
        public readonly object PlayerLock = new object();
        private DateTime playerTimer;

        // GUILDS
        private Dictionary<string, Guild> guilds;

        // PARTIES
        private List<Party> parties;

        // DYNAMIC OBJECTS
        private Dictionary<string, IDynamicObject> dynamicObjects;
        private DateTime dynamicObjectTimer;

        // MAPS
        private Dictionary<string, Map> maps;
        private DateTime mapTimer;

        // EVENTS
        private IWorldEvent worldEvent;
        private DateTime eventTimer;
        
        // MISC
        private TimeOfDay timeOfDay;

        public World() 
        {
            maps = new Dictionary<string, Map>();
            npcs = new Dictionary<int, Npc>();
            players = new Dictionary<int, Character>();
            guilds = new Dictionary<string, Guild>();
            parties = new List<Party>();
            dynamicObjects = new Dictionary<string, IDynamicObject>();

            npcIndexes = new bool[Globals.MaximumTotalNpcs];
            for (int index = 0; index < Globals.MaximumTotalNpcs; index++) npcIndexes[index] = false;

            playerTimer = npcTimer = npcActionTimer = spawnTimer = eventTimer = mapTimer = dynamicObjectTimer = DateTime.Now;                    
        }

        public void AddPlayer(Character character)
        {
            // check guild exists, if not, remove set guild to none.
            if (guilds.ContainsKey(character.LoadedGuildName))
                character.Guild = guilds[character.LoadedGuildName];
            else character.Guild = Guild.None; // TODO - problem log and/or notify player?

            // check map exists, if not send to bleeding isle
            if (maps.ContainsKey(character.LoadedMapName))
                character.CurrentMap = maps[character.LoadedMapName];
            else if (maps.ContainsKey(Globals.NeutralZoneName))
                character.CurrentMap = maps[Globals.NeutralZoneName];
            else return;

            character.StatusChanged += new OwnerHandler(OnStatusChanged);
            character.MotionChanged += new MotionHandler(OnMotionChanged);
            character.DamageTaken += new DamageHandler(OnDamageTaken);
            character.VitalsChanged += new VitalsChangedHandler(OnVitalsChanged);
            character.Killed += new DeathHandler(OnKilled);
            character.ItemDropped += new ItemHandler(OnItemDropped);
            character.ItemPickedUp += new ItemHandler(OnItemPickedUp);
            character.GuildCreated += new GuildHandler(GuildCreated);
            character.GuildDisbanded += new GuildHandler(GuildDisbanded);
            character.ItemPurchased += new ItemPurchaseHandler(OnItemPurchased);
            character.ItemSold += new ItemSoldHandler(OnItemSold);
            character.MessageLogged += new LogHandler(MessageLogged);
            lock (PlayerLock) { players.Add(character.ClientID, character); }
            MessageLogged("[" + character.Name + "] connected", LogType.Game);
            character.IsReady = true;

            Send(character, CommandType.ResponseInitPlayer, CommandMessageType.Confirm);
        }

        public void RemovePlayer(int playerID)
        {
            try
            {
                if (players.ContainsKey(playerID))
                {
                    SendLogEventToNearbyPlayers(players[playerID], CommandMessageType.Reject); // tells nearby players to remove this object from the map

                    if (players.ContainsKey(players[playerID].WhisperIndex))
                    {
                        players[players[playerID].WhisperIndex].WhisperIndex = -1;
                        players[players[playerID].WhisperIndex].Notify(CommandMessageType.NotifyWhisperOff, "");
                    }

                    players[playerID].Remove();

                    lock (PlayerLock)
                    {
                        players.Remove(playerID);
                    }
                }
            }
            catch { }
        }

        public void GiveStarterItems(Character player)
        {
            int itemCount = Globals.MaximumEquipment; // index starts after equipment

            foreach (KeyValuePair<int, GenderType> item in Globals.NewPlayerItems)
                if (ItemConfiguration.ContainsKey(item.Key))
                {
                    switch (item.Value)
                    {
                        case GenderType.None: player.AddInventoryItem(ItemConfiguration[item.Key].Copy()); break;
                        default: if (player.Gender == item.Value) player.AddInventoryItem(ItemConfiguration[item.Key].Copy()); break;
                            //case GenderType.None: player.Inventory[itemCount] = ItemConfiguration[item.Key].Copy(); break;
                            //default: if (player.Gender == item.Value) player.Inventory[itemCount] = ItemConfiguration[item.Key].Copy(); break;
                    }
                    itemCount++;
                }
        }

        /// <summary>
        /// Gets a list of nearby targets around the subject for attacks that split damage such as bow crit and chain lightning
        /// </summary>
        /// <param name="searcher">The object performing the search</param>
        /// <param name="searchRange">Range of which the search will happen</param>
        /// <param name="targetCount">Maximum number of targets searched for</param>
        /// <param name="target">The subject of the search (uses the searcher if null)</param>
        /// <returns></returns>
        public List<IOwner> GetNearbyTargets(IOwner searcher, int searchRange, int targetCount, IOwner target = null)
        {
            int distance;
            IOwner subject = (target != null ? target : searcher); // the subject to search

            List<IOwner> targets = new List<IOwner>();

            for (int i = 1; i < Math.Min(searchRange*8, Globals.EmptyPositionX.Length); i++)
            {
                if (targets.Count >= targetCount) break;
                int x = subject.X + Globals.EmptyPositionX[i];
                int y = subject.Y + Globals.EmptyPositionY[i];

                if (searcher.CurrentMap[y][x].IsOccupied)
                {
                    IOwner owner = searcher.CurrentMap[y][x].Owner;
                    if (searcher.Side == owner.Side && (owner.OwnerType == OwnerType.Player && !((Character)owner).IsCriminal)) break; // dont search same side unless criminal
                    if (owner.OwnerType == OwnerType.Player && ((Character)owner).IsAdmin) break;
                    if (searcher.Side != OwnerSide.Neutral && owner.Side == OwnerSide.Neutral) break; // aresden/elvine do not attack travellers

                    if ((searcher.X - owner.X) >= (searcher.Y - owner.Y))
                        distance = searcher.X - owner.X;
                    else distance = searcher.Y - owner.Y;

                    if (distance < searchRange)
                        targets.Add(owner);
                }
            }

            // make the primary target get targetted with the rest of the hits
            if (targets.Count < targetCount && target != null)
            {
                int remaining = targetCount - targets.Count;
                for (int i = 0; i < remaining; i++)
                    targets.Add(target);
            }

            return targets;
        }
       
        /// <summary>
        /// Handles player movement, actions etc sent by client.
        /// </summary>
        /// <param name="data">Data sent by client.</param>
        /// <param name="clientID">Internal client ID.</param>
        public void PlayerProcess(byte[] data, int clientID)
        {
            if (!players.ContainsKey(clientID))
            {
                LogMessage("World.PlayerProcess: Client not found: " + clientID, LogType.Game);
                return;
            }

            int sourceX, sourceY, itemIndex;
            string characterName, guildName, databaseId;

            int typeTemp = (int)BitConverter.ToInt32(data, 0);
            CommandType type;

            if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                switch (type)
                {
                    case CommandType.RequestInitData: InitPlayerData(clientID); break;
                    case CommandType.RequestResolutionChange: SetPlayerResolution(clientID, (Resolution)(int)data[6]); break;
                    case CommandType.RequestNews: Send(clientID, CommandType.ResponseNews, CommandMessageType.Reject, News.GetBytes(News.Length)); break; // dont check size anymore, always send
                    case CommandType.RequestRestart: if (players[clientID].IsDead) players[clientID].Revive(); break;
                    case CommandType.RequestFullObjectData:
                        int id = BitConverter.ToInt16(data, 4);

                        if (id < 10000 && (id != 0) && players.ContainsKey(id))
                            Send(clientID, CommandType.Motion, CommandMessageType.ObjectStop, players[id].GetFullObjectData(players[clientID]));
                        else if (id - 10000 != 0 && npcs.ContainsKey(id - 10000))
                            Send(clientID, CommandType.Motion, CommandMessageType.ObjectStop, npcs[id - 10000].GetFullObjectData(players[clientID]));
                        break;
                    case CommandType.RequestMerchantData: GetMerchantData(clientID, data); break;
                    case CommandType.RequestCreateGuild: players[clientID].CreateGuild(Encoding.ASCII.GetString(data, 36, 20).Trim('\0')); break;
                    case CommandType.RequestDisbandGuild: players[clientID].DisbandGuild(Encoding.ASCII.GetString(data, 36, 20).Trim('\0')); break;
                    case CommandType.RequestMotion:
                        MotionType motion;
                        if (Enum.TryParse<MotionType>(BitConverter.ToInt16(data, 4).ToString(), out motion))
                        {
                            sourceX = BitConverter.ToInt16(data, 6);
                            sourceY = BitConverter.ToInt16(data, 8);
                            MotionDirection direction = (MotionDirection)((int)data[10]);
                            int destinationX = BitConverter.ToInt16(data, 11); // for attacking only
                            int spellNumber = destinationX; // for casting only
                            int destinationY = BitConverter.ToInt16(data, 13); // for attacking only
                            int clientTime;

                            switch (motion)
                            {
                                case MotionType.Attack:
                                case MotionType.Dash:
                                    AttackType attackType = (AttackType)((int)data[15]);
                                    int targetID = BitConverter.ToInt16(data, 16);
                                    ObjectType objectType = (ObjectType)BitConverter.ToInt16(data, 18);

                                    bool isDash = (motion == MotionType.Dash ? true : false);
                                    bool successful = false;

                                    if (isDash)
                                        if (players[clientID].Move(sourceX, sourceY, direction)) //TODO add pathfinding if blocked
                                        {
                                            Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectConfirmMove));
                                        }
                                        else
                                        {
                                            Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectRejectMove));
                                            break;
                                        }

                                    switch (objectType)
                                    {
                                        case ObjectType.Owner:
                                        case ObjectType.DeadOwner:
                                            if (targetID != 0)
                                            {
                                                IOwner target = targetID > 10000 
                                                    ? npcs.ContainsKey(targetID - 10000) ? (IOwner)npcs[targetID - 10000] : null
                                                    : players.ContainsKey(targetID) ? (IOwner)players[targetID] : null;

                                                if (target != null)
                                                {
                                                    if (target.OwnerType == OwnerType.Npc)
                                                        switch (((Npc)target).NpcType) // special cases for building and farming etc
                                                        {
                                                            case NpcType.Crops: // farming crops and attacking them
                                                                if (players[clientID].Weapon != null && players[clientID].Weapon.RelatedSkill == SkillType.Farming) // plants can be "killed" with conventional weapons
                                                                    successful = players[clientID].Farm(sourceX, sourceY, direction, destinationX, destinationY, (Npc)target);
                                                                else successful = players[clientID].Attack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash);
                                                                break;
                                                            case NpcType.CrusadeArrowTower:
                                                            case NpcType.CrusadeCannonTower:
                                                            case NpcType.CrusadeDetector:
                                                            case NpcType.CrusadeManaCollector: // building crusade towers and attacking them
                                                                if (target.Side == players[clientID].Side && ((Npc)target).BuildPoints > 0 && players[clientID].Weapon != null && players[clientID].Weapon.RelatedSkill == SkillType.Mining) // buildPoints = 0 means the structure is complete. imcomplete structures can be "killed" also
                                                                    successful = players[clientID].Build(sourceX, sourceY, direction, destinationX, destinationY, (Npc)target);
                                                                else successful = players[clientID].Attack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash);
                                                                break;
                                                            default: successful = players[clientID].Attack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash); break;
                                                        }
                                                    else successful = players[clientID].Attack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash);
                                                }
                                            }
                                            break;
                                        case ObjectType.DynamicObject:
                                            if (players[clientID].CurrentMap[destinationY][destinationX].DynamicObject != null) // handle dynamic objects
                                            {
                                                IDynamicObject dynamicTarget = players[clientID].CurrentMap[destinationY][destinationX].DynamicObject;
                                                switch (players[clientID].CurrentMap[destinationY][destinationX].DynamicObject.Type)
                                                {
                                                    case DynamicObjectType.Rock:
                                                    case DynamicObjectType.Gem:
                                                        successful = players[clientID].Mine(sourceX, sourceY, direction, destinationX, destinationY, (Mineral)dynamicTarget);
                                                        break;
                                                }
                                            }
                                            break;
                                    }

                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmAttack, players[clientID].GetMotionData(MotionType.Attack, CommandMessageType.ObjectConfirmAttack));                                      

                                    if (isDash) SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectAttackDash, destinationX, destinationY, 0, (int)attackType);
                                    else SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectAttack, destinationX, destinationY, 0, (int)attackType);

                                    break;
                                case MotionType.Move:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    if (players[clientID].Move(sourceX, sourceY, direction))
                                    {
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMove));
                                        SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectMove);
                                    }
                                    else Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectRejectMove));
                                    break;
                                case MotionType.Run:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    if (players[clientID].Run(sourceX, sourceY, direction))
                                    {
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMove));
                                        SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectRun);
                                    }
                                    else Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectRejectMove));
                                    break;
                                case MotionType.Idle:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    players[clientID].Idle(sourceX, sourceY, direction);

                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMotion, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMotion));
                                    SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectStop);
                                    break;
                                case MotionType.PickUp:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    players[clientID].PickUp(sourceX, sourceY, direction);

                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMotion, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMotion));
                                    SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectPickUp);
                                    break;
                                case MotionType.Magic:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    players[clientID].PrepareMagic(sourceX, sourceY, direction);
                                    
                                    if (!players[clientID].IsCombatMode) players[clientID].ToggleCombatMode();
                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMotion, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMotion));
                                    SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectMagic, spellNumber, 0, 0);
                                    break;
                                case MotionType.Fly:
                                    if (players[clientID].Fly(sourceX, sourceY, direction))
                                    {
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectConfirmMove)); // MotionType.Move to keep sync
                                        SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectTakeDamageAndFly, players[clientID].LastDamage, 0,(int)players[clientID].LastDamageType, 0); //TODO add damageType to value 3
                                    }
                                    else { Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectRejectMove)); }
                                    break;
                            }
                        }
                        break;
                    case CommandType.Common:
                        CommandMessageType subType;
                        if (Enum.TryParse<CommandMessageType>(BitConverter.ToInt16(data, 4).ToString(), out subType))
                        {
                            // these stay the same for all Common commands
                            sourceX = BitConverter.ToInt16(data, 6);
                            sourceY = BitConverter.ToInt16(data, 8);
                            MotionDirection direction = (MotionDirection)((int)data[10]);
                            switch (subType)
                            {
                                case CommandMessageType.PartyRequest:
                                    switch ((PartyRequestType)(int)data[6])
                                    {
                                        case PartyRequestType.Join: if (players.ContainsKey(BitConverter.ToInt32(data, 7))) players[clientID].JoinParty(players[BitConverter.ToInt32(data, 7)]); break;
                                        case PartyRequestType.Withdraw: players[clientID].LeaveParty(); break;
                                        case PartyRequestType.Dismiss: break; //TODO
                                    }
                                    break;
                                case CommandMessageType.BuySpell: players[clientID].LearnSpell(BitConverter.ToInt32(data, 6), true); break;
                                case CommandMessageType.UnLearnSpell: players[clientID].UnLearnSpell(BitConverter.ToInt32(data, 6)); break; 
                                case CommandMessageType.UpgradeItem: players[clientID].UpgradeItem(); break;
                                case CommandMessageType.ClearUpgradeItems: players[clientID].Inventory.ClearUpgradeItems(); break;
                                case CommandMessageType.Craft: players[clientID].Craft(new int[] { (int)data[31], (int)data[32], (int)data[33], (int)data[34], (int)data[35], (int)data[36] }); break;
                                case CommandMessageType.Alchemy: players[clientID].Alchemy(new int[] { (int)data[11], (int)data[12], (int)data[13], (int)data[14], (int)data[15], (int)data[16] }); break;
                                case CommandMessageType.Manufacture: players[clientID].Manufacture(-1, new int[] { (int)data[31], (int)data[32], (int)data[33], (int)data[34], (int)data[35], (int)data[36] }); break; // TODO finish this
                                case CommandMessageType.Slate: players[clientID].Slate(new int[] { (int)data[11], (int)data[12], (int)data[13], (int)data[14], (int)data[15], (int)data[16] }); break;
                                case CommandMessageType.ToggleCombatMode: players[clientID].ToggleCombatMode(); break;
                                case CommandMessageType.ToggleSafeMode: players[clientID].ToggleSafeMode(); break;
                                case CommandMessageType.UseSpecialAbility: players[clientID].UseSpecialAbility(BitConverter.ToInt32(data, 6)); break;
                                case CommandMessageType.BuyMerchantItem:  // buy single item
                                    players[clientID].BuyItem((int)data[6], BitConverter.ToInt32(data, 7));
                                    break;
                                case CommandMessageType.SellMerchantItem: // sell single item
                                    players[clientID].SellItem((int)data[6], BitConverter.ToInt32(data, 7));
                                    break;
                                case CommandMessageType.SellMerchantItemList: // sell multiple items
                                    for (int i = 0; i < (int)data[10]; i++)
                                        players[clientID].SellItem((int)data[6], BitConverter.ToInt32(data, 11 + (i * 4)));
                                    break;
                                case CommandMessageType.RepairMerchantItem: // repair single item
                                    players[clientID].RepairItem((int)data[6], BitConverter.ToInt32(data, 7));
                                    break;
                                case CommandMessageType.RepairMerchantItemList: // repair multiple items
                                    for (int i = 0; i < (int)data[10]; i++)
                                        players[clientID].RepairItem((int)data[6], BitConverter.ToInt32(data, 11 + (i * 4)));
                                    break;
                                case CommandMessageType.TradeRequest: TradeRequest(clientID, BitConverter.ToInt32(data, 6), BitConverter.ToInt32(data, 10)); break;
                                case CommandMessageType.TradeResponse: TradeResponse(clientID, BitConverter.ToInt32(data, 6), ((int)data[10] == 1)); break;
                                case CommandMessageType.TradeAddItem: TradeAddItem(clientID, BitConverter.ToInt32(data, 6)); break;
                                case CommandMessageType.TradeRemoveItem: TradeRemoveItem(clientID, BitConverter.ToInt32(data, 6)); break;
                                case CommandMessageType.TradeCancel: TradeCancel(clientID); break;
                                case CommandMessageType.TradeAccept: TradeAccept(clientID, (bool)((int)data[6] == 1)); break;
                                case CommandMessageType.ItemToWarehouse:
                                    players[clientID].ItemToWarehouse(BitConverter.ToInt32(data, 6), BitConverter.ToInt32(data, 10));
                                    break;
                                case CommandMessageType.ItemFromWarehouse:
                                    players[clientID].ItemFromWarehouse(BitConverter.ToInt32(data, 6), BitConverter.ToInt32(data, 10));
                                    break;
                                case CommandMessageType.SetItemSet: players[clientID].SetItemSet((int)data[6]); break;
                                case CommandMessageType.ResetItemSet: players[clientID].ClearItemSet((int)data[6]); break;
                                case CommandMessageType.UseItemSet: players[clientID].UseItemSet((int)data[6]); break;
                                case CommandMessageType.ChangeUpgradeItem: players[clientID].ChangeUpgradeItem(BitConverter.ToInt32(data, 6)); break;
                                default:
                                    int value1 = BitConverter.ToInt32(data, 11);
                                    int value2 = BitConverter.ToInt32(data, 15);
                                    int value3 = BitConverter.ToInt32(data, 19);
                                    string stringValue = ""; int value4 = 0;
                                    Character c; Npc n;
                                    if (data.Length > 27) // what a mess.. stay the same... except in some cases it seems
                                    {
                                        //LogMessage(subType.ToString() + "Length: " + data.Length, LogType.Test);
                                        stringValue = Encoding.ASCII.GetString(data, 23, 30).Trim('\0');
                                        value4 = BitConverter.ToInt32(data, 53);
                                    }
                                    switch (subType)
                                    {
                                        case CommandMessageType.CrusadeSetDuty: players[clientID].SetCrusadeDuty(value1); break;
                                        case CommandMessageType.CrusadeSummon: players[clientID].SummonCrusadeUnit(value1, value2, value3, value4); break;
                                        case CommandMessageType.CrusadeSetTeleportLocation: players[clientID].Guild.CrusadeTeleportLocation = new Location(stringValue.Substring(0,10).Trim('\0') , value1, value2); break;
                                        case CommandMessageType.CrusadeSetBuildLocation: players[clientID].Guild.CrusadeBuildLocation = new Location(stringValue.Substring(0, 10).Trim('\0'), value1, value2); break;
                                        case CommandMessageType.CrusadeTeleport: 
                                            if (players[clientID].Guild.CrusadeTeleportLocation != null &&
                                                players[clientID].Teleport(players[clientID].Guild.CrusadeTeleportLocation, MotionDirection.South)) InitPlayerData(clientID);
                                            break;
                                        case CommandMessageType.MapStatus: players[clientID].GetMapStatusData(value1, stringValue.Substring(0, 10).Trim('\0')); break;
                                        case CommandMessageType.Fish: players[clientID].Fish(); break;
                                        case CommandMessageType.CastMagic: // TODO Character.CastMagic and GameServer.CastMagic merge? so this is on 1 line
                                            bool notifyFailed = false; // players see "failed" above head
                                            if ((World.MagicConfiguration.ContainsKey(value3 - 100)) &&
                                                (players[clientID].CastMagic(sourceX, sourceY, World.MagicConfiguration[value3 - 100], out notifyFailed)))
                                            {
                                                SendMagicEventToNearbyPlayers(players[clientID], CommandMessageType.CastMagic, value1, value2, value3);
                                                CastMagic(players[clientID], value1, value2, World.MagicConfiguration[value3 - 100]);
                                            }
                                            else if (notifyFailed) SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectTakeDamage); // failed above head if casting probability fails (hunger/weather etc)
                                            break;
                                        case CommandMessageType.GiveItem: players[clientID].GiveItem((int)data[10], value1, value2, value3, value4, stringValue); break;  // alternative use of data[10] as itemIndex
                                        case CommandMessageType.DropItem: players[clientID].DropItem(value1, value2, value3); break;
                                        case CommandMessageType.SwapItem: players[clientID].SwapItem(value1, value2); break;
                                        case CommandMessageType.SwapWarehouseItem: players[clientID].SwapWarehouseItem(value1, value2); break;
                                        case CommandMessageType.EquipItem: players[clientID].EquipItem(value1); break;
                                        case CommandMessageType.UnEquipItem: players[clientID].UnequipItem(value1, value2); break;
                                        case CommandMessageType.UseItem: players[clientID].UseItem(value1, value2, value3, value4); break;
                                        case CommandMessageType.UseSkill: players[clientID].UseSkill((SkillType)value1); break; // value2 and value3 defined as parameters on HG but unused by client and hg
                                        //case CommandMessageType.BuyItem: players[clientID].BuyItem(value1, value2); break;
                                        //case CommandMessageType.SellItem: players[clientID].SellItem(value1, value2, value3, stringValue); break;
                                        case CommandMessageType.SellItemConfirm: players[clientID].SellItemConfirm(value1, value2, value3); break;
                                        //case CommandMessageType.RepairItem: players[clientID].RepairItem(value1, value2, stringValue); break;
                                        //case CommandMessageType.RepairItemConfirm: players[clientID].RepairItemConfirm(value1, stringValue); break;
                                        // to work with old client, uncomment this and comment the new one
                                        //case CommandMessageType.GetGuildName: if (players[value1] != null) players[clientID].Notify(CommandMessageType.NotifyGuildName, players[value1].GuildRank, value2, 0, players[value1].Guild.Name); break; // when someone hovers mouse over player, it is then cached (using cache id)
                                        case CommandMessageType.GetGuildName: if (players.ContainsKey(value1)) players[clientID].Notify(CommandMessageType.NotifyGuildName, players[value1].GuildRank, value1, 0, players[value1].Guild.Name); break; // when someone hovers mouse over player, it is then cached (using object id)
                                        case CommandMessageType.EnsureMagicSkill: players[clientID].LearnSkill(4, Globals.MinimumSkillLevel); break; // weird way to ensure at least 20% magic.....
                                        case CommandMessageType.HeldenianFlag: players[clientID].GetHeldenianFlag(); break;
                                        case CommandMessageType.JoinGuildApprove: if (FindPlayer(stringValue.Substring(0, 10).Trim('\0'), out c)) c.JoinGuild(players[clientID]); break;
                                        case CommandMessageType.HandlePartyRequest: players[clientID].HandlePartyRequest((bool)((int)data[6] == 1)); break;
                                        case CommandMessageType.TalkToNpc: if (FindNpc(value1, out n)) players[clientID].TalkToNpc(n); break;
                                        case CommandMessageType.AcceptQuest: players[clientID].AcceptQuest(); break;
                                        case CommandMessageType.DismissGuildsmanApprove: /*TODO*/ break;
                                        case CommandMessageType.DismissGuildsmanReject: /*TODO*/ break;
                                    }
                                    break;
                            }
                        }
                        break;
                    case CommandType.SetItemPosition:
                        itemIndex = (int)data[6];
                        sourceX = BitConverter.ToInt16(data, 7);
                        sourceY = BitConverter.ToInt16(data, 9);
                        if (itemIndex >= Globals.MaximumEquipment && itemIndex < Globals.MaximumTotalItems && players[clientID].Inventory[itemIndex] != null)
                            players[clientID].Inventory[itemIndex].BagPosition = new Location(sourceX, sourceY);
                        break;
                    case CommandType.Chat:
                        sourceX = BitConverter.ToInt16(data, 6);
                        sourceY = BitConverter.ToInt16(data, 8);
                        characterName = Encoding.ASCII.GetString(data, 10, 10).Trim('\0');
                        string message = Encoding.UTF8.GetString(data, 22, (data.Length - 22)).Trim('\0');

                        // TODO - send to login server which will copy chat to other game servers
                        //loginServerClient.Send("CHAT|" + sourceX + "|" + sourceY + "|" + characterName + "|" + message + "|");
                        players[clientID].Talk(message);
                        break;
                    case CommandType.ChangeStatsLevelUp:
                        players[clientID].ChangeStats(BitConverter.ToInt16(data, 6), 
                                                      BitConverter.ToInt16(data, 10), 
                                                      BitConverter.ToInt16(data, 14),
                                                      BitConverter.ToInt16(data, 18),
                                                      BitConverter.ToInt16(data, 22),
                                                      BitConverter.ToInt16(data, 26)); 
                        break;
                    case CommandType.ChangeStatsMajestics:
                        // Stat enum holds the hex values for each stat... copied from game.cpp of original source, Character.ChangeStat handles these in a switch
                        players[clientID].ChangeStat((Stat)data[6]);
                        players[clientID].ChangeStat((Stat)data[7]);
                        players[clientID].ChangeStat((Stat)data[8]);
                        break;
                    case CommandType.RequestTeleport:
                        // has the teleporter been defined? if not, set the player back to it's last location (avoids client side causing players to get stuck)
                        if (!players[clientID].CurrentLocation.IsTeleport)
                             players[clientID].Teleport(players[clientID].CurrentMap, players[clientID].LastX, players[clientID].LastY);
                        else players[clientID].Teleport(maps[players[clientID].CurrentLocation.Teleport.Destination.MapName],
                                                            players[clientID].CurrentLocation.Teleport.Destination.X,
                                                            players[clientID].CurrentLocation.Teleport.Destination.Y,
                                                            players[clientID].CurrentLocation.Teleport.Direction);
                        break;
                    case CommandType.RequestHeldenianTeleport:
                        if (IsHeldenianBattlefield || IsHeldenianSeige)
                            switch (Heldenian.Mode)
                            {
                                case HeldenianType.Battlefield:
                                    switch (players[clientID].Side)
                                    {
                                        case OwnerSide.Aresden: players[clientID].Teleport(maps[Globals.HeldenianBattleFieldName], 68, 225); break;
                                        case OwnerSide.Elvine: players[clientID].Teleport(maps[Globals.HeldenianBattleFieldName], 202, 70); break;
                                    }
                                    break;
                                case HeldenianType.CastleSeige:
                                    switch (players[clientID].Side)
                                    {
                                        case OwnerSide.Aresden:
                                        case OwnerSide.Elvine:
                                            if (players[clientID].Side == Heldenian.CastleDefender)
                                                 players[clientID].Teleport(maps[Globals.HeldenianRampartName], 81, 42);
                                            else players[clientID].Teleport(maps[Globals.HeldenianRampartName], 156, 153);
                                            break;
                                    }                                        
                                    break;
                            }
                        break;
                    case CommandType.RequestCitizenship:
                        byte[] command = new byte[10];
                        if (players[clientID].BecomeCitizen())
                        {
                            command[0] = (byte)1;
                            command[1] = (byte)(int)players[clientID].Town;
                            Send(clientID, CommandType.ResponseCitizenship, CommandMessageType.Confirm, command); // TODO dodgy but meh
                        }
                        else
                        {
                            command[0] = (byte)0;
                            command[1] = (byte)0;
                            Send(clientID, CommandType.ResponseCitizenship, CommandMessageType.Confirm, command);
                        }
                        break;
                    case CommandType.RequestWarehouseItem:
                        itemIndex = (int)data[6];
                        int bagIndex;
                        //if (players[clientID].RemoveWarehouseItem(itemIndex, out bagIndex))
                        //     Send(clientID, CommandType.ResponseWarehouseItem, CommandMessageType.Confirm, new byte[] { (byte)itemIndex, (byte)bagIndex });
                        //else Send(clientID, CommandType.ResponseWarehouseItem, CommandMessageType.Reject);
                        break;
                    case CommandType.RequestAngel:
                        switch (BitConverter.ToInt16(data, 26))
                        {
                            case 1: players[clientID].AddInventoryItem(ItemConfiguration[557].Copy()); break; // str angel
                            case 2: players[clientID].AddInventoryItem(ItemConfiguration[558].Copy()); break; // dex angel
                            case 3: players[clientID].AddInventoryItem(ItemConfiguration[559].Copy()); break; // int angel
                            case 4: players[clientID].AddInventoryItem(ItemConfiguration[560].Copy()); break; // mag angel
                        }
                        players[clientID].Majestics -= 5;
                        players[clientID].Notify(CommandMessageType.NotifyMajestics, players[clientID].Majestics);
                        break;
                    case CommandType.RequestSellItemList:
                        for (int i = 0; i < 12; i++)
                            if (data.Length > 6 + (i * 2) + 1)
                                if (players[clientID].Inventory[data[6 + (i * 2)]] != null)
                                    players[clientID].SellItemConfirm(data[6 + (i * 2)], data[6 + (i * 2) + 1], players[clientID].Inventory[data[6 + (i * 2)]].ItemId);

                        break;
                    case CommandType.CheckConnection: break; // unused asynchronous now
                    default: MessageLogged("Unknown Command: " + string.Format("0x{0:x8}", typeTemp), LogType.Game); break;
                }
            else MessageLogged("Unknown Command: " + string.Format("0x{0:x8}", typeTemp), LogType.Game);
        }

        /// <summary>
        /// Finds an online player by name.
        /// </summary>
        /// <param name="name">Name of the online character to find.</param>
        /// <param name="character">Character object returned to caller.</param>
        /// <returns>True or False to indicate the player was found.</returns>
        public bool FindPlayer(string name, out Character character)
        {
            character = null;
            foreach (Map map in maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (players.ContainsKey(map.Players[p]))
                    {
                        Character c = players[map.Players[p]];
                        if (c != null && c.Name.Equals(name))
                        {
                            character = c;
                            return true;
                        }
                    }

            return false;
        }

        public bool FindNpc(int type, out Npc npcObj)
        {
            npcObj = null;
            foreach (Map map in maps.Values)
                for (int n = 0; n < map.Npcs.Count; n++)
                    if (npcs.ContainsKey(map.Npcs[n]))
                    {
                        Npc npc = npcs[map.Npcs[n]];
                        if (npc != null && npc.Type == type)
                        {
                            npcObj = npc;
                            return true;
                        }
                    }

            return false;
        }

        public void GetMerchantData(int clientID, byte[] clientData)
        {
            // process client request
            List<ItemStat> statFilters = new List<ItemStat>();
            List<ItemCategory> categoryFilters = new List<ItemCategory>();
            int merchantId = (int)clientData[6];
            int page = BitConverter.ToInt16(clientData, 7);
            ItemDisplayGroup filter = (ItemDisplayGroup)(int)clientData[9];
            string textFilter = Encoding.ASCII.GetString(clientData, 10, 20).Trim('\0');

            int statFilterCount = (int)clientData[30];
            for (int f = 32; f < 32 + statFilterCount; f++)
                statFilters.Add((ItemStat)(int)clientData[f]);

            int categoryFilterCount = (int)clientData[31];
            for (int f = 32 + statFilterCount; f < 32 + statFilterCount + categoryFilterCount; f++)
                categoryFilters.Add((ItemCategory)(int)clientData[f]);

            
            // create server response
            if (!MerchantConfiguration.ContainsKey(merchantId)) return;

            int maxPages = (MerchantConfiguration[merchantId].Count / 7) + (MerchantConfiguration[merchantId].Count % 7 > 0 ? 1 : 0);
            int pageStart = Math.Max(((page-1) * 7), 0);

            int size = 0;
            byte[] data = new byte[4 + (7 * 70)];

            /* FILTERING AND SORTING */
            List<MerchantItem> sortedList = MerchantConfiguration[merchantId].Values.ToList(); // sort by value
            sortedList = sortedList.OrderByDescending(x => x.Item.GetGoldValue()).ToList();
            if (statFilters.Count > 0)
                sortedList = sortedList.Where(s => statFilters.All(f => s.Item.Stats.ContainsKey(f))).ToList(); // AND gate
            if (categoryFilters.Count > 0)
                sortedList = sortedList.Where(s => categoryFilters.Any(f => s.Item.Category.Equals(f))).ToList(); // OR gate
            if (!string.IsNullOrEmpty(textFilter))
                sortedList = sortedList.Where(s => s.Item.FriendlyName.ToLower().Contains(textFilter.ToLower())).ToList();

            Buffer.BlockCopy(sortedList.Count.GetBytes(), 0, data, 0, 2);
            size += 2;
            data[size] = (byte)merchantId;
            size++;
            // page count added below loop
            int countIndex = size;
            size++;

            int index = pageStart; int count = 0;
            for (int i = pageStart; i < pageStart+7; i++)
            {
                if (i < sortedList.Count)
                {
                    MerchantItem merchantItem = sortedList[i];

                    Buffer.BlockCopy(merchantItem.Id.GetBytes(), 0, data, size, 4);
                    size += 4;
                    data[size] = (byte)(merchantItem.Unlimited == true ? 1 : 0);
                    size++;

                    // start building an item to turn in to a byte array
                    byte[] itemData = merchantItem.Item.GetData(0);
                    Buffer.BlockCopy(itemData, 0, data, size, itemData.Length);
                    size += itemData.Length;

                    count++;
                }
            }
            data[countIndex] = (byte)count;


            Send(players[clientID], CommandType.ResponseMerchantData, CommandMessageType.Confirm, data);
        }

        public void TradeRequest(int requesterID, int targetID, int firstItemIndex)
        {
            if (!players.ContainsKey(requesterID)) return;
            if (!players.ContainsKey(targetID)) return;
            if (players[requesterID].PlayType == PlayType.PvP)
            {
                SendClientMessage(players[requesterID], "PvP characters cannot trade items.");
                return;
            }
            if (players[targetID].PlayType == PlayType.PvP)
            {
                SendClientMessage(players[targetID], "Cannot trade items with a PvP character.");
                return;
            }
            if (players[requesterID].TradePartnerID != -1 || players[targetID].TradePartnerID != -1)
            {
                TradeResponse(targetID, requesterID, false); // one of the players already in a trade
                return;
            }

            players[requesterID].TradeList.Clear();
            players[requesterID].TradePartnerID = targetID; // assign requester's partner ID
            players[targetID].TradeList.Clear();
            players[targetID].TradePartnerID = -1; // reset ID

            byte[] data = new byte[70];
            Buffer.BlockCopy(requesterID.GetBytes(), 0, data, 0, 4);
            Buffer.BlockCopy(players[requesterID].Name.GetBytes(10), 0, data, 4, 10); // send requester's name to target
            if (players[requesterID].Inventory[firstItemIndex] != null)
            {
                players[requesterID].TradeList.Add(firstItemIndex); // add item to trade list

                byte[] itemData = players[requesterID].Inventory[firstItemIndex].GetData(firstItemIndex);
                Buffer.BlockCopy(itemData, 0, data, 14, itemData.Length);
            }

            Send(players[targetID], CommandType.CommonEvent, CommandMessageType.TradeRequest, data);
        }

        public void TradeAddItem(int clientID, int itemIndex)
        {
            if (!players.ContainsKey(clientID)) return;
            if (players[clientID].TradePartnerID == -1) return;
            if (!players.ContainsKey(players[clientID].TradePartnerID)) return;

            // reset accepted state
            players[clientID].TradeAccepted = players[players[clientID].TradePartnerID].TradeAccepted = false;

            byte[] data = new byte[60];
            Buffer.BlockCopy(clientID.GetBytes(), 0, data, 0, 4);
            if (players[clientID].Inventory[itemIndex] != null)
            {
                players[clientID].TradeList.Add(itemIndex); // add item to list

                byte[] itemData = players[clientID].Inventory[itemIndex].GetData(itemIndex);
                Buffer.BlockCopy(itemData, 0, data, 4, itemData.Length);
            }

            Send(players[players[clientID].TradePartnerID], CommandType.CommonEvent, CommandMessageType.TradeAddItem, data);
        }

        public void TradeRemoveItem(int clientID, int itemIndex)
        {
            if (!players.ContainsKey(clientID)) return;
            if (players[clientID].TradePartnerID == -1) return;
            if (!players.ContainsKey(players[clientID].TradePartnerID)) return;

            // reset accepted state
            players[clientID].TradeAccepted = players[players[clientID].TradePartnerID].TradeAccepted = false;

            // remove item from list
            players[clientID].TradeList.Remove(itemIndex);

            byte[] data = new byte[15];
            Buffer.BlockCopy(clientID.GetBytes(), 0, data, 0, 4);
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, data, 4, 2);

            Send(players[players[clientID].TradePartnerID], CommandType.CommonEvent, CommandMessageType.TradeRemoveItem, data);
        }

        public void TradeResponse(int targetID, int requesterID, bool startTrade)
        {
            if (!players.ContainsKey(targetID)) return;
            if (!players.ContainsKey(requesterID)) return;

            if (!startTrade) 
                 players[requesterID].TradePartnerID = -1; // cancel the requester's request
            else players[targetID].TradePartnerID = requesterID; // confirm target's partner ID

            byte[] data = new byte[20];
            Buffer.BlockCopy(targetID.GetBytes(), 0, data, 0, 4);
            Buffer.BlockCopy(players[targetID].Name.GetBytes(10), 0, data, 4, 10); // send target's name to original requester
            data[14] = (byte)(startTrade ? 1 : 0);

            Send(players[requesterID], CommandType.CommonEvent, CommandMessageType.TradeResponse, data);
        }

        public void TradeCancel(int clientID)
        {
            if (!players.ContainsKey(clientID)) return;
            if (players[clientID].TradePartnerID == -1) return;
            if (!players.ContainsKey(players[clientID].TradePartnerID)) return;

            Send(players[players[clientID].TradePartnerID], CommandType.CommonEvent, CommandMessageType.TradeCancel);

            // reset IDs and accepted state
            players[clientID].TradePartnerID = players[players[clientID].TradePartnerID].TradePartnerID = -1;
            players[clientID].TradeAccepted = players[players[clientID].TradePartnerID].TradeAccepted = false;
            // clear trade lists
            players[clientID].TradeList.Clear();
            players[players[clientID].TradePartnerID].TradeList.Clear();
        }

        public void TradeAccept(int clientID, bool state)
        {
            if (!players.ContainsKey(clientID)) return;
            if (players[clientID].TradePartnerID == -1) return;
            if (!players.ContainsKey(players[clientID].TradePartnerID)) return;

            // set trade accepted state
            players[clientID].TradeAccepted = state;

            byte[] data = new byte[6];
            data[0] = (byte)(state ? 1 : 0);

            Send(players[players[clientID].TradePartnerID], CommandType.CommonEvent, CommandMessageType.TradeAccept, data);

            // both players accepted
            if (players[clientID].TradeAccepted && players[players[clientID].TradePartnerID].TradeAccepted)
            {
                // reset accepted state
                players[clientID].TradeAccepted = players[players[clientID].TradePartnerID].TradeAccepted = false;

                foreach (int itemIndex in players[clientID].TradeList)
                    if (players[clientID].Inventory[itemIndex] != null)
                    {
                        // store item
                        Item tradeItem = players[clientID].Inventory[itemIndex];
                        tradeItem.SetNumber = 0;
                        // remove the item from player 1
                        players[clientID].DepleteItem(itemIndex);
                        // add item to player 2
                        players[players[clientID].TradePartnerID].AddInventoryItem(tradeItem);

                        // highest valued item
                        if (tradeItem.GetGoldValue(true) > players[players[clientID].TradePartnerID].Titles[TitleType.HighestValueItem])
                        {
                            players[players[clientID].TradePartnerID].Titles[TitleType.HighestValueItem] = tradeItem.GetGoldValue(true);
                            players[players[clientID].TradePartnerID].Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestValueItem, players[players[clientID].TradePartnerID].Titles[TitleType.HighestValueItem]);
                        }
                    }

                foreach (int itemIndex in players[players[clientID].TradePartnerID].TradeList)
                    if (players[players[clientID].TradePartnerID].Inventory[itemIndex] != null)
                    {
                        // store item
                        Item tradeItem = players[players[clientID].TradePartnerID].Inventory[itemIndex];
                        tradeItem.SetNumber = 0;
                        // remove the item from player 2
                        players[players[clientID].TradePartnerID].DepleteItem(itemIndex);
                        // add item to player 1
                        players[clientID].AddInventoryItem(tradeItem);

                        // highest valued item
                        if (tradeItem.GetGoldValue(true) > players[clientID].Titles[TitleType.HighestValueItem])
                        {
                            players[clientID].Titles[TitleType.HighestValueItem] = tradeItem.GetGoldValue(true);
                            players[clientID].Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestValueItem, players[clientID].Titles[TitleType.HighestValueItem]);
                        }
                    }


                Send(players[clientID], CommandType.CommonEvent, CommandMessageType.TradeComplete);
                Send(players[players[clientID].TradePartnerID], CommandType.CommonEvent, CommandMessageType.TradeComplete);

                // cleanup
                players[clientID].TradePartnerID = players[players[clientID].TradePartnerID].TradePartnerID = -1;
                players[clientID].TradeList.Clear();
                players[players[clientID].TradePartnerID].TradeList.Clear();
            }
        }

        public void InitPlayerData(int clientID)
        {
            Character character = players[clientID];
            character.Init();

            Send(character, CommandType.ResponseInitData, CommandMessageType.Confirm, character.GetInitData());

            int startX = -Globals.OffScreenCells;
            int startY = -Globals.OffScreenCells;
            int cellX = 0;
            int cellY = 0;
            bool partialData = true;

            while (partialData)
            {
                Send(character, CommandType.ResponseInitMapData, CommandMessageType.Confirm, character.GetInitMapData(startX, startY, out partialData, out cellX, out cellY));
                startX = cellX;
                startY = cellY;
            }

            Send(character, CommandType.ResponseInitDataPlayerStats, CommandMessageType.Confirm, character.GetStatsData());
            Send(character, CommandType.ResponseInitDataPlayerItems, CommandMessageType.Confirm, character.GetItemsData());
            //character.Notify(CommandMessageType.NotifyItemBagPositions); no longer used in V2 (downloaded with Item.GetData())
            SendLogEventToNearbyPlayers(players[clientID], CommandMessageType.Confirm); // tells nearby players to add this object to the map

            // notify about the public event
            if (character.CurrentMap.PublicEvent != null) character.CurrentMap.PublicEvent.Notify(character);
        }

        public void SetPlayerResolution(int clientID, Resolution resolution)
        {
            Character character = players[clientID];
            character.Resolution = resolution;
            character.Teleport(character.CurrentMap, character.X, character.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="destinationX"></param>
        /// <param name="destinationY"></param>
        /// <param name="spell"></param>
        /// <remarks>
        /// AREA hits send the hitX and hitY of the centre point, and not the owner's location. This ensures players fly away from the center point.
        /// </remarks>
        public void CastMagic(IOwner o, int destinationX, int destinationY, Magic spell, bool isScroll = false) //TODO go through this and add safe mode/party mode safeguards 
        {
            if (o.OwnerType == OwnerType.Npc)
            {
                Npc caster = (Npc)o;
                IOwner owner;

                switch (spell.Type)
                {
                    case MagicType.Paralyze:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.CurrentLocation.IsSafe || caster.CurrentLocation.IsSafe) break; // cant para in safe or from safe
                            if (owner.Side == OwnerSide.Neutral) break; // cant para travellers
                            if (owner.Side == caster.Side) break; //  cant para own town

                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                owner.SetMagicEffect(spell);
                        }
                        break;
                    case MagicType.DamageSingle: // damages target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }
                        break;
                    case MagicType.DamageSingleSPDown: // damages target and reduce SP
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.DepleteSP(Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6), true);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }
                        break;
                    case MagicType.DamageArea: // damages target and all surrounding area (2 hits on target)
                    case MagicType.Tremor:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.TurnUndead: // applies magic type undead
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        owner.SetMagicEffect(spell);
                                }
                        break;
                    case MagicType.DamageAreaNoSingle: // damages surrounding area of target, but not the target itself individually (1 hit on target)
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingleSPDown: // as above but also reduces SP
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.ArmourBreak: // damages target, area and reduces endurance of armour
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.TakeArmourDamage(spell.Effect7 * 2);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.TakeArmourDamage(spell.Effect7 * 2);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.Ice: // damages target, area and freezes target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.DamageLine:
                    case MagicType.DamageLineSPDown:
                    case MagicType.IceLine:
                        // Hit 1: "line of site". each "point" in the LOS should have a cross-shaped hit, except the last point (next to caster) 
                        // note that like AREA spells, the center point is sent to Character.MagicAttack(x, y... to ensure players are flown away from the center point
                        List<int[]> hitLocations = Utility.GetMagicLineLocations(caster.X, caster.Y, destinationX, destinationY);
                        foreach (int[] hitLocation in hitLocations)
                        {
                            int x = hitLocation[0];
                            int y = hitLocation[1];

                            if (caster.CurrentMap[y][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x + 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x + 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y + 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y + 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x - 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x - 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y - 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y - 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                        }

                        // Hit 2: Destination should hit like damagearea
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                            owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        else if (spell.Type == MagicType.DamageLineSPDown)
                                            owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }

                        // Hit 3: Destination should also be hit like damagesingle
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                    owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                else if (spell.Type == MagicType.DamageLineSPDown)
                                    owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                            }
                        }



                        // Note/TODO: for DamageLineSPDown (earth-shock-wave), damage is done twice (???)
                        break;


                    /* THESE SPELLS BELOW HAVE BEEN ADDED FOR HUMAN AIS. NEEDS CLEANING UP */
                    case MagicType.Summon: // TODO how to decide this on NPCs? hardcoded for now
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.Summons.Count <= 5)
                            {
                                string npcName;
                                int result = Dice.Roll(1, 10);
                                if (result < 5) result = 5;

                                if (World.SummonConfiguration.ContainsKey(result))
                                {
                                    npcName = World.SummonConfiguration[result];
                                    CreateNpc(npcName, owner.CurrentMap, owner.X, owner.Y, owner);
                                }
                            }
                        }
                        break;
                    case MagicType.Protect:
                    case MagicType.Berserk:
                    case MagicType.Barbs:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            owner.SetMagicEffect(spell);
                        }
                        break;
                    case MagicType.HPUpSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            if (caster.CurrentMap[destinationY][destinationX].Owner.MagicEffects.ContainsKey(MagicType.TurnUndead))
                                caster.CurrentMap[destinationY][destinationX].Owner.TakeDamage(DamageType.Spirit, Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), MotionDirection.None);
                            else
                                caster.CurrentMap[destinationY][destinationX].Owner.ReplenishHP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                        break;
                    case MagicType.SPUpSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            caster.CurrentMap[destinationY][destinationX].Owner.ReplenishSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                        break;
                    case MagicType.SPDownSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            caster.CurrentMap[destinationY][destinationX].Owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                        break;
                    case MagicType.SPUpArea:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                    caster.CurrentMap[y][x].Owner.ReplenishSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                        break;
                    case MagicType.SPDownArea:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                    caster.CurrentMap[y][x].Owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                        break;
                }
            }
            else if (o.OwnerType == OwnerType.Player)
            {
                Character caster = (Character)o;
                IOwner owner = null;
                List<Character> attackCounterReceivers = new List<Character>(); //For any spells that don't do damage
                List<Character> supportCounterReceivers = new List<Character>(); //For any spells that don't do damage

                switch (spell.Type)
                {
                    case MagicType.Scan:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                SendClientMessage(caster, owner.Name + " - HP: " + owner.HP + "/" + owner.MaxHP + "   MP: " + owner.MP + "/" + owner.MaxMP);
                        }
                        break;
                    case MagicType.Teleport: // TODO some locations cant recall from, plus jail
                        if (destinationX == caster.X && destinationY == caster.Y)
                        {
                            if ((DateTime.Now - caster.LastDamageTime).TotalSeconds < 10)
                                SendClientMessage(caster, "You cannot recall within 10 seconds of taking damage!");
                            else
                            {
                                switch (caster.Side)
                                {
                                    case OwnerSide.Aresden: caster.Teleport(maps[Globals.AresdenTownName], -1, -1); break;
                                    case OwnerSide.Elvine: caster.Teleport(maps[Globals.ElvineTownName], -1, -1); break;
                                    case OwnerSide.Neutral: caster.Teleport(maps[Globals.TravellerTownName], -1, -1); break;
                                }
                            }
                        }
                        break;
                    case MagicType.Possession:
                        if (!caster.CurrentMap[destinationY][destinationX].IsOccupied &&
                             caster.CurrentMap[destinationY][destinationX].Items.Count > 0)
                            if (caster.AddInventoryItem(caster.CurrentMap[destinationY][destinationX].GetItem()))
                            {
                                Item nextItem;
                                if (caster.CurrentMap[destinationY][destinationX].Items.Count <= 0)
                                    nextItem = Item.Empty();
                                else
                                {
                                    Item next = caster.CurrentMap[destinationY][destinationX].Items.CheckItem();
                                    if (next == null)
                                        nextItem = Item.Empty();
                                    else nextItem = next;
                                }
                                SendItemEventToNearbyPlayers(caster, CommandMessageType.SetItem, destinationX, destinationY, nextItem);
                            }
                        break;
                    case MagicType.Cancellation:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied
                            && caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Player)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            for (int i = 0; i < 50; i++)
                                if (owner.MagicEffects.ContainsKey((MagicType)i))
                                    switch ((MagicType)i)
                                    {
                                        case MagicType.Ice:
                                        case MagicType.Berserk:
                                        case MagicType.Inhibition:
                                        case MagicType.Paralyze:
                                        case MagicType.Poison:
                                        case MagicType.UnlimitedSP: break;
                                        default: owner.RemoveMagicEffect((MagicType)i); break;
                                    }

                            if (!caster.IsAlly(owner))attackCounterReceivers.Add((Character)owner); //Maybe support?
                        }
                        break;
                    case MagicType.Summon: //TODO only summon on town?
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied &&
                            caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Player)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.Summons.Count <= caster.Skills[SkillType.Magic].Level / 20)
                            {
                                string npcName;
                                int result = Dice.Roll(1, caster.Skills[SkillType.Magic].Level / 10);
                                if (result < caster.Skills[SkillType.Magic].Level / 20) result = caster.Skills[SkillType.Magic].Level / 20;

                                if (World.SummonConfiguration.ContainsKey(result))
                                {
                                    npcName = World.SummonConfiguration[result];
                                    CreateNpc(npcName, owner.CurrentMap, owner.X, owner.Y, owner);
                                }
                            }
                        }
                        break;
                    case MagicType.CreateItem:
                        switch (spell.Effect1)
                        {
                            case 1: // food
                                Item food;
                                if (Dice.Roll(1, 2) == 1)
                                    food = World.ItemConfiguration[95].Copy(); // meat
                                else food = World.ItemConfiguration[94].Copy(); // baguette
                                if (caster.CurrentMap[destinationY][destinationX].SetItem(food))
                                    SendItemEventToNearbyPlayers(caster, CommandMessageType.DropItem, destinationX, destinationY, food);
                                break;
                        }
                        break;
                    case MagicType.Invisibility:
                        switch (spell.Effect1)
                        {
                            case 1: // invisibility
                                if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                                {
                                    owner = caster.CurrentMap[destinationY][destinationX].Owner;
                                    owner.SetMagicEffect(spell);
                                    if (owner.OwnerType == OwnerType.Player && caster.IsAlly(owner)) { supportCounterReceivers.Add((Character)owner); }
                                }
                                break;
                            case 2: // detect invisibility
                                for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                    for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++) // get everything in range 
                                        if (caster.CurrentMap[y][x].IsOccupied)
                                        {
                                            owner = caster.CurrentMap[y][x].Owner;
                                            owner.RemoveMagicEffect(spell.Type);
                                            if (owner.OwnerType == OwnerType.Player && !caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                                        }
                                break;
                        }
                        break;
                    case MagicType.HPUpSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            if (caster.CurrentMap[destinationY][destinationX].Owner.MagicEffects.ContainsKey(MagicType.TurnUndead))
                                caster.CurrentMap[destinationY][destinationX].Owner.TakeDamage(DamageType.Spirit, Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), MotionDirection.None);
                            else
                            {
                                owner = caster.CurrentMap[destinationY][destinationX].Owner;
                                owner.ReplenishHP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                                if (owner.OwnerType == OwnerType.Player && caster.IsAlly(owner)) { supportCounterReceivers.Add((Character)owner); }
                            }
                        break;
                    case MagicType.SPUpSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            owner.ReplenishSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                            if (owner.OwnerType == OwnerType.Player && caster.IsAlly(owner)) { supportCounterReceivers.Add((Character)owner); }
                        }
                        break;
                    case MagicType.SPDownSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                            if (owner.OwnerType == OwnerType.Player && !caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                        }
                        break;
                    case MagicType.SPUpArea:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    owner.ReplenishSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                                    if (owner.OwnerType == OwnerType.Player && caster.IsAlly(owner)) { supportCounterReceivers.Add((Character)owner); }
                                }
                        break;
                    case MagicType.SPDownArea:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                                    if (owner.OwnerType == OwnerType.Player && !caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                                }
                        break;
                    case MagicType.Paralyze:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.CurrentLocation.IsSafe || caster.CurrentLocation.IsSafe) break; // cant para in safe or from safe
                            if (owner.Side == OwnerSide.Neutral) break; // cant para travellers
                            if (owner.Side == caster.Side) break; //  cant para own town

                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.SetMagicEffect(spell);
                                if (owner.OwnerType == OwnerType.Player) { attackCounterReceivers.Add((Character)owner); }
                            }
                        }
                        break;
                    case MagicType.Poison:
                        switch (spell.Effect1)
                        {
                            case 0: // cure
                                if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                                {
                                    owner = caster.CurrentMap[destinationY][destinationX].Owner;
                                    owner.RemoveMagicEffect(spell.Type);
                                    if (owner.OwnerType == OwnerType.Player && caster.IsAlly(owner)) { supportCounterReceivers.Add((Character)owner); }
                                }
                                break;
                            case 1: // poison
                                if (caster.CurrentMap[destinationY][destinationX].IsOccupied &&
                                    !caster.CurrentMap[destinationY][destinationX].Owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM) &&
                                    !caster.CurrentMap[destinationY][destinationX].Owner.EvadePoison())
                                {
                                    owner = caster.CurrentMap[destinationY][destinationX].Owner;
                                    owner.SetMagicEffect(spell);
                                    if (owner.OwnerType == OwnerType.Player && !caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                                }
                                break;
                        }
                        break;
                    case MagicType.Inhibition:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            if (caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Npc) break; // cant use these on NPC

                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.SetMagicEffect(spell);
                                if (owner.OwnerType == OwnerType.Player && !caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                            }
                        }
                        break;
                    case MagicType.Confuse:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    if (caster.CurrentMap[y][x].Owner.OwnerType == OwnerType.Npc) break; // cant use these on NPC

                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.SetMagicEffect(spell);
                                        if (!caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                                    }
                                }
                        break;
                    case MagicType.Protect: //Possibly add to counters?
                    case MagicType.Berserk:
                    case MagicType.Barbs:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            if (caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Npc) break;// cant zerk or prot NPCs
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            owner.SetMagicEffect(spell);
                            if (owner.OwnerType == OwnerType.Player && caster.IsAlly(owner)) { supportCounterReceivers.Add((Character)owner); }
                        }
                        break;
                    case MagicType.DamageSingle: // damages target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }
                        break;
                    case MagicType.DamageSingleSPDown: // damages target and reduce SP
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.DepleteSP(Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6), true);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }
                        break;
                    case MagicType.DamageArea: // damages target and all surrounding area (2 hits on target)
                    case MagicType.Tremor:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingle: // damages surrounding area of target, but not the target itself individually (1 hit on target)
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingleSPDown: // as above but also reduces SP
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3), true);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.TurnUndead: // applies magic type undead
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.SetMagicEffect(spell);
                                        if (owner.OwnerType == OwnerType.Player && !caster.IsAlly(owner)) { attackCounterReceivers.Add((Character)owner); }
                                    }
                                }
                        break;
                    case MagicType.ArmourBreak: // damages target, area and reduces endurance of armour
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.TakeArmourDamage(spell.Effect7 * 2);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.TakeArmourDamage(spell.Effect7 * 2);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.Ice: // damages target, area and freezes target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.CreateDynamic:
                        if (IsCrusade) // cannot cast in towns or farms during crusade
                            if (caster.CurrentMap.Name.Equals(Globals.AresdenTownName) ||
                                caster.CurrentMap.Name.Equals(Globals.ElvineTownName) ||
                                caster.CurrentMap.Name.Equals(Globals.ElvineFarmName) ||
                                caster.CurrentMap.Name.Equals(Globals.AresdenFarmName))
                                return;
                        Dice dice = new Dice(spell.Effect1, spell.Effect2, spell.Effect3);
                        switch ((DynamicObjectType)spell.Effect7)
                        {
                            case DynamicObjectType.PoisonCloudBegin:
                            case DynamicObjectType.Fire:
                            case DynamicObjectType.SpikeField:
                                switch ((DynamicObjectShape)spell.Effect8)
                                {
                                    case DynamicObjectShape.Wall:
                                        int rangeX, rangeY;
                                        switch (Utility.GetNextDirection(caster.X, caster.Y, destinationX, destinationY))
                                        {
                                            default:
                                            case MotionDirection.North: rangeX = 1; rangeY = 0; break;
                                            case MotionDirection.NorthEast: rangeX = 1; rangeY = 1; break;
                                            case MotionDirection.East: rangeX = 0; rangeY = 1; break;
                                            case MotionDirection.SouthEast: rangeX = -1; rangeY = 1; break;
                                            case MotionDirection.South: rangeX = 1; rangeY = 0; break;
                                            case MotionDirection.SouthWest: rangeX = -1; rangeY = -1; break;
                                            case MotionDirection.West: rangeX = 0; rangeY = -1; break;
                                            case MotionDirection.NorthWest: rangeX = 1; rangeY = -1; break;
                                        }

                                        caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX, destinationY, spell.LastTime, spell.Effect4, o, dice);

                                        for (int i = 1; i < spell.RangeX; i++)
                                        {
                                            caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX + (rangeX * i), destinationY + (rangeY * i), spell.LastTime, spell.Effect4, o, dice);
                                            caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX - (rangeX * i), destinationY - (rangeY * i), spell.LastTime, spell.Effect4, o, dice);
                                        }

                                        break;
                                    case DynamicObjectShape.Field: // note -1 on range.
                                        for (int y = destinationY - spell.RangeY + 1; y <= destinationY + spell.RangeY - 1; y++)
                                            for (int x = destinationX - spell.RangeX + 1; x <= destinationX + spell.RangeX - 1; x++)
                                            {
                                                caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, x, y, spell.LastTime, spell.Effect4, o, dice);
                                            }
                                        break;
                                }
                                break;
                            case DynamicObjectType.IceStorm:
                                caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX, destinationY, spell.LastTime, spell.Effect4, o, dice);
                                break;
                            default: break;
                        }

                        break;
                    case MagicType.Resurrection:
                        // TODO - resurrection code
                        break;
                    case MagicType.Spiral:
                    case MagicType.GhostSpiral:
                        // Hit 1: Spiral hit locations
                        List<int[]> spiralHitLocations = Utility.GetMagicSpiralLocations(o.X, o.Y);
                        foreach (int[] hitLocation in spiralHitLocations)
                        {
                            int x = hitLocation[0];
                            int y = hitLocation[1];

                            if (caster.CurrentMap[y][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                        }

                        // Hit 2: Area damage around caster
                        for (int y = o.Y - spell.RangeY; y <= o.Y + spell.RangeY; y++)
                            for (int x = o.X - spell.RangeX; x <= o.X + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (owner != o && !owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }

                        break;
                    case MagicType.DamageLine:
                    case MagicType.DamageLineSPDown:
                    case MagicType.IceLine:
                        // Hit 1: "line of site". each "point" in the LOS should have a cross-shaped hit, except the last point (next to caster) 
                        // note that like AREA spells, the center point is sent to Character.MagicAttack(x, y... to ensure players are flown away from the center point
                        List<int[]> hitLocations = Utility.GetMagicLineLocations(caster.X, caster.Y, destinationX, destinationY);
                        foreach (int[] hitLocation in hitLocations)
                        {
                            int x = hitLocation[0];
                            int y = hitLocation[1];

                            if (caster.CurrentMap[y][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x + 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x + 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y + 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y + 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x - 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x - 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y - 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y - 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                        }

                        // Hit 2: Destination should hit like damagearea
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                            owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        else if (spell.Type == MagicType.DamageLineSPDown)
                                            owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }

                        // Hit 3: Destination should also be hit like damagearea
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                    owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                else if (spell.Type == MagicType.DamageLineSPDown)
                                    owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9), true);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                            }
                        }



                        // Note: for DamageLineSPDown (earth-shock-wave), hit 3 is done twice in original code (???)
                        break;
                    case MagicType.Polymorph:
                        // TODO - do we really want polymorph? =\
                        break;
                    case MagicType.ExploitCorpse:
                        int exploitCount = 0; // holds counter for limited exploitation
                        switch (spell.Effect1)
                        {
                            case 1: // createdynamic
                                DynamicObjectType dynamicType;
                                if (Enum.TryParse<DynamicObjectType>(spell.Effect2.ToString(), out dynamicType))
                                {
                                    for (int y = o.Y - spell.RangeY; y <= o.Y + spell.RangeY; y++)
                                        for (int x = o.X - spell.RangeX; x <= o.X + spell.RangeX; x++)
                                            if (o.CurrentMap[y][x].DeadOwner != null && !o.CurrentMap[y][x].DeadOwner.IsCorpseExploited)
                                            {
                                                caster.CurrentMap.CreateDynamicObject(dynamicType, x, y, spell.LastTime, spell.Effect3);
                                                o.CurrentMap[y][x].DeadOwner.IsCorpseExploited = true;
                                                SendMotionEventToNearbyPlayers((IOwner)o.CurrentMap[y][x].DeadOwner, CommandMessageType.ObjectDead);
                                            }
                                }
                                break;
                            case 2: // consume
                                for (int y = o.Y - spell.RangeY; y <= o.Y + spell.RangeY; y++)
                                    for (int x = o.X - spell.RangeX; x <= o.X + spell.RangeX; x++)
                                        if (o.CurrentMap[y][x].DeadOwner != null && !o.CurrentMap[y][x].DeadOwner.IsCorpseExploited && exploitCount < 4)
                                        {
                                            caster.ReplenishHP(Dice.Roll(spell.Effect2, spell.Effect3, spell.Effect4), true);
                                            o.CurrentMap[y][x].DeadOwner.IsCorpseExploited = true;
                                            SendMotionEventToNearbyPlayers((IOwner)o.CurrentMap[y][x].DeadOwner, CommandMessageType.ObjectDead);
                                            exploitCount++;
                                        }
                                break;
                            case 3: // raise dead
                                for (int y = o.Y - spell.RangeY; y <= o.Y + spell.RangeY; y++)
                                    for (int x = o.X - spell.RangeX; x <= o.X + spell.RangeX; x++)
                                        if (o.CurrentMap[y][x].DeadOwner != null && !o.CurrentMap[y][x].DeadOwner.IsCorpseExploited)
                                            if (o.Summons.Count < Globals.MaximumSummons)
                                            {
                                                CreateNpc("Skeleton-Minion", o.CurrentMap, x, y, o);
                                                o.CurrentMap[y][x].DeadOwner.IsCorpseExploited = true;
                                                SendMotionEventToNearbyPlayers((IOwner)o.CurrentMap[y][x].DeadOwner, CommandMessageType.ObjectDead);
                                                exploitCount++;
                                            }
                                break;
                        }
                        break;
                    case MagicType.Kinesis:
                        int kineticDamage = 0;
                        switch (spell.Effect1)
                        {
                            case 1: // push/throw
                                for (int hits = 0; hits < 2; hits++)
                                {
                                    if (spell.Effect2 > 0) // kinetic spell that does damage (e.g. throw/drag)
                                    {
                                        kineticDamage = Dice.Roll(spell.Effect2, spell.Effect3, spell.Effect4);
                                        kineticDamage += (int)(((double)kineticDamage * (double)(((double)(o.Strength) / 20.0f) / 100.0f)) + 0.5f); // 5% strenght bonus
                                        kineticDamage += (int)(((double)kineticDamage * (double)(((double)(o.Intelligence) / 20.0f) / 100.0f)) + 0.5f); // 5% intelligence bonus
                                    }
                                    List<int> hitIds = new List<int>();
                                    for (int y = o.Y - spell.RangeY; y <= o.Y + spell.RangeY; y++)
                                        for (int x = o.X - spell.RangeX; x <= o.X + spell.RangeX; x++)
                                            if (o.CurrentMap[y][x].Owner != null && o.CurrentMap[y][x].Owner != o && !hitIds.Contains(o.CurrentMap[y][x].Owner.ObjectId))
                                            {
                                                IOwner target = o.CurrentMap[y][x].Owner;
                                                if (o.OwnerType == OwnerType.Player && ((Character)o).IsSafeMode && o.Side == target.Side) continue; // safe mode doesnt hit own side
                                                hitIds.Add(target.ObjectId); // make sure we dont hit the same target twice in 1 iteration
                                                MotionDirection flyDirection = Utility.GetNextDirection(o.X, o.Y, target.X, target.Y); // towards the target
                                                switch (target.OwnerType)
                                                {
                                                    case OwnerType.Player:
                                                        ((Character)target).Notify(CommandMessageType.NotifyFly, (int)flyDirection, 0, (int)DamageType.Magic, 1);
                                                        if (!caster.IsAlly(target)) { attackCounterReceivers.Add((Character)target); }
                                                        break;
                                                    case OwnerType.Npc:
                                                        if ((int)target.Size <= spell.Effect9 && target.Fly(target.X, target.Y, flyDirection)) // check maximum fly size from Effect9 (small, medium, large)
                                                            SendMotionEventToNearbyPlayers(target, CommandMessageType.ObjectTakeDamageAndFly, 0, 0, (int)DamageType.Magic, 0);
                                                        break;
                                                }
                                                
                                                if (kineticDamage > 0) target.TakeDamage(o, DamageType.Melee, kineticDamage, flyDirection, false);
                                            }
                                }
                                break;
                            case 2: // pull/drag
                                for (int hits = 0; hits < 2; hits++)
                                {
                                    if (spell.Effect2 > 0) // kinetic spell that does damage (e.g. throw/drag)
                                    {
                                        kineticDamage = Dice.Roll(spell.Effect2, spell.Effect3, spell.Effect4);
                                        kineticDamage += (int)(((double)kineticDamage * (double)(((double)(o.Strength) / 20.0f) / 100.0f)) + 0.5f); // 5% strenght bonus
                                        kineticDamage += (int)(((double)kineticDamage * (double)(((double)(o.Intelligence) / 20.0f) / 100.0f)) + 0.5f); // 5% intelligence bonus
                                    }
                                    List<int> hitIds = new List<int>();
                                    for (int y = o.Y - spell.RangeY; y <= o.Y + spell.RangeY; y++)
                                        for (int x = o.X - spell.RangeX; x <= o.X + spell.RangeX; x++)
                                            if (o.CurrentMap[y][x].Owner != null && o.CurrentMap[y][x].Owner != o && !hitIds.Contains(o.CurrentMap[y][x].Owner.ObjectId))
                                            {
                                                IOwner target = o.CurrentMap[y][x].Owner;
                                                if (o.OwnerType == OwnerType.Player && ((Character)o).IsSafeMode && o.Side == target.Side) continue; // safe mode doesnt hit own side
                                                hitIds.Add(target.ObjectId); // make sure we dont hit the same target twice in 1 iteration
                                                MotionDirection flyDirection = Utility.GetNextDirection(target.X, target.Y, o.X, o.Y); // towards the target
                                                switch (target.OwnerType)
                                                {
                                                    case OwnerType.Player:
                                                        ((Character)target).Notify(CommandMessageType.NotifyFly, (int)flyDirection, 0, (int)DamageType.Magic, 1);
                                                        if (!caster.IsAlly(target)) { attackCounterReceivers.Add((Character)target); }
                                                        break;
                                                    case OwnerType.Npc:
                                                        if ((int)target.Size <= spell.Effect9 && target.Fly(target.X, target.Y, flyDirection)) // check maximum fly size from Effect9 (small, medium, large)
                                                            SendMotionEventToNearbyPlayers(target, CommandMessageType.ObjectTakeDamageAndFly, 0, 0, 0, 0);
                                                        break;
                                                }

                                                if (kineticDamage > 0) target.TakeDamage(o, DamageType.Melee, kineticDamage, flyDirection, false);
                                            }
                                }
                                break;
                        }
                        break;
                }

                if (!isScroll)
                {
                    int manaCost = Utility.GetManaCost(spell.ManaCost, caster.Inventory.ManaSaveBonus);
                    caster.DepleteMP(manaCost, false);
                }

                if (attackCounterReceivers.Count > 0)
                {
                    foreach (Character target in attackCounterReceivers)
                    {
                        if (target.DamagedByObjects.ContainsKey(caster.ClientID)) { target.DamagedByObjects[caster.ClientID] = DateTime.Now; }
                        else { target.DamagedByObjects.Add(caster.ClientID, DateTime.Now); }
                    }
                }

                if (supportCounterReceivers.Count > 0)
                {
                    foreach (Character supporter in supportCounterReceivers)
                    {
                        if (supporter.SupportedByObjects.ContainsKey(caster.ClientID)) { supporter.SupportedByObjects[caster.ClientID] = DateTime.Now; }
                        else { supporter.SupportedByObjects.Add(caster.ClientID, DateTime.Now); }
                    }
                }
            }
        }

        public void ExternalChat(string name, string message, GameColor mode)
        {
            if (mode == GameColor.GameMaster && message.StartsWith("/"))
            {
                Character c;

                string[] tokens = message.TrimStart('/').Split(' ');
                switch (tokens[0].ToLower())
                {
                    case "startapocalypse": StartWorldEvent(WorldEventType.Apocalypse); break;
                    case "startcrusade": StartWorldEvent(WorldEventType.Crusade); break;
                    case "startheldenian": StartWorldEvent(WorldEventType.Heldenian); break;
                    case "startctf": StartWorldEvent(WorldEventType.CaptureTheFlag); break;
                    case "endapocalypse":
                    case "endcrusade":
                    case "endheldenian":
                    case "endctf": EndWorldEvent(OwnerSide.Neutral); break; // manual end - draw
                    case "mute":
                        if (tokens.Length > 2)
                            if (FindPlayer(tokens[1].Trim(), out c))
                            {
                                c.MuteTime = new TimeSpan(0, Int32.Parse(tokens[2]), 0);
                                SendClientMessage(c, "You are muted for " + c.MuteTime.Hours + "h " + c.MuteTime.Minutes + "m and " + c.MuteTime.Seconds + "s");
                            }
                        break;
                    case "unmute":
                        if (tokens.Length > 1)
                            if (FindPlayer(tokens[1].Trim(), out c))
                                c.MuteTime = new TimeSpan(0, 0, 0);
                        break;
                    case "dc":
                    case "disconnect":
                        if (tokens.Length > 1)
                            if (FindPlayer(tokens[1].Trim(), out c))
                                if (tokens.Length > 2) c.Disconnect(tokens[2]);
                                else c.Disconnect("Not specified");
                        break;
                }
            }

            byte[] data = new byte[17 + message.Length];
            Buffer.BlockCopy((0).GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy((-1).GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy((-1).GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(name.GetBytes(10), 0, data, 6, 10);
            data[16] = (byte)((int)mode);
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, data, 17, message.Length);
            foreach (Map map in maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (players.ContainsKey(map.Players[p]))
                    {
                        Character player = players[map.Players[p]];
                        Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                    }
        }

        public void StartWorldEvent(WorldEventType eventType)
        {
            if (worldEvent != null) return;

            switch (eventType)
            {
                case WorldEventType.Crusade: worldEvent = World.CrusadeConfiguration.Copy(); break;
                case WorldEventType.Heldenian: worldEvent = World.HeldenianConfiguration.Copy(); break;
                case WorldEventType.Apocalypse: worldEvent = World.ApocalypseConfiguration.Copy(); break;
                case WorldEventType.CaptureTheFlag: worldEvent = World.CaptureTheFlagConfiguration.Copy(); break;
                case WorldEventType.KingOfTheHill:
                default: return;
            }
            worldEvent.CurrentWorld = this;
            worldEvent.Start();

            LogMessage(string.Format("{0} started.", eventType.ToString()), LogType.Events);
        }

        public void EndWorldEvent(OwnerSide side)
        {
            if (worldEvent == null) return;
            worldEvent.End(side);
            
            // update results
            if (WorldEventResults.ContainsKey(worldEvent.Result.Type))
                WorldEventResults[worldEvent.Result.Type] = worldEvent.Result;
            else WorldEventResults.Add(worldEvent.Result.Type, worldEvent.Result);

            // update datebase
            if (WorldEventEnded != null) WorldEventEnded(worldEvent.Result);
            LogMessage(string.Format("{0} ended.", worldEvent.Type.ToString()), LogType.Events);

            worldEvent = null;
        }

        /// <summary>
        /// Handles Timer-controlled functions.
        /// </summary>
        public void TimerProcess()
        {
            TimeSpan ts;

            // handles player timer for hp/mp/sp regen, hunger, poison, magic effects etc
            try
            {
                ts = DateTime.Now - playerTimer;
                if (ts.Seconds >= 3)
                {
                    foreach (Map map in maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (players.ContainsKey(map.Players[p]))
                            {
                                players[map.Players[p]].TimerProcess();
                            }

                    playerTimer = DateTime.Now;
                }
            }
            catch (Exception ex) { LogMessage("TimerProcess Players error: " + ex.ToString(), LogType.Error); }

            // handles npc timer for hp/mp/sp regen, poison, magic effects etc
            try
            {
                ts = DateTime.Now - npcTimer;
                if (ts.Seconds >= 3)
                {
                    foreach (Map map in maps.Values)
                        for (int n = 0; n < map.Npcs.Count; n++)
                            if (npcs.ContainsKey(map.Npcs[n]))
                                npcs[map.Npcs[n]].TimerProcess();

                    npcTimer = DateTime.Now;
                }
            }
            catch (Exception ex) { LogMessage("TimerProcess NPCs error" + ex.ToString(), LogType.Error); }

            // handles public events and world events e.g crusade, day/night mode and map effects e.g abby map thunder
            try
            {
                ts = DateTime.Now - eventTimer;
                if (ts.Seconds >= 2)
                {
                    CheckTimeOfDay();

                    if (worldEvent != null) 
                        worldEvent.TimerProcess();

                    foreach (Map map in maps.Values)
                        map.EventProcess();

                    eventTimer = DateTime.Now;
                }
            }
            catch (Exception ex) { LogMessage("TimerProcess Events error: " + ex.ToString(), LogType.Error); }

            // handles map events such as weather, item clearing, mineral spawning
            try
            {
                ts = DateTime.Now - mapTimer;
                if (ts.Seconds >= 20)
                {
                    foreach (Map map in maps.Values) map.TimerProcess();
                    mapTimer = DateTime.Now;
                }
            }
            catch (Exception ex) { LogMessage("TimerProcess Maps error: " + ex.ToString(), LogType.Error); }

            // handles registered dynamic objects that have timer processes, such as dynamic spells
            try
            {
                ts = DateTime.Now - dynamicObjectTimer;
                if (ts.Seconds >= 3)
                {
                    foreach (Map map in maps.Values)
                        for (int d = 0; d < map.DynamicObjects.Count; d++)
                            if (dynamicObjects.ContainsKey(map.Name + map.DynamicObjects[d]))
                                dynamicObjects[map.Name + map.DynamicObjects[d]].TimerProcess();

                    dynamicObjectTimer = DateTime.Now;
                }
            }
            catch (Exception ex) { LogMessage("TimerProcess DynamicEvents error: " + ex.ToString() , LogType.Error); }
        }

        public bool CreateNpc(string npcName, Map map, int x, int y) { return CreateNpc(npcName, map, x, y, -1, null); }
        public bool CreateNpc(string npcName, Location location, int spawnID) { return CreateNpc(npcName, maps[location.MapName], location.X, location.Y, spawnID, null); }
        public bool CreateNpc(string npcName, Map map, int x, int y, IOwner summoner) { return CreateNpc(npcName, map, x, y, -1, summoner); }
        public bool CreateNpc(string npcName, Map map, int x, int y, NpcPerk perk) { return CreateNpc(npcName, map, x, y, -1, null, perk); }
        public bool CreateNpc(string npcName, Map map, int x, int y, int spawnID, IOwner summoner, NpcPerk perk = NpcPerk.None)
        {
            if (World.NpcConfiguration.ContainsKey(npcName))
            {
                Npc npc = World.NpcConfiguration[npcName].Copy();
                npc.SpawnID = spawnID;
                if (npc.IsHuman) 
                    npc.GenerateHuman();

                int npcID = InitNpc(npc, map, x, y, summoner);

                if (npcID != -1)
                {
                    if (summoner != null)
                    {
                        npc.Summoner = summoner.ObjectId;
                        npc.MoveType = MovementType.Follow;
                        summoner.Summons.Add(npcID);
                    }
                    SendLogEventToNearbyPlayers(npc, CommandMessageType.Confirm);
                    return true;
                }
                else return false;
            }
            else return false;
        }

        public bool CreateAdvancedNpc(Map map, int x, int y)
        {
            return false;
        }

        /// <summary>
        /// Gets an available Npc index. Client limitations means that available numbers are limited.
        /// </summary>
        /// <returns>Available Npc index</returns>
        public int GetNpcIndex()
        {
            for (int index = 0; index < 20000; index++)
                if (npcIndexes[index] == false)
                {
                    npcIndexes[index] = true;
                    return index;
                }
            return -1;
        }

        /// <summary>
        /// Clears an index when an Npc is removed so that it is available for a new Npc.
        /// </summary>
        /// <param name="index">Index to be removed</param>
        public void ClearNpcIndex(int index)
        {
            npcIndexes[index] = false;
        }

        /// <summary>
        /// Initialized an Npc object on to a map and sets the summoner if they exist.
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="map"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="summoner"></param>
        /// <returns>ID of the Npc just created</returns>
        public int InitNpc(Npc npc, Location location) { return InitNpc(npc, maps[location.MapName], location.X, location.Y, null); }
        public int InitNpc(Npc npc, Location location, IOwner summoner) { return InitNpc(npc, maps[location.MapName], location.X, location.Y, summoner); }
        public int InitNpc(Npc npc, Map map, int x, int y, IOwner summoner)
        {
            if (npc == null || map == null) return -1;
            try
            {
                npc.ObjectId = GetNpcIndex(); // gets available index
                if (npc.ObjectId == -1) return -1; // -1 means no index available, cannot Init Npc
                if (summoner != null) npc.Side = summoner.Side;
                npc.MessageLogged += new LogHandler(MessageLogged);
                npc.Init(map, x, y);
                if (npc.Side == OwnerSide.Wild) map.TotalMobs++;
                npc.StatusChanged += new OwnerHandler(OnStatusChanged);
                npc.MotionChanged += new MotionHandler(OnMotionChanged);
                npc.DamageTaken += new DamageHandler(OnDamageTaken);
                npc.VitalsChanged += new VitalsChangedHandler(OnVitalsChanged);
                npc.Killed += new DeathHandler(OnKilled);
                npc.ItemDropped += new ItemHandler(OnItemDropped);
                lock (NpcLock) { npcs.Add(npc.ObjectId, npc); }

                return npc.ObjectId;
            }
            catch (Exception ex)
            {
                LogMessage("Error in InitNpc: " + ex.ToString(), LogType.Error);
                return -1;
            }
        }

        /// <summary>
        /// Removes an NPC from the game server.
        /// </summary>
        /// <param name="npcID">ID of the Npc to remove.</param>
        public void RemoveNPC(int npcID)
        {
            if (npcs.ContainsKey(npcID) && npcs[npcID] != null)
            {
                Npc npc = npcs[npcID];

                npc.Remove();
                if (npc.Side == OwnerSide.Wild) npc.CurrentMap.TotalMobs--;
                lock (NpcLock)
                {
                    npcs.Remove(npcID);
                }
                ClearNpcIndex(npcID); //TODO check into this for graphic spawn bug.
            }
        }

        /// <summary>
        /// Handles Npc movement, actions and AI.
        /// </summary>
        public void NpcProcess()
        {
            try
            {
                TimeSpan ts;

                // TODO - can any of these be in the Map or Npc classes? use events (replace CreateNpc) to notify nearby players
                if (!IsCrusade) // no spawns during crusade
                {
                    ts = DateTime.Now - spawnTimer;
                    if (ts.Seconds >= Globals.MobSpawnRate)
                    {
                        foreach (Map map in maps.Values)
                            if (!((IsHeldenianBattlefield || IsHeldenianSeige) && map.IsHeldenianMap)) // no spawns in heldenian maps during heldenian
                            {
                                new Thread(x =>
                                {
                                    for (int spawnID = 0; spawnID < map.MobSpawns.Count; spawnID++)
                                    {
                                        MobSpawn spawn = map.MobSpawns[spawnID];
                                        if (spawn.Count < spawn.Total)
                                        {
                                            if (CreateNpc(spawn.NpcName, spawn.Zone.GetRandomLocation(), spawnID)) spawn.Count++;
                                            continue; // only one npc should spawn in each spawn location per iteration
                                        }
                                    }
                                }).Start();
                               
                            }
                        spawnTimer = DateTime.Now;
                    }
                }

                ts = DateTime.Now - npcActionTimer;
                if (ts.Milliseconds >= 50)
                {
                    foreach (Map map in maps.Values)
                        new Thread(x =>
                        {
                            for (int n = 0; n < map.Npcs.Count; n++)
                            {
                                if (npcs.ContainsKey(map.Npcs[n]))
                                    npcs[map.Npcs[n]].ActionProcess();
                            }
                        }).Start();
                       
                         
                    npcActionTimer = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in npcprocess: " + ex.ToString());
            }
        }

        /// <summary>
        /// Checks the time of day and, and sets dark or lightness of a map. Players are notified if a change happens. Buildings are ignored.
        /// </summary>
        private void CheckTimeOfDay()
        {
            TimeOfDay tod;
            if ((DateTime.Now.Hour >= 20) || (DateTime.Now.Hour < 6))
                if (DateTime.Now.Month == 12)
                    tod = TimeOfDay.NightChristmas;
                else tod = TimeOfDay.Night;
            else tod = TimeOfDay.Day;

            if (tod != timeOfDay)
            {
                foreach (Map map in maps.Values)
                {
                    if (!map.IsBuilding) map.TimeOfDay = tod;
                    for (int p = 0; p < map.Players.Count; p++)
                        if (players.ContainsKey(map.Players[p]))
                        {
                            Character player = players[map.Players[p]];
                            if (!player.CurrentMap.IsBuilding) player.Notify(CommandMessageType.NotifyTimeChange, (int)tod);
                        }
                }
                timeOfDay = tod;
            }
        }

        public void SendClientMessage(Character owner, string message)
        {
            if (message.Length > 70) message.Substring(0, 69);

            byte[] data = new byte[17 + message.Length];
            Buffer.BlockCopy(((short)0).GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(((short)0).GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(((short)0).GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(("Server").GetBytes(10), 0, data, 6, 10);
            data[16] = 10;
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, data, 17, message.Length);

            Send(owner, CommandType.Chat, CommandMessageType.Confirm, data);
        }

        public void SendItemEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, Item item) { SendCommonEventToNearbyPlayers(owner, messageType, item.Sprite, item.SpriteFrame, item.Colour, (int)item.ColorType); }
        public void SendMagicEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int destinationX, int destinationY, int magicType) { SendCommonEventToNearbyPlayers(owner, messageType, destinationX, destinationY, magicType, 0); }
        public void SendCommonEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int value1, int value2, int value3, int value4)
        {
            byte[] data;

            data = new byte[14];
            Buffer.BlockCopy(BitConverter.GetBytes((short)owner.X), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)owner.Y), 0, data, 2, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, data, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, data, 6, 2);
            Buffer.BlockCopy(value3.GetBytes(), 0, data, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value4), 0, data, 12, 2);

            for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                if (players.ContainsKey(owner.CurrentMap.Players[p]))
                {
                    Character player = players[owner.CurrentMap.Players[p]];
                    if ((owner.OwnerType == OwnerType.Player &&
                        player.ClientID == owner.ObjectId) ||
                             player.IsWithinView(owner))
                        Send(player, CommandType.CommonEvent, messageType, data);
                }
        }

        public void SendItemEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int destinationX, int destinationY, Item item) { SendCommonEventToNearbyPlayers(owner, messageType, item.Sprite, item.SpriteFrame, item.Colour, (int)item.ColorType, destinationX, destinationY); }
        public void SendCommonEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int value1, int value2, int value3, int value4, int destinationX, int destinationY)
        {
            byte[] data;

            if (owner.OwnerType == OwnerType.Player)
            {
                Character character = (Character)owner;
                data = new byte[14];
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationX), 0, data, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationY), 0, data, 2, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, data, 4, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, data, 6, 2);
                Buffer.BlockCopy(value3.GetBytes(), 0, data, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value4), 0, data, 12, 2);

                for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                    if (players.ContainsKey(owner.CurrentMap.Players[p]))
                    {
                        Character player = players[owner.CurrentMap.Players[p]];
                        if (player.ClientID == character.ClientID ||
                                 player.IsWithinView(character))
                            Send(player, CommandType.CommonEvent, messageType, data);
                    }
            }
            else if (owner.OwnerType == OwnerType.Npc)
            {
                Npc npc = (Npc)owner;
                data = new byte[14];
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationX), 0, data, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationY), 0, data, 2, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, data, 4, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, data, 6, 2);
                Buffer.BlockCopy(value3.GetBytes(), 0, data, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value4), 0, data, 12, 2);

                for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                    if (players.ContainsKey(owner.CurrentMap.Players[p]))
                    {
                        Character player = players[owner.CurrentMap.Players[p]];
                        if (player.IsWithinView(npc))
                            Send(player, CommandType.CommonEvent, messageType, data);
                    }
            }
        }

        public void SendLogEventToNearbyPlayers(IOwner owner, CommandMessageType messageType) { SendEventToNearbyPlayers(owner, CommandType.LogEvent, messageType, 0, 0, 0); }
        public void SendMotionEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int value1 = 0, int value2 = 0, int value3 = 0, int value4 = 0) { SendEventToNearbyPlayers(owner, CommandType.Motion, messageType, value1, value2, value3, value4); }
        public void SendEventToNearbyPlayers(IOwner owner, CommandType commandType, CommandMessageType messageType, int value1, int value2, int value3, int value4 = 0)
        {
            try
            {
                byte[] data;
                bool notifyOwner = false; // sends data to the owner of the motion event when true
                int tempStatus; // holds status and changes depending on the nearby player. allies/foes receive different data accordingly

                switch (messageType)
                {
                    case CommandMessageType.ObjectNullAction:
                    case CommandMessageType.ObjectTakeDamage:
                    case CommandMessageType.ObjectVitalsChanged:
                    case CommandMessageType.ObjectDying:
                        notifyOwner = true;
                        break;
                }

                data = new byte[108];

                Buffer.BlockCopy(owner.ObjectId.GetBytes(), 0, data, 0, 4);
                data[4] = (byte)(int)owner.OwnerType;
                Buffer.BlockCopy(((short)owner.X).GetBytes(), 0, data, 5, 2);
                Buffer.BlockCopy(((short)owner.Y).GetBytes(), 0, data, 7, 2);
                data[9] = (byte)(int)owner.Direction;
                data[10] = (byte)((int)owner.Side);
                Buffer.BlockCopy(owner.Status.GetBytes(), 0, data, 11, 4);
                Buffer.BlockCopy(owner.Type.GetBytes(), 0, data, 15, 2);
                if (owner.OwnerType == OwnerType.Player)
                {
                    Buffer.BlockCopy(owner.Name.GetBytes(10), 0, data, 17, 10);
                    unchecked { tempStatus = owner.Status & (int)0x0F0FFFF7F; }
                }
                else
                {
                    Buffer.BlockCopy(BitConverter.GetBytes(owner.Id), 0, data, 17, 4);
                    tempStatus = 0;

                    for (int i = 0; i < Globals.MaximumNpcPerks; i++)
                        if (((Npc)owner).Perks.Count > i)
                            data[21 + i] = (byte)(((Npc)owner).Perks[i]);
                        else data[21 + i] = (byte)0;
                }
                if (owner.Helmet != null)
                {
                    Buffer.BlockCopy(owner.Helmet.ItemId.GetBytes(), 0, data, 27, 2); //start at 27
                    Buffer.BlockCopy(owner.Helmet.Colour.GetBytes(), 0, data, 29, 4);
                    data[33] = (byte)((int)owner.Helmet.ColorType);

                }
                if (owner.BodyArmour != null)
                {
                    Buffer.BlockCopy(owner.BodyArmour.ItemId.GetBytes(), 0, data, 34, 2);
                    Buffer.BlockCopy(owner.BodyArmour.Colour.GetBytes(), 0, data, 36, 4);
                    data[40] = (byte)((int)owner.BodyArmour.ColorType);
                }
                if (owner.Hauberk != null)
                {
                    Buffer.BlockCopy(owner.Hauberk.ItemId.GetBytes(), 0, data, 41, 2);
                    Buffer.BlockCopy(owner.Hauberk.Colour.GetBytes(), 0, data, 43, 4);
                    data[47] = (byte)((int)owner.Hauberk.ColorType);
                }
                if (owner.Leggings != null)
                {
                    Buffer.BlockCopy(owner.Leggings.ItemId.GetBytes(), 0, data, 48, 2);
                    Buffer.BlockCopy(owner.Leggings.Colour.GetBytes(), 0, data, 50, 4);
                    data[54] = (byte)((int)owner.Leggings.ColorType);
                }
                if (owner.Cape != null)
                {
                    Buffer.BlockCopy(owner.Cape.ItemId.GetBytes(), 0, data, 55, 2);
                    Buffer.BlockCopy(owner.Cape.Colour.GetBytes(), 0, data, 57, 4);
                    data[61] = (byte)((int)owner.Cape.ColorType);
                }
                if (owner.Weapon != null)
                {
                    Buffer.BlockCopy(owner.Weapon.ItemId.GetBytes(), 0, data, 62, 2);
                    Buffer.BlockCopy(owner.Weapon.Colour.GetBytes(), 0, data, 64, 4);
                    data[68] = (byte)((int)owner.Weapon.ColorType);
                }
                if (owner.Shield != null)
                {
                    Buffer.BlockCopy(owner.Shield.ItemId.GetBytes(), 0, data, 69, 2);
                    Buffer.BlockCopy(owner.Shield.Colour.GetBytes(), 0, data, 71, 4);
                    data[75] = (byte)((int)owner.Shield.ColorType);
                }
                if (owner.Boots != null)
                {
                    Buffer.BlockCopy(owner.Boots.ItemId.GetBytes(), 0, data, 76, 2);
                    Buffer.BlockCopy(owner.Boots.Colour.GetBytes(), 0, data, 78, 4);
                    data[82] = (byte)((int)owner.Boots.ColorType);
                }
                if (owner.Angel != null) //+8 from here on
                {
                    data[83] = (byte)((int)(AngelType)owner.Angel.Effect1);
                }
                data[84] = (byte)(owner.IsCombatMode ? 1 : 0);
                // 77 = special ability
                // 78 = item glow
                // 79 = misc - build points etc
                data[88] = (byte)(int)owner.SideStatus;

                switch (messageType)
                {
                    case CommandMessageType.ObjectTakeDamage:
                    case CommandMessageType.ObjectTakeDamageAndFly:
                    case CommandMessageType.ObjectDying:
                        Buffer.BlockCopy(value1.GetBytes(), 0, data, 89, 4); // damage
                        data[93] = (byte)value2; // stun attack
                        data[94] = (byte)value3; // damage type
                        data[95] = (byte)value4; // hit count
                        break;
                    case CommandMessageType.ObjectVitalsChanged:
                        Buffer.BlockCopy(value1.GetBytes(), 0, data, 89, 4); //amount
                        data[93] = (byte)value2; // type
                        break;
                    case CommandMessageType.ObjectAttack:
                        Buffer.BlockCopy(value1.GetBytes(), 0, data, 89, 2); // destination x
                        Buffer.BlockCopy(value2.GetBytes(), 0, data, 91, 2); // destination y
                        data[93] = (byte)((int)value4); // attackType
                        break;
                    case CommandMessageType.ObjectDead:
                        data[89] = (byte)(((IDeadOwner)owner).IsCorpseExploited ? 1 : 0);
                        break;
                    case CommandMessageType.ObjectMagic:
                        Buffer.BlockCopy(value1.GetBytes(), 0, data, 89, 2); // magic spell id
                        break;
                }

                for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                    if (players.ContainsKey(owner.CurrentMap.Players[p]))
                    {
                        Character player = players[owner.CurrentMap.Players[p]];
                        if (player.IsWithinView(owner))
                        {
                            if (owner.OwnerType == OwnerType.Player && (player.ClientID == ((Character)owner).ClientID && !notifyOwner))
                                continue; // dont notify of own changes unless specified by notifyOwner

                            // make sure allies/foes get correct data accordingly
                            int temp;
                            if (owner.OwnerType == player.OwnerType && player.Side != owner.Side)
                            {
                                if (((Character)owner).IsAdmin) temp = owner.Status;
                                else if (player.ClientID != ((Character)owner).ClientID) temp = tempStatus;
                                else temp = owner.Status;
                            }
                            else temp = owner.Status;

                            int status = ((0x0FFFFFFF & owner.Status) | (player.GetRelationship(owner) << 28));
                            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 11, 4);

                            Send(player, commandType, messageType, data);
                        }
                    }
            }
            catch (Exception ex)
            {
                LogMessage("SendEventToNearbyPlayers error: " + ex.ToString(), LogType.Game);
            }
        }

        public void SendEventToNearbyPlayers(CommandType commandType, CommandMessageType messageType, Map map, int x, int y, int value1, int value2, int value3, int value4)
        {
            try
            {
                byte[] data;

                data = new byte[17];
                Buffer.BlockCopy(BitConverter.GetBytes(x), 0, data, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(y), 0, data, 2, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, data, 4, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, data, 6, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(value3), 0, data, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(value4), 0, data, 12, 2);

                for (int p = 0; p < map.Players.Count; p++)
                    if (players.ContainsKey(map.Players[p])) 
                    {
                        Character player = players[map.Players[p]];

                        int rangeX = (Globals.Resolutions[(int)player.Resolution, (int)ResolutionSetting.CellsWide] / 2) + Globals.OffScreenCells;
                        int rangeY = (Globals.Resolutions[(int)player.Resolution, (int)ResolutionSetting.CellsHeigh] / 2) + Globals.OffScreenCells;

                        if ((player.CurrentMap == map) &&
                                 (player.X >= x - rangeX) &&
                                 (player.X <= x + rangeX) &&
                                 (player.Y >= y - rangeY) &&
                                 (player.Y <= y + rangeY))
                            Send(player, commandType, messageType, data);
                    }
            }
            catch (Exception ex)
            {
                LogMessage("SendEventToNearbyPlayers error: " + ex.ToString(), LogType.Game);
            }
        }

        public void Send(Character character, CommandType type, CommandMessageType messageType) { Send(character.ClientID, type, messageType, null); }
        public void Send(Character character, CommandType type, CommandMessageType messageType, byte[] sendData) { Send(character.ClientID, type, messageType, sendData); }
        public void Send(int clientID, CommandType type, CommandMessageType messageType) { Send(clientID, type, messageType, null); }
        public void Send(int clientID, CommandType commandType, CommandMessageType messageType, byte[] sendData)
        {
            byte[] data;

            switch (commandType)
            {
                case CommandType.ResponseInitPlayer:
                case CommandType.ResponseInitData:
                case CommandType.ResponseInitMapData:
                case CommandType.ResponseInitDataPlayerStats:
                case CommandType.ResponseInitDataPlayerItems:
                case CommandType.ResponseMotion:
                case CommandType.ResponseWarehouseItem:
                case CommandType.ResponseNews:
                case CommandType.ResponseCreateGuild:
                case CommandType.ResponseDisbandGuild:
                case CommandType.ResponseMerchantData:
                case CommandType.CommonEvent:
                case CommandType.Motion:
                case CommandType.LogEvent:
                case CommandType.DynamicObjectEvent:
                    if (sendData != null) // has payload?
                    {
                        data = new byte[sendData.Length + 15];
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(commandType), 0, data, 0, 4);
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(messageType), 0, data, 4, 2);
                        Buffer.BlockCopy(sendData, 0, data, 6, sendData.Length);
                        SendData(clientID, data);
                    }
                    else // no payload, usually a "Confirm" message
                    {
                        data = new byte[15];
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(commandType), 0, data, 0, 4);
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(messageType), 0, data, 4, 2);
                        SendData(clientID, data);
                    }
                    break;
                case CommandType.Chat:
                case CommandType.ResponseCitizenship:
                    data = new byte[sendData.Length + 15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(commandType), 0, data, 0, 4);
                    Buffer.BlockCopy(sendData, 0, data, 4, sendData.Length);
                    SendData(clientID, data);
                    break;
            }
        }

        private void OnMapWeatherChanged(Map map)
        {
            for (int p = 0; p < map.Players.Count; p++)
                if (players.ContainsKey(map.Players[p]))
                    players[map.Players[p]].Notify(CommandMessageType.NotifyWeatherChange, (int)map.Weather);
        }
        private void OnMapDynamicObjectCreated(IDynamicObject dynamicObject)
        {
            switch (dynamicObject.Type)
            {
                case DynamicObjectType.Fish:
                case DynamicObjectType.FishObject:
                case DynamicObjectType.Rock:
                case DynamicObjectType.Gem: // do not register any objects that dont implement the timer
                    break;
                default: dynamicObjects.Add(dynamicObject.CurrentMap.Name + dynamicObject.ID, dynamicObject); break;
            }
            
            SendEventToNearbyPlayers(CommandType.DynamicObjectEvent, CommandMessageType.Confirm, dynamicObject.CurrentMap, dynamicObject.X, dynamicObject.Y, (int)dynamicObject.Type, dynamicObject.ID, 0, 0);
        }

        private void OnMapDynamicObjectRemoved(IDynamicObject dynamicObject)
        {
            SendEventToNearbyPlayers(CommandType.DynamicObjectEvent, CommandMessageType.Reject, dynamicObject.CurrentMap, dynamicObject.X, dynamicObject.Y, (int)dynamicObject.Type, dynamicObject.ID, 0, 0);
        }

        private void OnVitalsChanged(IOwner owner, int amount, VitalType type)
        {
            SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectVitalsChanged, amount, (int)type);
        }

        private void OnDamageTaken(IOwner owner, DamageType type, IOwner attacker, int damage, MotionDirection flyDirection, int hitCount, int totalDamage)
        {    
            
            //Do combo here?
            if (owner.OwnerType == OwnerType.Player) 
            {

                Map map = owner.CurrentMap;
                int destinationX = owner.X, destinationY = owner.Y;
                switch (flyDirection)
                {
                    case MotionDirection.North: destinationY = destinationY - 1; break;
                    case MotionDirection.NorthEast: destinationX = destinationX + 1; destinationY = destinationY - 1; break;
                    case MotionDirection.East: destinationX = destinationX + 1; break;
                    case MotionDirection.SouthEast: destinationX = destinationX + 1; destinationY = destinationY + 1; break;
                    case MotionDirection.South: destinationY = destinationY + 1; break;
                    case MotionDirection.SouthWest: destinationX = destinationX - 1; destinationY = destinationY + 1; break;
                    case MotionDirection.West: destinationX = destinationX - 1; break;
                    case MotionDirection.NorthWest: destinationX = destinationX - 1; destinationY = destinationY - 1; break;
                }

                //No fly conditions
                if (!map[destinationY][destinationX].IsMoveable || map[destinationY][destinationX].IsBlocked || owner.MagicEffects.ContainsKey(MagicType.Paralyze) ||
                    (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height)) { flyDirection = MotionDirection.None; }

                if (flyDirection != MotionDirection.None) 
                {

                    Character player = (Character)owner;
                    switch (type)// players can be flown. different fly thresholds depending on DamageType
                    {
                        case DamageType.Magic:
                            if (player.CurrentMap.IsFightZone && damage >= Globals.MagicFlyDamageFightzone)
                                player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, totalDamage, (int)type, hitCount);
                            else if (damage >= Globals.MagicFlyDamage)
                                player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, totalDamage, (int)type, hitCount);
                            else // if (Dice.Roll(1, 100) <= attacker.StunChance) // stun removed from normal attacks
                                SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, totalDamage, 0, (int)type, hitCount);
                            break;
                        case DamageType.Melee:
                        case DamageType.Ranged:
                            if (player.CurrentMap.IsFightZone && damage >= Globals.MeleeFlyDamageFightzone)
                                player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, totalDamage, (int)type, hitCount);
                            else if (damage >= Globals.MeleeFlyDamage)
                                player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, totalDamage, (int)type, hitCount);
                            else //if (Dice.Roll(1, 100) <= attacker.StunChance) // stun removed from normal attacks
                                SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, totalDamage, 0, (int)type, hitCount);
                            break;
                        case DamageType.Environment:
                        case DamageType.Poison:
                        case DamageType.Spirit:
                            SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, totalDamage, 0, (int)type, hitCount);
                            break;
                    }
                }
                else SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, totalDamage, 0, (int)type, hitCount);
            }
            else SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, totalDamage, 0, (int)type, hitCount);
        }

        private void OnKilled(IOwner owner, IOwner killer, int damage, DamageType type, int hitCount)
        {
            SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectDying, damage, 0, (int)type, hitCount);

            switch (owner.OwnerType)
            {
                //case OwnerType.Player:
                //    if (killer != null && killer.OwnerType == OwnerType.Player) //TODO - multiple kill rewards for anyone who effected the player
                //        ((Character)killer).GetKillReward((Character)owner);// player kill reward
                //    break;
                case OwnerType.Player:
                    {
                        if (killer.OwnerType == OwnerType.Npc)
                        {
                            return;
                        }

                        Character character = (Character)owner;
                        ((Character)killer).GetKillReward(character, true);//Main Killer Reward
                        foreach (KeyValuePair<int,DateTime> attacker in character.DamagedByObjects)
                        {
                            if (players[attacker.Key] != null)
                            {
                                Character killHelpers = players[attacker.Key];
                                if (killer.Name != killHelpers.Name) //Assist Killers Reward
                                {
                                    killHelpers.GetKillReward(character, false);
                                }
                            }
                        }
                        break;
                    }
                case OwnerType.Npc:
                    if (((Npc)owner).Perks.Contains(NpcPerk.Explosive))// fire strike
                    {
                        SendMagicEventToNearbyPlayers(owner, CommandMessageType.CastMagic, owner.X, owner.Y, 30 + 100);
                        CastMagic(owner, owner.X, owner.Y, MagicConfiguration[30]);
                    }
                    else if (((Npc)owner).Perks.Contains(NpcPerk.CriticalExplosive)) // mass fire strike
                    {
                        SendMagicEventToNearbyPlayers(owner, CommandMessageType.CastMagic, owner.X, owner.Y, 61 + 100);
                        CastMagic(owner, owner.X, owner.Y, MagicConfiguration[61]);
                    }
                    break;
            }
        }

        private void OnItemPurchased(string databaseId, int count)
        {
            ItemPurchased(databaseId, count);
        }

        private void OnItemSold(MerchantItem item, bool stack)
        {
            item.Item.SetNumber = 0;
            ItemSold(item, stack);
        }

        private void OnItemDropped(IOwner owner, Item item)
        {
            item.SetNumber = 0;

            if (owner.OwnerType == OwnerType.Npc)
            {
                Npc npc = (Npc)owner;
                if (npc.DropCount > 1){  SendItemEventToNearbyPlayers(owner, CommandMessageType.DropItem, npc.DropX, npc.DropY, item); }
                else { SendItemEventToNearbyPlayers(owner, CommandMessageType.DropItem, item); }
            }
            else { SendItemEventToNearbyPlayers(owner, CommandMessageType.DropItem, item); }
        }

        private void OnItemPickedUp(IOwner owner, Item item)
        {
            item.SetNumber = 0;
            SendItemEventToNearbyPlayers(owner, CommandMessageType.SetItem, item);
        }

        private void OnMotionChanged(IOwner owner, CommandMessageType motionType, int destinationX, int destinationY)
        {
            // TODO instead of doing this in PlayerProcess() - make use of events
        }

        private void OnStatusChanged(IOwner owner)
        {
            SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectNullAction);
        }

        /// <summary>
        /// Adds a map to the list of maps for this game server.
        /// </summary>
        /// <param name="name">Internal name of the map. Sent between client and server.</param>
        /// <param name="friendlyName">Friendly name of the map. For readability.</param>
        /// <param name="amdFileName">File name of the map including directory. e.g Maps/map.amd</param>
        /// <param name="isBuilding">Specifies whether this is a building for day/night cycles and weather to ignore.</param>
        /// <param name="isFightZone">Specifies whether this is a fightzone.</param>
        /// <param name="mapReader">An open xml reader with the config for the maps.</param>
        public void AddMap(string name, string friendlyName, string amdFileName, bool isBuilding, bool isFightZone, BuildingType type, bool isSafeMap, bool lootEnabled, bool isHuntZone, string music, XmlReader mapReader)
        {
            Map map = new Map(amdFileName, name, friendlyName);
            map.Parse(mapReader);
            map.IsBuilding = (type != BuildingType.None) ? true : isBuilding; // if building type is specified, it is a building. otherwise, check Building="True" from config
            map.IsSafeMap = isSafeMap;
            map.IsFightZone = isFightZone;
            map.LootEnabled = lootEnabled;
            map.IsHuntZone = isHuntZone;
            map.BuildingType = type;
            map.MusicName = music;
            map.WeatherChanged += new MapHandler(OnMapWeatherChanged);
            map.DynamicObjectCreated += new DynamicObjectHandler(OnMapDynamicObjectCreated);
            map.DynamicObjectRemoved += new DynamicObjectHandler(OnMapDynamicObjectRemoved);
            map.MessageLogged += new LogHandler(LogMessage);
            maps.Add(name, map);
        }

        public void LogMessage(string message, LogType type)
        {
            if (MessageLogged != null) MessageLogged(message, type);
        }

        /// <summary>
        /// Loads map data from the amd files specified in config.xml. Error messages returned based on Map.Load result.
        /// </summary>
        /// <returns>True indicates all maps loaded. False indicates at least 1 failure.</returns>
        public bool LoadMaps()
        {
            int failues = 0;
            foreach (Map map in maps.Values)
                if (!map.IsLoaded)
                    switch (map.Load())
                    {
                        case MapLoadResult.Success:
                            for (int spawnID = 0; spawnID < map.NpcSpawns.Count; spawnID++)
                            {
                                NpcSpawn spawn = map.NpcSpawns[spawnID];
                                Npc npc = World.NpcConfiguration[spawn.Name].Copy();
                                npc.MoveType = spawn.MoveType;
                                npc.IsFriendly = true; // the differnce between an Npc and a Mob
                                npc.SpawnID = spawnID;
                                if (spawn.MoveType == MovementType.RandomArea) { InitNpc(npc, spawn.Zone.GetRandomLocation(), null); }
                                else { InitNpc(npc, spawn.Location, null); }
                            }

                            MessageLogged(map.FriendlyName + " map loaded [" + map.Name + "]", LogType.Game);
                            break;
                        case MapLoadResult.MapNotFound:
                            MessageLogged(map.FriendlyName + " not loaded... unable to find " + map.FileName, LogType.Game);
                            failues++;
                            break;
                        case MapLoadResult.MapNotAccessible:
                            MessageLogged(map.FriendlyName + " not loaded... " + map.FileName + " access denied. Check file permissions", LogType.Game);
                            failues++;
                            break;
                        case MapLoadResult.Error:
                            MessageLogged(map.FriendlyName + " not loaded... unknown error.", LogType.Game);
                            failues++;
                            break;
                    }

            CheckTimeOfDay();

            if (failues == 0) return true;
            else return false;
        }

        public DataTable FormatPlayerList()
        {
            DataTable playerData = new DataTable();
            playerData.Columns.Add("Name");
            playerData.Columns.Add("Town");
            playerData.Columns.Add("Level");
            playerData.Columns.Add("Guild");
            playerData.Columns.Add("Map");

            try
            {
                foreach (Map map in maps.Values)
                    for (int p = 0; p < map.Players.Count; p++)
                        if (players.ContainsKey(map.Players[p]))
                            playerData.Rows.Add(new object[] 
                                            { players[map.Players[p]].Name,
                                              players[map.Players[p]].Town.ToString(),
                                              players[map.Players[p]].Level,
                                              players[map.Players[p]].Guild.Name,
                                              map.FriendlyName });
            }
            catch (Exception ex)
            {
                LogMessage("Error formating list for GUI controls: " + ex.ToString(), LogType.Error);
            }

            return playerData;
        }

        public Magic GetSpellByName(string name)
        {
            Magic spell = null;

            for (int i = 0; i < Globals.MaximumSpells; i++)
                if (World.MagicConfiguration.ContainsKey(i))
                {
                    //LogMessage("Searching: " + name + "   against:  " + World.MagicConfiguration[i].Name, LogType.Game);
                    if (World.MagicConfiguration[i].Name.Trim().Equals(name.Trim()))
                    {
                        spell = World.MagicConfiguration[i];
                        break;
                    }
            }

            return spell;
        }

        public int NpcCount
        {
            get
            {
                int count = 0;
                foreach (Map map in maps.Values)
                    count += map.Npcs.Count;
                return count;
            }
        }

        public Dictionary<int, Character> Players { get { return players; } set { players = value; } }
        public Dictionary<int, Npc> Npcs { get { return npcs; } set { npcs = value; } }
        public Dictionary<string, IDynamicObject> DynamicObjects { get { return dynamicObjects; } set { dynamicObjects = value; } }
        public Dictionary<string, Map> Maps { get { return maps; } set { maps = value; } }
        public Dictionary<string, Guild> Guilds { get { return guilds; } set { guilds = value; } }
        public List<Party> Parties { get { return parties; } set { parties = value; } }
        public IWorldEvent CurrentEvent { get { return worldEvent; } }
        public bool IsApocalypse { get { return (worldEvent != null && worldEvent.Type == WorldEventType.Apocalypse); } }
        public Apocalypse Apocalypse { get { return ((Apocalypse)worldEvent); } }
        public bool IsCrusade { get { return (worldEvent != null && worldEvent.Type == WorldEventType.Crusade); } }
        public Crusade Crusade { get { return ((Crusade)worldEvent); } }
        public bool IsHeldenianBattlefield { get { return (worldEvent != null && worldEvent.Type == WorldEventType.Heldenian && ((Heldenian)worldEvent).Mode == HeldenianType.Battlefield); } }
        public Heldenian Heldenian { get { return ((Heldenian)worldEvent); } }
        public bool IsHeldenianSeige { get { return (worldEvent != null && worldEvent.Type == WorldEventType.Heldenian && ((Heldenian)worldEvent).Mode == HeldenianType.CastleSeige); } }
        public CaptureTheFlag CaptureTheFlag { get { return ((CaptureTheFlag)worldEvent); } }
        public bool IsCaptureTheFlag { get { return ((worldEvent != null) && worldEvent.Type == WorldEventType.CaptureTheFlag); } }
        public TimeOfDay TimeOfDay { get { return timeOfDay; } }
    }
}
