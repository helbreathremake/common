﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;

namespace Helbreath.Common
{
    public static class NpcHelper
    {
        public static Dictionary<string, Npc> LoadNpcs(string configFilePath, Dictionary<int, Item> itemConfiguration, out Dictionary<string,LootTable> primaryLoot, out Dictionary<string, LootTable> secondaryLoot)
        {
            Dictionary<string, Npc> npcs = new Dictionary<string, Npc>();
            primaryLoot = new Dictionary<string, LootTable>();
            secondaryLoot = new Dictionary<string, LootTable>();

            string npcName = "";
            string lootName = ""; // reduce size of try blocks?

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Npc"))
                        if (!npcs.ContainsKey(reader["Name"]))
                        {
                            Npc npc;
                            npc = Npc.ParseXml(reader);
                            npcs.Add(npc.Name, npc);
                            npcName = npc.Name; // for logging

                            // add a new merchant if it doesn't already exist
                            if (npc.MerchantId > 0 && !World.MerchantConfiguration.ContainsKey(npc.MerchantId))
                                World.MerchantConfiguration.Add(npc.MerchantId, new Dictionary<int, MerchantItem>());

                            LootTable drops = new LootTable(npc.Name); // primary drops
                            LootTable secDrops = new LootTable(npc.Name); // secondary drops
                            XmlReader lootReader = reader.ReadSubtree();
                            while (lootReader.Read())
                                if (lootReader.IsStartElement())
                                    switch (lootReader.Name)
                                    {
                                        case "Item":
                                            lootName = lootReader["Name"];
                                            LootDrop drop;
                                            int itemId;
                                            ItemRarity rarity;
                                            if (Enum.TryParse<LootDrop>(lootReader["Drop"], out drop))
                                                switch (drop)
                                                {
                                                    case LootDrop.Primary:
                                                        if (Int32.TryParse(lootReader["ItemId"], out itemId))
                                                        {
                                                            if (itemConfiguration.ContainsKey(itemId))
                                                            {
                                                                if (Enum.TryParse<ItemRarity>(lootReader["Rarity"], out rarity)) //If rarity specificed in npc loot drop, else default item rarity
                                                                {
                                                                    if (rarity != ItemRarity.None)
                                                                        drops[rarity].Add(itemId);
                                                                }
                                                                else if (itemConfiguration[itemId].Rarity != ItemRarity.None)
                                                                    drops[itemConfiguration[itemId].Rarity].Add(itemId);
                                                            }
                                                            else { }
                                                        }
                                                        break;
                                                    case LootDrop.Secondary:
                                                        if (Int32.TryParse(lootReader["ItemId"], out itemId))
                                                        {
                                                            if (itemConfiguration.ContainsKey(itemId))
                                                            {
                                                                if (Enum.TryParse<ItemRarity>(lootReader["Rarity"], out rarity)) //If rarity specificed in npc loot drop, else default item rarity
                                                                {
                                                                    if (rarity != ItemRarity.None)
                                                                        secDrops[rarity].Add(itemId);
                                                                }
                                                                else if (itemConfiguration[itemId].Rarity != ItemRarity.None)
                                                                    secDrops[itemConfiguration[itemId].Rarity].Add(itemId);
                                                            }
                                                            else { }
                                                        }
                                                        break;
                                                }
                                            break;
                                    }
                            primaryLoot.Add(npc.Name, drops);
                            secondaryLoot.Add(npc.Name, secDrops);
                        }

            reader.Close();

            return npcs;
        }

        public static Dictionary<int, Npc> LoadNpcs(string configFilePath)
        {
            Dictionary<int, Npc> npcs = new Dictionary<int, Npc>();

            string npcName = "";

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Npc"))
                        if (!npcs.ContainsKey(Int32.Parse(reader["Id"])))
                        {
                            Npc npc = Npc.ParseXml(reader);
                            npcs.Add(npc.Id, npc);
                            npcName = npc.Name; // for logging
                        }

            reader.Close();

            return npcs;
        }

        public static string GetNpcFriendlyName(Dictionary<int, Npc> config, int id, int status)
        {
            object npc = config.FirstOrDefault(s => s.Value.Id == id);

            string ret = string.Empty;

            if (npc == null || ((KeyValuePair<int, Npc>)npc).Value == null) ret = "Unknown";
            else ret = ((KeyValuePair<int, Npc>) npc).Value.FriendlyName;

            if ((status & 0x20) != 0) ret += " Berserked";
            if ((status & 0x40) != 0) ret += " Frozen";
            //add hasted?
            return ret;
        }

        public static void SetNpcPerk(Npc npc, NpcPerk perk)
        {
            unchecked
            {
                npc.Status = npc.Status & (int)0xFFFFF0FF;
                int temp = (int)perk;
                temp = temp << 8;
                npc.Status = npc.Status | temp;
            }
        }

        public static string GetNpcPerkName(NpcPerk perk)
        {
            string ret = string.Empty;

            switch (perk)
            {
                default:
                case NpcPerk.None: break;
                case NpcPerk.Clairvoyant: ret = "Clairvoyant"; break;//"Clairvoyant"
                case NpcPerk.DestructionOfMagic: ret = "Destruction of Protection"; break;//was "Destruction of Magic Protection" - changed functionality to destroy all protection spells
                case NpcPerk.AntiPhysical: ret = "Anti-Physical Damage"; break;//"Anti-Physical Damage"
                case NpcPerk.AntiMagic: ret = "Ant-Magic Damage"; break;//"Anti-Magic Damage"
                case NpcPerk.Poisonous: ret = "Poisonous"; break;//"Poisonous"
                case NpcPerk.CriticalPoisonous: ret = "Critical Poisonous"; break;//"Critical Poisonous"
                case NpcPerk.Explosive: ret = "Explosive"; break;//"Explosive"
                case NpcPerk.CriticalExplosive: ret = "Critical Explosive"; break;//"Critical Explosive"
            }

            return ret;
        }
    }
}
