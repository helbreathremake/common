﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Helbreath.Common
{
    public class WorldEventResult
    {
        private string id;
        private DateTime startDate;
        private DateTime endDate;
        private WorldEventType type;
        private OwnerSide winner;

        public WorldEventResult(WorldEventType type)
        {
            this.type = type;
        }

        public void Parse(DataRow data)
        {
            id = data["ID"].ToString();
            startDate = Convert.ToDateTime(data["StartTime"].ToString());
            endDate = Convert.ToDateTime(data["EndTime"].ToString());

            OwnerSide sideParser;
            winner = (Enum.TryParse<OwnerSide>(data["Winner"].ToString(), out sideParser)) ? sideParser : OwnerSide.None;
        }

        public String ID { get { return id; } }
        public DateTime StartDate { get { return startDate; } set { startDate = value; } }
        public DateTime EndDate { get { return endDate; } set { endDate = value; } }
        public WorldEventType Type { get { return type; } set { type = value; } }
        public OwnerSide Winner { get { return winner; } set { winner = value; } }
    }
}
