﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Helbreath.Common
{
    public static class Utility
    {
        public static int GetManaCost(int manaCost, int manaSave) //Used to have isSafe
        {
            if (manaSave > Globals.MaximumManaSave) manaSave = Globals.MaximumManaSave;

            int cost = (int)((double)manaCost - (((double)manaSave / 100.0f) * (double)manaCost));
            if (cost <= 0) cost = 1;
            return cost;
        }

        public static int GetCastingProbability(int magicSkill, int magicLevel, int level, int intelligence, WeatherType weather, int bonus, int sp)
        {
            int probability = (int)(((double)(Math.Max(1, magicSkill) / 100.0f) * Globals.MagicCastingProbability[magicLevel]));
            probability += (intelligence > 50 ? ((intelligence - 50) / 2) : 0);

            int ability = (level / 10);
            if (magicLevel > ability)
            {
                int modifier = (int)(((double)(level - magicLevel * 10) )
                                    / ((double)Math.Abs(magicLevel - ability) * 10)
                                    * ((double)Math.Abs(magicLevel - ability) * Globals.MagicCastingLevelPenalty[magicLevel]));

                probability -= Math.Abs(magicLevel - ability) * Globals.MagicCastingLevelPenalty[magicLevel] - modifier;
            }
            else
            {
                probability += 5 * Math.Abs(magicLevel - ability);
            }

            switch (weather)
            {
                case WeatherType.Rain: probability -= (probability/24); break;
                case WeatherType.RainMedium: probability -= (probability/12); break;
                case WeatherType.RainHeavy: probability -= (probability/5); break;
            }

            probability += bonus;

            if (sp < 1) probability = 10;

            return Math.Max(probability , 1);
        }

        /// <summary>
        /// Gets the Greatest Common Divisor of 2 numbers
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        public static int DetermineGCD(int num1, int num2)
        {
            while (num1 != num2)
            {
                if (num1 > num2)
                    num1 = num1 - num2;

                if (num2 > num1)
                    num2 = num2 - num1;
            }

            return num1;

        }

        /// <summary>
        /// Gets the Lowest Common Multiple of 2 numbers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int DetermineLCM(int a, int b)
        {
            int num1, num2;
            if (a > b)
            {
                num1 = a; num2 = b;
            }
            else
            {
                num1 = b; num2 = a;
            }

            for (int i = 1; i <= num2; i++)
            {
                if ((num1 * i) % num2 == 0)
                {
                    return i * num1;
                }
            }
            return num2;
        }

        public static List<int[]> GetMagicSpiralLocations(int destinationX, int destinationY)
        {
            List<int[]> spiralHitLocations = new List<int[]>();
            spiralHitLocations.Add(new int[] { destinationX + 1, destinationY });
            spiralHitLocations.Add(new int[] { destinationX + 2, destinationY });
            spiralHitLocations.Add(new int[] { destinationX + 2, destinationY + 1 });
            spiralHitLocations.Add(new int[] { destinationX + 2, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX + 1, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX - 1, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY + 1 });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY - 1 });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX - 1, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX + 1, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX + 2, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX + 3, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY - 1 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY + 1 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY + 3 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX + 3, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX + 2, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX + 1, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX - 1, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX - 3, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY + 4 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY + 3 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY + 2 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY + 1 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY - 1 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY - 2 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY - 3 });
            spiralHitLocations.Add(new int[] { destinationX - 4, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX - 3, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX - 2, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX - 1, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX + 1, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX + 2, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX + 3, destinationY - 4 });
            spiralHitLocations.Add(new int[] { destinationX + 4, destinationY - 4 });

            return spiralHitLocations;
        }

        public static List<int[]> GetMagicLineLocations(int sourceX, int sourceY, int targetX, int targetY)
        {
            List<int[]> hitLocations = new List<int[]>();

            if (sourceX == targetX && sourceY == targetY)
            {
                hitLocations.Add(new int[2] { sourceX, sourceY });
                return hitLocations;
            }

            int rangeX = targetX - sourceX;
            int rangeY = targetY - sourceY;
            int incrementX, incrementY;

            if (rangeX >= 0) incrementX = 1;
            else
            {
                incrementX = -1;
                rangeX = -rangeX;
            }

            if (rangeY >= 0) incrementY = 1;
            else
            {
                incrementY = -1;
                rangeY = -rangeY;
            }

            for (int hit = 2; hit < 10; hit++)
            {
                int error = 0, count = 0;
                int resultX = sourceX, resultY = sourceY;

                if (rangeX > rangeY)
                {
                    for (int index = 0; index <= rangeX; index++)
                    {
                        error += rangeY;
                        if (error > rangeX)
                        {
                            error -= rangeX;
                            resultY += incrementY;
                        }
                        resultX += incrementX;
                        count++;
                        if (count >= hit) break;
                    }
                }
                else
                {
                    for (int index = 0; index <= rangeY; index++)
                    {
                        error += rangeX;
                        if (error > rangeY)
                        {
                            error -= rangeY;
                            resultX += incrementX;
                        }
                        resultY += incrementY;
                        count++;
                        if (count >= hit) break;
                    }
                }

                hitLocations.Add(new int[2] { resultX, resultY });
            }

            return hitLocations;
        }

        public static MotionDirection GetFlyDirection(int sourceX, int sourceY, int targetX, int targetY)
        {
            if (sourceX > targetX)
            {
                if (sourceY > targetY) return MotionDirection.NorthWest;
                else if (sourceY == targetY) return MotionDirection.West;
                else if (sourceY < targetY) return MotionDirection.SouthWest;
            }
            else if (sourceX == targetX)
            {
                if (sourceY > targetY) return MotionDirection.North;
                else if (sourceY == targetY) return MotionDirection.None;
                else if (sourceY < targetX) return MotionDirection.South;
            }
            else if (sourceX < targetX)
            {
                if (sourceY > targetY) return MotionDirection.NorthEast;
                else if (sourceY == targetY) return MotionDirection.East;
                else if (sourceY < targetY) return MotionDirection.SouthEast;
            }
            
            return MotionDirection.None;
        }

        public static MotionDirection GetNextDirection(int sourceX, int sourceY, int destinationX, int destinationY)
        {
            int x, y;
            MotionDirection direction = MotionDirection.None;

            x = sourceX - destinationX;
            y = sourceY - destinationY;

            if ((x == 0) && (y == 0)) direction = MotionDirection.None;

            if (x == 0)
            {
                if (y > 0) direction = MotionDirection.North;
                if (y < 0) direction = MotionDirection.South;
            }
            if (y == 0)
            {
                if (x > 0) direction = MotionDirection.West;
                if (x < 0) direction = MotionDirection.East;
            }
            if ((x > 0) && (y > 0)) direction = MotionDirection.NorthWest;
            if ((x < 0) && (y > 0)) direction = MotionDirection.NorthEast;
            if ((x > 0) && (y < 0)) direction = MotionDirection.SouthWest;
            if ((x < 0) && (y < 0)) direction = MotionDirection.SouthEast;

            return direction;
        }

        public static int[][] GetMoveLocations(MotionDirection direction, Resolution resolution, int offScreenCells)
        {
            List<int[]> locations = new List<int[]>();
                  
            int cellsWide = Globals.Resolutions[(int)resolution, 2];
            int cellsHigh = Globals.Resolutions[(int)resolution, 3];

            switch (direction)
            {
                case MotionDirection.None:
                    {
                        break;
                    }
                case MotionDirection.North:
                    {
                        for (int i = -offScreenCells; i < cellsWide + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = i;
                            location[1] = -offScreenCells;
                            locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.NorthEast:
                    {
                        //north
                        for (int i = -offScreenCells; i < cellsWide + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = i;
                            location[1] = -offScreenCells;
                            locations.Add(location);
                        }

                        //east
                        for (int i = -offScreenCells; i < cellsHigh + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = cellsWide + offScreenCells - 1;
                            location[1] = i;
                            if (!locations.Contains(location)) locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.East:
                    {
                        for (int i = -offScreenCells; i < cellsHigh + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = cellsWide + offScreenCells - 1;
                            location[1] = i;
                            locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.SouthEast:
                    {
                        //east
                        for (int i = -offScreenCells; i < cellsHigh + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = cellsWide + offScreenCells - 1;
                            location[1] = i;
                            locations.Add(location);
                        }

                        //south
                        for (int i = -offScreenCells; i < cellsWide + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = i;
                            location[1] = cellsHigh + offScreenCells - 1;
                            if (!locations.Contains(location)) locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.South:
                    {
                        for (int i = -offScreenCells; i < cellsWide + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = i;
                            location[1] = cellsHigh + offScreenCells - 1;
                            locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.SouthWest:
                    {
                        //south
                        for (int i = -offScreenCells; i < cellsWide + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = i;
                            location[1] = cellsHigh + offScreenCells - 1;
                            locations.Add(location);
                        }

                        //west
                        for (int i = -offScreenCells; i < cellsHigh + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = -offScreenCells;
                            location[1] = i;
                            if (!locations.Contains(location)) locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.West:
                    {
                        for (int i = -offScreenCells; i < cellsHigh + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = -offScreenCells;
                            location[1] = i;
                            locations.Add(location);
                        }
                        break;
                    }
                case MotionDirection.NorthWest:
                    {
                        //north
                        for (int i = -offScreenCells; i < cellsWide + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = i;
                            location[1] = -offScreenCells;
                            locations.Add(location);
                        }

                        //west
                        for (int i = -offScreenCells; i < cellsHigh + offScreenCells; i++)
                        {
                            int[] location = new int[2];
                            location[0] = -offScreenCells;
                            location[1] = i;
                            if (!locations.Contains(location)) locations.Add(location);
                        }
                        break;
                    }
            }

            int[][] finalLocations = locations.ToArray();
            return finalLocations;
        }

        public static int GetRange(SkillType skillType, bool critical, bool extendedRange = false)
        {
            int range = 0;
            switch (skillType)
            {
                default:
                case SkillType.None: range = 0; break;
                case SkillType.Hand: range = 1; break;
                case SkillType.ShortSword: range = 1; break;
                case SkillType.Fencing: range = critical ? 5 : 1; break;
                case SkillType.LongSword: range = critical ? 3 : 1; break;
                case SkillType.Axe: range = critical ? 2 : 1; break;
                case SkillType.Hammer: range = critical ? 2 : 1; break;
                case SkillType.Staff: range = critical ? 2 : 1; break;
                case SkillType.BattleStaff: range = critical ? 5 : 4; break;
                case SkillType.Archery: range = critical ? 9 : 8; break; // TODO: short 6 : 5
                case SkillType.Mining: range = 1; break;
            }

            if (extendedRange) range += 2;

            return range;
        }

        public static bool IsSelected(int mouseX, int mouseY, int left, int top, int right, int bottom) //0 based so included left & top but not right or bottom
        {
            if (mouseX >= left && mouseX < right &&
                        mouseY >= top && mouseY < bottom)
            {
                return true;
            }
            return false;
        }

        public static bool IsSelected(int mouseX, int mouseY, int left, int top, int right, int bottom, int shiftX, int shiftY)
        {
            if (mouseX + shiftX > left && mouseX + shiftX < right &&
                        mouseY + shiftY > top && mouseY + shiftY < bottom)
            {
                return true;
            }
            return false;
        }

        public static WeaponType GetWeaponType(int appearance2)
        {
            int type = ((appearance2 & 0x0FF0) >> 4);

            if (type == 0) return WeaponType.Hand;
            else if (type >= 1 && type < 3) return WeaponType.ShortSword;
            else if (type >= 3 && type < 20)
            {
                if (type == 7 || type == 18) return WeaponType.Fencing;
                else return WeaponType.LongSword;
            }
            else if (type >= 20 && type < 29) return WeaponType.Axe;
            else if (type >= 30 && type < 33) return WeaponType.Hammer;
            else if (type >= 34 && type < 40) return WeaponType.Wand;
            else if (type >= 40 && type < 41) return WeaponType.ShortBow;
            else if (type >= 41 && type < 50) return WeaponType.LongBow;
            else if (type == 29 || type == 33) return WeaponType.LongSword;
            else if (type >= 50 && type < 60) return WeaponType.BattleStaff;
            else if (type >= 60 && type < 70) return WeaponType.Throwing;
            else return WeaponType.None;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        public static string ByteArrayToString(byte[] ba, int startIndex, int length)
        {
            byte[] na = new byte[length];
            Buffer.BlockCopy(ba, startIndex, na, 0, length);
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }


        /// <summary>
        /// Legacy Encryption algorithm used by existing client
        /// </summary>
        /// <param name="message">Message to be sent to client.</param>
        /// <param name="encryptionKey">Character used as encryption key. NULL if no encryption.</param>
        /// <returns>Byte array of encrypted data to be sent to client.</returns>
        public static byte[] Encrypt(byte[] originalData, bool useEncryption)
        {
            if (originalData.Length > short.MaxValue) return null; // max short size
            int size = originalData.Length;
            if (size + 3 > Globals.MaximumDataBuffer) return null; // max buffer size

            char encryptionKey;
            if (useEncryption)
            {
                Random rnd = new Random();
                encryptionKey = (char)rnd.Next(1, 255);
            }
            else encryptionKey = (char)0;

            byte[] data = new byte[size + 3];

            data[0] = (byte)encryptionKey;
            Buffer.BlockCopy(BitConverter.GetBytes(size), 0, data, 1, 2); // data[1] and data[2]
            Buffer.BlockCopy(originalData, 0, data, 3, size);
            if (encryptionKey != 0)
                for (int i = 0; i < size; i++)
                {
                    data[3 + i] += (byte)(i ^ encryptionKey);
                    data[3 + i] = (byte)(data[3 + i] ^ (encryptionKey ^ (size - i)));
                }

            return data;
        }

        /// <summary>
        /// Legacy Decryption algorithm used by existing client
        /// </summary>
        /// <param name="data">Data received from client.</param>
        /// <returns>Byte array of decrypted data received from client.</returns>
        public static byte[] Decrypt(byte[] data)
        {
            char key = (char)data[0];
            int size = BitConverter.ToInt16(data, 1);
            size -= 3;

            if (size > Globals.MaximumDataBuffer) size = Globals.MaximumDataBuffer; // max buffer

            if (key != 0)
                for (int i = 0; i < size; i++)
                {
                    data[3 + i] = (byte)(data[3 + i] ^ (key ^ ((size) - i)));
                    data[3 + i] -= (byte)(i ^ key);
                }

            byte[] ret = new byte[size];
            Buffer.BlockCopy(data, 3, ret, 0, size);
            return ret;
        }

        public static byte[] GetCommandTypeBytes(CommandType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        public static byte[] GetCommandTypeBytes(CommandMessageType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        public static byte[] GetCommandTypeBytes(MotionType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        public static Image GetImage(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            Bitmap temp = new Bitmap(stream);
            Bitmap bitmap = new Bitmap(temp);
            temp.Dispose();
            temp = null;
            stream.Close();
            stream = null;

            return (Image)bitmap;
        }

        public static Image GetImageFrame(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            Bitmap temp = new Bitmap(stream);
            Bitmap bitmap = new Bitmap(temp);
            temp.Dispose();
            temp = null;
            stream.Close();
            stream = null;

            return (Image)bitmap;
        }

        public static string GetText(byte[] data)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetString(data);
        }
    }

    public static class Extensions
    {

        public static byte[] GetBytes(this int value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this short value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this bool value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this long value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this string value, int maxWidth)
        {
            if (string.IsNullOrEmpty(value)) value = string.Empty;

            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetBytes(value.PadRight(maxWidth, '\0'));
        }

        public static string Concatenate(this string[] value, int start, int count)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = start; i < start+count; i++)
                if (i < value.Length)
                    builder.Append(value[i]);

            return builder.ToString();

        }
    }
}
