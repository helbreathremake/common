﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common
{
    public class Dice
    {
        private int dice;
        private int faces;
        private int additional;
        private static Random r = new Random((int)DateTime.Now.Ticks);

        public Dice(int dice, int faces, int additional)
        {
            this.dice = dice;
            this.faces = faces;
            this.additional = additional;
        }

        /// <summary>
        /// Number of die.
        /// </summary>
        public Int32 Die
        {
            get { return dice; }
        }

        /// <summary>
        /// Number of faces per dice.
        /// </summary>
        public Int32 Faces
        {
            get { return faces; }
        }

        /// <summary>
        /// Additional number to add after all die are rolled.
        /// </summary>
        public Int32 Bonus
        {
            get{ return additional;}
        }

        /// <summary>
        /// Rolls the die(ce).
        /// </summary>
        /// <returns></returns>
        public int Roll()
        {
            int value = 0;

            for (int i = 0; i < dice; i++) value += r.Next(faces + 1); // upper bound is exclusive

            return Math.Max(value + additional, dice);
        }

        /// <summary>
        /// Rolls the die(ce).
        /// </summary>
        /// <param name="dice">Number of die to roll.</param>
        /// <param name="faces">Number of faces per die.</param>
        /// <param name="additional">Bonuses added after all rolls are done.</param>
        /// <returns>Result of the rolls.</returns>
        public static int Roll(int dice, int faces, int additional)
        {
            int value = 0;

            for (int i = 0; i < dice; i++) value += r.Next(faces + 1); // upper bound is exclusive

            return Math.Max(value + additional, dice);
        }

        /// <summary>
        /// Rolls the die(ce).
        /// </summary>
        /// <param name="dice">Number of die to roll.</param>
        /// <param name="faces">Number of faces per die.</param>
        /// <returns>Result of the rolls.</returns>
        public static int Roll(int dice, int faces)
        {
            int value = 0;

            for (int i = 0; i < dice; i++) value += r.Next(faces+1); // upper bound is exclusive

            return Math.Max(value, dice); // shouldnt be lower than Dice*0?
        }
    }
}
