﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public class Trade
    {
        public int PrimaryPlayerId;
        public int SecondaryPlayerId;
        public List<Item> PrimaryItemList;
        public List<Item> SecondaryItemList;
        public bool PrimaryPlayerAccepted;
        public bool SecondaryPlayerAccepted;

        public Trade(int player1Id, int player2Id)
        {
            this.PrimaryPlayerId = player1Id;
            this.SecondaryPlayerId = player2Id;
        }
    }
}
