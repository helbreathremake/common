﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets.Objects.Dynamic;

namespace Helbreath.Common.Assets.Objects
{
    public interface IOwner
    {
        int Id { get; set; }
        int ObjectId { get; }
        int Type { get; }
        OwnerType OwnerType { get; }
        Dictionary<MagicType, MagicEffect> MagicEffects { get; }
        Dictionary<ConditionGroup, MagicEffect> Conditions { get; }
        OwnerSize Size { get; }
        OwnerSide Side { get; }
        OwnerSideStatus SideStatus { get; }
        MotionDirection Direction { get; }
        Map CurrentMap { get; }
        MapTile CurrentLocation { get; }
        int Appearance1 { get; }
        int Appearance2 { get; }
        int Appearance3 { get; }
        int Appearance4 { get; }
        int AppearanceColour { get; }
        Item Helmet { get; }
        Item BodyArmour { get; }
        Item Hauberk { get; }
        Item Leggings { get; }
        Item Boots { get; }
        Item Cape { get; }
        Item Weapon { get; }
        Item Shield { get; }
        Item Angel { get; }
        int Status { get; }
        int X { get; }
        int Y { get; }
        bool IsCombatMode { get; }
        bool IsDead { get; }
        bool IsRemoved { get; }
        bool IsInvisible { get; }
        string Name { get; }
        int HP { get; }
        int MP { get; }
        int MaxHP { get; }
        int MaxMP { get; }
        List<int> Summons { get; set; }
        int StunChance { get; }
        int Level { get; }
        int Strength { get; }
        int Dexterity { get; }
        int Intelligence { get; }
        int Magic { get; }
        int Vitality { get; }

        void Die(IOwner killer, int damage, DamageType type, int hitCount, bool dropLoot);
        void DepleteMP(int points, bool notify);
        void DepleteHP(int points);
        void DepleteSP(int points, bool notify);
        void DepleteHunger(int points, bool notify);
        void ReplenishMP(int points, bool notify);
        void ReplenishHP(int points, bool notify);
        void ReplenishSP(int points, bool notify);
        void ReplenishHunger(int points, bool notify);
        bool SetMagicEffect(Magic magic, bool firm = false);
        bool RemoveMagicEffect(MagicType type);
        int TakeDamage(DamageType type, int damage, MotionDirection flyDirection);
        int TakeDamage(DamageType type, int damage, MotionDirection flyDirection, bool notify);
        int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify = true);
        int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify, int hitCount, int totalDamage);
        void TakeArmourDamage(int damage);
        bool PrepareMagic(int sourceX, int sourceY, MotionDirection direction);
        //void Attack(int destinationX, int destinationY, IOwner target, int attackType, bool isDash); - TODO move attack code out of World
        bool Attack(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, IOwner target, AttackType attackType, bool isDash);
        bool MagicAttack(int hitX, int hitY, IOwner target, Magic spell, MagicTarget magicTarget);
        bool Idle(int sourceX, int sourceY, MotionDirection direction);
        bool Run(int sourceX, int sourceY, MotionDirection direction);
        bool Move(int sourceX, int sourceY, MotionDirection direction);
        bool Fly(int sourceX, int sourceY, MotionDirection direction);
        bool Init(Map targetMap, int targetX, int targetY);
        bool Remove();
        bool EvadeMelee(int attackerHitChance);
        bool EvadeMagic(int attackerHitChance, bool ignorePFM);
        bool EvadeIce();
        bool EvadePoison();
        bool AbsorbMagic(MagicAttribute element, ref int damage, int pierceValue);
        bool AbsorbMelee(ref int damage, EquipType hitLocation, int armourDamage, bool stripAttempt, int pierceValue);

        bool IsWithinRange(IOwner other, int rangeX, int rangeY);
        bool IsWithinRange(IOwner other, int rangeX, int rangeY, int modifier);
        bool IsWithinRange(IDynamicObject other, int rangeX, int rangeY, int modifier);
        bool IsWithinRange(Location location, int range);
        bool IsValidTarget(IOwner owner);
        bool IsValidTarget(IOwner owner, int rangeX, int rangeY);
        bool IsAlly(IOwner owner);
        byte[] GetFullObjectData(Character requester);
        void Talk(string message);
    }
}
