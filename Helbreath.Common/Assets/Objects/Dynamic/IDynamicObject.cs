﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    public interface IDynamicObject
    {
        int ID { get; }
        DynamicObjectType Type { get; }
        Map CurrentMap { get; }
        MapTile CurrentLocation { get; }
        int X { get; }
        int Y { get; }
        bool IsTraversable { get; }

        void Init(Map targetMap, int targetX, int targetY);
        void TimerProcess();
        void Remove();
    }
}
