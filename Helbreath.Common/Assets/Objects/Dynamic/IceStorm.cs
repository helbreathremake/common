﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    class IceStorm : IDynamicObject
    {
        public event DynamicObjectHandler IceStormRemoved;

        private IOwner owner;
        private int id;
        private int x, y;
        private Item item;
        private int difficulty;

        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private int freezeChance;
        private Dice dice;

        public IceStorm(int id)
        {
            this.id = id;
        }

        public IceStorm(int id, IOwner owner, Map map, int mapX, int mapY, int freezeChance, TimeSpan lastTime, DynamicObjectHandler IceStormRemoved, Dice dice)
        {
            this.id = id;
            this.owner = owner;
            this.map = map;
            this.mapX = mapX;
            this.mapY = mapY;
            this.freezeChance = freezeChance;
            this.lastTime = lastTime;
            this.IceStormRemoved += IceStormRemoved;
            this.dice = dice;
            creationTime = DateTime.Now;
            map[mapY][mapX].SetDynamicObject(this);
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.IceStorm; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }
        public int FreezeChance { get { return freezeChance; } set { freezeChance = value; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);
            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();

            // TODO - hardcoded range
            for (int y = mapY - 2; y <= mapY + 2; y++)
            {
                for (int x = mapX - 2; x <= mapX + 2; x++)
                {
                    if (map[y][x].IsOccupied)
                    {
                        // do some environmental damage
                        bool tryIce = false;
                        IOwner target = map[y][x].Owner;
                        int damage = dice.Roll();

                        //Owner a player
                        if (owner.OwnerType == OwnerType.Player)
                        {
                            Character iceStormOwner = (Character)owner;
                            tryIce = iceStormOwner.EnvironmentalAttack(target, dice);
                        }
                        else //Owner null or npc
                        {
                            target.TakeDamage(owner, DamageType.Environment, damage, MotionDirection.None, true);
                            tryIce = true;
                        }

                        //if (!target.EvadeMagic(500, false)) // static. maybe make changable in configs?
                        //WaterMA of target decides chance
                        if (tryIce && !target.EvadeIce())
                            target.SetMagicEffect(new Magic(MagicType.Ice, 20));
                    }
                }
            }
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (IceStormRemoved != null) IceStormRemoved(this);
        }
    }
}
