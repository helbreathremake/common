﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    public class Flag : IDynamicObject
    {
        public event DynamicObjectHandler FlagRemoved;

        private int id;
        private OwnerSide side;

        private int mapX;
        private int mapY;
        private Map map;

        public Flag(int id)
        {
            this.id = id;
        }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);
        }

        public void TimerProcess()
        {
            return;
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (FlagRemoved != null) FlagRemoved(this);
        }

        public int ID
        {
            get { return id; }
        }

        public DynamicObjectType Type
        {
            get
            {
                switch (side)
                {
                    default:
                    case OwnerSide.Aresden: return DynamicObjectType.AresdenFlag;
                    case OwnerSide.Elvine: return DynamicObjectType.ElvineFlag;
                }
            }
        }

        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }

        public OwnerSide Side
        {
            get { return side; }
            set { side = value; }
        }

        public bool IsWithinRange(IOwner other, int range)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - range) &&
                (mapX <= other.X + range) &&
                (mapY >= other.Y - range) &&
                (mapY <= other.Y + range))
                return true;
            else return false;
        }

        public bool IsWithinRange(IDynamicObject other, int range)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - range) &&
                (mapX <= other.X + range) &&
                (mapY >= other.Y - range) &&
                (mapY <= other.Y + range))
                return true;
            else return false;
        }
    }
}
