﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common;    

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    public class Fish : IDynamicObject
    {
        public event DynamicObjectHandler FishRemoved;

        private int id;
        private int x, y;
        private Item item;
        private int difficulty;
        private FishType type;

        private int mapX;
        private int mapY;
        private Map map;

        public Fish(int id)
        {
            this.id = id;
        }

        public int ID
        {
            get { return id; }
        }

        public DynamicObjectType Type
        {
            get
            {
                switch (type)
                {
                    default:
                    case FishType.Fish: return DynamicObjectType.Fish;
                    case FishType.Item: return DynamicObjectType.FishObject;
                }
            }
        }

        public FishType FishType
        {
            get { return type; }
        }

        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return false; } }

        public Item FishItem
        {
            get { return item; }
        }

        public int Difficulty
        {
            get { return difficulty; }
        }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);
            GenerateFish();
        }

        public void TimerProcess()
        {
            return;
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (FishRemoved != null) FishRemoved(this);
        }

        private void GenerateFish()
        {
            type = FishType.Fish; // default type
            switch (Dice.Roll(1, 9))
            {
                case 1:
                    difficulty = Dice.Roll(1, 10, 5);
                    item = World.ItemConfiguration[323].Copy(); // red carp
                    break;
                case 2:
                    difficulty = Dice.Roll(1, 5, 15);
                    item = World.ItemConfiguration[324].Copy(); // green carp
                    break;
                case 3:
                    difficulty = Dice.Roll(1, 10, 20);
                    item = World.ItemConfiguration[325].Copy(); // gold carp
                    break;
                case 4:
                    difficulty = 1;
                    item = World.ItemConfiguration[326].Copy(); // crucian carp
                    break;
                case 5:
                    difficulty = Dice.Roll(1, 15, 1);
                    item = World.ItemConfiguration[327].Copy(); // blue sea bream
                    break;
                case 6:
                    difficulty = Dice.Roll(1, 18, 1);
                    item = World.ItemConfiguration[328].Copy(); // salmon
                    break;
                case 7:
                    difficulty = Dice.Roll(1, 12, 1);
                    item = World.ItemConfiguration[329].Copy(); // red sea bream
                    break;
                case 8:
                    difficulty = Dice.Roll(1, 10, 1);
                    item = World.ItemConfiguration[330].Copy(); // gray mullet
                    break;
                case 9:
                    type = FishType.Item; // ensures that bubbles are shown by setting this fish's Type to DynamicObject.FishObject
                    switch (Dice.Roll(1, 150))
                    {
                        case 1:
                        case 2:
                        case 3:
                            difficulty = Dice.Roll(4, 4, 20);
                            item = World.ItemConfiguration[224].Copy(); // power green
                            break;
                        case 10:
                        case 11:
                            difficulty = Dice.Roll(4, 4, 20);
                            item = World.ItemConfiguration[225].Copy(); // super power green
                            break;
                        case 20:
                            difficulty = Dice.Roll(4, 4, 5);
                            item = World.ItemConfiguration[4].Copy(); // dagger+1 TODO should be +2 but I dont have the item config!
                            break;
                        case 30:
                            difficulty = Dice.Roll(4, 4, 10);
                            item = World.ItemConfiguration[17].Copy(); // long sword +2
                            break;
                        case 40:
                            difficulty = Dice.Roll(4, 4, 15);
                            item = World.ItemConfiguration[24].Copy(); // scimitar +2
                            break;
                        case 50:
                            difficulty = Dice.Roll(4, 4, 35);
                            item = World.ItemConfiguration[33].Copy(); // rapier +2
                            break;
                        case 60:
                            difficulty = Dice.Roll(4, 4, 40);
                            item = World.ItemConfiguration[53].Copy(); //flamberge +2
                            break;
                        case 70:
                            difficulty = Dice.Roll(4, 4, 30);
                            item = World.ItemConfiguration[69].Copy(); // waraxe +2
                            break;
                        case 90:
                            difficulty = Dice.Roll(4, 4, 30);
                            item = World.ItemConfiguration[195].Copy(); // ruby
                            break;
                        case 96:
                            difficulty = Dice.Roll(4, 4, 30);
                            item = World.ItemConfiguration[194].Copy(); // diamond
                            break;
                        default:
                            difficulty = Dice.Roll(1, 10);
                            item = World.ItemConfiguration[257].Copy(); // long boots
                            break;
                    }
                    break;
            }
        }
    }
}
