﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    class Fire : IDynamicObject
    {
        public event DynamicObjectHandler FireRemoved;

        private IOwner owner;

        private int id;
        private int x, y;
        private Item item;
        private int difficulty;
        private DynamicObjectType type;
        private int burn;

        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private Dice dice;

        public Fire(int id)
        {
            this.id = id;
        }

        public Fire(int id, IOwner owner, Map map, int mapX, int mapY, DynamicObjectType type, int burn, TimeSpan lastTime, DynamicObjectHandler FireRemoved, Dice dice)
        {
            this.id = id;
            this.owner = owner;
            this.map = map;
            this.mapX = mapX;
            this.mapY = mapY;
            this.type = type;
            this.burn = burn;
            this.lastTime = lastTime;
            this.FireRemoved += FireRemoved;
            this.dice = dice;
            creationTime = DateTime.Now;
            map[mapY][mapX].SetDynamicObject(this);
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return type; } set { type = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);
            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();

            if (CurrentLocation.IsOccupied)
            {
                // do some environmental damage
                bool tryFire = false;
                IOwner target = CurrentLocation.Owner;
                int damage = dice.Roll();

                //Owner a player
                if (owner.OwnerType == OwnerType.Player)
                {
                    Character fireOwner = (Character)owner;
                    tryFire = fireOwner.EnvironmentalAttack(target, dice);
                }
                else //Owner of spikes is null or npc
                {
                    target.TakeDamage(owner, DamageType.Environment, damage, MotionDirection.None, true);
                    tryFire = true;
                }

                //if (tryFire && !target.EvadeMagic(500, false)) // static. maybe make changable in configs?
                //TODO CreateBurn! 
                //FireMA to decide chance?
            }                                                           
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (FireRemoved != null) FireRemoved(this);
        }
    }
}
