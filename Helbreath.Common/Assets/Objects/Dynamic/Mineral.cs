﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    public class Mineral : IDynamicObject
    {
        public event DynamicObjectHandler MineralRemoved;

        private int id;
        private int x, y;
        private int remainingMaterials;
        private MineralType type;

        private int mapX;
        private int mapY;
        private Map map;

        public Mineral(int id, MineralType type)
        {
            this.id = id;
            this.type = type;
        }

        /// <summary>
        /// Initilizes this Mineral on the map.
        /// </summary>
        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;

            int x, y;

            if (targetMap.GetEmptyTile(targetX, targetY, out x, out y))
            {
                this.mapX = x;
                this.mapY = y;
                targetMap[y][x].SetDynamicObject(this);
            }

            this.remainingMaterials = TotalMaterials;
        }

        public void TimerProcess()
        {
            return;
        }

        /// <summary>
        /// Removes this Mineral from the map
        /// </summary>
        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (MineralRemoved != null) MineralRemoved(this);
        }

        public Item Mine()
        {
            Item mineral = null;
            switch (type)
            {
                case MineralType.RockEasy:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                        case 2:
                        case 3: mineral = World.ItemConfiguration[199].Copy(); break; // coal
                        case 4: mineral = World.ItemConfiguration[200].Copy(); break; // ironore
                        case 5: mineral = World.ItemConfiguration[293].Copy(); break; // bronze
                    }
                    break;
                case MineralType.RockMedium:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                        case 2:
                        case 3: mineral = World.ItemConfiguration[199].Copy(); break; // coal
                        case 4: mineral = World.ItemConfiguration[200].Copy(); break; // ironore
                        case 5:
                            switch (Dice.Roll(1, 3))
                            {
                                case 2: mineral = World.ItemConfiguration[290].Copy(); break; // silver
                                default: mineral = World.ItemConfiguration[289].Copy(); break; // gold
                            }
                            break;
                    }
                    break;
                case MineralType.RockHard:
                    switch (Dice.Roll(1, 6))
                    {
                        case 1: mineral = World.ItemConfiguration[199].Copy(); break; // coal
                        case 2:
                        case 3:
                        case 4:
                        case 5: mineral = World.ItemConfiguration[200].Copy(); break; //ironore
                        case 6:
                            switch (Dice.Roll(1, 8))
                            {
                                case 3:
                                    switch (Dice.Roll(1, 2))
                                    {
                                        case 1: mineral = World.ItemConfiguration[290].Copy(); break; // silver
                                        default: mineral = World.ItemConfiguration[289].Copy(); break; // gold  was IronOre - bug in old code?
                                    }
                                    break;
                                default: mineral = World.ItemConfiguration[200].Copy(); break; // ironore
                            }
                            break;
                    }
                    break;
                case MineralType.RockExtreme:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                        case 2: mineral = World.ItemConfiguration[199].Copy(); break; // coal
                        case 3:
                        case 4: mineral = World.ItemConfiguration[200].Copy(); break; // ironore
                        case 5:
                            switch (Dice.Roll(1, 5))
                            {
                                case 1:
                                case 2: mineral = World.ItemConfiguration[289].Copy(); break; // gold
                                case 3:
                                case 4: mineral = World.ItemConfiguration[290].Copy(); break; // silver
                                case 5: mineral = World.ItemConfiguration[294].Copy(); break; //mithril
                            }
                            break;
                    }
                    break;
                case MineralType.GemEasy:
                    switch (Dice.Roll(1, 18))
                    {
                        case 3: mineral = World.ItemConfiguration[196].Copy(); break;  // sapphire
                        default: mineral = World.ItemConfiguration[202].Copy(); break; // crystal
                    }
                    break;
                case MineralType.GemHard:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                            switch (Dice.Roll(1, 6))
                            {
                                case 3: mineral = World.ItemConfiguration[197].Copy(); break; // emerald
                                default: mineral = World.ItemConfiguration[202].Copy(); break; // crystal
                            }
                            break;
                        case 2:
                            switch (Dice.Roll(1, 6))
                            {
                                case 3: mineral = World.ItemConfiguration[196].Copy(); break;  // sapphire
                                default: mineral = World.ItemConfiguration[202].Copy(); break; // crystal
                            }
                            break;
                        case 3:
                            switch (Dice.Roll(1, 6))
                            {
                                case 3: mineral = World.ItemConfiguration[195].Copy(); break; // ruby
                                default: mineral = World.ItemConfiguration[202].Copy(); break; // crystal
                            }
                            break;
                        case 4: mineral = World.ItemConfiguration[202].Copy(); break; // crystal
                        case 5:
                            switch (Dice.Roll(1, 12))
                            {
                                case 3: mineral = World.ItemConfiguration[194].Copy(); break; // diamond
                                default: mineral = World.ItemConfiguration[202].Copy(); break; // crystal
                            }
                            break;
                    }
                    break;
            }

            if (mineral != null)
            {
                remainingMaterials--;
                if (remainingMaterials <= 0) Remove();
            }

            return mineral;
        }

        public int ID
        {
            get { return id; }
        }

        public DynamicObjectType Type
        {
            get
            {
                switch (type)
                {
                    default:
                    case MineralType.RockEasy: return DynamicObjectType.Rock;
                    case MineralType.RockMedium: return DynamicObjectType.Rock;
                    case MineralType.RockHard: return DynamicObjectType.Rock;
                    case MineralType.RockExtreme: return DynamicObjectType.Rock;
                    case MineralType.GemEasy: return DynamicObjectType.Gem;
                    case MineralType.GemHard: return DynamicObjectType.Gem;
                }
            }
        }

        public int Difficulty
        {
            get
            {
                switch (type)
                {
                    default:
                    case MineralType.RockEasy: return 10;
                    case MineralType.RockMedium: return 15;
                    case MineralType.RockHard: return 20;
                    case MineralType.RockExtreme: return 50;
                    case MineralType.GemEasy: return 70;
                    case MineralType.GemHard: return 90;
                }
            }
        }

        public int TotalMaterials
        {
            get
            {
                switch (type)
                {
                    default:
                    case MineralType.RockEasy: return 20;
                    case MineralType.RockMedium: return 15;
                    case MineralType.RockHard: return 10;
                    case MineralType.RockExtreme: return 8;
                    case MineralType.GemEasy: return 6;
                    case MineralType.GemHard: return 4;
                }
            }
        }

        public int RemainingMaterials
        {
            get { return remainingMaterials; }
            set { remainingMaterials = value; }
        }

        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return false; } }
    }
}
