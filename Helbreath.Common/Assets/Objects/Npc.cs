﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath.Common;
using Helbreath.Common.Assets.Objects.Dynamic;

namespace Helbreath.Common.Assets.Objects
{
    public class Npc : IOwner, IDeadOwner
    {
        public event OwnerHandler StatusChanged;
        public event MotionHandler MotionChanged;
        public event DamageHandler DamageTaken;
        public event VitalsChangedHandler VitalsChanged;
        public event DeathHandler Killed;
        public event ItemHandler ItemDropped;
        public event LogHandler MessageLogged;

        private Dictionary<MagicType, MagicEffect> magicEffects;
        private Dictionary<ConditionGroup, MagicEffect> conditions;
        private int objectId;
        private int spawnId;

        private string name;
        private string friendlyName;
        private string sprite;
        private int spriteColour;
        private int type;
        private MotionDirection direction;
        private MotionTurn turn; // which direction does this npc turn when hitting an obstacle during path finding
        private MotionType action;
        private MovementType moveType;
        private int appearance1;
        private int appearance2;
        private int appearance3;
        private int appearance4;
        private int appearanceColour;
        private int status;
        private bool canDropLoot; // flag for secondary drops
        private GenderType killersGender = GenderType.None;
        private bool isCombatMode;
        private bool isDead;
        private bool isRemoved;
        private bool isFading;
        private bool isCorpseExploited;
        private bool isFriendly; // for wh/ch/bs guys
        private bool isPacifist; // for non-attackers
        private DateTime deadTime;
        private OwnerSize size;
        private OwnerSide side;
        private Guild guild;
        private MonsterDifficulty monsterDifficulty;
        private int dropCount;
        private int dropX;
        private int dropY;

        private int level;
        private int strength;
        private int dexterity;
        private int vitality;
        private int magic;
        private int intelligence;
        private string[] spells; // general npc spells
        private List<NpcPerk> perks;

        private int hp;
        private int mp;
        private int maxHP;
        private int maxMP;
        private int maxGold;
        private Dice experience;
        private int remainingExperience;
        private int pa; // natural physical absorption
        private int ma; // natural magical absorption
        private MagicAttribute absorptionType; // natural magical absorption type
        private int attackRange;
        private int searchRange; // target search

        private int mapX;
        private int mapY;
        private Location waypoint;
        private Map map;

        private Dice baseDamage;

        private List<int> summons;
        private int summoner;
        private IOwner target;
        private IOwner objective;

        private DateTime lastDamagedTime;
        private DateTime hpUpTime;    // hp regen
        private DateTime mpUpTime;    // mp regen
        private DateTime poisonTime;  // poison time
        private DateTime lastActionTime; // last action
        private DateTime summonTime; // time summoned
        private TimeSpan maximumDeadTime; // time before disappear
        private TimeSpan actionTime; // time between each action

        private int buildType; // TODO rename these maybe? bit unfriendly (properties also)
        private int buildPoints; // crops/sade structures etc
        private int buildLimit; // limit for crop yeild probability

        // Merchant data
        private int merchantId;
        private MerchantType merchantType;

        // General AI
        private int actionCount;
        private int failedActionCount; // number of times the last action failed

        // Human AI
        private bool isHuman;
        private bool isRunning;
        private List<Item> equipment;
        private GenderType gender;
        private SkinType skin;
        private int hairStyle;
        private int hairColour;
        private int underwearColour;
        private AIActionCategory aiCategory;
        private Dictionary<AIActionCategory, int> aiSpells;
        private AIBehaviour aiBehaviour;
        private AIClass aiClass;
        private AIEquipment aiEquipment;
        private AIState aiState;
        private AIDifficulty aiDifficulty;
        private int castedSpell;
        private DateTime castTime;

        public Npc()
        {
            this.isHuman = false;
            this.name = "Unknown";
            this.perks = new List<NpcPerk>();
            Init();
        }

        public Npc(string name)
        {
            this.isHuman = false;
            this.name = name;
            this.perks = new List<NpcPerk>();
            Init();
        }

        public Npc Copy()
        {
            Npc npc = new Npc(this.name);
            npc.Id = Id;
            npc.FriendlyName = FriendlyName;
            npc.Sprite = Sprite;
            npc.SpriteColour = SpriteColour;
            npc.Type = Type;
            npc.Size = Size;
            npc.Side = Side;
            npc.BaseDamage = BaseDamage;
            npc.AttackRange = AttackRange;
            npc.SearchRange = SearchRange;
            npc.Dexterity = Dexterity;
            npc.Vitality = Vitality;
            npc.Magic = Magic;
            npc.Spells = Spells;
            npc.Experience = Experience;
            npc.RemainingExperience = RemainingExperience;
            npc.HP = MaxHP;
            npc.MP = MaxMP;
            npc.PhysicalAbsorption = PhysicalAbsorption;
            npc.MagicAbsorption = MagicAbsorption;
            npc.MaximumDeadTime = MaximumDeadTime;
            npc.ActionTime = ActionTime.Add(new TimeSpan(0, 0, 0, 0, Dice.Roll(1, 300))); // // randomize action time so they dont look robotic!
            npc.MaximumGold = MaximumGold;
            npc.CurrentAction = CurrentAction;
            npc.MoveType = MoveType;
            npc.Behaviour = Behaviour;
            npc.Class = Class;
            npc.State = State;
            npc.Difficulty = Difficulty;
            npc.Equipment = Equipment;
            npc.IsHuman = IsHuman;
            npc.MerchantId = MerchantId;
            npc.MerchantType = MerchantType;
            npc.Level = Level;
            npc.MonsterDifficulty = MonsterDifficulty;
            npc.DropCount = dropCount;

            return npc;
        }

        public static Npc ParseXml(XmlReader r)
        {
            Npc npc = new Npc(r["Name"]);
            npc.Id = (r["Id"] != null) ? Int32.Parse(r["Id"]) : 0;
            npc.FriendlyName = (r["FriendlyName"] != null) ? r["FriendlyName"] : npc.Name;
            npc.Sprite = (r["Sprite"] != null) ? r["Sprite"] : "";
            npc.SpriteColour = (r["Colour"] != null) ? Int32.Parse(r["Colour"]) : 0;
            npc.Type = (r["Type"] != null) ? Int32.Parse(r["Type"]) : 0;

            MonsterDifficulty monsterDifficulty;
            if (r["MobDifficulty"] != null && Enum.TryParse<MonsterDifficulty>(r["MobDifficulty"], out monsterDifficulty))
                npc.MonsterDifficulty = monsterDifficulty;
            else npc.MonsterDifficulty = MonsterDifficulty.Normal;

            OwnerSize ownerSize;
            if (r["Size"] != null && Enum.TryParse<OwnerSize>(r["Size"], out ownerSize))
                npc.Size = ownerSize;
            else npc.Size = OwnerSize.Small;

            OwnerSide ownerSide;
            if (r["Side"] != null && Enum.TryParse<OwnerSide>(r["Side"], out ownerSide))
                npc.Side = ownerSide;
            else npc.Side = OwnerSide.Wild;

            npc.AttackRange = (r["AttackRange"] != null) ? Int32.Parse(r["AttackRange"]) : 1;
            npc.SearchRange = (r["SearchRange"] != null) ? Int32.Parse(r["SearchRange"]) : 5;
            npc.BaseDamage = (r["BaseDamageThrow"] != null) ? new Dice(Int32.Parse(r["BaseDamageThrow"]), Int32.Parse(r["BaseDamageRange"]), 0) : new Dice(1, 10, 0);
            npc.Strength = (r["Strength"] != null) ? Int32.Parse(r["Strength"]) : 0;
            npc.Dexterity = (r["Dexterity"] != null) ? Int32.Parse(r["Dexterity"]) : 0;
            npc.Vitality = (r["Vitality"] != null) ? Int32.Parse(r["Vitality"]) : 0;
            npc.Magic = (r["Magic"] != null) ? Int32.Parse(r["Magic"]) : 0;
            npc.Spells = (r["Spells"] != null) ? r["Spells"].Split(',') : new string[0];
            npc.Experience = (r["BaseExperienceThrow"] != null) ? (new Dice(Int32.Parse(r["BaseExperienceThrow"]), 4, Int32.Parse(r["BaseExperienceThrow"]))) : (new Dice(1, 10, 0));
            npc.MaximumDeadTime = (r["MaximumDeadTime"] != null) ? TimeSpan.Parse(r["MaximumDeadTime"]) : new TimeSpan(0, 0, 0, Globals.MobSpawnRate);
            npc.ActionTime = (r["ActionTime"] != null) ? TimeSpan.Parse(r["ActionTime"]) : new TimeSpan(0, 0, 0, 1);
            npc.PhysicalAbsorption = (r["PhysicalAbsorption"] != null) ? Int32.Parse(r["PhysicalAbsorption"]) : 0;
            npc.MagicAbsorption = (r["MagicAbsorption"] != null) ? Int32.Parse(r["MagicAbsorption"]) : 0;
            npc.MaximumGold = (r["MaximumGold"] != null) ? Int32.Parse(r["MaximumGold"]) : 0;
            npc.IsHuman = (r["IsHuman"] != null && Boolean.Parse(r["IsHuman"]));

            if (r["IsStationary"] != null && Boolean.Parse(r["IsStationary"].ToString()))
            {
                npc.CurrentAction = MotionType.Idle;
                npc.MoveType = MovementType.None;
            }

            if (r["IsStationaryAttack"] != null && Boolean.Parse(r["IsStationaryAttack"].ToString()))
            {
                npc.CurrentAction = MotionType.AttackStationary;
                npc.MoveType = MovementType.None;
            }

            AIBehaviour behaviour;
            if (r["AIBehaviour"] != null && Enum.TryParse<AIBehaviour>(r["AIBehaviour"], out behaviour))
                npc.Behaviour = behaviour;

            AIClass aiClass;
            if (r["AIClass"] != null && Enum.TryParse<AIClass>(r["AIClass"], out aiClass))
                npc.Class = aiClass;

            AIDifficulty difficulty;
            if (r["AIDifficulty"] != null && Enum.TryParse<AIDifficulty>(r["AIDifficulty"], out difficulty))
                npc.Difficulty = difficulty;

            AIEquipment equipment;
            if (r["AIEquipment"] != null && Enum.TryParse<AIEquipment>(r["AIEquipment"], out equipment))
                npc.Equipment = equipment;

            if (r["MerchantId"] != null)
                npc.MerchantId = Int32.Parse(r["MerchantId"]);
            MerchantType merchantType;
            if (r["MerchantType"] != null && Enum.TryParse<MerchantType>(r["MerchantType"], out merchantType))
                npc.MerchantType = merchantType;
            else npc.MerchantType = MerchantType.None;

            if (r["Level"] != null) npc.Level = Int32.Parse(r["Level"]);
            else npc.Level = 1;

            if (r["DropCount"] != null) npc.dropCount = Int32.Parse(r["DropCount"]);
            else npc.dropCount = 1;

            return npc;
        }

        private void Init()
        {
            this.direction = MotionDirection.South;
            this.action = MotionType.Undefined;
            this.moveType = MovementType.Undefined;
            this.isDead = false;
            this.isFading = false;
            this.isCorpseExploited = false;
            this.waypoint = null;
            this.spawnId = -1;
            this.guild = Guild.None; // no guild by default
            this.summoner = -1;
            this.summons = new List<int>(Globals.MaximumSummons);
            this.turn = Dice.Roll(1, 100) < 50 ? MotionTurn.Right : MotionTurn.Left;
        }

        /// <summary>
        /// Initilizes this Npc on the map.
        /// </summary>
        public bool Init(Map targetMap, int targetX, int targetY) //TODO check boss tilelocations?
        {
            this.map = targetMap;
            magicEffects = new Dictionary<MagicType, MagicEffect>();
            conditions = new Dictionary<ConditionGroup, MagicEffect>();

            int x, y;

            switch (MonsterDifficulty)
            {
                case MonsterDifficulty.LargeBoss:
                //{
                //    if (targetMap.GetEmptyTiles(targetX, targetY, out x, out y)) //TODO set all 9 spaces as this owner?
                //    {
                //        this.mapX = x;
                //        this.mapY = y;

                //        for (int yi = y - 1; yi <= y + 1; yi++)
                //        {
                //            for (int xi = x - 1; xi <= x + 1; xi++)
                //            {
                //                targetMap[yi][xi].SetOwner(this);
                //            }
                //        }

                //        targetMap.Npcs.Add(objectId);
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //    break;
                //}
                case MonsterDifficulty.None:
                case MonsterDifficulty.Boss:
                case MonsterDifficulty.Normal:
                    {
                        if (targetMap.GetEmptyTile(targetX, targetY, out x, out y))
                        {
                            this.mapX = x;
                            this.mapY = y;
                            targetMap[y][x].SetOwner(this);
                            targetMap.Npcs.Add(objectId);
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    }
            }

            // if no action/movetype defined by configs, then set to Move/randomarea as default
            if (action == MotionType.Undefined)
                if (isHuman) action = MotionType.Idle;
                else action = MotionType.Move;
            if (moveType == MovementType.Undefined) moveType = MovementType.RandomArea;
            lastActionTime = DateTime.Now;

            if (experience != null) remainingExperience = experience.Roll();

            // was 1% chance to get a perk, now 10%, 5%, 2%
            if (side == OwnerSide.Wild)
            {
                for (int i = 0; i < Globals.MaximumNpcPerks; i++)
                {
                    NpcPerk perk = (NpcPerk)Dice.Roll(1, Enum.GetNames(typeof(NpcPerk)).Length - 1); // randomize
                    if (!perks.Contains(perk)) // don't add more than once
                        switch (i)
                        {
                            case 0: if (Dice.Roll(1, 5) == 2) perks.Add(perk); break; // 20% chance for first perk
                            case 1: if (Dice.Roll(1, 10) == 2) perks.Add(perk); break; // 10% chance for second perk
                            case 2:
                            default: if (Dice.Roll(1, 20) == 2) perks.Add(perk); break; // 5% chance for third perk or there after
                        }
                }

                // 2% chance to be berserked (was 1%)
                if (Dice.Roll(1, 50) == 2) SetMagicEffect(World.MagicConfiguration[50], true);
            }
            return true;
        }

        public void GenerateHuman()
        {
            if (!IsHuman) return;

            // randomize equipment if Random is specified
            if (this.aiEquipment == AIEquipment.Random)
                switch (Dice.Roll(1, 4))
                {
                    case 1: this.aiEquipment = AIEquipment.Heavy; break;
                    case 2: this.aiEquipment = AIEquipment.Light; break;
                    case 3: this.aiEquipment = AIEquipment.Medium; break;
                    default: this.aiEquipment = AIEquipment.Civilian; break;
                }

            if (this.aiClass == AIClass.Random)
                switch (Dice.Roll(1, 4))
                {
                    case 1: this.aiClass = AIClass.Archer; break;
                    case 2: this.aiClass = AIClass.BattleMage; break;
                    case 3: this.aiClass = AIClass.Mage; break;
                    default: this.aiClass = AIClass.Warrior; break;
                }

            if (this.aiBehaviour == AIBehaviour.Random)
                switch (Dice.Roll(1, 4))
                {
                    case 1: this.aiBehaviour = AIBehaviour.Berserker; break;
                    case 2: this.aiBehaviour = AIBehaviour.Defender; break;
                    case 3: this.aiBehaviour = AIBehaviour.Healer; break;
                    default: this.aiBehaviour = AIBehaviour.Supporter; break;
                }

            // set up appearance values
            skin = (SkinType)Dice.Roll(1, 3); // 0,1,2
            hairColour = Dice.Roll(1, 15) - 1; //
            hairStyle = Dice.Roll(1, 4) - 1; // 0,1,2,3
            underwearColour = Dice.Roll(1, 4) - 1; // 
            gender = (Dice.Roll(1, 2) == 1) ? GenderType.Male : GenderType.Female;

            // inherited
            Type = (gender == GenderType.Male ? 1 : 4) + ((int)skin - 1);

            Appearance1 = Appearance2 = Appearance3 = Appearance4 = AppearanceColour = 0;
            Appearance1 = Appearance1 | underwearColour;
            Appearance1 = Appearance1 | (hairStyle << 8);
            Appearance1 = Appearance1 | (hairColour << 4);

            GenerateHumanEquipment();
            GenerateHumanStats();
            GenerateHumanAppearance();
        }

        private void GenerateHumanStats()
        {
            aiSpells = new Dictionary<AIActionCategory, int>();

            // TODO difficulties
            switch (aiClass)
            {
                case AIClass.Archer:
                    strength = 170;
                    intelligence = 120;


                    break;
                case AIClass.BattleMage:
                    strength = 150;
                    magic = 150;
                    intelligence = 150;

                    switch (aiBehaviour)
                    {
                        case AIBehaviour.Healer:
                            aiSpells.Add(AIActionCategory.Healing, 21); // great heal

                            switch (Dice.Roll(1, 2))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 58); break; // push
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 59); break; // pull
                            }

                            aiSpells.Add(AIActionCategory.Misc, 94); // resurrection
                            break;
                        case AIBehaviour.Defender:
                            aiSpells.Add(AIActionCategory.Protect, 44); // great defence shield

                            switch (Dice.Roll(1, 2))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 58); break; // push
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 59); break; // pull
                            }

                            aiSpells.Add(AIActionCategory.Misc, 39); // putrid corpse
                            break;
                        case AIBehaviour.Supporter:
                            switch (Dice.Roll(1, 2))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Buff, 68); break; // kinetic armour
                                case 2: aiSpells.Add(AIActionCategory.Buff, 69); break; // barbs
                            }

                            aiSpells.Add(AIActionCategory.Debuff, 67); // turn undead
                            aiSpells.Add(AIActionCategory.Misc, 48); // raise minions
                            break;
                        case AIBehaviour.Berserker:
                            aiSpells.Add(AIActionCategory.Healing, 21); // great heal

                            switch (Dice.Roll(1, 2))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 78); break; // throw
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 79); break; // drag
                            }
                            break;
                    }

                    break;
                case AIClass.Mage:
                    strength = 40;
                    magic = 200;
                    intelligence = 200;

                    switch (aiBehaviour)
                    {
                        case AIBehaviour.Healer:
                            aiSpells.Add(AIActionCategory.Healing, 21); // great heal
                            switch (Dice.Roll(1, 5))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 60); break; // energy strike
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 63); break; // mass chill wind
                                case 3: aiSpells.Add(AIActionCategory.Offensive, 57); break; // ice strike
                                case 4: aiSpells.Add(AIActionCategory.Offensive, 61); break; // mass fire strike
                                case 5: aiSpells.Add(AIActionCategory.Offensive, 64); break; // earth worm strike
                            }

                            aiSpells.Add(AIActionCategory.Misc, 94); // resurrection
                            break;
                        case AIBehaviour.Defender:
                            aiSpells.Add(AIActionCategory.Protect, 33); // pfm

                            switch (Dice.Roll(1, 4))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 70); break; // bloody shock wave
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 72); break; // mass ice strike
                                case 3: aiSpells.Add(AIActionCategory.Offensive, 74); break; // lightning strike
                                case 4: aiSpells.Add(AIActionCategory.Offensive, 66); break; // armour break
                            }
                            break;
                        case AIBehaviour.Supporter:
                            switch (Dice.Roll(1, 2))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Buff, 50); break; // berserk
                                case 2: aiSpells.Add(AIActionCategory.Buff, 49); break; // haste
                            }

                            aiSpells.Add(AIActionCategory.Debuff, 76); // cancellation
                            aiSpells.Add(AIActionCategory.Misc, 31); // summon creature

                            switch (Dice.Roll(1, 4))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 70); break; // bloody shock wave
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 72); break; // mass ice strike
                                case 3: aiSpells.Add(AIActionCategory.Offensive, 74); break; // lightning strike
                                case 4: aiSpells.Add(AIActionCategory.Offensive, 66); break; // armour break
                            }
                            break;
                        case AIBehaviour.Berserker:
                            aiSpells.Add(AIActionCategory.Healing, 21); // great heal

                            switch (Dice.Roll(1, 4))
                            {
                                case 1: aiSpells.Add(AIActionCategory.Offensive, 96); break; // earth shock wave
                                case 2: aiSpells.Add(AIActionCategory.Offensive, 91); break; // blizzard
                                case 3: aiSpells.Add(AIActionCategory.Offensive, 82); break; // mass magic missile
                                case 4: aiSpells.Add(AIActionCategory.Offensive, 81); break; // meteor strike
                            }
                            break;
                    }
                    break;
                case AIClass.Warrior:
                    strength = 200;
                    magic = 50;
                    intelligence = 70;

                    aiSpells.Add(AIActionCategory.Healing, 21); // great heal
                    switch (Dice.Roll(1, 2))
                    {
                        case 1: aiSpells.Add(AIActionCategory.Buff, 50); break; // berserk
                        case 2: aiSpells.Add(AIActionCategory.Buff, 49); break; // haste
                    }
                    aiSpells.Add(AIActionCategory.Protect, 33); // pfm
                    break; // TODO
            }

            HP = MaxHP;
            MP = MaxMP;

            // update values based on generated equipment
            if (equipment[(int)EquipType.DualHand] != null) AttackRange = equipment[(int)EquipType.DualHand].CriticalRange;
            if (equipment[(int)EquipType.DualHand] != null) BaseDamage = equipment[(int)EquipType.DualHand].DamageSmall;
        }

        private void GenerateHumanEquipment()
        {
            equipment = new List<Item>(16);
            for (int i = 0; i < 15; i++) equipment.Add(null);

            // festive fun. civilian AI have a chance for santa suit
            bool isFullSuit = (DateTime.Now.Month == 12 && aiEquipment == AIEquipment.Civilian && Dice.Roll(1, 5) > 4) ? true : false;

            // TODO put these items in to config? hard coded and messy
            switch (aiEquipment)
            {
                case AIEquipment.Civilian:
                    if (isFullSuit)
                    {
                        equipment[(int)EquipType.FullBody] = (gender == GenderType.Male) ? World.ItemConfiguration[459].Copy() : World.ItemConfiguration[460].Copy(); // santa suit M and W
                    }
                    else
                    {
                        equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration[258].Copy() : World.ItemConfiguration[269].Copy(); // shirt M and W
                        equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[282].Copy() : (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration[271].Copy() : World.ItemConfiguration[272].Copy(); // tunic, bodice, long bodice
                        equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration[264].Copy() : World.ItemConfiguration[265].Copy() : (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration[278].Copy() : World.ItemConfiguration[277].Copy(); //trousers M, knee trousers M, trousers W and skirt
                        switch (Dice.Roll(1, 3))
                        {
                            case 1: equipment[(int)EquipType.Feet] = World.ItemConfiguration[257].Copy(); break; // boots
                            case 2: equipment[(int)EquipType.Feet] = World.ItemConfiguration[256].Copy(); break; // shoes
                            default: break;
                        }
                        // randomize colours of clothes  // TODO - hard coded 15 colours, may change with new client
                        // TODO Dyes
                        //equipment[(int)EquipType.Arms].Colour = Dice.Roll(1, 15);
                        //equipment[(int)EquipType.Body].Colour = Dice.Roll(1, 15);
                        //equipment[(int)EquipType.Legs].Colour = Dice.Roll(1, 15);
                        //if (equipment[(int)EquipType.Feet] != null) equipment[(int)EquipType.Feet].Colour = Dice.Roll(1, 15);
                    }

                    // chance to have small shield
                    equipment[(int)EquipType.LeftHand] = (Dice.Roll(1, 3) == 1) ? World.ItemConfiguration[75].Copy() : null; // wood shield

                    break;
                case AIEquipment.Light:
                    equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration[259].Copy() : World.ItemConfiguration[270].Copy(); // hauberk M and W
                    equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration[266].Copy() : World.ItemConfiguration[280].Copy(); // chain house M and W

                    switch (aiClass)
                    {
                        case AIClass.Warrior:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["FullHelm(M)"].Copy() : World.ItemConfiguration["FullHelm(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[262].Copy() : World.ItemConfiguration[275].Copy(); // scale mail M and W
                            equipment[(int)EquipType.DualHand] = World.ItemConfiguration[47].Copy(); // great sword
                            break;
                        case AIClass.Mage:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["Wizard-Hat(M)"].Copy() : World.ItemConfiguration["Wizard-Hat(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[334].Copy() : World.ItemConfiguration[335].Copy(); // robe M and W
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[396].Copy(); // magic wand ms10
                            break;
                        case AIClass.Archer:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["Wizard-Cap(M)"].Copy() : World.ItemConfiguration["Wizard-Cap(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[260].Copy() : World.ItemConfiguration[273].Copy(); // leather armour M and W
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[71].Copy(); // shortbow
                            break;
                        case AIClass.BattleMage:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["Helm(M)"].Copy() : World.ItemConfiguration["Helm(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[261].Copy() : World.ItemConfiguration[274].Copy(); // chain mail M and W
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[564].Copy(); // energy battle staff
                            break;
                    }
                    break;
                case AIEquipment.Medium:
                    equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration[393].Copy() : World.ItemConfiguration[394].Copy(); // knight hauberk M and W
                    equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration[389].Copy() : World.ItemConfiguration[390].Copy(); // knight plate leggings M and W

                    switch (aiClass)
                    {
                        case AIClass.Warrior:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[391].Copy() : World.ItemConfiguration[392].Copy(); // knight full helm M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[387].Copy() : World.ItemConfiguration[388].Copy(); // knight plate mail M and W
                            equipment[(int)EquipType.DualHand] = World.ItemConfiguration[51].Copy(); // flamberge
                            break;
                        case AIClass.Mage:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[450].Copy() : World.ItemConfiguration[454].Copy(); // wizard hat M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[397].Copy() : World.ItemConfiguration[398].Copy(); // knight robe M and W
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[166].Copy(); // magic wand ms20
                            break;
                        case AIClass.Archer:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[449].Copy() : World.ItemConfiguration[453].Copy(); // hood M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[260].Copy() : World.ItemConfiguration[273].Copy(); // leather armour M and W
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[72].Copy(); // longbow
                            break;
                        case AIClass.BattleMage:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[336].Copy() : World.ItemConfiguration[338].Copy(); // battle helm M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[590].Copy() : World.ItemConfiguration[591].Copy(); // battle plate mail M and W

                            switch (Dice.Roll(1, 2))
                            {
                                default:
                                case 1: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[565].Copy(); break; // fire battle staff
                                case 2: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[566].Copy(); break; // ice battle staff
                            }
                            break;
                    }
                    break;
                case AIEquipment.Heavy:
                    equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration[407].Copy() : World.ItemConfiguration[425].Copy(); // dark knight hauberk M and W
                    equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration[409].Copy() : World.ItemConfiguration[427].Copy(); // dark knight leggings M and W

                    switch (aiClass)
                    {
                        case AIClass.Warrior:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[408].Copy() : World.ItemConfiguration[426].Copy(); // dark knight full helm M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[411].Copy() : World.ItemConfiguration[429].Copy(); // dark knight plate mail M and W

                            switch (Dice.Roll(1, 3))
                            {
                                default:
                                case 1: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[345].Copy(); break; // giant sowrd
                                case 2: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[495].Copy(); break; // barbarian hammer
                                case 3: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[445].Copy(); break; // temple sword
                            }

                            break;
                        case AIClass.Mage:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[562].Copy() : World.ItemConfiguration[563].Copy(); // dark mage hat M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[416].Copy() : World.ItemConfiguration[436].Copy(); // dark mage robe M and W

                            switch (Dice.Roll(1, 3))
                            {
                                case 1: equipment[(int)EquipType.RightHand] = World.ItemConfiguration[178].Copy(); break; // magic wand ms30
                                case 2: equipment[(int)EquipType.RightHand] = World.ItemConfiguration[446].Copy(); break; // templar wand
                                case 3: equipment[(int)EquipType.RightHand] = World.ItemConfiguration[512].Copy(); break; // berserk wand ms10
                            }
                            break;
                        case AIClass.Archer:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[449].Copy() : World.ItemConfiguration[453].Copy(); // hood M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[260].Copy() : World.ItemConfiguration[273].Copy(); // leather armour M and W
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[72].Copy(); // longbow
                            break;
                        case AIClass.BattleMage:
                            equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[336].Copy() : World.ItemConfiguration[338].Copy(); // battle helm M and W
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[590].Copy() : World.ItemConfiguration[591].Copy(); // battle plate mail M and W

                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration[568].Copy(); // meteor battle staff
                            break;
                    }
                    break;
                case AIEquipment.Hero:
                    if (Side == OwnerSide.Aresden)
                    {
                        equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration[245].Copy() : World.ItemConfiguration[246].Copy(); // aresden hero hauberk
                        equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration[249].Copy() : World.ItemConfiguration[250].Copy(); // aresden hero leggings

                        switch (aiClass)
                        {
                            case AIClass.Warrior:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[237].Copy() : World.ItemConfiguration[238].Copy(); // aresden hero plate
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[229].Copy() : World.ItemConfiguration[230].Copy(); // aresden hero helm

                                switch (Dice.Roll(1, 3))
                                {
                                    default:
                                    case 1: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[345].Copy(); break; // giant sowrd
                                    case 2: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[322].Copy(); break; // battle axe
                                    case 3: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[456].Copy(); break; // battle hammer
                                }
                                break;
                            case AIClass.Archer:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[571].Copy() : World.ItemConfiguration[572].Copy(); // aresden hero leather
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[575].Copy() : World.ItemConfiguration[576].Copy(); // aresden hero hood

                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration[523].Copy();
                                break;
                            case AIClass.BattleMage:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[577].Copy() : World.ItemConfiguration[578].Copy(); // aresden hero battle plate
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[233].Copy() : World.ItemConfiguration[234].Copy(); // aresden hero cap

                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration[568].Copy();
                                break;
                            case AIClass.Mage:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[241].Copy() : World.ItemConfiguration[242].Copy(); // aresden hero robe
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[233].Copy() : World.ItemConfiguration[234].Copy(); // aresden hero cap
                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration[395].Copy(); // magic wand ms20
                                break;
                        }

                        switch (Dice.Roll(1, 5))
                        {
                            case 1: equipment[(int)EquipType.Back] = World.ItemConfiguration[226].Copy(); break; // aresden hero cape
                            case 2: equipment[(int)EquipType.Back] = World.ItemConfiguration[253].Copy(); break; // aresden hero cape +1
                        }
                    }
                    else
                    {
                        equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration[247].Copy() : World.ItemConfiguration[248].Copy(); // elvine hero hauberk
                        equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration[251].Copy() : World.ItemConfiguration[252].Copy(); // elvine hero legs

                        switch (aiClass)
                        {
                            case AIClass.Warrior:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[239].Copy() : World.ItemConfiguration[240].Copy(); // elvine hero plate
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[231].Copy() : World.ItemConfiguration[232].Copy(); // elvine hero helm

                                switch (Dice.Roll(1, 3))
                                {
                                    default:
                                    case 1: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[345].Copy(); break; // giant sword
                                    case 2: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[322].Copy(); break; // battle axe
                                    case 3: equipment[(int)EquipType.DualHand] = World.ItemConfiguration[456].Copy(); break; // battle hammer
                                }
                                break;
                            case AIClass.Archer:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[569].Copy() : World.ItemConfiguration[570].Copy(); // aresden hero leather
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[573].Copy() : World.ItemConfiguration[574].Copy(); // aresden hero hood

                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration[523].Copy();
                                break;
                            case AIClass.BattleMage:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[579].Copy() : World.ItemConfiguration[580].Copy(); // aresden hero battle plate
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[235].Copy() : World.ItemConfiguration[236].Copy(); // elvine hero cap

                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration[568].Copy();
                                break;
                            case AIClass.Mage:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration[243].Copy() : World.ItemConfiguration[244].Copy(); // elvine hero robe
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration[235].Copy() : World.ItemConfiguration[236].Copy(); // elvine hero cap
                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration[395].Copy(); // magic wand ms20
                                break;
                        }

                        switch (Dice.Roll(1, 5))
                        {
                            case 1: equipment[(int)EquipType.Back] = World.ItemConfiguration[227].Copy(); break; // elvine hero cape
                            case 2: equipment[(int)EquipType.Back] = World.ItemConfiguration[254].Copy(); break; // elvine hero cape +1
                        }
                    }

                    if (equipment[(int)EquipType.DualHand] != null)
                        equipment[(int)EquipType.DualHand].Colour = (int)GameColor.Critical;

                    break;
            }
        }

        private void GenerateHumanAppearance()
        {
            int temp;

            /*for (int i = 0; i < 15; i++)
                if (equipment[i] != null)
                {
                    Item item = equipment[i];

                    switch (item.EquipType)
                    {
                        case EquipType.RightHand:
                            temp = Appearance2;
                            temp = temp & 0xF00F;
                            temp = temp | (item.Appearance << 4);
                            Appearance2 = temp;

                            temp = AppearanceColour;
                            temp = temp & 0x0FFFFFFF;
                            temp = temp | (item.Colour << 28);
                            AppearanceColour = temp;

                            unchecked
                            {
                                int speed = item.Speed - ((Strength) / 13);
                                if (speed < 0) speed = 0;

                                temp = Status;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | speed;
                                Status = temp;
                            }
                            break;
                        case EquipType.LeftHand:
                            temp = Appearance2;
                            temp = temp & 0xFFF0;
                            temp = temp | (item.Appearance);
                            Appearance2 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xF0FFFFFF;
                                temp = temp | (item.Colour << 24);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.DualHand:
                            temp = Appearance2;
                            temp = temp & 0xF00F;
                            temp = temp | (item.Appearance << 4);
                            Appearance2 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0x0FFFFFFF;
                                temp = temp | (item.Colour << 28);
                                AppearanceColour = temp;
                            }

                            unchecked
                            {
                                int speed = item.Speed - ((Strength) / 13);
                                if (speed < 0) speed = 0;

                                temp = Status;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | speed;
                                Status = temp;
                            }
                            break;
                        case EquipType.Body:
                            if (item.Appearance < 100)
                            {
                                temp = Appearance3;
                                temp = temp & 0x0FFF;
                                temp = temp | (item.Appearance << 12);
                                Appearance3 = temp;
                            }
                            else
                            {
                                temp = Appearance3;
                                temp = temp & 0x0FFF;
                                temp = temp | ((item.Appearance - 100) << 12);
                                Appearance3 = temp;

                                temp = Appearance4;
                                temp = temp | 0x080;
                                Appearance4 = temp;
                            }

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFF0FFFFF;
                                temp = temp | (item.Colour << 20);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Back:
                            temp = Appearance4;
                            temp = temp & 0xF0FF;
                            temp = temp | (item.Appearance << 8);
                            Appearance4 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFF0FFFF;
                                temp = temp | (item.Colour << 16);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Arms:
                            temp = Appearance3;
                            temp = temp & 0xFFF0;
                            temp = temp | (item.Appearance);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFF0FFF;
                                temp = temp | (item.Colour << 12);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Legs:
                            temp = Appearance3;
                            temp = temp & 0xF0FF;
                            temp = temp | (item.Appearance << 8);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFFF0FF;
                                temp = temp | (item.Colour << 8);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Feet:
                            temp = Appearance4;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            Appearance4 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFFFF0F;
                                temp = temp | (item.Colour << 4);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Head:
                            temp = Appearance3;
                            temp = temp & 0xFF0F;
                            temp = temp | (item.Appearance << 4);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | (item.Colour);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.FullBody:
                            temp = Appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFF0FFFF;
                                AppearanceColour = temp;
                            }
                            break;
                    }
                }*/
        }

        public void Talk(string message)
        {

        }

        public bool SetMagicEffect(Magic magic, bool firm = false)
        {
            if (magicEffects.ContainsKey(magic.Type)) return false;
            magicEffects.Add(magic.Type, new MagicEffect(magic, (firm ? DateTime.Now.AddYears(1) : DateTime.Now.Add(magic.LastTime))));

            // flags and notify
            unchecked
            {
                switch (magic.Type)
                {
                    case MagicType.Berserk:
                        status = status | 0x00000020;
                        break;
                    case MagicType.Invisibility:
                        status = status | 0x00000010;
                        break;
                    case MagicType.IceLine: // uses Ice effect type
                    case MagicType.Ice:
                        status = status | 0x00000040;
                        break;
                    case MagicType.Protect:
                        switch (magic.Effect1)
                        {
                            case 1: status = status | 0x08000000; break; // arrow
                            case 2:
                            case 5: status = status | 0x04000000; break; // magic
                            case 3:
                            case 4: status = status | 0x02000000; break; // physical
                        }
                        break;
                    case MagicType.Poison:
                        status = status | 0x00000080;
                        poisonTime = DateTime.Now;
                        break;
                    case MagicType.Paralyze: break;
                    case MagicType.TurnUndead: break;
                }
            }

            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        public bool RemoveMagicEffect(MagicType type)
        {
            if (!magicEffects.ContainsKey(type)) return false;

            // flags and notify
            unchecked
            {
                switch (type)
                {
                    case MagicType.Berserk:
                        status = status & (int)0xFFFFFFDF;
                        break;
                    case MagicType.Invisibility:
                        status = status & (int)0xFFFFFFEF;
                        break;
                    case MagicType.Ice:
                        status = status & (int)0xFFFFFFBF;
                        break;
                    case MagicType.Protect:
                        switch (magicEffects[type].Magic.Effect1)
                        {
                            case 1: status = status & (int)0xF7FFFFFF; break; // arrows
                            case 2:
                            case 5: status = status & (int)0xFBFFFFFF; break; // magic
                            case 3:
                            case 4: status = status & (int)0xFDFFFFFF; break; // physical
                        }
                        break;
                    case MagicType.Poison:
                        status = status & (int)0xFFFFFF7F;
                        break;
                    case MagicType.Paralyze: break;
                }
            }

            magicEffects.Remove(type);
            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        /*private IOwner TargetSearch(AIBehaviour behaviou)
        {
            bool ally = false;
            IOwner target = null;
            ally = false;

            if (HasObjective) target = objective;
            else
            {
                List<IOwner> allyTargets = new List<IOwner>();
                List<IOwner> enemyTargets = new List<IOwner>();
                switch (aiBehaviour)
                {
                    case AIBehaviour.Supporter:
                    case AIBehaviour.Healer:
                    case AIBehaviour.Defender:
                        if (Dice.Roll(1, 100) < 40) ally = true; // 40% chance to target ally
                        break;
                    case AIBehaviour.Berserker:
                        if (Dice.Roll(1, 100) < 10) // 10% chance to target self, berserkers are selfish
                        {
                            target = this;
                            goto FoundTarget;
                        }
                        break;
                }

                
                for (int p = 0; p < map.Players.Count; p++)
                    if (Cache.World.Players.ContainsKey(map.Players[p]))
                        if (IsValidTarget(Cache.World.Players[map.Players[p]]))
                        {
                            if (IsAlly(Cache.World.Players[map.Players[p]])) 
                                 allyTargets.Add(Cache.World.Players[map.Players[p]]);
                            else enemyTargets.Add(Cache.World.Players[map.Players[p]]);
                        }

                
                for (int n = 0; n < map.Npcs.Count; n++)
                    if (Cache.World.Npcs.ContainsKey(map.Npcs[n]))
                        if (IsValidTarget(Cache.World.Npcs[map.Npcs[n]]))
                        {
                            if (IsAlly(Cache.World.Npcs[map.Npcs[n]])) allyTargets.Add(Cache.World.Npcs[map.Npcs[n]]);
                            else enemyTargets.Add(Cache.World.Npcs[map.Npcs[n]]);
                        }

                if (ally && allyTargets.Count > 0)
                {
                    switch (aiBehaviour) // behaviour affects target selection
                    {
                        case AIBehaviour.Healer:
                            // order targets by % of remaining HP, and selects the first one
                            IOwner tempTarget = allyTargets.OrderBy(t => (((double)t.MaxHP / (double)t.HP) * 100.0f)).ToList()[0];
                            // if the first target has less than 100% health, select this target (dont heal when we dont need to)
                            if (tempTarget.HP < tempTarget.MaxHP)
                                target = tempTarget;
                            break;
                        case AIBehaviour.Supporter:

                            break;
                        case AIBehaviour.Defender:

                            break;
                        case AIBehaviour.Berserker: break; // handled before loops
                        default: 
                            // find a random target
                            if (allyTargets.Count > 1)
                                 target = allyTargets[Dice.Roll(1, allyTargets.Count - 1)];
                            else target = allyTargets[0];
                            break;
                    }
                }

                if ((target == null || !ally) && enemyTargets.Count > 0)
                    target = enemyTargets[0]; // nearest enemy
            }

            FoundTarget:
            return target;
        }

        private bool CanCast(IOwner target, out Magic spell)
        {
            spell = null;

            
            if (IsAlly(target))
            {
                // get valid defensive spell
                // TODO - check spell suitability
                foreach (string s in spells)
                {
                    Magic magic = Cache.World.GetSpellByName(s);
                    switch (aiBehaviour)
                    {
                        case AIBehaviour.Supporter:
                            if (magic.Category == MagicCategory.Support && mp >= magic.ManaCost)
                            {
                                spell = magic;
                                goto SpellCheck;
                            }
                            break;
                        case AIBehaviour.Healer:
                            if (magic.Category == MagicCategory.Utility && mp >= magic.ManaCost)
                            {
                                spell = magic;
                                goto SpellCheck;
                            }
                            break;
                        case AIBehaviour.Defender:
                            if (magic.Category == MagicCategory.Defensive && mp >= magic.ManaCost)
                            {
                                spell = magic;
                                goto SpellCheck;
                            }
                            break;
                    }

                }
            }
            else
            {
                // check for valid offensive spell
                // TODO - check spell suitablity (based on target elemental defensive capabilities)
                foreach (string s in spells)
                {
                    Magic magic = Cache.World.GetSpellByName(s);
                    if (magic.Category == MagicCategory.Offensive && mp >= magic.ManaCost)
                    {
                        spell = magic;
                        break;
                    }
                }
            }

            SpellCheck:

            
            if (spell != null)
            {
                if (Shield != null) return false; // no shield casting
                if (Weapon != null && Weapon.RelatedSkill != SkillType.Staff) return false; // staff casting only

                return true;
            }
            else return false;
        }*/

        /*private void HumanActionProcess()
        {
            switch (action)
            {
                case MotionType.Idle:
                    action = MotionType.Move;
                    break;
                case MotionType.Move:
                    Magic spell = null;

                    // find a target if not already
                    if (!HasTarget && (!isFriendly && !isPacifist && !(side == OwnerSide.Neutral))) target = TargetSearch(aiBehaviour);
                    // perform action if target acquired
                    if (HasTarget)
                    {
                        // re-check the validity of the target, and clear if necessary
                        if (!IsValidTarget(target)) ClearTarget();
                        else
                        {
                            // put human npcs in to combat mode if they have acquired a target
                            if (!isCombatMode) ToggleCombatMode();

                            direction = Utility.GetNextDirection(X, Y, target.X, target.Y);
                            if (target.IsAlly(this)) // check if target is an ally
                            {
                                // check that a valid ally-targetting spell was found and they are within range
                                if (CanCast(target, out spell))
                                {
                                    castedSpell = spell.Index;
                                    PrepareMagic(X, Y, direction);
                                }
                                else ClearTarget(); // no spell found, clear target
                            }
                            else
                            {
                                // check that a valid enemy-targetting spell was found, and they are within range
                                if (CanCast(target, out spell))
                                {
                                    castedSpell = spell.Index;
                                    PrepareMagic(X, Y, direction);
                                }
                                else action = MotionType.Attack; // no spell found, melee/ranged attack
                            }
                        }
                    }
                    else // move normally accoring to movetype
                    {
                        // put human npcs in to peace mode if they have no target
                        if (isCombatMode) ToggleCombatMode();

                        if (Move()) Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                    }

                    // range check - am I fighting at an adequate range?
                    //          no: retreat or move closer to get in range yes: continue                    

                    // hp check - do I have enough hp to fight?
                    //          - does my behaviour weigh on berserker? weight: 30%, else 70% chance to retreat
                    //          no: retreat, yes: continue
                    //          - do I have invisibility?
                    //          yes: invisi  no: retreat

                    // mage/bmage only
                    // mp check - do I have enough mp to fight?
                    //          no: run and invisi, yes: cast magic
                    break;
                case MotionType.Attack:
                    int rangeX, rangeY;
                    AttackType attackType;

                    if (HasTarget)
                    {
                        // re-check the validity of the target, and clear if necessary
                        if (!IsValidTarget(target)) ClearTarget();
                        else
                        {
                            rangeX = Math.Abs(X - target.X);
                            rangeY = Math.Abs(Y - target.Y);

                            if ((rangeX >= 0 && rangeX <= attackRange) && (rangeY >= 0 && rangeY <= attackRange))
                            {
                                direction = Utility.GetNextDirection(X, Y, target.X, target.Y);
                                Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectAttack, target.X, target.Y, 0, (int)AttackType.Normal);
                                Attack(X, Y, direction, target.X, target.Y, target, AttackType.Normal, false);
                                failedActionCount = 0;
                            }
                            else
                            {
                                if (moveType == MovementType.None) ClearTarget();
                                else
                                {
                                    if (Move())
                                    {
                                        if (isRunning)
                                            Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectRun);
                                        else Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                                    }
                                    failedActionCount++; // failure is not an option!
                                }
                            }
                        }
                    }
                    break;
                case MotionType.Magic: // 
                    if ((DateTime.Now - castTime).TotalMilliseconds >= (88 + (magicEffects.ContainsKey(MagicType.Ice) ? 22 : 0)))
                    {
                        Cache.World.SendMagicEventToNearbyPlayers(this, CommandMessageType.CastMagic, target.X, target.Y, castedSpell + 100);
                        Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectAttack, target.X, target.Y, 0, (int)AttackType.Normal);
                        if (!target.EvadeMagic(GetMagicHitChance(World.MagicConfiguration[castedSpell]), World.MagicConfiguration[castedSpell].IgnorePFM))
                            Cache.World.CastMagic(this, target.X, target.Y, World.MagicConfiguration[castedSpell]);
                        DepleteMP(World.MagicConfiguration[castedSpell].ManaCost);
                        action = MotionType.Idle;
                        ClearTarget();
                    }
                    break;
                case MotionType.Dead:
                    if (isDead)
                    {
                        if ((DateTime.Now - deadTime).Seconds >= maximumDeadTime.Seconds) Cache.World.RemoveNPC(objectId);
                    }
                    break;
            }
        }*/

        private void SelectTarget()
        {
            target = null;
            List<IOwner> targetAllies = new List<IOwner>();
            List<IOwner> targetEnemies = new List<IOwner>();
            aiCategory = AIActionCategory.Offensive;

            // ROLL AN ACTION CATEGORY, DIFFERENT WEIGHTING FOR DIFFERENT AI BEHAVIOURS
            int chance;
            switch (aiBehaviour)
            {
                case AIBehaviour.Healer:
                    chance = Dice.Roll(1, 100);
                    if (chance <= 40) aiCategory = AIActionCategory.Healing;
                    else if (chance <= 90) aiCategory = AIActionCategory.Offensive;
                    else aiCategory = AIActionCategory.Misc;
                    break;
                case AIBehaviour.Supporter:
                    chance = Dice.Roll(1, 100);
                    if (chance <= 35) aiCategory = AIActionCategory.Buff;
                    else if (chance <= 50) aiCategory = AIActionCategory.Debuff;
                    else if (chance <= 90) aiCategory = AIActionCategory.Offensive;
                    else aiCategory = AIActionCategory.Misc;
                    break;
                case AIBehaviour.Defender:
                    chance = Dice.Roll(1, 100);
                    if (chance <= 35) aiCategory = AIActionCategory.Protect;
                    else if (chance <= 95) aiCategory = AIActionCategory.Offensive;
                    else aiCategory = AIActionCategory.Misc;
                    break;
                case AIBehaviour.Berserker:
                    chance = Dice.Roll(1, 100);
                    if (chance <= 15) aiCategory = AIActionCategory.Protect;
                    else if (chance <= 30) aiCategory = AIActionCategory.Healing;
                    else if (chance <= 95) aiCategory = AIActionCategory.Offensive;
                    else aiCategory = AIActionCategory.Misc;
                    break;
            }

            // SPECIAL CASES FOR TARGET SELECTION
            if (aiBehaviour == AIBehaviour.Berserker) // berserkers are self sufficient
                switch (aiCategory)
                {
                    case AIActionCategory.Healing:
                    case AIActionCategory.Buff:
                    case AIActionCategory.Protect:
                        target = this;
                        break;
                }

            // MAIN TARGET SELECTION CODE
            if (target == null)
            {
                for (int ix = mapX - searchRange; ix < mapX + searchRange * 2 + 1; ix++)
                    for (int iy = mapY - searchRange; iy < mapY + searchRange * 2 + 1; iy++)
                        if (!(iy < 0 || iy > map.Height - 1 || ix < 0 || ix > map.Width - 1) && (map[iy][ix].IsOccupied))
                        {
                            IOwner owner = map[iy][ix].Owner;
                            if (IsAlly(owner)) targetAllies.Add(owner);
                            else targetEnemies.Add(owner);
                        }

                switch (aiCategory)
                {
                    case AIActionCategory.Healing: // order allies by remaining HP, then check if anyone has less than 100%. heal the lowest
                        IOwner tempTarget = targetAllies.OrderBy(t => (((double)t.HP / (double)t.MaxHP) * 100.0f)).ToList()[0];
                        if (tempTarget.HP < tempTarget.MaxHP) target = tempTarget;
                        break;
                    case AIActionCategory.Buff: // find an ally who doesn't already have the buff we are able to cast
                        if (aiSpells.ContainsKey(AIActionCategory.Buff))
                            foreach (IOwner ally in targetAllies)
                                if (!ally.MagicEffects.ContainsKey(World.MagicConfiguration[aiSpells[AIActionCategory.Buff]].Type))
                                {
                                    target = ally;
                                    break;
                                }
                        break;
                    case AIActionCategory.Protect: // find an ally who oesn't already have the protection we are able to cast
                        if (aiSpells.ContainsKey(AIActionCategory.Protect))
                            foreach (IOwner ally in targetAllies)
                                if (!ally.MagicEffects.ContainsKey(World.MagicConfiguration[aiSpells[AIActionCategory.Protect]].Type))
                                {
                                    target = ally;
                                    break;
                                }
                        break;
                    case AIActionCategory.Debuff: break; // TODO
                    case AIActionCategory.Offensive: // special cases for certain offensive spells
                        if (aiSpells.ContainsKey(AIActionCategory.Offensive))
                        {
                            switch (World.MagicConfiguration[aiSpells[AIActionCategory.Offensive]].Type)
                            {
                                case MagicType.GhostSpiral:
                                case MagicType.Kinesis: target = this; break; // bmage spell center on caster
                            }
                        }
                        break;
                    default:
                    case AIActionCategory.Misc:  // specific spell targetting
                        if (aiSpells.ContainsKey(AIActionCategory.Misc))
                        {
                            switch (World.MagicConfiguration[aiSpells[AIActionCategory.Misc]].Type)
                            {
                                case MagicType.ExploitCorpse: target = this; break;
                                case MagicType.Summon: target = this; break;
                                case MagicType.Resurrection:
                                    foreach (IOwner ally in targetAllies)
                                        if (ally.IsDead)
                                        {
                                            target = ally;
                                            break;
                                        }
                                    break;
                            }
                        }
                        break;
                }
            }

            if (target == null && targetEnemies.Count > 0)
                if (targetEnemies.Count > 1)
                    target = targetEnemies[Dice.Roll(1, targetEnemies.Count) - 1];
                else target = targetEnemies[0];
        }

        private void HumanActionProcess()
        {


            switch (action)
            {
                case MotionType.Idle: // reset and decide next action
                    aiState = AIState.None;
                    action = MotionType.Move;
                    SelectTarget();
                    break;
                case MotionType.Move:
                    if (HasTarget)
                    {
                        if (!IsValidTarget(target)) ClearTarget();
                        else
                        {
                            if (aiState == AIState.Retreat)
                            {
                                if (Retreat()) Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                                if (!IsWithinRange(target, 3, 3)) aiState = AIState.None;
                            }
                            else if (aiSpells.ContainsKey(aiCategory))
                            {
                                castedSpell = aiSpells[aiCategory];
                                PrepareMagic(X, Y, direction);
                            }
                            else action = MotionType.Attack;
                        }
                    }
                    else if (Move()) Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                    break;
                case MotionType.Magic:
                    if ((DateTime.Now - castTime).TotalMilliseconds > 88 + (magicEffects.ContainsKey(MagicType.Ice) ? 22 : 0))
                    {
                        Cache.World.SendMagicEventToNearbyPlayers(this, CommandMessageType.CastMagic, target.X, target.Y, World.MagicConfiguration[aiSpells[aiCategory]].Index + 100);
                        Cache.World.CastMagic(this, target.X, target.Y, World.MagicConfiguration[castedSpell], true);

                        switch (aiCategory)
                        {
                            case AIActionCategory.Offensive: // keep enemy at range when fighting with magic
                                if (!IsAlly(target) && IsWithinRange(target, 3, 3))
                                {
                                    aiState = AIState.Retreat;
                                    action = MotionType.Move;
                                }
                                else action = MotionType.Idle;
                                break;
                            default: // re-do target selection if not using offensive spells
                                ClearTarget();
                                action = MotionType.Idle;
                                break;
                        }
                    }
                    break;
                case MotionType.Attack:
                    if (HasTarget)
                    {
                        if (!IsValidTarget(target)) ClearTarget();
                        else
                        {
                            // TODO improve critical rate based on difficulty?
                            AttackType attackType = (Dice.Roll(1, 5) == 1 ? AttackType.Critical : AttackType.Normal);
                            int range = (Weapon != null ? Utility.GetRange(Weapon.RelatedSkill, (attackType == AttackType.Critical), Weapon.HasExtendedRange)
                                                        : 1);
                            if (IsWithinRange(target, range, range))
                            {
                                direction = Utility.GetNextDirection(X, Y, target.X, target.Y);
                                Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectAttack, target.X, target.Y, 0, (int)attackType);
                                Attack(X, Y, direction, target.X, target.Y, target, attackType, false);
                            }
                            else
                            {
                                if (moveType == MovementType.None) ClearTarget();
                                else
                                {
                                    if (Move())
                                        if (isRunning)
                                            Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectRun);
                                        else Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                                }
                            }
                        }
                    }
                    break;
                case MotionType.Dead:
                    if (isDead)
                    {
                        //Wait till faded to remove
                        if (!isFading && (DateTime.Now - deadTime).Seconds >= maximumDeadTime.Seconds) { isFading = true; Cache.World.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject); }
                        else if (isFading && (DateTime.Now - deadTime).Seconds >= maximumDeadTime.Seconds + 2) { Cache.World.RemoveNPC(objectId); }
                    }
                    break;
            }
        }

        public void ActionProcess()
        {
            TimeSpan ts;
            if (IsSummoned)
            {
                ts = DateTime.Now - summonTime;
                if (ts.Seconds >= Globals.NpcSummonDuration) Die(null, -1, (DamageType)0, 1, false);
            }

            ts = DateTime.Now - lastActionTime;
            if (ts.TotalMilliseconds >= actionTime.TotalMilliseconds)
            {
                lastActionTime = DateTime.Now;

                if (IsHuman) HumanActionProcess();
                else
                {
                    switch (action)
                    {
                        case MotionType.Idle:
                            if (Idle())
                                switch (NpcType)
                                {
                                    case NpcType.CrusadeManaCollector:
                                    case NpcType.CrusadeDetector:
                                        Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectAttack, X + 1, Y + 1, 0, (int)AttackType.Normal);
                                        break;
                                }
                            break;
                        case MotionType.Dead:
                            if (isDead)
                            {
                                if (!isFading && (DateTime.Now - deadTime).Seconds >= maximumDeadTime.Seconds)
                                {
                                    isFading = true;
                                    Cache.World.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject);
                                    if (!IsSummoned && canDropLoot) DropLoot(LootDrop.Secondary);
                                }// body parts, rares etc}
                                else if (isFading && (DateTime.Now - deadTime).Seconds >= maximumDeadTime.Seconds + 2) { Cache.World.RemoveNPC(objectId); }
                            }
                            break;
                        case MotionType.Move:
                            if (Move()) Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                            // search for targets
                            if (!isFriendly && !isPacifist && !(side == OwnerSide.Neutral)) TargetSearch();
                            break;
                        case MotionType.AttackStationary:
                            // search for targets
                            if (!isFriendly && !isPacifist && !(side == OwnerSide.Neutral)) TargetSearch();
                            break;
                        case MotionType.Attack:
                            int rangeX, rangeY, spell;
                            AttackType attackType;

                            if (HasTarget)
                            {
                                if (target.IsDead || target.IsRemoved || target.CurrentMap != map ||
                                    (target.IsInvisible && !perks.Contains(NpcPerk.Clairvoyant)) ||
                                    failedActionCount > 20) ClearTarget();
                                else
                                {
                                    rangeX = Math.Abs(X - target.X);
                                    rangeY = Math.Abs(Y - target.Y);


                                    if ((rangeX > 1 && rangeX <= 9) && (rangeY > 1 && rangeY <= 9) &&
                                        CastMagic(target.X, target.Y, target, out spell))
                                    {
                                        this.direction = Utility.GetNextDirection(X, Y, target.X, target.Y);

                                        Cache.World.SendMagicEventToNearbyPlayers(this, CommandMessageType.CastMagic, target.X, target.Y, spell + 100);
                                        Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectAttack, target.X, target.Y, 0, (int)AttackType.Normal);
                                        if (!target.EvadeMagic(GetMagicHitChance(World.MagicConfiguration[spell]), World.MagicConfiguration[spell].IgnorePFM))
                                            Cache.World.CastMagic(this, target.X, target.Y, World.MagicConfiguration[spell]);
                                        DepleteMP(World.MagicConfiguration[spell].ManaCost, false);
                                        failedActionCount = 0;
                                    }
                                    else if ((rangeX >= 0 && rangeX <= attackRange) && (rangeY >= 0 && rangeY <= attackRange))
                                    {
                                        direction = Utility.GetNextDirection(X, Y, target.X, target.Y);
                                        Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectAttack, target.X, target.Y, 0, (int)AttackType.Normal);
                                        Attack(X, Y, direction, target.X, target.Y, target, AttackType.Normal, false);
                                        failedActionCount = 0;
                                    }
                                    else
                                    {
                                        if (moveType == MovementType.None) ClearTarget(); //TODO change to specific distance before dropping target?
                                        else
                                        {
                                            if (Move()) Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMove);
                                            failedActionCount++; // failure is not an option!
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Actions to be performed on a timer such as mp/hp regeneration etc.
        /// </summary>
        public void TimerProcess()
        {
            if (isDead) return;

            TimeSpan ts;

            // handle poison time
            if (magicEffects.ContainsKey(MagicType.Poison))
            {
                ts = DateTime.Now - poisonTime;
                if (ts.TotalSeconds >= Globals.PoisonTime)
                {
                    //int damage = Dice.Roll(1, magicEffects[MagicType.Poison].Magic.Effect2);
                    int damage = magicEffects[MagicType.Poison].Magic.Effect2;
                    if (hp - damage < 1) damage = hp - 1;// cant go below 1 hp
                    DepleteHP(damage);
                    DamageTaken(this, DamageType.Poison, null, damage, MotionDirection.None, 1, damage);
                    if (EvadePoison()) RemoveMagicEffect(MagicType.Poison);
                    poisonTime = DateTime.Now;
                }
            }

            // handle magic effect removal if time has expired
            if (magicEffects.Count > 0)
                for (int i = 0; i < 50; i++)
                    if (magicEffects.ContainsKey((MagicType)i))
                    {
                        MagicEffect effect = magicEffects[(MagicType)i];
                        if (effect.Magic.Type != MagicType.Poison) // poison have their own timer
                        {
                            ts = DateTime.Now - effect.StartTime;
                            if (ts.TotalSeconds >= effect.Magic.LastTime.TotalSeconds) // use total seconds as some spells last over 59 seconds
                                RemoveMagicEffect(effect.Magic.Type);
                        }
                    }
        }

        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(null, type, damage, flyDirection, true); }
        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection, bool notify) { return TakeDamage(null, type, damage, flyDirection, notify); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(attacker, type, damage, flyDirection, true); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify) { return TakeDamage(attacker, type, damage, flyDirection, notify, 1, damage); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify, int hitCount, int totalDamage)
        {
            if (isDead) return 0;

            DepleteHP(damage);

            // remove paralyze for different damage types
            if (magicEffects.ContainsKey(MagicType.Paralyze))
                switch (type)
                {
                    case DamageType.Magic: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 2) RemoveMagicEffect(MagicType.Paralyze); break;
                    case DamageType.Ranged:
                    case DamageType.Melee: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 1) RemoveMagicEffect(MagicType.Paralyze); break;
                    case DamageType.Environment: RemoveMagicEffect(MagicType.Paralyze); break;
                }

            if (hp <= 0)
            {
                Die(attacker, totalDamage, type, hitCount, (!(IsSummoned || (attacker != null && attacker.OwnerType == OwnerType.Npc))));
                if (!IsSummoned) return remainingExperience;
                else return 0;
            }
            else
            {
                if (notify) DamageTaken(this, type, attacker, damage, flyDirection, hitCount, totalDamage);

                // counter attack
                if (!IsPacifist && type != DamageType.Environment && attacker != null && attacker.ObjectId != summoner) //Maybe set attacker to environmental damage
                    switch (action)
                    {
                        case MotionType.Attack: // change target if already attacking
                        case MotionType.AttackStationary:
                            if (Dice.Roll(1, 3) == 1)
                            {
                                if (target != null)
                                {
                                    int currentRX = Math.Abs(X - target.X);
                                    int currentRY = Math.Abs(Y - target.Y);
                                    int newRX = Math.Abs(X - attacker.X);
                                    int newRY = Math.Abs(Y - attacker.Y);

                                    if (newRX < currentRX || newRY < currentRY)
                                        target = attacker;
                                }
                            }
                            break;
                        default: // set target if not already attacking
                            action = MotionType.Attack;
                            target = attacker;
                            break;
                    }

                if (!IsSummoned)
                {
                    if ((remainingExperience - damage) >= 0)
                    {
                        remainingExperience -= damage;
                        return remainingExperience;
                    }
                    else return 0;
                }
                else return 0;
            }
        }

        public void TakeArmourDamage(int damage)
        {
            // unused at the moment
        }

        public void DepleteMP(int points, bool notify)
        {
            int totalDepleted = points;
            int tempMp = mp;
            mp -= points;
            if (mp < 0)
            {
                totalDepleted = tempMp;
                mp = 0;
            }

            if (notify && totalDepleted != 0) VitalsChanged(this, -totalDepleted, VitalType.Mana);
        }
        public void ReplenishMP(int points, bool notify)
        {
            int totalReplenished = points;
            int tempMp = mp;
            mp += points;
            if (NpcType != NpcType.CrusadeGrandMagicGenerator && // grand magic generators maxMP is determined in crusade config (ManaPerStrike)
                mp > MaxMP)
            {
                totalReplenished = maxMP - tempMp;
                mp = MaxMP;
            }

            if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Mana);
        }
        public void DepleteHP(int points)
        {
            hp -= points;
            if (hp < 0) hp = 0;
        }
        public void ReplenishHP(int points, bool notify)
        {
            int totalReplenished = points;
            int tempHp = hp;
            hp += points;
            if (hp > MaxHP)
            {
                totalReplenished = maxHP - tempHp;
                hp = MaxHP;
            }

            if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Health);
        }
        public void DepleteSP(int points, bool notify)
        {
            // VitalsChanged(this, totalDepleted, DepleteType.Stamina);
        }
        public void ReplenishSP(int points, bool notify)
        {
            // VitalsChanged(this, totalReplenished, VitalType.Health);
        }

        public void ReplenishHunger(int points, bool notify)
        {
            //int totalReplenished = points;
            //int tempHunger = hunger;
            //hunger += points;

            //if (hunger > 100)
            //{
            //    totalReplenished = 100 - tempHunger;
            //    hunger = 100;
            //}

            //hpStock += points;
            ////if (hpStock > 500) hpStock = 500; Why limit this?

            //Notify(CommandMessageType.NotifyHunger, hunger);
            //if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Hunger);
        }

        public void DepleteHunger(int points, bool notify)
        {
            //int totalDepleted = points;
            //int tempHunger = hunger;
            //hunger -= points;

            //if (hunger < 0)
            //{
            //    totalDepleted = tempHunger;
            //    hunger = 0;
            //}

            //Notify(CommandMessageType.NotifyHunger, hunger);
            //if (notify && totalDepleted != 0) VitalsChanged(this, -totalDepleted, VitalType.Hunger);
        }

        public void Die(IOwner killer, int damage, DamageType type, int hitCount, bool dropLoot)
        {
            if (isDead) return; // avoid die-ception

            isDead = true;
            canDropLoot = dropLoot; // stored for npc removal, secondary drop
            action = MotionType.Dead;
            deadTime = DateTime.Now;
            map[mapY][mapX].ClearOwner();
            map[mapY][mapX].SetDeadOwner(this);

            if (IsSummoned && Cache.World.Players.ContainsKey(summoner)) Cache.World.Players[summoner].Summons.Remove(objectId);

            if (Cache.World.IsHeldenianBattlefield)
                if (NpcType == NpcType.HeldenianArrowTower || NpcType == NpcType.HeldenianCannonTower)
                    Cache.World.Heldenian.TowerDestroyed(side);

            Killed(this, killer, damage, type, hitCount);

            // titles
            if (killer != null)
                switch (killer.OwnerType)
                {
                    case OwnerType.Player:
                        Character character = (Character)killer;
                        ((Character)killer).Titles[TitleType.MonstersKilled]++;
                        ((Character)killer).Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonstersKilled, ((Character)killer).Titles[TitleType.MonstersKilled]);
                        killersGender = character.Gender;
                        break;
                }

            if (dropLoot) DropLoot(LootDrop.Primary, killer); //TODO add boss
        }

        /// <summary>
        /// Npc drops loot.
        /// </summary>
        /// <param name="drop">Specifies if this is a Primary or Secondary drop.</param>
        /// <param name="killer">Specifies the killer to determine gold and drop modifiers</param>
        public void DropLoot(LootDrop drop, IOwner killer = null)
        {
            if (IsSummoned) return; // summons dont drop items
            if (Cache.World.IsCrusade) return; // no drops in sade
            if (!map.LootEnabled) return; // b.i etc
            if ((Cache.World.IsHeldenianBattlefield || Cache.World.IsHeldenianSeige) && map.IsHeldenianMap) return; // no drops in heldenian on heldenian maps
            int luckBonus = 0;
            if (killer != null && killer.OwnerType == OwnerType.Player)
            {
                luckBonus += ((Character)killer).MaxLuck;
            }

            Dictionary<string, LootTable> lootTable;
            switch (drop)
            {
                case LootDrop.Primary:
                    if (Dice.Roll(1, 100) >= Globals.PrimaryDropRate) return;
                    lootTable = World.PrimaryLootConfiguration;
                    // TODO - 35% drop rate if heldenian winner
                    break;
                case LootDrop.Secondary:
                    if (monsterDifficulty != MonsterDifficulty.Boss && monsterDifficulty != MonsterDifficulty.LargeBoss && Dice.Roll(1, 100) >= Globals.SecondaryDropRate) return;// Bosses always drop second drops
                    lootTable = World.SecondaryLootConfiguration;
                    break;
                default: return;
            }

            if (!lootTable.ContainsKey(name)) return; // no loot table exists for this npc

            for (int lootCount = 0; (lootCount < dropCount && lootCount < 25); lootCount++) //25 is currently max amount based off of Globals.EmptyPositionX/Y 
            {
                dropX = X + Globals.EmptyPositionX[lootCount];
                dropY = Y + Globals.EmptyPositionY[lootCount];

                ItemRarity rarity = ItemRarity.UltraRare; //TODO (set to normal)Force UltraRare always for testing, normally is VeryCommon
                //int rarityChance = Dice.Roll(1, 100000);
                //if (rarityChance > 99000) rarity = ItemRarity.UltraRare; // 1000/100000 - 1%
                //else if (rarityChance >= 95000 && rarityChance < 99000) rarity = ItemRarity.VeryRare; // 4000/100000 - 4%
                //else if (rarityChance >= 88000 && rarityChance < 95000) rarity = ItemRarity.Rare; // 7000/1000000 - 7%
                //else if (rarityChance >= 70000 && rarityChance < 88000) rarity = ItemRarity.Uncommon; // 18000/100000 - 18%
                //else if (rarityChance >= 40000 && rarityChance < 70000) rarity = ItemRarity.Common; // 30000/100000 - 30%
                //else rarity = ItemRarity.VeryCommon; // 40000/100000 - 40%

                // luck bonus
                if (luckBonus > 0)
                {
                    int probability = Dice.Roll(1, 100);
                    if (probability <= luckBonus) // e.g. 5% luck means 5% chance to get an ItemRarity increase
                        switch (rarity)
                        {
                            case ItemRarity.VeryCommon: rarity = ItemRarity.Common; break;
                            case ItemRarity.Common: rarity = ItemRarity.Uncommon; break;
                            case ItemRarity.Uncommon: rarity = ItemRarity.Rare; break;
                            case ItemRarity.Rare: rarity = ItemRarity.VeryRare; break;
                            case ItemRarity.VeryRare: rarity = ItemRarity.UltraRare; break;
                            case ItemRarity.UltraRare: rarity = ItemRarity.UltraRare; break;
                        }
                }

                Item loot = null;

                int result;
                if (drop == LootDrop.Secondary && (monsterDifficulty == MonsterDifficulty.Boss || monsterDifficulty == MonsterDifficulty.LargeBoss)) { result = 100; } //100% second drop for bosses
                else { result = Dice.Roll(1, 100); }
                if (result <= 25 && drop != LootDrop.Secondary && maxGold > 0) // 25% gold
                {
                    loot = World.ItemConfiguration[86].Copy(); // gold

                    int count = Math.Max(Dice.Roll(1, maxGold), 1);

                    // check for gold % bonuses
                    if (killer != null && killer.OwnerType == OwnerType.Player)
                    {
                        int bonus = ((Character)killer).Inventory.GoldBonus;
                        if (bonus > 0) count += (int)((((double)count) / 100.0f) * ((double)bonus));
                    }

                    loot.Count = count;
                }
                else if (result > 25 && result <= 35 && drop != LootDrop.Secondary) // 10% commodity
                {
                    switch (Dice.Roll(1, 10))
                    {
                        default:
                        case 1:
                            loot = World.ItemConfiguration[73].Copy();
                            loot.Count = Dice.Roll(5, 25); //Random amount of arrows
                            break; // arrow
                        case 2: // potions - 25% chance to get big pot
                            switch (Dice.Roll(1, 4))
                            {
                                default:
                                case 1: loot = World.ItemConfiguration[87].Copy(); break; // small health pot
                                case 2: loot = World.ItemConfiguration[88].Copy(); break; // big health pot
                            }
                            break;
                        case 3:
                            switch (Dice.Roll(1, 4))
                            {
                                default:
                                case 1: loot = World.ItemConfiguration[89].Copy(); break; // small mana pot
                                case 2: loot = World.ItemConfiguration[90].Copy(); break; // big mana pot
                            }
                            break;
                        case 4:
                            switch (Dice.Roll(1, 4))
                            {
                                default:
                                case 1: loot = World.ItemConfiguration[91].Copy(); break; // small staminar pot
                                case 2: loot = World.ItemConfiguration[92].Copy(); break; // big staminar pot
                            }
                            break;
                    }
                }
                else if (result > 35 && result <= 75 && drop != LootDrop.Secondary) // 40% item
                {
                    if (lootTable[name].GetLoot(rarity).Count <= 0) return;
                    loot = World.ItemConfiguration[lootTable[name].GetLoot(rarity)[Math.Max(Dice.Roll(1, lootTable[name].GetLoot(rarity).Count) - 1, 0)]].Copy();
                    loot.GenerateStats(level, luckBonus); // v2
                }
                else if (result > 75) // 25% weapon/armour or secondary drop
                {
                    if (lootTable[name].GetLoot(rarity).Count <= 0) return;
                    loot = World.ItemConfiguration[lootTable[name].GetLoot(rarity)[Math.Max(Dice.Roll(1, lootTable[name].GetLoot(rarity).Count) - 1, 0)]].Copy();
                    loot.GenerateStats(level, luckBonus); // v2
                }

                //Switches item drop if doesn't match players geneder
                //TODO possibly create item groups for which "class" a player is to drop items based off of. 
                //TODO Adjust to use killersMaxluck to trigger this?
                if (loot != null)
                {
                    if (loot.GenderLimit != GenderType.None && killersGender != GenderType.None && loot.GenderLimit != killersGender) //Force drops to be same gender as killer
                    {
                        List<Item> itemList = new List<Item>();

                        foreach (int dropIndex in lootTable[name].GetLoot(rarity))
                        {
                            Item item = World.ItemConfiguration[dropIndex];
                            if (item.GenderLimit == killersGender) { itemList.Add(item); }
                        }

                        if (itemList.Count > 0)
                        {
                            int itemID = Dice.Roll(1, itemList.Count) - 1; //Could clean up so we don't need -1 on end Dice.Roll() doesnt return 0
                            loot = World.ItemConfiguration[itemList[itemID].ItemId].Copy();
                            loot.GenerateStats(level, luckBonus);
                        }
                    }
                }

                if (loot != null) // drop it and notify
                {
                    loot.Identified = false; // not been seen before - for titles


                    for (int i = 0; i < 25; i++) //Find empty spot to drop, 25 is currently max amount based off of Globals.EmptyPositionX/Y 
                    {
                        if (map[dropY][dropX].SetItem(loot)) { ItemDropped(this, loot); break; }
                        else
                        {
                            dropX = dropX + Globals.EmptyPositionX[i];
                            dropY = dropY + Globals.EmptyPositionY[i];
                        }

                    }
                }
            }
        }

        public bool CastMagic(int destinationX, int destinationY, IOwner target, out int spellId)
        {
            Magic magic = null;
            spellId = -1;

            if (this.magic <= 0) return false; // no magic attribute
            if (spells.Length <= 0) return false; // no spells
            if (Dice.Roll(1, 2) != 1) return false; // chance to fail

            // TODO this needs some AI to determine best spell to use

            for (int i = 0; i < spells.Length; i++)
            {
                magic = Cache.World.GetSpellByName(spells[i]); // find spell by name from config - helper method

                if (magic != null)
                    if (mp >= magic.ManaCost)
                    {
                        if (magic.Type == MagicType.Paralyze && Dice.Roll(1, 3) != 2) continue; // dont keep trying to cast it
                        else
                        {
                            spellId = magic.Index;
                            break; // find one
                        }
                    }
                    else return false;
            }

            if (magic != null) return true;
            else return false;
        }

        public bool MagicAttack(int hitX, int hitY, IOwner target, Magic spell, MagicTarget magicTarget)
        {
            int damage = 0;

            if (target == null) return false;
            if (target.IsDead) return false;
            if (target.OwnerType == OwnerType.Npc && target.Type == this.Type) return false; // cant hit same species - TODO use species group instead of type integer
            if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false;
            if (!CurrentMap.IsAttackEnabled) return false;
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false;

            // different effect values are used for the targeted object or for collatoral object in the "area of effect"
            if (magicTarget == MagicTarget.Single) damage = Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3);
            else if (magicTarget == MagicTarget.Area) damage = Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6);

            MotionDirection flyDirection = Utility.GetFlyDirection(hitX, hitY, target.X, target.Y);

            damage += (int)(((double)damage * (double)(((double)(magic) / 3.3f) / 100.0f)) + 0.5f); // 33% bonus from Magic stat
            if (CurrentMap.IsFightZone) damage = (int)((double)damage * 1.3);// fightzone 30% bonus

            // do damage absorb. return false if 0 damage remaining
            if (!target.AbsorbMagic(spell.Attribute, ref damage, 0)) return false; //TODO add piercing value to NPC's?

            target.TakeDamage(this, DamageType.Magic, damage, flyDirection);

            return false;
        }

        public int GetMagicHitChance(Magic spell)
        {
            int level = 0; // virtual level, determined by size
            switch (size)
            {
                case OwnerSize.Small: level = 60; break;
                case OwnerSize.Medium: level = 90; break;
                case OwnerSize.Large: level = 120; break;
            }

            int magicCircle = (spell.Index / 10) + 1;
            int magicLevel = (level / 10);
            int hitChance = 100; // equivalent to 100% skill
            double temp1 = ((double)hitChance / 100.0f);
            double temp2 = ((double)(temp1 * (double)Globals.MagicCastingProbability[magicCircle]));
            hitChance = (int)temp2;

            if (magicCircle != magicLevel)
                if (magicCircle > magicLevel)
                {
                    temp1 = (double)((magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle]);
                    temp2 = (double)((magicCircle - magicLevel) * 10);
                    double temp3 = ((double)level / temp2) * temp1;
                    hitChance -= Math.Abs(Math.Abs(magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle] - (int)temp3);
                }
                else hitChance += 5 * Math.Abs(magicCircle - magicLevel);

            switch (CurrentMap.Weather)
            {
                case WeatherType.Rain: hitChance -= (hitChance / 24); break; // 4% reduction
                case WeatherType.RainMedium: hitChance -= (hitChance / 12); break; // 8% reduction
                case WeatherType.RainHeavy: hitChance -= (hitChance / 5); break; // 20% reduction
            }

            int result = Dice.Roll(1, 100);
            if (hitChance < result) return -1;

            if (magic > 50) hitChance += (magic) - 50;
            if (hitChance < 0) hitChance = 1;

            return hitChance;
        }

        public bool PrepareMagic(int sourceX, int sourceY, MotionDirection direction)
        {
            if (spells.Length <= 0 && aiSpells.Count <= 0) return false;
            if (mp <= 0) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            castTime = DateTime.Now;
            action = MotionType.Magic;

            Cache.World.SendMotionEventToNearbyPlayers(this, CommandMessageType.ObjectMagic, castedSpell, 0, 0);

            return true;
        }

        public bool Attack(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, IOwner target, AttackType attackType, bool isDash)
        {
            if ((sourceX != this.mapX || sourceY != this.mapY)) return false;
            if (magicEffects.ContainsKey(MagicType.Paralyze)) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction;

            if (target == null) return false;
            if (target.IsDead) return false;
            if (targetX != target.X || targetY != target.Y) return false;
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            int damage = baseDamage.Roll();
            if (IsBerserked) damage *= 2;

            if (attackType == AttackType.Critical)
            {
                switch (aiDifficulty)
                {
                    case AIDifficulty.Easy: damage += (int)(((double)damage * ((double)(Globals.MaximumLevel * 0.3) / 100.0f)) + 0.5f); break;
                    default:
                    case AIDifficulty.Normal: damage += (int)(((double)damage * ((double)(Globals.MaximumLevel * 0.5) / 100.0f)) + 0.5f); break;
                    case AIDifficulty.Hard: damage += (int)(((double)damage * ((double)(Globals.MaximumLevel * 0.7) / 100.0f)) + 0.5f); break;
                    case AIDifficulty.Heroic: damage += (int)(((double)damage * ((double)(Globals.MaximumLevel * 0.8) / 100.0f)) + 0.5f); break;
                    case AIDifficulty.Godly: damage += (int)(((double)damage * ((double)Globals.MaximumLevel / 100.0f)) + 0.5f); break;
                }

                // weapon type critical bonuses
                if (Weapon != null)
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.Archery: damage += (damage / 10); break;
                        case SkillType.ShortSword: damage *= 2; break;
                        case SkillType.LongSword: damage += (damage / 10); break;
                        case SkillType.Axe: damage += (damage / 5); break;
                        case SkillType.Hammer: damage += (damage / 5); break;
                        case SkillType.Staff: damage += (damage / 5); break;
                    }
            }

            int hitChance = HitChance;

            damage = damage <= 0 ? 1 : damage;
            hitChance += 50;

            if (target.EvadeMelee(hitChance)) return false;

            if (Perks.Contains(NpcPerk.Poisonous))
                if (!target.EvadePoison()) target.SetMagicEffect(new Magic(MagicType.Poison, 30, 30));
            if (Perks.Contains(NpcPerk.CriticalPoisonous))
                if (!target.EvadePoison()) target.SetMagicEffect(new Magic(MagicType.Poison, 40, 65));

            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (character.IsAdmin) return false; // cant hurt admin!

                    // endurance damage amount and hammer strip
                    int armourDamage = 1; bool stripAttempt = false;
                    if (Weapon != null)
                    {
                        switch (Weapon.RelatedSkill)
                        {
                            case SkillType.Hammer: armourDamage = 20; stripAttempt = true; break;
                            case SkillType.Axe: armourDamage = 3; break;
                            default: armourDamage = 1; break;
                        }
                        armourDamage += Weapon.StripBonus;
                    }

                    // roll dice to get chance to hit particular item location
                    EquipType hitLocation;
                    int hitLocationChance = Dice.Roll(1, 10000);
                    if ((hitLocationChance >= 5000) && (hitLocationChance < 7500)) hitLocation = EquipType.Legs; // 25%
                    else if ((hitLocationChance >= 7500) && (hitLocationChance < 9000)) hitLocation = EquipType.Arms; // 15%
                    else if ((hitLocationChance >= 9000) && (hitLocationChance <= 10000)) hitLocation = EquipType.Head; // 10%
                    else hitLocation = EquipType.Body; // 50%

                    if (!character.AbsorbMelee(ref damage, hitLocation, armourDamage, stripAttempt, 0)) return false;

                    if (Perks.Contains(NpcPerk.DestructionOfMagic) && character.MagicEffects.ContainsKey(MagicType.Protect))
                        character.RemoveMagicEffect(MagicType.Protect);

                    character.TakeDamage(this, DamageType.Melee, damage, direction);
                    return true;
                case OwnerType.Npc:
                    Npc npc = (Npc)target;

                    if (damage == 0) return false;

                    npc.TakeDamage(this, DamageType.Melee, damage, direction);

                    if (Perks.Contains(NpcPerk.DestructionOfMagic) && npc.MagicEffects.ContainsKey(MagicType.Protect))
                        npc.RemoveMagicEffect(MagicType.Protect);
                    return true;
            }

            return false;
        }

        public bool Idle() { return Idle(mapX, mapY, direction); }
        public bool Idle(int sourceX, int sourceY, MotionDirection direction)
        {
            switch (NpcType)
            {
                case NpcType.CrusadeManaCollector:
                    if (!Cache.World.IsCrusade) return false;

                    if (IsBuilt)
                    {
                        actionCount++;
                        if (actionCount >= 3)
                        {
                            for (int y = sourceY - 5; y <= sourceY + 5; y++)
                                for (int x = sourceX - 5; x <= sourceX + 5; x++)
                                    if (map[y][x].IsOccupied)
                                        switch (map[y][x].Owner.OwnerType)
                                        {
                                            case OwnerType.Player:
                                                Character player = (Character)map[y][x].Owner;
                                                player.ReplenishMP(Dice.Roll(1, player.Magic + player.MagicBonus), false);
                                                break;
                                            case OwnerType.Npc:
                                                Npc npc = (Npc)map[y][x].Owner;
                                                if (npc.NpcType == NpcType.CrusadeManaStone)
                                                {
                                                    if (npc.ActionCount >= 3)
                                                        Cache.World.Crusade.CollectedMana[(int)side] += 3;
                                                    else Cache.World.Crusade.CollectedMana[(int)side] += npc.ActionCount;
                                                    npc.ActionCount = 0;
                                                }
                                                else npc.ReplenishMP(Dice.Roll(1, npc.Magic), false);
                                                break;
                                        }
                            actionCount = 0;
                        }
                    }
                    break;
                case NpcType.CrusadeManaStone:
                    if (!Cache.World.IsCrusade) return false;

                    actionCount++;
                    if (actionCount > 5) actionCount = 5;
                    break;
                case NpcType.CrusadeEnergyShield: break;
                case NpcType.CrusadeDetector:
                    if (!Cache.World.IsCrusade) return false;

                    if (IsBuilt)
                    {
                        actionCount++;
                        if (actionCount >= 3)
                        {
                            for (int y = sourceY - 10; y <= sourceY + 10; y++)
                                for (int x = sourceX - 10; x <= sourceX + 10; x++)
                                    if (map[y][x].IsOccupied && map[y][x].Owner.Side != side)
                                        map[y][x].Owner.RemoveMagicEffect(MagicType.Invisibility);

                            actionCount = 0;
                        }
                    }
                    break;
                case NpcType.CrusadeGrandMagicGenerator:
                    if (!Cache.World.IsCrusade) return false;

                    actionCount++;
                    if (actionCount >= 3)
                    {
                        if (Cache.World.Crusade.CollectedMana[(int)side] >= 15) // TODO - in config?
                        {
                            Cache.World.Crusade.CollectedMana[(int)side] = 0;

                            ReplenishMP(1, false);
                            MessageLogged(string.Format("{0} Grand Magic Generator mana: {1}", side.ToString(), mp), LogType.Events);
                            if (mp >= Cache.World.Crusade.ManaPerStrike)
                                switch (side)
                                {
                                    case OwnerSide.Elvine:
                                        if (Cache.World.Crusade.PrepareMeteorStrike(OwnerSide.Aresden))
                                        {
                                            foreach (Map map in Cache.World.Maps.Values)
                                                for (int p = 0; p < map.Players.Count; p++)
                                                    if (Cache.World.Players.ContainsKey(map.Players[p]))
                                                    {
                                                        Character player = Cache.World.Players[map.Players[p]];
                                                        if (player.CurrentMap.Name.Equals(Globals.ElvineTownName))
                                                            player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 1);
                                                        else player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 2);
                                                    }

                                            DepleteMP(mp, false);
                                        }
                                        break;
                                    case OwnerSide.Aresden:
                                        if (Cache.World.Crusade.PrepareMeteorStrike(OwnerSide.Elvine))
                                        {
                                            foreach (Map map in Cache.World.Maps.Values)
                                                for (int p = 0; p < map.Players.Count; p++)
                                                    if (Cache.World.Players.ContainsKey(map.Players[p]))
                                                    {
                                                        Character player = Cache.World.Players[map.Players[p]];
                                                        if (player.CurrentMap.Name.Equals(Globals.AresdenTownName))
                                                            player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 3);
                                                        else player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 4);
                                                    }

                                            DepleteMP(mp, false);
                                        }
                                        break;
                                }
                        }
                        actionCount = 0;
                    }
                    break;
                default: return false;
            }

            return true;
        }

        public bool Run(int sourceX, int sourceY, MotionDirection direction)
        {
            return Move();
        }

        public bool Fly(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;

            if (!Enum.IsDefined(typeof(MotionDirection), direction)) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;

            int destinationX = mapX, destinationY = mapY;
            switch (direction)
            {
                case MotionDirection.North: destinationY = destinationY - 1; break;
                case MotionDirection.NorthEast: destinationX = destinationX + 1; destinationY = destinationY - 1; break;
                case MotionDirection.East: destinationX = destinationX + 1; break;
                case MotionDirection.SouthEast: destinationX = destinationX + 1; destinationY = destinationY + 1; break;
                case MotionDirection.South: destinationY = destinationY + 1; break;
                case MotionDirection.SouthWest: destinationX = destinationX - 1; destinationY = destinationY + 1; break;
                case MotionDirection.West: destinationX = destinationX - 1; break;
                case MotionDirection.NorthWest: destinationX = destinationX - 1; destinationY = destinationY - 1; break;
            }

            if (!map[destinationY][destinationX].IsMoveable) return false;
            if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

            if (!map[destinationY][destinationX].IsBlocked)
            {
                map[mapY][mapX].ClearOwner();
                this.direction = direction;
                mapX = destinationX;
                mapY = destinationY;
                map[destinationY][destinationX].SetOwner(this);

                return true;
            }
            else return false;
        }

        private bool Retreat()
        {
            MotionDirection direction = map.GetNextDirection(target.X, target.Y, mapX, mapY, turn);

            bool ret = Move(mapX + Globals.MoveDirectionX[(int)direction], mapY + Globals.MoveDirectionY[(int)direction], direction);

            // retreat always be running
            isRunning = true;

            if (!ret)
            {
                failedActionCount++;
                return false;
            }
            else
            {
                failedActionCount = 0;
                return true;
            }
        }

        public bool Move()
        {
            // if no waypoint, waypoint reached or action keeps failing, then get a new waypoint
            if (waypoint == null || (waypoint.X == mapX && waypoint.Y == mapY) || (failedActionCount > 2))
                switch (moveType)
                {
                    case MovementType.None: return false; // static npcs such as shopkeeper
                    case MovementType.RandomArea:
                        if (isFriendly)
                        { if (IsSpawned && map.NpcSpawns[spawnId] != null) waypoint = map.NpcSpawns[spawnId].Zone.GetRandomLocation(); }
                        else
                        {
                            if (IsSpawned && map.MobSpawns[spawnId] != null) waypoint = map.MobSpawns[spawnId].Zone.GetRandomLocation();
                            else moveType = MovementType.Random; // if not spawned, set it to random
                        }
                        break;
                    case MovementType.Random:
                        waypoint = new Location(Dice.Roll(1, map.Width), Dice.Roll(1, map.Height));
                        break;
                    case MovementType.Guard:
                        break;
                    case MovementType.Follow:
                        if (IsSummoned && CurrentMap.Players.Contains(summoner))
                            waypoint = new Location(Cache.World.Players[summoner].X, Cache.World.Players[summoner].Y);
                        else moveType = MovementType.Random;
                        break;
                }

            // update waypoint if summoner has changed position
            if (action != MotionType.Attack && moveType == MovementType.Follow && IsSummoned && CurrentMap.Players.Contains(summoner))
            {
                if (waypoint.X != Cache.World.Players[summoner].X && waypoint.Y != Cache.World.Players[summoner].Y)
                    waypoint = new Location(Cache.World.Players[summoner].X, Cache.World.Players[summoner].Y);

                if (Cache.World.Players[summoner].IsWithinRange(this, 2, 2)) { return false; }
            }

            if (waypoint == null) { failedActionCount++; return false; }

            MotionDirection direction = map.GetNextDirection(mapX, mapY, waypoint.X, waypoint.Y, turn);

            bool ret = Move(mapX + Globals.MoveDirectionX[(int)direction], mapY + Globals.MoveDirectionY[(int)direction], direction);

            if (isHuman && (Math.Abs(mapX - waypoint.X) > 2 || Math.Abs(mapY - waypoint.Y) > 2))
                isRunning = true;
            else isRunning = false;

            if (!ret)
            {
                failedActionCount++;
                return false;
            }
            else
            {
                failedActionCount = 0;
                return true;
            }
        }

        public List<Location> GetLargeBossMoveTitles(MotionDirection direction)
        {
            List<Location> locations = new List<Location>();
            Location loc = new Location(0, 0);

            switch (direction)
            {
                case MotionDirection.North: // (X, Y)
                    {
                        loc.X = -1; loc.Y = -2; locations.Add(loc);
                        loc.X = 0; loc.Y = -2; locations.Add(loc);
                        loc.X = 1; loc.Y = -2; locations.Add(loc);
                        break;
                    }
                case MotionDirection.NorthEast:
                    {
                        loc.X = 0; loc.Y = -2; locations.Add(loc);
                        loc.X = 1; loc.Y = -2; locations.Add(loc);
                        loc.X = 2; loc.Y = -2; locations.Add(loc);
                        loc.X = 2; loc.Y = -1; locations.Add(loc);
                        loc.X = 2; loc.Y = 0; locations.Add(loc);
                        break;
                    }
                case MotionDirection.East:
                    {
                        loc.X = 2; loc.Y = -1; locations.Add(loc);
                        loc.X = 2; loc.Y = 0; locations.Add(loc);
                        loc.X = 2; loc.Y = 1; locations.Add(loc);
                        break;
                    }
                case MotionDirection.SouthEast:
                    {
                        loc.X = 0; loc.Y = 2; locations.Add(loc);
                        loc.X = 1; loc.Y = 2; locations.Add(loc);
                        loc.X = 2; loc.Y = 2; locations.Add(loc);
                        loc.X = 2; loc.Y = 1; locations.Add(loc);
                        loc.X = 2; loc.Y = 0; locations.Add(loc);
                        break;
                    }
                case MotionDirection.South:
                    {
                        loc.X = -1; loc.Y = 2; locations.Add(loc);
                        loc.X = 0; loc.Y = 2; locations.Add(loc);
                        loc.X = 1; loc.Y = 2; locations.Add(loc);
                        break;
                    }
                case MotionDirection.SouthWest:
                    {
                        loc.X = 0; loc.Y = 2; locations.Add(loc);
                        loc.X = -1; loc.Y = 2; locations.Add(loc);
                        loc.X = -2; loc.Y = 2; locations.Add(loc);
                        loc.X = -2; loc.Y = 1; locations.Add(loc);
                        loc.X = -2; loc.Y = 0; locations.Add(loc);
                        break;
                    }
                case MotionDirection.West:
                    {
                        loc.X = -2; loc.Y = -1; locations.Add(loc);
                        loc.X = -2; loc.Y = 0; locations.Add(loc);
                        loc.X = -2; loc.Y = 1; locations.Add(loc);
                        break;
                    }
                case MotionDirection.NorthWest:
                    {
                        loc.X = 0; loc.Y = -2; locations.Add(loc);
                        loc.X = -1; loc.Y = -2; locations.Add(loc);
                        loc.X = -2; loc.Y = -2; locations.Add(loc);
                        loc.X = -2; loc.Y = -1; locations.Add(loc);
                        loc.X = -2; loc.Y = 0; locations.Add(loc);
                        break;
                    }
                case MotionDirection.None:
                    {

                        loc.X = -1; loc.Y = -1; locations.Add(loc);
                        loc.X = 0; loc.Y = -1; locations.Add(loc);
                        loc.X = 1; loc.Y = -1; locations.Add(loc);
                        loc.X = -1; loc.Y = 0; locations.Add(loc);
                        loc.X = 0; loc.Y = 0; locations.Add(loc);
                        loc.X = 1; loc.Y = 0; locations.Add(loc);
                        loc.X = -1; loc.Y = 1; locations.Add(loc);
                        loc.X = 0; loc.Y = 1; locations.Add(loc);
                        loc.X = 1; loc.Y = 1; locations.Add(loc);
                        break;
                    }
            }
            return locations;
        }

        public bool Move(int destinationX, int destinationY, MotionDirection direction)
        {
            if (isDead) return false;
            if (magicEffects.ContainsKey(MagicType.Paralyze)) return false;

            switch (MonsterDifficulty)
            {
                case MonsterDifficulty.LargeBoss: //TODO THINK ABOUT THIS?
                                                  //{
                                                  //    List<Location> tiles = GetLargeBossMoveTitles(direction);

                //    //Test tile locations
                //    if (direction != MotionDirection.None)
                //    {
                //        foreach (Location loc in tiles)
                //        {
                //            if (!map[destinationY + loc.Y][destinationX + loc.X].IsMoveable) return false;
                //            if (destinationX + loc.X < 0 || destinationX + loc.X > map.Width || destinationY + loc.Y < 0 || destinationY + loc.Y > map.Height) return false; // map boundaries                     
                //            if (map[destinationY + loc.Y][destinationX + loc.X].IsBlocked) return false;
                //        }
                //    }

                //    //Clear Owner
                //    for (int yi = mapY - 1; yi <= mapY + 1; yi++)
                //    {
                //        for (int xi = mapX - 1; xi <= mapX + 1; xi++)
                //        {
                //            map[yi][xi].ClearOwner();
                //        }
                //    }

                //    //Update
                //    this.direction = direction;
                //    mapX = destinationX;
                //    mapY = destinationY;

                //    //Update Tiles
                //    for (int yi = mapY - 1; yi <= mapY + 1; yi++)
                //    {
                //        for (int xi = mapX - 1; xi <= mapX + 1; xi++)
                //        {
                //            map[yi][xi].SetOwner(this);

                //            // spike field
                //            if (map[yi][xi].IsDynamicOccupied &&
                //                map[yi][xi].DynamicObject.Type == DynamicObjectType.SpikeField)
                //            {
                //                Spikes spikes = (Spikes)map[destinationY][destinationX].DynamicObject;
                //                spikes.DoDamage(this);
                //            }
                //        }
                //    }
                //    return true;
                //}
                case MonsterDifficulty.Boss:
                case MonsterDifficulty.Normal:
                case MonsterDifficulty.None:
                    {
                        if (!map[destinationY][destinationX].IsMoveable) return false;
                        if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

                        if (!map[destinationY][destinationX].IsBlocked)
                        {
                            map[mapY][mapX].ClearOwner();
                            this.direction = direction;
                            mapX = destinationX;
                            mapY = destinationY;
                            map[destinationY][destinationX].SetOwner(this);

                            // spike field
                            if (map[destinationY][destinationX].IsDynamicOccupied &&
                                map[destinationY][destinationX].DynamicObject.Type == DynamicObjectType.SpikeField)
                            {
                                Spikes spikes = (Spikes)map[destinationY][destinationX].DynamicObject;
                                spikes.DoDamage(this);
                            }
                            return true;
                        }
                        else return false;
                    }
            }
            return false;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades the incoming attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded or not.</returns>
        public bool EvadeMelee(int attackerHitChance)
        {
            int evadeChance = dexterity * 2;
            if (magicEffects.ContainsKey(MagicType.Protect))
                switch (magicEffects[MagicType.Protect].Magic.Effect1)
                {
                    case 3: evadeChance += 40; break; // defence shield
                    case 4: evadeChance += 100; break; // greater defence shield
                }

            int hitChance = (int)(((double)attackerHitChance / (double)evadeChance) * 50.0f);

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else return true;
        }


        public bool AbsorbMelee(ref int damage, EquipType hitLocation, int armourDamage, bool stripAttempt, int pierceValue)
        {
            if (IsFriendly) return false;

            int absorption = PhysicalAbsorption;
            if (Perks.Contains(NpcPerk.AntiPhysical)) absorption += 25;
            if (absorption > Globals.MaximumPhysicalAbsorption) absorption = Globals.MaximumPhysicalAbsorption;
            if (pierceValue > 0) absorption -= pierceValue;

            //Physical absorption
            damage -= (int)(((double)damage / 100.0f) * (double)absorption);

            if (damage <= 0) return false;
            else return true;
        }

        public bool AbsorbMagic(MagicAttribute element, ref int damage, int pierceValue)
        {
            if (IsFriendly) return false;

            int absorption = MagicAbsorption;
            if (Perks.Contains(NpcPerk.AntiMagic)) absorption += 25;
            if (absorption > Globals.MaximumMagicalAbsorption) absorption = Globals.MaximumMagicalAbsorption;
            if (pierceValue > 0) absorption -= pierceValue;

            // Magic absorption
            damage -= (int)(((double)damage / 100.0f) * (double)absorption);

            // PFM
            if (MagicEffects.ContainsKey(MagicType.Protect) && MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                damage /= 2;

            if (damage <= 0) return false;
            else return true;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades a magic attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <param name="ignorePFM">Does this spell ignore PFM?</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded or not.</returns>
        public bool EvadeMagic(int attackerHitChance, bool ignorePFM)
        {
            if (attackerHitChance == -1) return true;

            int evadeChance = 0; // was magicresist in old configs. now calculated by their magic value
            if (magic > 50) evadeChance += (magic) - 50;

            if (magicEffects.ContainsKey(MagicType.Protect))
                if (magicEffects[MagicType.Protect].Magic.Effect1 == 5) return true; // AMP
                else if (MagicEffects[MagicType.Protect].Magic.Effect1 == 2 && !ignorePFM) return true; // PFM

            if (evadeChance < 1) evadeChance = 1;

            double temp1 = ((double)attackerHitChance / (double)evadeChance);
            double temp2 = ((double)(temp1 * 50.0f));
            int hitChance = (int)temp2;

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else return true;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades the being frozen or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded being frozen or not.</returns>
        public bool EvadeIce()
        {
            if (absorptionType == MagicAttribute.Water) return true; // ice/water monsters cant be frozen

            int evadeChance = 66; // TODO ice resistance previously determined by magicresistance - (magicresistance /3) (around 66% max chance)
            if (evadeChance < 1) evadeChance = 1;
            if (evadeChance > Globals.MaximumFreezeProtection) evadeChance = Globals.MaximumFreezeProtection;

            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades the being poisoned or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded being poisoned or not.</returns>
        public bool EvadePoison()
        {
            int evadeChance = 10; // static according to old sources
            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        public void ClearTarget()
        {
            target = null;

            if (moveType == MovementType.None)
                action = MotionType.AttackStationary;
            else action = MotionType.Move;
        }

        public void TargetSearch()
        {
            bool targetFound = false;
            int distance;

            if (HasObjective)
            {
                target = objective;
                targetFound = true;
            }
            else
            {
                for (int ix = mapX - searchRange; ix < mapX + searchRange * 2 + 1; ix++)
                    for (int iy = mapY - searchRange; iy < mapY + searchRange * 2 + 1; iy++)
                    {
                        if (targetFound) break;
                        if (map.Width <= ix || map.Height <= iy)
                        {
                            MessageLogged(string.Format("Npc target search out of bounds: {0}, {1}, wm: {2} wts: {3}, hm: {4}, hts: {5}", map.Name, this.Id, map.Width, ix, map.Height, iy), LogType.Error);

                        }

                        if (!(iy < 0 || iy > map.Height - 1 || ix < 0 || ix > map.Width - 1) && (map[iy][ix].IsOccupied))
                        {
                            IOwner owner = map[iy][ix].Owner;
                            if (owner == null) continue;

                            if (owner.Side == side &&
                                ((owner.OwnerType == OwnerType.Player && !((Character)owner).IsCriminal)
                                 || owner.OwnerType == OwnerType.Npc))
                                break; // dont attack same side unless criminal
                            if (owner.OwnerType == OwnerType.Player && ((Character)owner).IsAdmin) break;
                            if (side != OwnerSide.Neutral && owner.Side == OwnerSide.Neutral)
                                break; // aresden/elvine do not attack travellers
                            if (owner.OwnerType == OwnerType.Npc && owner.Side == OwnerSide.Neutral)
                                break; // doesnt attack neutral npcs
                            if (owner.IsInvisible && !Perks.Contains(NpcPerk.Clairvoyant)) break;

                            if ((mapX - owner.X) >= (mapY - owner.Y))
                                distance = mapX - owner.X;
                            else distance = mapY - owner.Y;

                            if (distance < 100)
                            {
                                target = owner;
                                targetFound = true;
                                break;
                            }
                        }
                    }
            }

            if (targetFound) action = MotionType.Attack;
        }

        public bool IsWithinRange(IOwner other, int rangeX, int rangeY) { return IsWithinRange(other, rangeX, rangeY, 0); }
        public bool IsWithinRange(IOwner other, int rangeX, int rangeY, int modifier)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        public bool IsWithinRange(IDynamicObject other, int rangeX, int rangeY, int modifier)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        public bool IsWithinRange(Location location, int range)
        {
            if ((mapX >= location.X - range) &&
                (mapX <= location.X + range) &&
                (mapY >= location.Y - range) &&
                (mapY <= location.Y + range))
                return true;
            else return false;
        }

        public bool IsWithinView(IOwner other)
        {
            int rangeX = (Globals.Resolutions[(int)Resolution.Standard, (int)ResolutionSetting.CellsWide] / 2) + Globals.OffScreenCells;
            int rangeY = (Globals.Resolutions[(int)Resolution.Standard, (int)ResolutionSetting.CellsHeigh] / 2) + Globals.OffScreenCells;

            if ((CurrentMap == other.CurrentMap) &&
                (mapX >= other.X - rangeX) &&
                (mapX <= other.X + rangeX) &&
                (mapY >= other.Y - rangeY) &&
                (mapY <= other.Y + rangeY))
                return true;
            else return false;
        }

        public bool IsValidTarget(IOwner owner)
        {
            if (owner.IsDead) return false;
            if (owner.IsRemoved) return false;
            if (owner.IsInvisible && !perks.Contains(NpcPerk.Clairvoyant)) return false;
            if (!IsWithinView(owner)) return false;

            return true;
        }

        public bool IsValidTarget(IOwner owner, int rangeX, int rangeY)
        {
            if (owner.IsDead) return false;
            if (owner.IsRemoved) return false;
            if (owner.IsInvisible && !perks.Contains(NpcPerk.Clairvoyant)) return false;
            if (!IsWithinRange(owner, rangeX, rangeY)) return false;

            return true;
        }

        public bool IsAlly(IOwner owner)
        {
            return (owner.Side == Side);
        }

        /// <summary>
        /// Removes this NPC from the map
        /// </summary>
        public bool Remove()
        {
            isRemoved = true;

            if (IsSpawned) map.MobSpawns[spawnId].Count--;
            //if (!IsSummoned && canDropLoot) DropLoot(LootDrop.Secondary); // body parts, rares etc
            if (isDead && map[mapY][mapX].DeadOwner == this) map[mapY][mapX].ClearDeadOwner();
            else if (map[mapY][mapX].Owner == this) map[mapY][mapX].ClearOwner(); //Could be bug?
            map.Npcs.Remove(objectId);

            return true;
        }

        public byte[] GetFullObjectData(Character requester)
        {
            byte[] data = new byte[46];

            Buffer.BlockCopy(objectId.GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(mapX.GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(mapY.GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(type.GetBytes(), 0, data, 6, 2);
            data[8] = (byte)((int)direction);
            //Buffer.BlockCopy(name.GetBytes(5), 0, data, 9, 5);
            Buffer.BlockCopy(Id.GetBytes(), 0, data, 9, 4);
            Buffer.BlockCopy(appearance1.GetBytes(), 0, data, 14, 2);
            Buffer.BlockCopy(appearance2.GetBytes(), 0, data, 16, 2);
            Buffer.BlockCopy(appearance3.GetBytes(), 0, data, 18, 2);
            Buffer.BlockCopy(appearance4.GetBytes(), 0, data, 20, 2);
            Buffer.BlockCopy(appearanceColour.GetBytes(), 0, data, 22, 4);
            int status = ((0x0FFFFFFF & this.status) | ((requester.GetNpcRelationship(this)) << 28));
            Buffer.BlockCopy(status.GetBytes(), 0, data, 26, 4);
            if (isDead)
                data[30] = 1;
            else data[30] = 0;
            data[31] = (byte)(int)side;
            data[32] = (byte)(int)SideStatus;

            // todo
            /*for (int i = 0; i < 7; i++)
            {
                data[31 + i] = (byte)(conditions.ContainsKey((ConditionGroup)i) ? conditions[(ConditionGroup)i].Magic.Effect1 : 0);
            }*/

            return data;
        }

        /// <summary>
        /// Id of the Npc as per the Npc.xml config
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Id generated by the World when initialized on the map
        /// </summary>
        public int ObjectId { set { objectId = value; } get { return objectId; } }
        public int SpawnID { set { spawnId = value; } get { return spawnId; } }
        public string Name { set { name = value; } get { return name; } }
        public string FriendlyName { get { return friendlyName; } set { friendlyName = value; } }
        public string Sprite { get { return sprite; } set { sprite = value; } }
        public int SpriteColour { get { return spriteColour; } set { spriteColour = value; } }
        public MovementType MoveType { get { return moveType; } set { moveType = value; } }
        public Guild Guild { get { return guild; } set { guild = value; } }
        public int Type { set { type = value; } get { return type; } }
        public NpcType NpcType { get { if (Enum.IsDefined(typeof(NpcType), type)) return (NpcType)type; else return NpcType.None; } }
        public bool IsHuman { get { return isHuman; } set { isHuman = value; } }
        public bool IsMerchant { get { return (merchantType != MerchantType.None); } }
        public int MerchantId { get { return merchantId; } set { merchantId = value; } }
        public MerchantType MerchantType { get { return merchantType; } set { merchantType = value; } }
        public bool IsRunning { get { return (isHuman && isRunning); } set { isRunning = value; } }
        public OwnerType OwnerType { get { return OwnerType.Npc; } }
        public int Level { get { return level; } set { level = value; } }
        public int Strength { get { return strength; } set { strength = value; } }
        public int Dexterity { set { dexterity = value; } get { return dexterity; } }
        public int Vitality { set { vitality = value; } get { return vitality; } }
        public int Magic { set { magic = value; } get { return magic; } }
        public int Intelligence { get { return intelligence; } set { intelligence = value; } }
        public string[] Spells { set { spells = value; } get { return spells; } }
        public int AttackRange { set { attackRange = value; } get { return attackRange; } }
        public int SearchRange { set { searchRange = value; } get { return searchRange; } }
        public OwnerSize Size { set { size = value; } get { return size; } }
        public OwnerSide Side { set { side = value; } get { return side; } }
        public MonsterDifficulty MonsterDifficulty { get { return monsterDifficulty; } set { monsterDifficulty = value; } }
        public OwnerSideStatus SideStatus { get { return (summoner != -1 ? OwnerSideStatus.Summon : OwnerSideStatus.Combatant); } }
        public MotionDirection Direction { get { return direction; } set { direction = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public int Status { get { return status; } set { status = value; } }
        public bool IsDead { get { return isDead; } }
        public bool IsRemoved { get { return isRemoved; } }
        public bool IsCorpseExploited { get { return isCorpseExploited; } set { isCorpseExploited = value; } }
        public DateTime DeadTime { get { return deadTime; } }
        public int Appearance1 { set { appearance1 = value; } get { return appearance1; } }
        public int Appearance2 { set { appearance2 = value; if (StatusChanged != null) StatusChanged(this); } get { return appearance2; } }
        public int Appearance3 { set { appearance3 = value; } get { return appearance3; } }
        public int Appearance4 { set { appearance4 = value; } get { return appearance4; } }
        public int AppearanceColour { set { appearanceColour = value; } get { return appearanceColour; } }

        public int MagicAbsorption
        {
            get
            {
                if (ma > Globals.MaximumMagicalAbsorption)
                    return Globals.MaximumMagicalAbsorption;
                else return ma;
            }
            set { ma = value; }
        }

        public int PhysicalAbsorption
        {
            get
            {
                if (pa > Globals.MaximumPhysicalAbsorption)
                    return Globals.MaximumPhysicalAbsorption;
                else return pa;
            }
            set { pa = value; }
        }

        public int HP
        {
            set { hp = value; }
            get { return hp; }
        }

        public int BuildType { set { buildType = value; } get { return buildType; } }
        public int BuildPoints { set { buildPoints = value; } get { return buildPoints; } }
        public int BuildLimit { set { buildLimit = value; } get { return buildLimit; } }

        /// <summary>
        /// Calculates the maximum hit points this Npc can have.
        /// </summary>
        public int MaxHP
        {
            get { return (vitality * 3) + (strength / 2); }
        }

        public int MP
        {
            set { mp = value; }
            get { return mp; }
        }

        /// <summary>
        /// Calculates the maximum magic points this Npc can have.
        /// </summary>
        public int MaxMP
        {
            get { return (magic * 3); }
        }

        public MotionType CurrentAction
        {
            get { return action; }
            set { action = value; }
        }

        public bool IsSpawned
        {
            get { return (spawnId != -1); }
        }

        public List<int> Summons { get { return summons; } set { summons = value; } }
        public bool IsSummoned { get { return (summoner != -1); } }
        public int Summoner
        {
            get { return summoner; }
            set
            {
                summoner = value;
                summonTime = DateTime.Now;
            }
        }

        public Dice BaseDamage
        {
            set { baseDamage = value; }
            get { return baseDamage; }
        }

        public TimeSpan MaximumDeadTime
        {
            get { return maximumDeadTime; }
            set { maximumDeadTime = value; }
        }

        public Dice Experience
        {
            get { return experience; }
            set { experience = value; }
        }

        public int RemainingExperience
        {
            get { return remainingExperience; }
            set { remainingExperience = value; }
        }

        /// <summary>
        /// Chance for this Npc to hit the target. Based on dexterity * 2 + an additional 30%.
        /// </summary>
        public int HitChance
        {
            get { return (dexterity * 2) + (int)((double)(dexterity * 2) * 0.3); }
        }

        public Dictionary<MagicType, MagicEffect> MagicEffects { get { return magicEffects; } }
        public Dictionary<ConditionGroup, MagicEffect> Conditions { get { return conditions; } }
        public TimeSpan ActionTime { get { return actionTime; } set { actionTime = value; } }
        public DateTime LastActionTime { get { return lastActionTime; } set { lastActionTime = value; } }
        public DateTime SummonTime { get { return summonTime; } set { summonTime = value; } }
        public bool IsFriendly { get { return isFriendly; } set { isFriendly = value; } }
        public bool IsPacifist { get { return isPacifist; } set { isPacifist = value; } }
        public bool HasTarget { get { return target != null; } }
        public bool HasObjective { get { return (objective != null); } }
        public IOwner Target { get { return target; } set { target = value; } }
        public IOwner Objective { get { return objective; } set { objective = value; } }
        public Boolean IsBerserked { get { return magicEffects.ContainsKey(MagicType.Berserk); } }
        public bool IsInvisible { get { return magicEffects.ContainsKey(MagicType.Invisibility); } }
        public bool IsBuilt { get { return buildPoints == 0; } }
        public int FailedActionCount { get { return failedActionCount; } set { failedActionCount = value; } }
        public int ActionCount { get { return actionCount; } set { actionCount = value; } }
        public int MaximumGold { get { return maxGold; } set { maxGold = value; } }
        public int StunChance { get { return 99; } }
        public List<NpcPerk> Perks { get { return perks; } }
        public DateTime LastDamagedTime { get { return lastDamagedTime; } set { lastDamagedTime = value; } }
        public int DropCount { get { return dropCount; } set { dropCount = value; } }
        public int DropX { get { return dropX; } }
        public int DropY { get { return dropY; } }

        public void ToggleCombatMode()
        {
            if (!IsHuman) return;

            //int appearance = ((this.Appearance2 & 0xF000) >> 12);

            //if (!IsCombatMode) this.Appearance2 = (0xF000 | Appearance2);
            //else this.Appearance2 = (0x0FFF & Appearance2);
            isCombatMode = !isCombatMode;

            if (StatusChanged != null) StatusChanged(this);
        }

        public AIDifficulty Difficulty { get { return aiDifficulty; } set { aiDifficulty = value; } }
        public AIBehaviour Behaviour { get { return aiBehaviour; } set { aiBehaviour = value; } }
        public AIClass Class { get { return aiClass; } set { aiClass = value; } }
        public AIEquipment Equipment { get { return aiEquipment; } set { aiEquipment = value; } }
        public AIState State { get { return aiState; } set { aiState = value; } }
        public List<Item> EquipedItems { get { return equipment; } set { equipment = value; } }

        /// <summary>
        /// Quick access to the Weapon currently equipped.
        /// </summary>
        public Item Weapon
        {
            get
            {
                if (equipment == null) return null;
                if (equipment[(int)EquipType.DualHand] != null)
                    return equipment[(int)EquipType.DualHand];
                else if (equipment[(int)EquipType.RightHand] != null)
                    return equipment[(int)EquipType.RightHand];
                else return null;
            }
        }

        /// <summary>
        /// Quick access to the Shield currently equipped.
        /// </summary>
        public Item Shield
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.LeftHand]; }
        }

        /// <summary>
        /// Quick access to the Necklace currently equipped.
        /// </summary>
        public Item Necklace
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Neck]; }
        }

        /// <summary>
        /// Quick access to the Helmet currently equipped.
        /// </summary>
        public Item Helmet
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Head]; }
        }

        /// <summary>
        /// Quick access to the Body Armour currently equipped.
        /// </summary>
        public Item BodyArmour
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Body]; }
        }

        /// <summary>
        /// Quick access to the Hauberk currently equipped.
        /// </summary>
        public Item Hauberk
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Arms]; }
        }

        /// <summary>
        /// Quick access to the Leggings currently equipped.
        /// </summary>
        public Item Leggings
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Legs]; }
        }

        /// <summary>
        /// Quick access to the Boots currently equipped.
        /// </summary>
        public Item Boots
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Feet]; }
        }

        /// <summary>
        /// Quick access to the Cape currently equipped.
        /// </summary>
        public Item Cape
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Back]; }
        }

        /// <summary>
        /// Quick access to the Angel currently equipped.
        /// </summary>
        public Item Angel
        {
            get { if (equipment == null) return null; else return equipment[(int)EquipType.Utility]; }
        }

        public Boolean IsCombatMode { get { return isCombatMode; } }
    }
}
