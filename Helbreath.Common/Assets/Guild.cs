﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets.Objects;

namespace Helbreath.Common.Assets
{
    public class GuildMember
    {
        private string id, name, rank;

        public GuildMember(string id, string name, string rank)
        {
            this.id = id;
            this.name = name;
            this.rank = rank;
        }

        public string ID { get { return id; } }
        public string Name { get { return name; } }
        public string Rank { get { return rank; } }
    }

    public class Guild : IEquatable<Guild>
    {
        private string name;
        private string id;
        private World world;
        private Dictionary<string, GuildMember> members; // for storing info even if character offline, could be used for guild member list
        private Location crusadeTeleportLocation;
        private Location crusadeBuildLocation;
        private List<Npc> crusadeStructures;

        private DateTime foundedOn;
        private string foundedBy;

        public Guild(string name, string id)
        {
            this.name = name;
            this.id = id;
            members = new Dictionary<string, GuildMember>();
        }

        public void SendMessage(string message)
        {

        }

        public void Send(CommandType type, CommandMessageType messageType)
        {
            // TODO
            world.Send(1, type, messageType);
        }

        public string Name { get { return name; } }
        public string ID { get { return id; } }
        public Dictionary<string, GuildMember> Members { get { return members; } }
        public Location CrusadeTeleportLocation { get { return crusadeTeleportLocation; } set { crusadeTeleportLocation = value; } }
        public Location CrusadeBuildLocation { get { return crusadeBuildLocation; } set { crusadeBuildLocation = value; } }
        public List<Npc> CrusadeStructures { get { return crusadeStructures; } set { crusadeStructures = value; } }
        public World World { get { return world; } set { world = value; } }
        public DateTime FoundedOn { get { return foundedOn; } set { foundedOn = value; } }
        public string FoundedBy { get { return foundedBy; } set { foundedBy = value; } }

        public bool Equals(Guild other)
        {
            return (other.Name.Equals(this.name));
        }

        bool IEquatable<Guild>.Equals(Guild other)
        {
            return (other.Name.Equals(this.name));
        }

        public static Guild None
        {
            get { return new Guild("NONE", ""); }
        }
    }
}
