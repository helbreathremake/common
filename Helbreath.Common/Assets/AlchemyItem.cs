﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Helbreath.Common.Assets
{
    public class AlchemyItem
    {
        private int itemId;
        private string name;
        private int difficulty;
        private int skillLimit;
        private Dictionary<int, int> ingredients;

        public AlchemyItem()
        {
            ingredients = new Dictionary<int, int>();
        }

        public static AlchemyItem ParseXml(XmlReader r)
        {
            AlchemyItem alchItem = new AlchemyItem();
            alchItem.ItemId = Int32.Parse(r["ItemId"]);
            alchItem.Difficulty = Int32.Parse(r["Difficulty"]);
            alchItem.SkillLimit = Int32.Parse(r["SkillLevel"]);
            XmlReader ingrediantReader = r.ReadSubtree();
            while (ingrediantReader.Read())
                if (ingrediantReader.IsStartElement() && ingrediantReader.Name.Equals("Ingrediant") && !alchItem.Ingredients.ContainsKey(Int32.Parse(r["ItemId"])))
                    alchItem.Ingredients.Add(Int32.Parse(r["ItemId"]), Int32.Parse(r["Count"]));
            ingrediantReader.Close();

            return alchItem;
        }

        public int ItemId { get { return itemId; } set { itemId = value; } }
        public string Name { get { return name; } set { name = value; } }
        public int Difficulty { get { return difficulty; } set { difficulty = value; } }
        public int SkillLimit { get { return skillLimit; } set { skillLimit = value; } }
        public Dictionary<int, int> Ingredients { get { return ingredients; } set { ingredients = value; } }
    }   
}
