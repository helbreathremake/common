﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml;

namespace Helbreath.Common.Assets
{
    public class Item : IEquatable<Item>
    {
        private int slotId;
        private int count;
        private int itemId;
        private string friendlyName;
        private int sprite; // appearance in bag/on ground
        private int spriteFrame; // appearance in bag/on ground
        private int colour;
        private GameColor colorType;
        private int appearance; // appearance on character
        private int levelLimit; 
        private int speed; 
        private bool identified;
        private Guid specificID;

        private ItemSet set;
        private ItemCategory category;
        private ItemRarity rarity;
        private EquipType equipType;
        private ItemType type;
        private ItemEffectType effectType;
        private GenderType genderLimit;
        private DrawEffectType drawEffect;
        private ItemQuality quality;
        private ItemUpgradeType upgradeType;

        private int baseMaximumEndurance;
        private int endurance;
        private int weight;
        private int price;
        private bool isUpgradeable;
        private bool isForSale;
        private bool hasExtendedRange;
        //private uint attribute;
        private int level; // v2
        private Dictionary<ItemStat, int> stats; // v2
        private int effect1;
        private int effect2;
        private int effect3;
        private int effect4;
        private int effect5;
        private int effect6;
        private ItemSpecialAbilityType specialAbilityType;
        private int specialEffect1;
        private int specialEffect2;
        private int specialEffect3;
        private SkillType relatedSkill;
        private int stripBonus;

        private int minimumStrength;
        private int minimumIntelligence;
        private int minimumDexterity;
        private int minimumMagic;
        private int minimumVitality;
        private int minimumAgility;

        // character bindings
        private string boundID;
        private string boundName;
        private bool isEquipped;
        private Location bagPosition;

        private DateTime dropTime;

        private int setNumber;
        private bool isUpgradeItem;
        private bool isUpgradeIngredient;

        // client
        private string spriteFile;
        private string spriteFileMale;
        private string spriteFileFemale;
        private int bagX;
        private int bagY;
        private int bagOldX;
        private int bagOldY;

        public Item()
        {
            //this.colour = (int)GameColor.Normal;
            colorType = GameColor.Normal;
            this.identified = true;
            stats = new Dictionary<ItemStat, int>();
            specificID = Guid.NewGuid();
        }

        public Item(int id)
        {
            this.itemId = id;
            //this.colour = (int)GameColor.Normal;
            colorType = GameColor.Normal;
            this.quality = ItemQuality.None;
            this.identified = true;
            stats = new Dictionary<ItemStat, int>();
            specificID = Guid.NewGuid();
        }

        public Item(int sprite, int spriteFrame, GameColor colorType)
        {
            this.sprite = sprite;
            this.spriteFrame = spriteFrame;
            //this.colour = colour;
            this.colorType = colorType; 
            this.quality = ItemQuality.None;
            this.identified = true;
            stats = new Dictionary<ItemStat, int>();
            specificID = Guid.NewGuid();
        }

        public void ParseData(byte[] data)
        {
            int index = 0;
            slotId = BitConverter.ToInt16(data, index);
            index+= 2;
            itemId = BitConverter.ToInt32(data, index);
            index += 4;
            count = BitConverter.ToInt32(data, index);
            index += 4;
            level = (int)data[index];
            index++;
            equipType = (EquipType)((int)data[index]);
            index++;
            isEquipped = ((int)data[index] == 1) ? true : false;
            index++;
            endurance = BitConverter.ToInt16(data, index);
            index += 2;
            genderLimit = (GenderType)((int)data[index]);
            index++;
            bagX = BitConverter.ToInt16(data, index);
            index += 2;
            bagY = BitConverter.ToInt16(data, index);
            index += 2;
            sprite = BitConverter.ToInt16(data, index);
            index += 2;
            spriteFrame = BitConverter.ToInt16(data, index);
            index += 2;
            colour = BitConverter.ToInt32(data, index);
            index += 4;
            specialEffect2 = BitConverter.ToInt32(data, index);
            index += 4;
            quality = (ItemQuality)(int)data[index];
            index++;
            level = (int)data[index];
            index++;
            int statCount = (int)data[index];
            index++;
            for (int i = 0; i < statCount; i++)
            {
                stats.Add((ItemStat)(int)data[index], (int)data[index+1]);
                index += 2;
            }
            setNumber = (int)data[index];
            index++;
            colorType = (GameColor)((int)data[index]);
            index++;
        }

        public static Item Parse(byte[] data)
        {
            Item i = new Item();
            i.ParseData(data);
            return i;
        }

        /// <summary>
        /// Creates an empty Item with Sprite, SpriteFrame and Colour set to 0. Used for emptying a cell on the client side.
        /// </summary>
        /// <returns>Item object with Sprite, SpriteFrame and Colour set to 0.</returns>
        public static Item Empty()
        {
            return new Item(-1, -1, GameColor.None);
        }

        public Item Copy() { return Copy(1); }
        public Item Copy(int itemCount)
        {
            Item item = new Item(itemId);
            item.FriendlyName = friendlyName;
            item.Count = itemCount;
            item.Type = type;
            item.EquipType = equipType;
            item.EffectType = effectType;
            item.Effect1 = effect1;
            item.Effect2 = effect2;
            item.Effect3 = effect3;
            item.Effect4 = effect4;
            item.Effect5 = effect5;
            item.Effect6 = effect6;
            item.BaseMaximumEndurance = baseMaximumEndurance;
            item.Endurance = MaximumEndurance;
            item.SpecialAbilityType = specialAbilityType;
            item.Sprite = sprite;
            item.SpriteFrame = spriteFrame;
            item.Price = price;
            item.Weight = weight;
            item.Appearance = appearance;
            item.Speed = speed;
            item.LevelLimit = levelLimit;
            item.GenderLimit = genderLimit;
            item.SpecialEffect1 = specialEffect1;
            item.SpecialEffect2 = specialEffect2;
            item.RelatedSkill = relatedSkill;
            item.Category = category;
            item.Set = set;
            item.Colour = colour;
            item.colorType = colorType;
            item.Rarity = rarity;
            item.IsForSale = isForSale;
            item.IsUpgradeable = isUpgradeable;
            item.HasExtendedRange = hasExtendedRange;
            item.DrawEffect = drawEffect;
            item.SpriteFile = spriteFile;
            item.SpriteFileMale = spriteFileMale;
            item.SpriteFileFemale = spriteFileFemale;
            item.Quality = quality;
            item.UpgradeType = upgradeType;

            // item bindings - new/copied items are unbound
            item.BoundID = Globals.UnboundItem;
            item.BoundName = "Unbound";
            item.bagPosition = new Location(40, 30);

            //Every item has a different Guid
            item.specificID = Guid.NewGuid();

            return item;
        }

        public bool CanUpgrade(Item ingredient)
        {
            if (level >= Globals.MaximumItemLevel) return false;
            if (ingredient.upgradeType !=  ItemUpgradeType.Ingredient) return false;

            switch (upgradeType)
            {
                case ItemUpgradeType.Xelima: if (ingredient.EffectType == ItemEffectType.Xelima) return true; break;
                case ItemUpgradeType.Merien: if (ingredient.EffectType == ItemEffectType.Merien) return true; break;
                case ItemUpgradeType.Majestic:
                case ItemUpgradeType.Additive: break;
            }

            return false;
        }

        public double UpgradeChance()
        {
            double baseModifier = 0;

            switch (quality)
            {
                case ItemQuality.Flimsy: baseModifier += 0; break;
                case ItemQuality.Sturdy: baseModifier += 4; break;
                case ItemQuality.Reinforced: baseModifier += 8; break;
                case ItemQuality.Studded: baseModifier += 12; break;
            }

            switch (rarity)
            {
                case ItemRarity.VeryCommon: baseModifier += 0; break;
                case ItemRarity.Common: baseModifier += 4; break;
                case ItemRarity.Uncommon: baseModifier += 8; break;
                case ItemRarity.Rare: baseModifier += 12; break;
                case ItemRarity.VeryRare: baseModifier += 16; break;
                case ItemRarity.UltraRare: baseModifier += 20; break;
            }

            double baseChance = 100 - baseModifier;
            double chance = Math.Max(1, baseChance - (((double)level) * (baseChance / (double)Globals.MaximumItemLevel)));

            if (upgradeType == ItemUpgradeType.Majestic || upgradeType == ItemUpgradeType.Additive)
                chance = 100;

            return chance;
        }

        public int MajesticUpgradeCost() //TODO Expand on this
        {
            int cost = (level * (level + 6) / 8) + 2;

            return cost;
        }

        public static Item ParseXml(XmlReader r)
        {
            Item item = new Item(Int32.Parse(r["Id"]));
            item.FriendlyName = (r["FriendlyName"] != null) ? r["FriendlyName"] : "Missing Name";
            ItemType itemType;
            if (Enum.TryParse<ItemType>(r["Type"], out itemType))
                item.Type = itemType;
            EquipType equipType;
            if (Enum.TryParse<EquipType>(r["EquipType"], out equipType))
                item.EquipType = equipType;
            else item.EquipType = EquipType.None;
            ItemEffectType effectType;
            if (Enum.TryParse<ItemEffectType>(r["EffectType"], out effectType))
                item.EffectType = effectType;
            ItemRarity rarity;
            if (Enum.TryParse<ItemRarity>(r["Rarity"], out rarity))
                item.Rarity = rarity;
            else item.Rarity = ItemRarity.None; // means it cannot drop. fail safe in case configs are incorrect and rares drop too common!
            item.Effect1 = Int32.Parse(r["Effect1"]);
            item.Effect2 = Int32.Parse(r["Effect2"]);
            item.Effect3 = Int32.Parse(r["Effect3"]);
            item.Effect4 = Int32.Parse(r["Effect4"]);
            item.Effect5 = Int32.Parse(r["Effect5"]);
            item.Effect6 = Int32.Parse(r["Effect6"]);
            item.BaseMaximumEndurance = Int32.Parse(r["MaximumEndurance"]);
            ItemSpecialAbilityType specialAbilityType;
            if (Enum.TryParse<ItemSpecialAbilityType>(r["SpecialAbilityType"], out specialAbilityType))
                item.SpecialAbilityType = specialAbilityType;
            item.Sprite = Int32.Parse(r["Sprite"]);
            item.SpriteFrame = Int32.Parse(r["SpriteFrame"]);
            if (r["Price"] != null)
                item.Price = Int32.Parse(r["Price"]);
            else item.Price = 0;
            item.Appearance = Int32.Parse(r["Appearance"]);
            item.Speed = Int32.Parse(r["Speed"]);
            item.LevelLimit = Int32.Parse(r["LevelLimit"]);
            GenderType gender;
            if (Enum.TryParse<GenderType>(r["GenderLimit"], out gender))
                item.GenderLimit = gender;
            else item.GenderLimit = GenderType.None;
            item.SpecialEffect1 = Int32.Parse(r["SpecialEffect1"]);
            SkillType skill;
            if (Enum.TryParse<SkillType>(r["RelatedSkill"], out skill))
                item.RelatedSkill = skill;
            else item.RelatedSkill = SkillType.None;
            ItemCategory category;
            if (r["Category"] != null && Enum.TryParse<ItemCategory>(r["Category"], out category))
                item.Category = category;
            else item.Category = ItemCategory.None;
            ItemSet set;
            if (r["ItemSet"] != null && Enum.TryParse<ItemSet>(r["ItemSet"], out set))
                item.Set = set;
            else item.Set = ItemSet.None;
            item.IsForSale = (r["IsForSale"] != null) ? Boolean.Parse(r["IsForSale"]) : false;
            item.IsUpgradeable = (r["IsUpgradeable"] != null) ? Boolean.Parse(r["IsUpgradeable"]) : false;
            item.HasExtendedRange = (r["ExtendedRange"] != null) ? Boolean.Parse(r["ExtendedRange"]) : false;
            item.MinimumStrength = (r["MinimumStrength"] != null) ? (Int32.Parse(r["MinimumStrength"]) > Globals.MaximumStat + 10) ? Globals.MaximumStat + 10 : Int32.Parse(r["MinimumStrength"]) : 0;
            item.MinimumDexterity = (r["MinimumDexterity"] != null) ? Int32.Parse(r["MinimumDexterity"]) : 0;
            item.MinimumVitality = (r["MinimumVitality"] != null) ? Int32.Parse(r["MinimumVitality"]) : 0;
            item.MinimumMagic = (r["MinimumMagic"] != null) ? Int32.Parse(r["MinimumMagic"]) : 0;
            item.MinimumIntelligence = (r["MinimumIntelligence"] != null) ? Int32.Parse(r["MinimumIntelligence"]) : 0;
            item.MinimumAgility = (r["MinimumAgility"] != null) ? Int32.Parse(r["MinimumAgility"]) : 0;
            item.Weight = (r["Weight"] != null) ? Int32.Parse(r["Weight"]) : Math.Min(item.MinimumStrength * 100, Globals.MaximumStat * 100);
            item.StripBonus = (r["StripBonus"] != null) ? Int32.Parse(r["StripBonus"]) : 0;
            ItemUpgradeType upgrade;
            if (r["UpgradeType"] != null && Enum.TryParse<ItemUpgradeType>(r["UpgradeType"], out upgrade))
                item.UpgradeType = upgrade;
            else item.UpgradeType = ItemUpgradeType.None;

            if (r["SpecialEffect2"] != null)
            {
                string s = r["SpecialEffect2"];
                if (item.Category == ItemCategory.Dyes && s.StartsWith("#"))
                    item.SpecialEffect2 = Math.Min((int)GameColor.Normal, ColorTranslator.FromHtml(s).ToArgb());
                else item.SpecialEffect2 = Int32.Parse(s);
            }

            if (r["Colour"] != null)
            {
                string s = r["Colour"];
                if (s.StartsWith("#"))
                    item.Colour = Math.Min((int)GameColor.Normal, ColorTranslator.FromHtml(s).ToArgb());
                else item.Colour = Math.Min((int)GameColor.Normal, Int32.Parse(s));
            }
            //TODO - if custom color loaded, assign custom to colorType
            item.colorType = GameColor.Normal;

            if (r["SpriteFileMale"] != null) 
                item.SpriteFileMale = r["SpriteFileMale"].ToString();
            else item.SpriteFileMale = string.Empty;
            if (r["SpriteFileFemale"] != null) 
                item.SpriteFileFemale = r["SpriteFileFemale"].ToString();
            else item.SpriteFileFemale = string.Empty;

            // overrides male/female
            if (r["SpriteFile"] != null) 
                item.SpriteFileMale = item.SpriteFileFemale = item.SpriteFile = r["SpriteFile"].ToString();
            else item.SpriteFile = string.Empty;

            if (r["DrawEffect"] != null)
            {
                DrawEffectType drawEffect;
                if (Enum.TryParse<DrawEffectType>(r["DrawEffect"], out drawEffect))
                    item.DrawEffect = drawEffect;
                else item.DrawEffect = DrawEffectType.None;
            }
            return item;
        }

        public void GenerateStats(int dropLevel, int luckBonus)
        {
            // exclude certain items and non-equipment from stats
            switch (equipType)
            {
                case EquipType.LeftFinger:
                case EquipType.RightFinger:
                case EquipType.Utility:
                case EquipType.Neck:
                case EquipType.None: return;
            }

            int probability;
            
            ///* DEFINE QUALITY */
            //if (dropLevel <= 2) quality = ItemQuality.Flimsy; // 100% chance flimsy
            //else if (dropLevel > 2 && dropLevel <= 5)
            //{
            //    probability = Dice.Roll(1, 100);
            //    if (probability >= 75) quality = ItemQuality.Sturdy; // 25% chance sturdy
            //    else quality = ItemQuality.Flimsy; // 75% chance flimsy
            //}
            //else if (dropLevel > 5 && dropLevel <= 9)
            //{
            //    probability = Dice.Roll(1, 100);
            //    if (probability >= 75) quality = ItemQuality.Reinforced; // 25% chance reinforced
            //    else if (probability >= 30) quality = ItemQuality.Sturdy; // 45% chance sturdy
            //    else quality = ItemQuality.Flimsy; // 30% chance flimsy
            //}
            //else if (dropLevel >= 10)
            //{
            //    probability = Dice.Roll(1, 100);
            //    if (probability >= 75) quality = ItemQuality.Studded; // 25% chance studded
            //    else if (probability >= 30) quality = ItemQuality.Reinforced; // 45% chance reinforce
            //    else quality = ItemQuality.Sturdy; // 30% chance flimsy
            //}

            probability = Dice.Roll(1, 100);
            if (probability >= 80) quality = ItemQuality.Studded; // 25% chance studded
            else if (probability >= 60) quality = ItemQuality.Reinforced; // 45% chance reinforce
            else if (probability >= 40) quality = ItemQuality.Sturdy; // 30% chance flimsy
            else if (probability >= 20) quality = ItemQuality.Flimsy;
            else quality = ItemQuality.None;

            //Color based off of quality
            switch (quality)
            {
                case ItemQuality.Flimsy: colorType = GameColor.Flimsy; break;
                case ItemQuality.Sturdy: colorType = GameColor.Sturdy; break;
                case ItemQuality.Reinforced: colorType = GameColor.Reinforced; break;
                case ItemQuality.Studded: colorType = GameColor.Studded; break;
                case ItemQuality.None:
                default: break;
            }

            /* DEFINE STATS */
            stats = new Dictionary<ItemStat, int>();

            ItemStat stat;
            int statValue = 1;
            int statCount = 5; //TODO FIX forced stat count

            // chances for more stats, maximum of 5
            //probability = Dice.Roll(1, 100);
            //if (probability > 50) statCount++; // 50% chance for 2 stats
            //if (probability > 75 && dropLevel > 2) statCount++; // 25% chance for 3 stats
            //if (probability > 85 && dropLevel > 3) statCount++; // 15% chance for 4 stats
            //if (probability > 95 && dropLevel > 5) statCount++; // 5% chance for 5 stats

            // get list of potential stats based on item type
            List<ItemStat> statsList = new List<ItemStat>();
            foreach (ItemStat type in (ItemStat[])Enum.GetValues(typeof(ItemStat)))
                if (!statsList.Contains(type))
                {
                    switch (effectType)
                    {
                        case ItemEffectType.Attack:
                        case ItemEffectType.AttackActivation:
                        case ItemEffectType.AttackBow:
                        case ItemEffectType.AttackDefence:
                        case ItemEffectType.AttackMagic:
                        case ItemEffectType.AttackManaSave:
                        case ItemEffectType.AttackMaxHPDown:
                            // add potential stats that effect weapons (40 - 79)
                            if ((int)type >= 40 && (int)type < 80) statsList.Add(type);
                            break;
                        case ItemEffectType.Defence:
                        case ItemEffectType.DefenceActivation:
                        case ItemEffectType.DefenceAntiMine:
                            // add potential stats that effect armour (0 - 39)
                            if ((int)type >= 0 && (int)type < 40) statsList.Add(type);
                            break;
                    }

                    // add potential stats that effect both (80 to 200)
                    if ((int)type >= 80 && (int)type < 200) statsList.Add(type);
                }


            // start adding stats to the item
            for (int i = 0; i < statCount; i++)
            {
                if (statsList.Count <= 0) break; // no available potential stats left in the list (possibly due to removal from conflicts)

                // generate a stat
                probability = Dice.Roll(1, statsList.Count - 1);
                stat = (ItemStat)statsList[probability];

                // generate a value
                probability = 30000;
                //probability = Dice.Roll(1, 30000);    --/TODO Adjust to normal, changed for testing purposes 

                if ((probability >= 1) && (probability < 5110)) statValue = 1;  // 5110/30000 = 17%
                else if ((probability >= 5110) && (probability < 9610)) statValue = 2;  // 4500/30000 = 15%
                else if ((probability >= 9610) && (probability < 13610)) statValue = 3;  // 4000/30000 = 13.3%
                else if ((probability >= 13610) && (probability < 17110)) statValue = 4;  // 3500/30000 = 11.6%
                else if ((probability >= 17110) && (probability < 20110)) statValue = 5;  // 3000/30000 = 10%
                else if ((probability >= 20110) && (probability < 22360)) statValue = 6;  // 2250/30000 = 7.5%
                else if ((probability >= 22360) && (probability < 23860)) statValue = 7;  // 1500/30000 = 5%
                else if ((probability >= 23860) && (probability < 25110)) statValue = 8;  // 1250/30000 = 4.1%
                else if ((probability >= 25110) && (probability < 26110)) statValue = 9;  // 1000/30000 = 3.33%
                else if ((probability >= 26110) && (probability < 27010)) statValue = 10; // 900/30000 = 3%
                else if ((probability >= 27010) && (probability < 28740)) statValue = 11; // 750/30000 = 2.5%
                else if ((probability >= 28740) && (probability < 29240)) statValue = 12; // 500/30000 = 1.6%
                else if ((probability >= 29240) && (probability < 29540)) statValue = 13; // 300/30000 = 1%
                else if ((probability >= 29540) && (probability < 29740)) statValue = 14; // 200/30000 = 0.66%
                else if ((probability >= 29740) && (probability < 29840)) statValue = 15; // 100/30000 = 0.33%
                else if ((probability >= 29840) && (probability < 29915)) statValue = 16; // 75/30000 = 0.25%
                else if ((probability >= 29915) && (probability < 29965)) statValue = 17; // 50/30000 = 0.16%
                else if ((probability >= 29965) && (probability < 29985)) statValue = 18; // 20/30000 = 0.06%
                else if ((probability >= 29985) && (probability < 29995)) statValue = 19; // 10/30000 = 0.03%
                else if ((probability >= 29995) && (probability <= 30000)) statValue = 20; // 6/30000 = 0.02%
                else statValue = 1;
                
                // luck bonus
                if (luckBonus > 0) statValue = Math.Min(20, statValue + Dice.Roll(1, luckBonus));

                //// value limits based on quality 
                //switch (quality)
                //{
                //    case ItemQuality.Flimsy: statValue = Math.Min(statValue, 5); break;
                //    case ItemQuality.Sturdy: statValue = Math.Min(statValue, 12); break;
                //    case ItemQuality.Reinforced: statValue = Math.Min(statValue, 18); break;
                //    case ItemQuality.Studded: break; // no limit
                //}

                // value limits, stat-specific
                switch (stat)
                {
                    case ItemStat.Critical: statValue = Math.Max(statValue, 5); break; // minimum 5
                    case ItemStat.Poison: statValue = Math.Max(statValue, 4); break; // minimum 4
                    case ItemStat.Endurance: statValue = Math.Max(statValue, 2); break; // minimum 2
                    case ItemStat.HittingProbability: statValue = Math.Max(statValue, 3); break; // minimum 3
                    case ItemStat.Agile: if (statValue > 10) statValue = 2; else statValue = 1; break; // maximum 2 (-26 strength)
                    case ItemStat.Gold:
                    case ItemStat.Experience:
                    case ItemStat.Vampiric:
                    case ItemStat.Zealous:
                    case ItemStat.HPIncrease:
                    case ItemStat.MPIncrease:
                    case ItemStat.SPIncrease:
                    case ItemStat.MagicAbsorption: statValue = Math.Max(statValue / 2, 1); break; // minimum 1, maximum 10
                }

                // remove stat from potential list
                statsList.Remove(stat);

                // remove conflicting stats from the potential list
                switch (stat)
                {
                    case ItemStat.Sharp: statsList.Remove(ItemStat.Ancient); statsList.Remove(ItemStat.Legendary); break;
                    case ItemStat.Ancient: statsList.Remove(ItemStat.Sharp); statsList.Remove(ItemStat.Legendary); break;
                    case ItemStat.Legendary: statsList.Remove(ItemStat.Sharp); statsList.Remove(ItemStat.Ancient); break;
                    case ItemStat.Zealous: statsList.Remove(ItemStat.Vampiric); statsList.Remove(ItemStat.Exhaustive); break;
                    case ItemStat.Vampiric: statsList.Remove(ItemStat.Zealous); statsList.Remove(ItemStat.Exhaustive); break;
                    case ItemStat.Exhaustive: statsList.Remove(ItemStat.Zealous); statsList.Remove(ItemStat.Vampiric); break;
                    case ItemStat.ManaConverting: statsList.Remove(ItemStat.CriticalChance); break;
                    case ItemStat.CriticalChance: statsList.Remove(ItemStat.ManaConverting); break;
                }
                       
                // colour the item accoring to certain stats (if not already)
                // TODO - add weighting to certain colours. e.g. legendary should take precendence over critical
                //if (colour == (int)GameColour.Normal)
                    switch (stat)
                    {
                        case ItemStat.Endurance: colorType = GameColor.Strong; break;
                        case ItemStat.Critical: colorType = GameColor.Critical; break;
                        case ItemStat.Agile: colorType = GameColor.Agile; break;
                        case ItemStat.Righteous: colorType = GameColor.Righteous; break;
                        case ItemStat.Poison: colorType = GameColor.Poison; break;
                        case ItemStat.Sharp: colorType = GameColor.Sharp; break;
                        case ItemStat.Ancient: colorType = GameColor.Ancient; break;
                        case ItemStat.Legendary: colorType = GameColor.Legendary; break;
                    }

                // finally, add the calculated stat and value to our item list
                if (!stats.ContainsKey(stat))
                    stats.Add(stat, statValue);
            }

            /* RANDOMIZE REMAINING ENDURANCE */
            probability = Dice.Roll(1, 100);
            endurance = (int)(((double)MaximumEndurance / 100) * probability);
        }

        /*public void GenerateAttribute()
        {
            int result, primaryValue, secondaryValue;
            ItemSpecialWeaponPrimaryType primaryType = ItemSpecialWeaponPrimaryType.None;
            ItemSpecialWeaponSecondaryType secondaryType = ItemSpecialWeaponSecondaryType.None;
            switch (effectType)
            {
                case ItemEffectType.Attack:
                    result = Dice.Roll(1, 10000);
                    if ((result >= 1) && (result <= 299)) {           primaryType = ItemSpecialWeaponPrimaryType.Light;     colour = (int)GameColour.Light; }
                    else if ((result >= 300) && (result <= 999)) {    primaryType = ItemSpecialWeaponPrimaryType.Endurance; colour = (int)GameColour.Strong; }
                    else if ((result >= 1000) && (result <= 2499)) {  primaryType = ItemSpecialWeaponPrimaryType.Critical;  colour = (int)GameColour.Critical; ; }
                    else if ((result >= 2500) && (result <= 4499)) {  primaryType = ItemSpecialWeaponPrimaryType.Agile;     colour = (int)GameColour.Agile; }
                    else if ((result >= 4500) && (result <= 6499)) {  primaryType = ItemSpecialWeaponPrimaryType.Righteous; colour = (int)GameColour.Righteous; }
                    else if ((result >= 6500) && (result <= 8099)) {  primaryType = ItemSpecialWeaponPrimaryType.Poison;    colour = (int)GameColour.Poison; }
                    else if ((result >= 8100) && (result <= 9699)) {  primaryType = ItemSpecialWeaponPrimaryType.Sharp;     colour = (int)GameColour.Sharp; }
                    else if ((result >= 9700) && (result <= 9999)) {  primaryType = ItemSpecialWeaponPrimaryType.Ancient;   colour = (int)GameColour.Ancient; }
                    else if ((result == 10000))                    {  primaryType = ItemSpecialWeaponPrimaryType.Legendary; colour = (int)GameColour.Legendary; }

                    result = Dice.Roll(1, 30000);
                    if ((result >= 1) && (result < 10000)) primaryValue = 1;  // 10000/29348 = 34%
                    else if ((result >= 10000) && (result < 17400)) primaryValue = 2;  // 6600/29348 = 22.4%
                    else if ((result >= 17400) && (result < 22400)) primaryValue = 3;  // 4356/29348 = 14.8%
                    else if ((result >= 22400) && (result < 25400)) primaryValue = 4;  // 2874/29348 = 9.7%
                    else if ((result >= 25400) && (result < 27400)) primaryValue = 5;  // 1897/29348 = 6.4%
                    else if ((result >= 27400) && (result < 28400)) primaryValue = 6;  // 1252/29348 = 4.2%
                    else if ((result >= 28400) && (result < 28900)) primaryValue = 7;  // 826/29348 = 2.8%
                    else if ((result >= 28900) && (result < 29300)) primaryValue = 8;  // 545/29348 = 1.85%
                    else if ((result >= 29300) && (result < 29700)) primaryValue = 9;  // 360/29348 = 1.2%
                    else if ((result >= 29700) && (result < 29880)) primaryValue = 10; // 237/29348 = 0.8%
                    else if ((result >= 29880) && (result < 29950)) primaryValue = 11; // 156/29348 = 0.5%
                    else if ((result >= 29950) && (result < 29990)) primaryValue = 12; // 103/29348 = 0.3%
                    else if ((result >= 29990) && (result <= 30000)) primaryValue = 13; // 68/29348 = 0.1%
                    else primaryValue = 1;

                    switch (primaryType)
                    {
                        case ItemSpecialWeaponPrimaryType.Critical: primaryValue = Math.Max(primaryValue, 5); break;
                        case ItemSpecialWeaponPrimaryType.Poison: primaryValue = Math.Max(primaryValue, 4); break;
                        case ItemSpecialWeaponPrimaryType.Light: primaryValue = Math.Max(primaryValue, 4); break;
                        case ItemSpecialWeaponPrimaryType.Endurance: primaryValue = Math.Max(primaryValue, 2); break;
                    }
                    // TODO - genlevel <= 2, max primary value is 7

                    attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));

                    if (Dice.Roll(1, 100) <= 40) // 40% chance to get second stat
                    {
                        result = Dice.Roll(1, 10000);
                        if ((result >= 1) && (result <= 4999))          secondaryType = ItemSpecialWeaponSecondaryType.HittingProbability;
                        else if ((result >= 5000) && (result <= 8499))  secondaryType = ItemSpecialWeaponSecondaryType.ConsecutiveDamage;
                        else if ((result >= 8500) && (result <= 9499))  secondaryType = ItemSpecialWeaponSecondaryType.Gold;
                        else if ((result >= 9500) && (result <= 10000)) secondaryType = ItemSpecialWeaponSecondaryType.Experience;

                        result = Dice.Roll(1, 30000);
                        if ((result >= 1) && (result < 10000)) secondaryValue = 1;  // 10000/29348 = 34%
                        else if ((result >= 10000) && (result < 17400)) secondaryValue = 2;  // 6600/29348 = 22.4%
                        else if ((result >= 17400) && (result < 22400)) secondaryValue = 3;  // 4356/29348 = 14.8%
                        else if ((result >= 22400) && (result < 25400)) secondaryValue = 4;  // 2874/29348 = 9.7%
                        else if ((result >= 25400) && (result < 27400)) secondaryValue = 5;  // 1897/29348 = 6.4%
                        else if ((result >= 27400) && (result < 28400)) secondaryValue = 6;  // 1252/29348 = 4.2%
                        else if ((result >= 28400) && (result < 28900)) secondaryValue = 7;  // 826/29348 = 2.8%
                        else if ((result >= 28900) && (result < 29300)) secondaryValue = 8;  // 545/29348 = 1.85%
                        else if ((result >= 29300) && (result < 29700)) secondaryValue = 9;  // 360/29348 = 1.2%
                        else if ((result >= 29700) && (result < 29880)) secondaryValue = 10; // 237/29348 = 0.8%
                        else if ((result >= 29880) && (result < 29950)) secondaryValue = 11; // 156/29348 = 0.5%
                        else if ((result >= 29950) && (result < 29990)) secondaryValue = 12; // 103/29348 = 0.3%
                        else if ((result >= 29990) && (result <= 30000)) secondaryValue = 13; // 68/29348 = 0.1%
                        else secondaryValue = 1;

                        switch (secondaryType)
                        {
                            case ItemSpecialWeaponSecondaryType.HittingProbability: secondaryValue = Math.Max(secondaryValue, 3); break;
                            case ItemSpecialWeaponSecondaryType.ConsecutiveDamage:  secondaryValue = Math.Min(secondaryValue, 7); break;
                            case ItemSpecialWeaponSecondaryType.Experience:         secondaryValue = 2; break;
                            case ItemSpecialWeaponSecondaryType.Gold:               secondaryValue = Math.Min(secondaryValue, 10); break; // 3% - 30%
                        }
                        // TODO - genlevel <= 2, max primary value is 7

                        attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));
                    }

                    break;
                case ItemEffectType.AttackManaSave:
                    primaryType = ItemSpecialWeaponPrimaryType.CastingProbability;
                    colour = (int)GameColour.Critical;

                    result = Dice.Roll(1, 30000);
                    if ((result >= 1) && (result < 10000)) primaryValue = 1;  // 10000/29348 = 34%
                    else if ((result >= 10000) && (result < 17400)) primaryValue = 2;  // 6600/29348 = 22.4%
                    else if ((result >= 17400) && (result < 22400)) primaryValue = 3;  // 4356/29348 = 14.8%
                    else if ((result >= 22400) && (result < 25400)) primaryValue = 4;  // 2874/29348 = 9.7%
                    else if ((result >= 25400) && (result < 27400)) primaryValue = 5;  // 1897/29348 = 6.4%
                    else if ((result >= 27400) && (result < 28400)) primaryValue = 6;  // 1252/29348 = 4.2%
                    else if ((result >= 28400) && (result < 28900)) primaryValue = 7;  // 826/29348 = 2.8%
                    else if ((result >= 28900) && (result < 29300)) primaryValue = 8;  // 545/29348 = 1.85%
                    else if ((result >= 29300) && (result < 29700)) primaryValue = 9;  // 360/29348 = 1.2%
                    else if ((result >= 29700) && (result < 29880)) primaryValue = 10; // 237/29348 = 0.8%
                    else if ((result >= 29880) && (result < 29950)) primaryValue = 11; // 156/29348 = 0.5%
                    else if ((result >= 29950) && (result < 29990)) primaryValue = 12; // 103/29348 = 0.3%
                    else if ((result >= 29990) && (result <= 30000)) primaryValue = 13; // 68/29348 = 0.1%
                    else primaryValue = 1;

                    // TODO - genlevel <= 2, max primary value is 7

                    attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));

                    if (Dice.Roll(1, 100) <= 40) // 40% chance to get second stat
                    {
                        result = Dice.Roll(1, 10000);
                        if ((result >= 1) && (result <= 4999))          secondaryType = ItemSpecialWeaponSecondaryType.HittingProbability;
                        else if ((result >= 5000) && (result <= 8499))  secondaryType = ItemSpecialWeaponSecondaryType.ConsecutiveDamage;
                        else if ((result >= 8500) && (result <= 9499))  secondaryType = ItemSpecialWeaponSecondaryType.Gold;
                        else if ((result >= 9500) && (result <= 10000)) secondaryType = ItemSpecialWeaponSecondaryType.Experience;

                        result = Dice.Roll(1, 30000);
                        if ((result >= 1) && (result < 10000)) secondaryValue = 1;  // 10000/29348 = 34%
                        else if ((result >= 10000) && (result < 17400)) secondaryValue = 2;  // 6600/29348 = 22.4%
                        else if ((result >= 17400) && (result < 22400)) secondaryValue = 3;  // 4356/29348 = 14.8%
                        else if ((result >= 22400) && (result < 25400)) secondaryValue = 4;  // 2874/29348 = 9.7%
                        else if ((result >= 25400) && (result < 27400)) secondaryValue = 5;  // 1897/29348 = 6.4%
                        else if ((result >= 27400) && (result < 28400)) secondaryValue = 6;  // 1252/29348 = 4.2%
                        else if ((result >= 28400) && (result < 28900)) secondaryValue = 7;  // 826/29348 = 2.8%
                        else if ((result >= 28900) && (result < 29300)) secondaryValue = 8;  // 545/29348 = 1.85%
                        else if ((result >= 29300) && (result < 29700)) secondaryValue = 9;  // 360/29348 = 1.2%
                        else if ((result >= 29700) && (result < 29880)) secondaryValue = 10; // 237/29348 = 0.8%
                        else if ((result >= 29880) && (result < 29950)) secondaryValue = 11; // 156/29348 = 0.5%
                        else if ((result >= 29950) && (result < 29990)) secondaryValue = 12; // 103/29348 = 0.3%
                        else if ((result >= 29990) && (result <= 30000)) secondaryValue = 13; // 68/29348 = 0.1%
                        else secondaryValue = 1;

                        switch (secondaryType)
                        {
                            case ItemSpecialWeaponSecondaryType.HittingProbability: secondaryValue = Math.Max(secondaryValue, 3); break;
                            case ItemSpecialWeaponSecondaryType.ConsecutiveDamage:  secondaryValue = Math.Min(secondaryValue, 7); break;
                            case ItemSpecialWeaponSecondaryType.Experience:         secondaryValue = 2; break;
                            case ItemSpecialWeaponSecondaryType.Gold:               secondaryValue = Math.Min(secondaryValue, 30); break;
                        }
                        // TODO - genlevel <= 2, max primary value is 7

                        attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));
                    }
                    break;
                case ItemEffectType.Defence: 
                    result = Dice.Roll(1, 10000);
                    if ((result >= 1) && (result <= 5999)) {             primaryType = ItemSpecialWeaponPrimaryType.Endurance; }
                    else if ((result >= 6000) && (result <= 8999)) {     primaryType = ItemSpecialWeaponPrimaryType.Light; }
                    else if ((result >= 9000) && (result <= 9554)) {     primaryType = ItemSpecialWeaponPrimaryType.ManaConverting; }
                    else if ((result >= 9555) && (result <= 10000)) {    primaryType = ItemSpecialWeaponPrimaryType.CriticalChance; }

                    result = Dice.Roll(1, 30000);
                    if ((result >= 1) && (result < 10000)) primaryValue = 1;  // 10000/29348 = 34%
                    else if ((result >= 10000) && (result < 17400)) primaryValue = 2;  // 6600/29348 = 22.4%
                    else if ((result >= 17400) && (result < 22400)) primaryValue = 3;  // 4356/29348 = 14.8%
                    else if ((result >= 22400) && (result < 25400)) primaryValue = 4;  // 2874/29348 = 9.7%
                    else if ((result >= 25400) && (result < 27400)) primaryValue = 5;  // 1897/29348 = 6.4%
                    else if ((result >= 27400) && (result < 28400)) primaryValue = 6;  // 1252/29348 = 4.2%
                    else if ((result >= 28400) && (result < 28900)) primaryValue = 7;  // 826/29348 = 2.8%
                    else if ((result >= 28900) && (result < 29300)) primaryValue = 8;  // 545/29348 = 1.85%
                    else if ((result >= 29300) && (result < 29700)) primaryValue = 9;  // 360/29348 = 1.2%
                    else if ((result >= 29700) && (result < 29880)) primaryValue = 10; // 237/29348 = 0.8%
                    else if ((result >= 29880) && (result < 29950)) primaryValue = 11; // 156/29348 = 0.5%
                    else if ((result >= 29950) && (result < 29990)) primaryValue = 12; // 103/29348 = 0.3%
                    else if ((result >= 29990) && (result <= 30000)) primaryValue = 13; // 68/29348 = 0.1%
                    else primaryValue = 1;

                    switch (primaryType)
                    {
                        case ItemSpecialWeaponPrimaryType.ManaConverting:
                        case ItemSpecialWeaponPrimaryType.CriticalChance: primaryValue = Math.Max(((primaryValue+1)/2), 1); break;
                        case ItemSpecialWeaponPrimaryType.Light: primaryValue = Math.Max(primaryValue, 4); break;
                        case ItemSpecialWeaponPrimaryType.Endurance: primaryValue = Math.Max(primaryValue, 2); break;
                    }
                    // TODO - genlevel <= 2, max primary value is 7.
                    // TODO - genlevel <= 3, for mana and crit, max primary value is 2
                    // TODO - capes max light and endurance is value 1

                    attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));

                    if (Dice.Roll(1, 100) <= 40) // 40% chance to get second stat
                    {
                        result = Dice.Roll(1, 10000);
                        if ((result >= 1) && (result <= 999)) secondaryType = ItemSpecialWeaponSecondaryType.PhysicalResistance;
                        else if ((result >= 1000) && (result <= 3999)) secondaryType = ItemSpecialWeaponSecondaryType.PoisonResistance;
                        else if ((result >= 4000) && (result <= 5499)) secondaryType = ItemSpecialWeaponSecondaryType.SPRecovery;
                        else if ((result >= 5500) && (result <= 6499)) secondaryType = ItemSpecialWeaponSecondaryType.HPRecovery;
                        else if ((result >= 6500) && (result <= 7499)) secondaryType = ItemSpecialWeaponSecondaryType.MPRecovery;
                        else if ((result >= 7500) && (result <= 9399)) secondaryType = ItemSpecialWeaponSecondaryType.MagicResistance;
                        else if ((result >= 9400) && (result <= 9799)) secondaryType = ItemSpecialWeaponSecondaryType.PhysicalAbsorption;
                        else if ((result >= 9800) && (result <= 10000)) secondaryType = ItemSpecialWeaponSecondaryType.MagicAbsorption;

                        result = Dice.Roll(1, 30000);
                        if ((result >= 1) && (result < 10000)) secondaryValue = 1;  // 10000/29348 = 34%
                        else if ((result >= 10000) && (result < 17400)) secondaryValue = 2;  // 6600/29348 = 22.4%
                        else if ((result >= 17400) && (result < 22400)) secondaryValue = 3;  // 4356/29348 = 14.8%
                        else if ((result >= 22400) && (result < 25400)) secondaryValue = 4;  // 2874/29348 = 9.7%
                        else if ((result >= 25400) && (result < 27400)) secondaryValue = 5;  // 1897/29348 = 6.4%
                        else if ((result >= 27400) && (result < 28400)) secondaryValue = 6;  // 1252/29348 = 4.2%
                        else if ((result >= 28400) && (result < 28900)) secondaryValue = 7;  // 826/29348 = 2.8%
                        else if ((result >= 28900) && (result < 29300)) secondaryValue = 8;  // 545/29348 = 1.85%
                        else if ((result >= 29300) && (result < 29700)) secondaryValue = 9;  // 360/29348 = 1.2%
                        else if ((result >= 29700) && (result < 29880)) secondaryValue = 10; // 237/29348 = 0.8%
                        else if ((result >= 29880) && (result < 29950)) secondaryValue = 11; // 156/29348 = 0.5%
                        else if ((result >= 29950) && (result < 29990)) secondaryValue = 12; // 103/29348 = 0.3%
                        else if ((result >= 29990) && (result <= 30000)) secondaryValue = 13; // 68/29348 = 0.1%
                        else secondaryValue = 1;

                        switch (secondaryType)
                        {
                            case ItemSpecialWeaponSecondaryType.PoisonResistance:
                            case ItemSpecialWeaponSecondaryType.PhysicalResistance:
                            case ItemSpecialWeaponSecondaryType.MagicResistance:
                            case ItemSpecialWeaponSecondaryType.PhysicalAbsorption:
                            case ItemSpecialWeaponSecondaryType.MagicAbsorption: secondaryValue = Math.Max(secondaryValue, 3); break;
                        }
                        // TODO - genlevel <= 2, max primary value is 7

                        attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));
                    }
                    break;
            }
        }*/

        /*public static uint CalculateAttribute(ItemSpecialWeaponPrimaryType primaryType, int primaryValue, ItemSpecialWeaponSecondaryType secondaryType, int secondaryValue)
        {
            uint attribute;

            # region Abbreviations
            switch (primaryType)
            {
                case ItemSpecialWeaponPrimaryType.CP: primaryType = ItemSpecialWeaponPrimaryType.CastingProbability; break;
                case ItemSpecialWeaponPrimaryType.C: primaryType = ItemSpecialWeaponPrimaryType.Critical; break;
                case ItemSpecialWeaponPrimaryType.CC: primaryType = ItemSpecialWeaponPrimaryType.CriticalChance; break;
                case ItemSpecialWeaponPrimaryType.MC: primaryType = ItemSpecialWeaponPrimaryType.ManaConverting; break;
                case ItemSpecialWeaponPrimaryType.P: primaryType = ItemSpecialWeaponPrimaryType.Poison; break;
            }

            switch (secondaryType)
            {
                case ItemSpecialWeaponSecondaryType.HP: secondaryType = ItemSpecialWeaponSecondaryType.HittingProbability; break;
                case ItemSpecialWeaponSecondaryType.HPR: secondaryType = ItemSpecialWeaponSecondaryType.HPRecovery; break;
                case ItemSpecialWeaponSecondaryType.MA: secondaryType = ItemSpecialWeaponSecondaryType.MagicAbsorption; break;
                case ItemSpecialWeaponSecondaryType.MR: secondaryType = ItemSpecialWeaponSecondaryType.MagicResistance; break;
                case ItemSpecialWeaponSecondaryType.MPR: secondaryType = ItemSpecialWeaponSecondaryType.MPRecovery; break;
                case ItemSpecialWeaponSecondaryType.PA: secondaryType = ItemSpecialWeaponSecondaryType.PhysicalAbsorption; break;
                case ItemSpecialWeaponSecondaryType.DR: secondaryType = ItemSpecialWeaponSecondaryType.PhysicalResistance; break;
                case ItemSpecialWeaponSecondaryType.PR: secondaryType = ItemSpecialWeaponSecondaryType.PoisonResistance; break;
            }
            #endregion

            switch (primaryType)
            {
                case ItemSpecialWeaponPrimaryType.Agile: primaryValue = 1; break;
                case ItemSpecialWeaponPrimaryType.Ancient: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.CastingProbability: primaryValue = Math.Min((primaryValue % 3 == 0) ? primaryValue / 3 : 1, 13); break;
                case ItemSpecialWeaponPrimaryType.Critical: primaryValue = Math.Min(primaryValue, 13) ; break;
                case ItemSpecialWeaponPrimaryType.CriticalChance: primaryValue = Math.Min(primaryValue, 7); break;
                case ItemSpecialWeaponPrimaryType.Endurance: primaryValue = Math.Min((primaryValue % 7 == 0) ? primaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponPrimaryType.Light: primaryValue = Math.Min((primaryValue % 7 == 0) ? primaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponPrimaryType.ManaConverting: primaryValue = Math.Min(primaryValue, 7); break;
                case ItemSpecialWeaponPrimaryType.None: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.Poison: primaryValue = Math.Min(primaryValue, 13); break;
                case ItemSpecialWeaponPrimaryType.Righteous: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.Sharp: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.Legendary: primaryValue = 0; break;
            }

            switch (secondaryType)
            {
                case ItemSpecialWeaponSecondaryType.ConsecutiveDamage: secondaryValue = Math.Min(secondaryValue, 7); break;
                case ItemSpecialWeaponSecondaryType.Experience: secondaryValue = Math.Min((secondaryValue % 10 == 0) ? secondaryValue / 10 : 1, 2); break;
                case ItemSpecialWeaponSecondaryType.Gold: secondaryValue = Math.Min((secondaryValue % 3 == 0) ? secondaryValue / 3 : 1, 10); break; // 3% - 30%
                case ItemSpecialWeaponSecondaryType.HittingProbability: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.HPRecovery: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.MagicAbsorption: secondaryValue = Math.Min((secondaryValue % 3 == 0) ? secondaryValue / 3 : 1, 13); break;
                case ItemSpecialWeaponSecondaryType.MagicResistance: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.MPRecovery: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.None: secondaryValue = 0; break;
                case ItemSpecialWeaponSecondaryType.PhysicalAbsorption: secondaryValue = Math.Min((secondaryValue % 3 == 0) ? secondaryValue / 3 : 1, 13); break;
                case ItemSpecialWeaponSecondaryType.PhysicalResistance: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.PoisonResistance: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.SPRecovery: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
            }

            attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));
            attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));

            return attribute;
        }*/

        /// <summary>
        /// Builds the byte array of this item to send to the client.
        /// </summary>
        /// <returns>Data to send to Client</returns>
        public byte[] GetData(int slot)
        {
            byte[] data = new byte[37 + (stats.Count*2)]; // 
            int size = 0;

            Buffer.BlockCopy(slot.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes(itemId), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)level;
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)(isEquipped ? 1 : 0);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)bagPosition.X), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)bagPosition.Y), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(colour.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(specialEffect2.GetBytes(), 0, data, size, 4);
            size += 4;

            /*Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            size += 4;*/
            // v2
            data[size] = (byte)(int)quality;
            size++;
            data[size] = (byte)level;
            size++;
            data[size] = (byte)stats.Count;
            size++;
            foreach (KeyValuePair<ItemStat, int> stat in stats)
            {
                data[size] = (byte)(int)stat.Key;
                size++;
                data[size] = (byte)(int)stat.Value;
                size++;
            }
            data[size] = (byte)setNumber;
            size++;
            data[size] = (byte)((int)colorType);
            size++;

            return data;
        }
        /*public byte[] GetData(int slot)
        {
            byte[] data = new byte[36];
            int size = 0;

            Buffer.BlockCopy(slot.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes(itemId), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)(isEquipped ? 1 : 0);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)levelLimit), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)weight), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(colour.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(specialEffect2.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            size += 4;

            return data;
        }*/

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it has moved from inventory to warehouse.
        /// </summary>
        /// <returns>Data to send to Client = 51 bytes.</returns>
        public byte[] GetInventoryToWarehouseData(int index)
        {
            byte[] data = new byte[37];
            int size = 0;

            data[size] = (byte)index;
            size++;
            data[size] = (byte)1;
            size++;
            Buffer.BlockCopy(itemId.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(count.GetBytes(), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)0; // not equipped
            size++;
            Buffer.BlockCopy(levelLimit.GetBytes(), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(endurance.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(weight.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(sprite.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(spriteFrame.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(colour.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(effect2.GetBytes(), 0, data, size, 2);
            size += 2;
            //Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            //size += 4;
            // TODO - fix for itemStat v2
            data[size] = (byte)specialEffect2;
            size++;
            data[size] = (byte)specialEffect3;
            size++;
            data[size] = (byte)((int)colorType);
            size++;

            return data;
        }

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it has just been purchased from shop.
        /// </summary>
        /// <param name="price">The price paid for this item. Including discounts.</param>
        /// <returns>Data to send to Client - 43 bytes.</returns>
        public byte[] GetPurchasedData(short price)
        {
            byte[] data = new byte[31];
            int size = 0;

            data[size] = (byte)1;
            size++;
            Buffer.BlockCopy(itemId.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)level;
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)0; // not equipped
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)levelLimit), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)weight), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(colour.GetBytes(), 0, data, size, 4);
            size += 4;
            Buffer.BlockCopy(price.GetBytes(), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)colorType);
            size++;

            return data;
        }

        /// <summary>
        /// Calculates the gold value of this item based on various factors such as endurance and stats
        /// </summary>
        /// <param name="maximumEndurance">Determines whether to get the gold value based on maximum endurance instead of remaining endurance.</param>
        /// <param name="modifier">TODO</param>
        /// <returns>Gold value as an integer</returns>
        public int GetGoldValue(bool maximumEndurance = false, int modifier = 0)
        {
            int baseValue = price; // base value
            if (stats.Count > 0) baseValue = Math.Max(baseValue, (250 + (stats.Count * 200))); // minimum 250-1000 if statted

            int value = baseValue;

            // value modifiers
            foreach (KeyValuePair<ItemStat, int> stat in stats)
            {
                switch (stat.Key)
                {
                    case ItemStat.Sharp: value += (int)((double)baseValue * 2.5); break; // 250% value
                    case ItemStat.Ancient: value += (int)((double)baseValue * 3); break; // 300% value
                    case ItemStat.Legendary: value += (int)((double)baseValue * 3.5); break; // 350% value
                    case ItemStat.HPRecovery:
                    case ItemStat.MPRecovery: 
                        if (stat.Value >= 10) // mp/hp rec >= 50%
                            value += (int)(((double)baseValue * 1.5) * stat.Value); // + 150% value per stat after 10
                        else value += (int)(((double)baseValue * 0.75) * stat.Value); // +75% value per stat points
                        break;
                    case ItemStat.Piercing:
                    case ItemStat.MaxCrits: 
                        if (stat.Value >= 6)
                            value += (int)(((double)baseValue * 1.3) * stat.Value); // + 130% value per stat
                        else value += (int)(((double)baseValue * 0.65) * stat.Value); // +65% value per stat points
                        break;
                    case ItemStat.Experience:
                    case ItemStat.Gold:
                    case ItemStat.Vampiric:
                    case ItemStat.Zealous:
                    case ItemStat.Exhaustive:
                        if (stat.Value >= 7)
                            value += (int)(((double)baseValue * 1.2) * stat.Value); // + 120% value per stat
                        else value += (int)(((double)baseValue * 0.6) * stat.Value); // +60% value per stat points
                        break;
                    case ItemStat.Poison:
                    case ItemStat.Righteous:
                    case ItemStat.Critical:
                    case ItemStat.ManaConverting:
                    case ItemStat.CriticalChance:
                    case ItemStat.ComboDamage:
                        if (stat.Value >= 12)
                            value += (int)(((double)baseValue * 1.18) * stat.Value); // + 118% value per stat
                        else value += (int)(((double)baseValue * 0.59) * stat.Value); // +59% value per stat points
                        break;
                    case ItemStat.MagicAbsorption:
                    case ItemStat.HPIncrease:
                    case ItemStat.MPIncrease:
                        if (stat.Value >= 5)
                            value += (int)(((double)baseValue * 1.17) * stat.Value); // + 117% value per stat
                        else value += (int)(((double)baseValue * 0.58) * stat.Value); // +58% value per stat points
                        break;
                    case ItemStat.CastingProbability:
                    case ItemStat.HittingProbability: 
                        if (stat.Value >= 15)
                            value += (int)(((double)baseValue * 1.16) * stat.Value); // + 116% value per stat
                        else value += (int)(((double)baseValue * 0.57) * stat.Value); // +57% value per stat points
                        break;
                    case ItemStat.AirAbsorption:
                    case ItemStat.EarthAbsorption:
                    case ItemStat.FireAbsorption:
                    case ItemStat.WaterAbsorption:
                    case ItemStat.PhysicalAbsorption: 
                        if (stat.Value >= 6)
                            value += (int)(((double)baseValue * 1.15) * stat.Value); // + 115% value per stat
                        else value += (int)(((double)baseValue * 0.56) * stat.Value); // +56% value per stat points
                        break;
                    case ItemStat.MagicResistance:
                    case ItemStat.PhysicalResistance: 
                        if (stat.Value >= 14)
                            value += (int)(((double)baseValue * 1.15) * stat.Value); // + 115% value per stat
                        else value += (int)(((double)baseValue * 0.55) * stat.Value); // +55% value per stat points
                        break;
                    case ItemStat.SPIncrease:
                    case ItemStat.SPRecovery:
                    case ItemStat.PoisonResistance: 
                        if (stat.Value >= 15)
                            value += (int)(((double)baseValue * 1.13) * stat.Value); // + 113% value per stat
                        value += (int)(((double)baseValue * 0.54) * stat.Value); // +54% value per stat points
                        break;
                    case ItemStat.Endurance:
                        if (stat.Value >= 17)
                            value += (int)(((double)baseValue) * stat.Value); // + 100% per stat
                        else value += (int)(((double)baseValue * 0.5) * stat.Value); // + 50% per stat
                        break;
                    case ItemStat.Agile: 
                        if (stat.Value == 2)
                            value += (int)(((double)baseValue) * stat.Value); // + 100% per stat
                        else value += (int)(((double)baseValue * 0.5) * stat.Value); // + 50% per stat
                        break; // +50% value per stat points
                }

                // 25% additional increase for each stat
                value += (int)(((double)baseValue * 0.25));
            }

            // level affects value
            if (level > 0)
            {
                if (level > 10) value += (int)(((double)baseValue * 2) * level); // +200% per level if over lvl 10
                else value += (int)(((double)baseValue) * level); // +100% per level if below or equal to lvl 10
            }

            // quality affects value
            switch (quality)
            {
                case ItemQuality.Flimsy: value = (int)((double)value * 0.33); break;
                case ItemQuality.Sturdy: value = (int)((double)value * 0.66); break;
                case ItemQuality.Reinforced: break; // no mod
                case ItemQuality.Studded: value = (int)((double)value * 1.33); break;
            }

            if (!maximumEndurance)
            {
                // endurance affects value
                double enduranceValue = ((double)endurance / MaximumEndurance); // get percentage of endurance remaining

                value = (int)((double)value * enduranceValue);
            }

            return Math.Max(value,1); // minimum 1 gold
        }

        public int GetRepairCost()
        {
            int cost = 0;

            double endurance = (((double)Endurance / (double)MaximumEndurance) * 100);
            double enduranceToRepair = 100 - endurance;

            cost = (int)(((double)(GetGoldValue(true) / 2) / 100) * enduranceToRepair);

            // repair commission
            switch (quality)
            {
                case ItemQuality.Flimsy: cost = (int)((double)cost * 1.05); break; // 5% commission
                case ItemQuality.Sturdy: cost = (int)((double)cost * 1.1); break; // 10% commission
                case ItemQuality.Reinforced: cost = (int)((double)cost * 1.25); break; // 25% commission
                case ItemQuality.Studded: cost = (int)((double)cost * 1.5); break; // 50% commission
            }

            return cost;
        }

        public int MinimumStrength { get { return minimumStrength; } set { minimumStrength = value; } }
        public int MinimumDexterity { get { return minimumDexterity; } set { minimumDexterity = value; } }
        public int MinimumVitality { get { return minimumVitality; } set { minimumVitality = value; } }
        public int MinimumIntelligence { get { return minimumIntelligence; } set { minimumIntelligence = value; } }
        public int MinimumMagic { get { return minimumMagic; } set { minimumMagic = value; } }
        public int MinimumAgility { get { return minimumAgility; } set { minimumAgility = value; } }

        public int Sprite
        {
            set { sprite = value; }
            get { return sprite; }
        }

        public int SpriteFrame
        {
            set { spriteFrame = value; }
            get { return spriteFrame; }
        }

        public int Colour { set { colour = value; } get { return colour; } }

        public GameColor ColorType { get { return colorType; } set { colorType = value; } }

        public int ItemId
        {
            set { itemId = value; }
            get { return itemId; }
        }
        public string FriendlyName { get { return friendlyName; } set { friendlyName = value; } }
        public int SlotId { get { return slotId; } set { slotId = value; } }
        public int Count
        {
            set { count = value; }
            get { return count; }
        }

        public ItemType Type
        {
            set { type = value; }
            get { return type; }
        }

        public EquipType EquipType
        {
            set { equipType = value; }
            get { return equipType; }
        }

        public int LevelLimit
        {
            set { levelLimit = value; }
            get { return levelLimit; }
        }

        public GenderType GenderLimit { set { genderLimit = value; } get { return genderLimit; } }
        public DrawEffectType DrawEffect { get { return drawEffect; } set { drawEffect = value; } }
        public string SpriteFile { get { return spriteFile; } set { spriteFile = value; } }
        public string SpriteFileMale { get { return spriteFileMale; } set { spriteFileMale = value; } }
        public string SpriteFileFemale { get { return spriteFileFemale; } set { spriteFileFemale = value; } }
        public int Endurance { set { endurance = value; } get { return endurance; } }
        public int Weight { set { weight = value; } get { return weight; } }
        public bool IsEquipped { set { isEquipped = value; } get { return isEquipped; } }
        public Location BagPosition { set { bagPosition = value; } get { return bagPosition; } }
        public int SpecialEffect1 { set { specialEffect1 = value; } get { return specialEffect1; } }
        public int SpecialEffect2 { set { specialEffect2 = value; } get { return specialEffect2; } }
        public int SpecialEffect3 { set { specialEffect3 = value; } get { return specialEffect3; } }
        public Dictionary<ItemStat, int> Stats { get { return stats; } }

        public int Appearance{ set { appearance = value; } get { return appearance; } }
        public bool Identified { set { identified = value; } get { return identified; } }

        public int Speed
        {
            set { speed = value; }
            get
            {
                if (stats.ContainsKey(ItemStat.Agile))
                    return speed - 1;
                else return speed;
            }
        }

        public int Price { get { return price; } set { price = value; } }
        public bool IsForSale { get { return isForSale; } set { isForSale = value; } }
        public bool IsBroken { get { return endurance <= 0; } }
        public bool HasExtendedRange { get { return hasExtendedRange; } set { hasExtendedRange = value; } }

        public int Effect1
        {
            get { return effect1; }
            set { effect1 = value; }
        }
        public int Effect2
        {
            get { return effect2; }
            set { effect2 = value; }
        }
        public int Effect3
        {
            get { return effect3; }
            set { effect3 = value; }
        }
        public int Effect4
        {
            get { return effect4; }
            set { effect4 = value; }
        }
        public int Effect5
        {
            get { return effect5; }
            set { effect5 = value; }
        }
        public int Effect6
        {
            get { return effect6; }
            set { effect6 = value; }
        }

        public int BaseMaximumEndurance { get { return baseMaximumEndurance; } set { baseMaximumEndurance = value; } }
        public int MaximumEndurance
        {
            get
            {
                int max = BaseMaximumEndurance;

                if (level > 0)
                    max += (max / 100) * level;
                 
                if (stats.ContainsKey(ItemStat.Endurance))
                    max += ((baseMaximumEndurance / 100) * stats[ItemStat.Endurance] * 5);

                switch (quality)
                {
                    case ItemQuality.Flimsy: max = (int)((double)max * 0.33); break;
                    case ItemQuality.Sturdy: max = (int)((double)max * 0.66); break;
                    case ItemQuality.Reinforced: break; // no mod
                    case ItemQuality.Studded: max = (int)((double)max * 1.33); break;
                }
                
                return max;
            }
        }

        public ItemRarity Rarity
        {
            set { rarity = value; }
            get { return rarity; }
        }

        public ItemQuality Quality
        {
            set { quality = value; }
            get { return quality; }
        }
        public ItemUpgradeType UpgradeType { get { return upgradeType; } set { upgradeType = value; } }

        public ItemEffectType EffectType
        {
            get { return effectType; }
            set { effectType = value; }
        }

        public ItemSpecialAbilityType SpecialAbilityType
        {
            get { return specialAbilityType; }
            set { specialAbilityType = value; }
        }

        public SkillType RelatedSkill { get { return relatedSkill; } set { relatedSkill = value; } }
        public int StripBonus { get { return stripBonus; } set { stripBonus = value; } }
        public ItemSet Set { get { return set; } set { set = value; } }
        public ItemCategory Category { get { return category; } set { category = value; } }

        public ItemDisplayGroup DisplayGroup
        {
            get
            {
                switch (Category)
                {
                   
                    case ItemCategory.Armour:
                    case ItemCategory.Robes:
                    case ItemCategory.Hats:
                    case ItemCategory.Clothes:
                    case ItemCategory.Shields:
                        return ItemDisplayGroup.Armour;
                    case ItemCategory.Weapons:
                    case ItemCategory.Staffs:
                    case ItemCategory.Bows:
                    case ItemCategory.BattleStaffs:
                        return ItemDisplayGroup.Weapons;
                    default: return ItemDisplayGroup.Ungrouped;
                }
            }
        }

        public string BoundID
        {
            set { boundID = value; }
            get { return boundID; }
        }

        public string BoundName
        {
            set { boundName = value; }
            get { return boundName; }
        }

        public DateTime DropTime
        {
            get { return dropTime; }
            set { dropTime = value; }
        }
        public int SetNumber { get { return setNumber; } set { setNumber = value; } }
        public bool IsStackable { get { return (type == ItemType.Consume || type == ItemType.Arrow); } }
        public bool IsGold { get { return ItemId == 86; } } // TODO fix hard coding
        public bool IsUpgradeable { get { return isUpgradeable; } set { isUpgradeable = value; } }
        public bool IsAngel { get { return (effectType == ItemEffectType.Angel); } }
        public int Level { get { return level; } set { level = value; } } // v2
        public Guid SpecificID { get { return specificID; } set { specificID = value; } }
        public bool IsUpgradeItem { get { return isUpgradeItem; } set { isUpgradeItem = value; } } //Used in inventory for storing index of selected upgrade item
        public bool IsUpgradeIngredient { get { return isUpgradeIngredient; } set { isUpgradeIngredient = value; } } //used in inventory for storying index of selected upgrade ingredient

        public int CastingProbabilityBonus
        {
            get
            {
                if (stats.ContainsKey(ItemStat.CastingProbability))
                    return stats[ItemStat.CastingProbability];
                else return 0;
            }
        }

        public int Range
        {
            get
            {
                switch (relatedSkill)
                {
                    case SkillType.Archery: return 9;
                    case SkillType.BattleStaff: return 4;
                    default: return 1;
                }
            }
        }

        public int CriticalRange
        {
            get
            {
                switch (relatedSkill)
                {
                    case SkillType.Archery: return 9;
                    case SkillType.Axe: return 2;
                    case SkillType.Fencing: return 4;
                    case SkillType.Hammer: return 2;
                    case SkillType.Hand: return 1;
                    case SkillType.LongSword: return 3;
                    case SkillType.ShortSword: return 2;
                    case SkillType.Staff: return 2;
                    case SkillType.BattleStaff: return 5;
                    default: return 1;
                }
            }
        }

        #region DEFENCE SPECIFIC VALUES
        public int PhysicalAbsorption
        {
            get
            {
                if (effectType == ItemEffectType.Defence ||
                    effectType == ItemEffectType.DefenceActivation)
                {
                    int value = effect1;

                    value += level;

                    switch (quality)
                    {
                        case ItemQuality.Flimsy: value += 2; break;
                        case ItemQuality.Sturdy: value += 4; break;
                        case ItemQuality.Reinforced: value += 6; break; // no mode
                        case ItemQuality.Studded: value += 8; break;
                    }

                    // dont allow items to be pushed below 1
                    value = Math.Max(value, 1);

                    if (level > 0) value += (level / 2);

                    return Math.Min(Math.Max(value,1), Globals.MaximumPhysicalAbsorption);
                }
                else return 0;
            }
        }

        public int PhysicalResistance
        {
            get
            {
                if (effectType == ItemEffectType.Defence ||
                    effectType == ItemEffectType.DefenceAntiMine ||
                    effectType == ItemEffectType.DefenceActivation)
                {
                    int value = effect2;

                    value += level;

                    switch (quality)
                    {
                        case ItemQuality.Flimsy: value += 2; break;
                        case ItemQuality.Sturdy: value += 4; break;
                        case ItemQuality.Reinforced: value += 6; break; // no mode
                        case ItemQuality.Studded: value += 8; break;
                    }

                    // dont allow items to be pushed below 1
                    value = Math.Max(value, 1);

                    if (level > 0) value += (level / 2);

                    return Math.Max(value,1);
                }
                else return 0;
            }
        }

        public int MagicAbsorption
        {
            get
            {
                if (effectType == ItemEffectType.Defence ||
                    effectType == ItemEffectType.DefenceActivation)
                {
                    int value = effect3;

                    value += level;

                    // only items with base MA are affected by quality and level (robes, bmage items etc)
                    if (effect3 > 0)
                    {
                        switch (quality)
                        {
                            case ItemQuality.Flimsy: value += 2; break;
                            case ItemQuality.Sturdy: value += 4; break;
                            case ItemQuality.Reinforced: value += 6; break; // no mode
                            case ItemQuality.Studded: value += 8; break;
                        }

                        // dont allow items to be pushed below 1
                        value = Math.Max(value, 1);

                        // every 4 levels is +1 MA, unless value is 0
                        if (level > 0) value += (level / 4);
                    }

                    return Math.Min(value, Globals.MaximumMagicalAbsorption);
                }
                else return 0;
            }
        }
        #endregion
        #region ATTACK SPECIFIC VALUES
        public int MagicDamageFacesMod
        {
            get
            {
                    int facesMod = 0;

                    if (stats.ContainsKey(ItemStat.Legendary)) facesMod += 3;
                    else if (stats.ContainsKey(ItemStat.Ancient)) facesMod += 2;
                    else if (stats.ContainsKey(ItemStat.Sharp)) facesMod += 1;

                    switch (quality)
                    {
                        case ItemQuality.Flimsy: facesMod += 1; break;
                        case ItemQuality.Sturdy: facesMod += 2; break;
                        case ItemQuality.Reinforced: facesMod += 3;  break; // no mode
                        case ItemQuality.Studded: facesMod += 4; break;
                    }

                    // dont allow items to be pushed below 0
                    facesMod = Math.Max(facesMod, 0);

                    return facesMod;
                }
        }

        public Dice DamageSmall
        {
            get
            {
                if (effectType == ItemEffectType.Attack ||
                    effectType == ItemEffectType.AttackBow ||
                    effectType == ItemEffectType.AttackDefence ||
                    effectType == ItemEffectType.AttackManaSave ||
                    effectType == ItemEffectType.AttackActivation ||
                    effectType == ItemEffectType.AttackMagic)
                {
                    int facesMod = 0;

                    if (stats.ContainsKey(ItemStat.Legendary)) facesMod += 3;
                    else if (stats.ContainsKey(ItemStat.Ancient)) facesMod += 2;
                    else if (stats.ContainsKey(ItemStat.Sharp)) facesMod += 1;

                    switch (quality)
                    {
                        case ItemQuality.Flimsy: facesMod += 1; break;
                        case ItemQuality.Sturdy: facesMod += 2; break;
                        case ItemQuality.Reinforced: facesMod += 3; break; 
                        case ItemQuality.Studded: facesMod += 4; break;
                    }

                    // dont allow items to be pushed below 1
                    facesMod = Math.Max(facesMod, 1);
                        
                    return new Dice(effect1, effect2 + facesMod, effect3 + level);
                }
                else return null;
            }
        }
        public Dice DamageLarge
        {
            get
            {
                if (effectType == ItemEffectType.Attack ||
                    effectType == ItemEffectType.AttackBow ||
                    effectType == ItemEffectType.AttackDefence ||
                    effectType == ItemEffectType.AttackManaSave ||
                    effectType == ItemEffectType.AttackActivation)
                {
                    int facesMod = 0;

                    if (stats.ContainsKey(ItemStat.Legendary)) facesMod += 3;
                    else if (stats.ContainsKey(ItemStat.Ancient)) facesMod += 2;
                    else if (stats.ContainsKey(ItemStat.Sharp)) facesMod += 1;

                    switch (quality)
                    {
                        case ItemQuality.Flimsy: facesMod -= 1; break;
                        case ItemQuality.Sturdy: facesMod -= 2; break;
                        case ItemQuality.Reinforced: facesMod += 3; break; // no mode
                        case ItemQuality.Studded: facesMod += 4; break;
                    }

                    // dont allow items to be pushed below 1
                    facesMod = Math.Max(facesMod, 1);
                        
                    return new Dice(effect4, effect5 + facesMod, effect6 + level);
                }
                else return null;
            }
        }
        #endregion

        public bool Equals(Item other)
        {
            return (other.ItemId == this.itemId);
        }

        bool IEquatable<Item>.Equals(Item other)
        {
            return (other.ItemId == this.itemId);
        }

        public bool GuidMatch(Item other)
        {
            return (other.specificID.Equals(this.specificID));
        }

        public int BagX { get { return bagX; } set { bagX = value; } }
        public int BagY { get { return bagY; } set { bagY = value; } }
        public int BagOldX { get { return bagOldX; } set { bagOldX = value; } }
        public int BagOldY { get { return bagOldY; } set { bagOldY = value; } }
    }

    public class ItemStack
    {
        private List<Item> items;

        public ItemStack()
        {
            items = new List<Item>(Globals.MaximumItemsPerTile);
        }

        public Item CheckItem()
        {
            if (items.Count > 0)
                return items[items.Count - 1];
            else return null;
        }

        public void AddItem(Item item)
        {
            if (items.Count >= Globals.MaximumItemsPerTile) items.RemoveAt(0);
            
            items.Add(item);
        }


        public void CycleItem(int location)
        {
            if(items.Count > 0 && items[location] != null)
            {                 
                Item item = items[location];
                items.RemoveAt(location);
                AddItem(item);
            }
        }

        public Item RemoveItem()
        {
            if (items.Count > 0)
            {
                Item item = items[items.Count - 1];
                items.RemoveAt(items.Count - 1);
                return item;
            }
            else return null;
        }

        public void ClearItem(int index)
        {
            if (items.Count > 0)
            {
                items.RemoveAt(index);
            }
        }

        public int Count { get { return items.Count; } }
        public Item this[int index] { get { return items[index]; } }
    }
}
