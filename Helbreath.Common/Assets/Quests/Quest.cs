﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Helbreath.Common.Assets.Quests
{
    public interface IQuestConfig
    {
        string Type { get; set; }
        string Map { get; set; }
        int Count { get; set; }
        int LocationX { get; set; }
        int LocationY { get; set; }
        int TargetX { get; set; }
        int TargetY { get; set; }
    }
        
    public class SlayerConfig : IQuestConfig
    {
        private string slayerType;
        private string slayerMap;
        private int slayerCount;

        public string Type { get; set; }
        public string Map { get; set; }
        public int Count { get; set; }
        public int LocationX { get; set; }
        public int LocationY { get; set; }
        public int TargetX { get; set; }
        public int TargetY { get; set; }
    }

    public class Quest
    {
        private int id;
        private string grantedBy;
        private QuestType type;
        private int minimumLevel;
        private int maximumLevel;
        private SkillType requiredSkill;
        private int requiredSkillLevel;
        private OwnerSide side;
        private TimeSpan timeLimit;
        private List<QuestReward> rewards;
        private IQuestConfig config;

        public Quest(int id)
        {
            this.id = id;

            rewards = new List<QuestReward>();
        }

        public static Quest ParseXml(XmlReader r)
        {
            Quest quest = new Quest(Int32.Parse(r["Id"]));
            quest.GrantedBy = r["GrantedBy"]; // TODO attach these to Npc configuration?
            quest.Type = (QuestType)Enum.Parse(typeof(QuestType), r["Type"]);
            switch (quest.Type)
            {
                case QuestType.Slayer:
                    quest.Config = new SlayerConfig();
                    quest.Config.Count = Int32.Parse(r["SlayerCount"]);
                    quest.Config.Map = r["SlayerMap"];
                    quest.Config.Type = r["SlayerType"];
                    break;
            }
            quest.MinimumLevel = (r["MinimumLevel"] != null) ? Int32.Parse(r["MinimumLevel"]) : 0;
            quest.MaximumLevel = (r["MaximumLevel"] != null) ? Int32.Parse(r["MaximumLevel"]) : Globals.MaximumLevel;
            quest.RequiredSkill = (r["RequiredSkill"] != null) ? (SkillType)Enum.Parse(typeof(SkillType), r["RequiredSkill"]) : SkillType.None;
            quest.RequiredSkillLevel = (r["RequiredSkillLevel"] != null) ? Int32.Parse(r["RequiredSkillLevel"]) : 0;
            quest.Side = (r["Side"] != null) ? (OwnerSide)Enum.Parse(typeof(OwnerSide), r["Side"]) : OwnerSide.None;
            quest.TimeLimit = (r["TimeLimit"] != null) ? TimeSpan.Parse(r["TimeLimit"]) : new TimeSpan(1, 0, 0, 0, 0);

            XmlReader rewardsReader = r.ReadSubtree();
            while (rewardsReader.Read())
                if (rewardsReader.IsStartElement() && rewardsReader.Name.Equals("Reward"))
                    quest.Rewards.Add(QuestReward.ParseXml(rewardsReader));
                                        
            rewardsReader.Close();

            return quest;
        }

        public void Start()
        {
            switch (type)
            {

            }
        }

        public bool Completed()
        {
            // TODO
            return false;
        }

        public int Id { get { return id; } }
        public string GrantedBy { get { return grantedBy; } set { grantedBy = value; } }
        public QuestType Type { get { return type; } set { type = value; } }
        public int MinimumLevel { get { return minimumLevel; } set { minimumLevel = value; } }
        public int MaximumLevel { get { return maximumLevel; } set { maximumLevel = value; } }
        public SkillType RequiredSkill { get { return requiredSkill; } set { requiredSkill = value; } }
        public int RequiredSkillLevel { get { return requiredSkillLevel; } set { requiredSkillLevel = value; } }
        public OwnerSide Side { get { return side; } set { side = value; } }
        public TimeSpan TimeLimit { get { return timeLimit; } set { timeLimit = value; } }
        public List<QuestReward> Rewards { get { return rewards; } set { rewards = value; } }

        public IQuestConfig Config { get { return config; } set { config = value; } }
    }
}
