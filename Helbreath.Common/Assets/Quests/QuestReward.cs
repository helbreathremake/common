﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Helbreath.Common.Assets.Quests
{
    public class QuestReward
    {
        private int count;
        private QuestRewardType type;
        private string itemName;

        public QuestReward()
        {

        }

        public static QuestReward ParseXml(XmlReader r)
        {
            QuestReward reward = new QuestReward();
            reward.Count = Int32.Parse(r["Amount"]);
            reward.Type = (QuestRewardType)Enum.Parse(typeof(QuestRewardType), r["Type"]);
            reward.ItemName = r["ItemName"];

            return reward;
        }
        public int Count { get { return count; } set { count = value; } }
        public QuestRewardType Type { get { return type; } set { type = value; } }
        public string ItemName { get { return itemName; } set { itemName = value; } }
    }
}
