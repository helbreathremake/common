﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets.Objects;

namespace Helbreath.Common.Assets
{
    public class Account
    {
        private int clientID;    // identity of ClientInfo object to track socket
        private string id;       // identity of the account from the database
        private string name;

        private Dictionary<String, Character> characters;

        public Account()
        {
        }

        public bool Load(DataRow data)
        {
            if (data == null) return false;

            try
            {
                name = data["Name"].ToString();
                id = data["ID"].ToString();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool LoadCharacters(DataTable data)
        {
            try
            {
                characters = new Dictionary<String, Character>();

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        Character c = new Character();
                        if (c.Load(row)) characters.Add(c.Name, c);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return true;
        }

        public byte[] GetLoginData()
        {
            byte[] data = new byte[26 + (characters.Count * 116)];

            Buffer.BlockCopy(BitConverter.GetBytes((short)3), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)82), 0, data, 2, 2);
            data[4] = (byte)0;
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 5, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 7, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 9, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 11, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 13, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 15, 2);
            data[17] = (byte)characters.Count;

            int size = 18;
            foreach (Character character in characters.Values)
            {
                Buffer.BlockCopy(character.Name.GetBytes(10), 0, data, size, 10);
                size += 10;
                data[size] = (byte)(int)character.PlayType;
                size++;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance1), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance2), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance3), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance4), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Gender), 0, data, size, 2);
                size += 2;
                data[size] = (byte)(int)character.Skin;
                size++;
                data[size] = (byte)(int)character.Side;
                size++;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Level), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.Experience), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Strength), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Vitality), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Dexterity), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Intelligence), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Magic), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Agility), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.AppearanceColour), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(character.LastLogin.Year.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Month.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Day.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Hour.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Minute.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Second.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LoadedMapName.GetBytes(10), 0, data, size, 10);
                size += 10;
                if (character.Helmet != null)
                {
                    Buffer.BlockCopy(character.Helmet.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Helmet.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.BodyArmour != null)
                {
                    Buffer.BlockCopy(character.BodyArmour.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.BodyArmour.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Hauberk != null)
                {
                    Buffer.BlockCopy(character.Hauberk.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Hauberk.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Leggings != null)
                {
                    Buffer.BlockCopy(character.Leggings.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Leggings.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Cape != null)
                {
                    Buffer.BlockCopy(character.Cape.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Cape.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Weapon != null)
                {
                    Buffer.BlockCopy(character.Weapon.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Weapon.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Shield != null)
                {
                    Buffer.BlockCopy(character.Shield.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Shield.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Boots != null)
                {
                    Buffer.BlockCopy(character.Boots.ItemId.GetBytes(), 0, data, size, 2);
                    size += 2;
                    Buffer.BlockCopy(character.Boots.Colour.GetBytes(), 0, data, size, 4);
                    size += 4;
                }
                else size += 6;
                if (character.Angel != null)
                    data[size] = (byte)((int)(AngelType)character.Angel.Effect1);
                size++;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(35), 0, data, 18 + (characters.Count * 116), 4);
            Buffer.BlockCopy(BitConverter.GetBytes(35), 0, data, 22 + (characters.Count * 116), 4);

            return data;
        }

        public byte[] GetNewCharacterData(string characterName)
        {
            byte[] data = new byte[12 + (characters.Count * 67)];

            Buffer.BlockCopy(characterName.GetBytes(10), 0, data, 0, 10);

            data[10] = (byte)characters.Count;
            int size = 11;
            foreach (Character character in characters.Values)
            {
                Buffer.BlockCopy(character.Name.GetBytes(10), 0, data, size, 10);
                size += 10;
                data[size] = (byte)1;
                size++;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance1), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance2), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance3), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance4), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Gender), 0, data, size, 2);
                size += 2;
                //Buffer.BlockCopy(BitConverter.GetBytes((short)character.Skin), 0, data, size, 2);
                //size += 2;
                data[size] = (byte)character.Skin;
                size++;
                data[size] = (byte)character.Town;
                size++;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Level), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.Experience), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Strength), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Vitality), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Dexterity), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Intelligence), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Magic), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Agility), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.AppearanceColour), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(character.LastLogin.Year.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Month.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Day.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Hour.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Minute.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LastLogin.Second.GetBytes(), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LoadedMapName.GetBytes(10), 0, data, size, 10);
                size += 10;
            }

            return data;
        }

        /// <summary>
        /// ClientInfo ID of this account from socket.
        /// </summary>
        public Int32 ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        public String Name
        {
            get { return name; }
        }

        /// <summary>
        /// Database ID of this account.
        /// </summary>
        public String ID
        {
            get { return id; }
        }

        public Character this[string characterName]
        {
            get { return characters[characterName]; }
        }

        public Dictionary<string, Character> Characters
        {
            get { return characters; }
            set { characters = value; }
        }
    }
}
