﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Xml;


namespace Helbreath.Common.Assets
{
    public class Skill
    {
        public event SkillHandler LevelUp;

        private SkillType type;
        private int experience;

        public Skill(SkillType type, int experience)
        {
            this.type = type;
            this.experience = experience;
        }

        public void Learn()
        {
            experience = Globals.SkillExperienceTable[Globals.MinimumSkillLevel];
        }

        public void MaxOut()
        {
            experience = Globals.SkillExperienceTable[100];
        }

        public int Experience
        {
            get { return experience; }
            set {
                if (Level < 100)
                {
                    if (Level >= Globals.MinimumSkillLevel)
                    {
                        int previousLevel = Level;
                        experience = value;

                        if (previousLevel < Level && LevelUp != null) LevelUp(this);
                    }
                }
                else experience = Globals.SkillExperienceTable[100];
            }
        }

        /// <summary>
        /// Calculates the skill level based on current experience.
        /// </summary>
        public int Level
        {
            get 
            {
                for (int i = 1; i < Globals.SkillExperienceTable.Length; i++)
                    if (experience < Globals.SkillExperienceTable[i])
                        return i - 1;

                return 0;
            }
        }

        public SkillType Type
        {
            get { return type; }
        }
    }
}