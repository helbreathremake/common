﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public class Game
    {
        private string name;
        private string ipAddress;
        private int port;

        private bool isRegistered;
        private List<Map> maps;

        public Game()
        {
            this.isRegistered = false;
            this.maps = new List<Map>();
        }

        public void AddMap(Map map)
        {
            maps.Add(map);
        
        }

        public bool Parse(string[] data)
        {
            try
            {
                this.name = data[1];
                this.ipAddress = data[2];
                this.port = Int32.Parse(data[3]);
                for (int i = 4; i < data.Length - 1; i++)
                {
                    Map map = new Map(data[i]);
                    maps.Add(map);
                }

                isRegistered = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Data sent to the client when Enter Game is requested.
        /// </summary>
        /// <returns>Byte array of data to be sent to client.</returns>
        public byte[] GetEnterGameSucceedData()
        {
            byte[] data = new byte[38];

            Buffer.BlockCopy(ipAddress.GetBytes(16), 0, data, 0, 16);
            Buffer.BlockCopy(BitConverter.GetBytes((short)port), 0, data, 16, 2);
            Buffer.BlockCopy(name.GetBytes(20), 0, data, 18, 20);

            return data;
        }

        public String Name
        {
            get { return name; }
        }

        public String IPAddress
        {
            get { return ipAddress; }
        }

        public Int32 Port
        {
            get { return port; }
        }

        public List<Map> Maps
        {
            get { return maps; }
        }

        public Map this[int mapIndex]
        {
            get { return maps[mapIndex];}
        }
    }
}
