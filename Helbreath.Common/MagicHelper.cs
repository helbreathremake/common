﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath.Common.Assets;

namespace Helbreath.Common
{
    public static class MagicHelper
    {
        public static Dictionary<int, Magic> LoadMagic(string configFilePath)
        {

            Dictionary<int, Magic> magics = new Dictionary<int, Magic>();

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Spell"))
                        if (!magics.ContainsKey(Int32.Parse(reader["Index"])))
                        {
                            Magic magic = Magic.ParseXml(reader);
                            magics.Add(magic.Index, magic);
                        }

            reader.Close();

            return magics;
        }
    }
}
