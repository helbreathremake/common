﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Events
{
    public interface IWorldEvent
    {
        World CurrentWorld { get; set; }
        WorldEventType Type { get; }
        void TimerProcess();
        void Start();
        void End(OwnerSide winner);
        List<int> NpcIDs { get; set; }
        WorldEventResult Result { get; }
    }
}
