﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;
using Helbreath.Common.Assets.Objects.Dynamic;

namespace Helbreath.Common.Events
{
    public class Heldenian : IWorldEvent
    {
        private World world;
        private HeldenianType mode;
        private List<WorldEventStructure> battlefieldStructures;
        private int aresdenStructures, elvineStructures;
        private int aresdenCasualties, elvineCasualties;
        private List<Location> castleGates;
        private Location castleWinningZone;
        private OwnerSide castleDefender;
        private Flag castleFlag;
        private List<int> npcIDs; // list of npcs associated with this event
        private bool isStarted;
        private DateTime heldenianStartTime;
        
        private WorldEventResult result;
        private DateTime startTime;

        public Heldenian()
        {
            battlefieldStructures = new List<WorldEventStructure>();

            castleGates = new List<Location>();
            npcIDs = new List<int>();
            isStarted = false;
        }

        public Heldenian Copy()
        {
            Heldenian heldenian = new Heldenian();
            heldenian.BattleFieldStructures = battlefieldStructures;
            heldenian.CastleGates = castleGates;
            heldenian.CastleWinningZone = castleWinningZone;

            return heldenian;
        }

        public void TowerDestroyed(OwnerSide side)
        {
            if (mode != HeldenianType.Battlefield) return;
            if (isStarted == false) return;

            switch (side)
            {
                case OwnerSide.Aresden: aresdenStructures--; break;
                case OwnerSide.Elvine: elvineStructures--; break;
            }

            foreach (Map map in world.Maps.Values)
                if (map.Name.Equals(Globals.HeldenianBattleFieldName))
                    for (int p = 0; p < map.Players.Count; p++)
                        if (world.Players.ContainsKey(map.Players[p]))
                            world.Players[map.Players[p]].Notify(CommandMessageType.NotifyHeldenianStatistics, aresdenStructures, elvineStructures, aresdenCasualties, elvineCasualties);
        }

        public void TimerProcess() 
        {
            TimeSpan ts = DateTime.Now - heldenianStartTime;
            if (ts.Minutes >= 5)
            {
                if (!isStarted)
                {
                    isStarted = true;
                    foreach (Map map in world.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (world.Players.ContainsKey(map.Players[p]))
                                world.Players[map.Players[p]].Notify(CommandMessageType.NotifyHeldenianStarted);
                }

                switch (mode) // check win conditions
                {
                    case HeldenianType.Battlefield:
                        if (aresdenStructures <= 0) world.EndWorldEvent(OwnerSide.Elvine); // elvine win
                        else if (elvineStructures <= 0) world.EndWorldEvent(OwnerSide.Aresden); // aresden win
                        else if (ts.Minutes >= 60) // if event goes longer for 1 hour, end it and calculate winner
                        {
                            OwnerSide winner = OwnerSide.None;
                            if (aresdenStructures > elvineStructures) winner = OwnerSide.Aresden;
                            else if (aresdenStructures < elvineStructures) winner = OwnerSide.Elvine;
                            else if (aresdenCasualties < elvineCasualties) winner = OwnerSide.Aresden;
                            else if (aresdenCasualties > elvineCasualties) winner = OwnerSide.Elvine;
                            world.EndWorldEvent(winner);
                        }
                        break;
                    case HeldenianType.CastleSeige:
                        if (ts.Minutes >= 60) // if event goes longer for 1 hour, defender wins
                        {
                            world.EndWorldEvent(castleDefender);
                        }
                        break;
                }
            }
        }

        public void Start()
        {
            startTime = DateTime.Now;
            //mode = HeldenianType.Battlefield; // TODO how do we set this? in / command maybe?
            mode = HeldenianType.CastleSeige;
            castleDefender = OwnerSide.Aresden;

            // TODO teleport all players out of battlefield map

            // clean battlefield map of all npcs
            foreach (Map map in world.Maps.Values)
                if (map.Name.Equals(Globals.HeldenianBattleFieldName))
                    for (int n = 0; n < map.Npcs.Count; n++)
                        if (world.Npcs.ContainsKey(map.Npcs[n]))
                            if (!world.Npcs[map.Npcs[n]].IsFriendly) // dont kill friendly npcs like shopkeeper!
                                world.Npcs[map.Npcs[n]].Die(null, -1, (DamageType)0, 1, false);

            // notify players heldenian has started
            foreach (Map map in world.Maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (world.Players.ContainsKey(map.Players[p]))
                    {
                        Character character = world.Players[map.Players[p]];
                        character.CrusadeWarContribution = 0;
                        character.CrusadeConstructionPoints = character.Agility * 300;
                        character.Notify(CommandMessageType.NotifyHeldenian);
                    }

            // setup heldenian mode
            switch (mode)
            {
                case HeldenianType.Battlefield:
                    foreach (WorldEventStructure structure in battlefieldStructures)
                    {
                        Npc npc = World.NpcConfiguration[structure.NpcName].Copy();
                        switch (npc.Side)
                        {
                            case OwnerSide.Aresden: aresdenStructures++; break;
                            case OwnerSide.Elvine: elvineStructures++; break;
                        }
                        npcIDs.Add(world.InitNpc(npc, structure.Location, null));
                    }
                    break;
                case HeldenianType.CastleSeige:
                    foreach (Location location in castleGates)
                    {
                        Npc gate = null;
                        switch (castleDefender)
                        {
                            case OwnerSide.Aresden: gate = World.NpcConfiguration["gate-a"].Copy(); break;
                            case OwnerSide.Elvine: gate = World.NpcConfiguration["gate-e"].Copy(); break;
                        }
                        gate.Direction = MotionDirection.East;
                        npcIDs.Add(world.InitNpc(gate, location, null));
                    }

                    castleFlag = world.Maps[Globals.HeldenianCastleName].SetFlag(castleWinningZone, castleDefender);
                    break;
            }

            heldenianStartTime = DateTime.Now;
        }

        public void End(OwnerSide winner)
        {
            foreach (Map map in world.Maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (world.Players.ContainsKey(map.Players[p]))
                    {
                        Character character = world.Players[map.Players[p]];
                        character.Notify(CommandMessageType.NotifyHeldenianEnded);
                        character.Notify(CommandMessageType.NotifyHeldenianVictory, (int)winner);
                    }

            foreach (int npcID in npcIDs) world.RemoveNPC(npcID);

            if (mode == HeldenianType.CastleSeige && castleFlag != null) castleFlag.Remove();

            result = new WorldEventResult(Type);
            result.StartDate = startTime;
            result.EndDate = DateTime.Now;
            result.Winner = winner;
        }

        public World CurrentWorld { get { return world; } set { world = value; } }
        public WorldEventType Type { get { return WorldEventType.Heldenian; } }
        public HeldenianType Mode { get { return mode; } }
        public List<WorldEventStructure> BattleFieldStructures { get { return battlefieldStructures; } set { battlefieldStructures = value; } }
        public List<Location> CastleGates { get { return castleGates; } set { castleGates = value; } }
        public Location CastleWinningZone { get { return castleWinningZone; } set { castleWinningZone = value; } }
        public bool IsStarted { get { return isStarted; } }
        public List<int> NpcIDs { get { return npcIDs; } set { npcIDs = value; } }
        public int AresdenStructures { get { return aresdenStructures; } }
        public int ElvineStructures { get { return elvineStructures; } }
        public int AresdenCasualties { get { return aresdenCasualties; } }
        public int ElvineCasualties { get { return elvineCasualties; } }
        public OwnerSide CastleDefender { get { return castleDefender; } }
        public WorldEventResult Result { get { return result; } }
    }
}
