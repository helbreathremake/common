﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;
using Helbreath.Common.Assets.Objects.Dynamic;

namespace Helbreath.Common.Events
{
    public class CaptureTheFlag : IWorldEvent
    {
        private int numberOfFlags;
        private Location[] flagLocations;
        private World world;
        private List<int> npcIDs; // list of npcs associated with this event

        private WorldEventResult result;
        private DateTime startTime;

        public CaptureTheFlag()
        {
            flagLocations = new Location[3];
            flagLocations[(int)OwnerSide.Aresden] = new Location(-1, -1);
            flagLocations[(int)OwnerSide.Elvine] = new Location(-1, -1);

            npcIDs = new List<int>();
        }

        public CaptureTheFlag Copy()
        {
            CaptureTheFlag ctf = new CaptureTheFlag();
            ctf.NumberOfFlags = numberOfFlags;
            ctf.FlagLocations = flagLocations;

            return ctf;
        }


        public void TimerProcess()
        {
            
        }

        public void Start()
        {
            startTime = DateTime.Now;

            foreach (Map map in world.Maps.Values)
                    for (int p = 0; p < map.Players.Count; p++)
                        if (world.Players.ContainsKey(map.Players[p]))
                        {
                            // TODO send notifications
                            // TODO create flags
                        }
        }

        public void End(OwnerSide winner)
        {
            // remove crusade npcs
            foreach (int npcID in npcIDs) world.RemoveNPC(npcID);

            foreach (Map map in world.Maps.Values)
                    for (int p = 0; p < map.Players.Count; p++)
                        if (world.Players.ContainsKey(map.Players[p]))
                        {
                            // TODO winners
                            if (world.Players[map.Players[p]].Town == winner) { }
                            // TODO if neutral then draw, notify players
                        }

            result = new WorldEventResult(Type);
            result.StartDate = startTime;
            result.EndDate = DateTime.Now;
            result.Winner = winner;
        }

        public int NumberOfFlags { get { return numberOfFlags; } set { numberOfFlags = value; } }
        public Location[] FlagLocations { get { return flagLocations; } set { flagLocations = value; } }
        public World CurrentWorld { get { return world; } set { world = value; } }
        public WorldEventType Type { get { return WorldEventType.CaptureTheFlag; } }
        public List<int> NpcIDs { get { return npcIDs; } set { npcIDs = value; } }
        public WorldEventResult Result { get { return result; } }
    }
}
