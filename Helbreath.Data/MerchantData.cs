﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Helbreath.Common;
using Helbreath.Common.Assets;

namespace Helbreath.Data
{
    public class MerchantData
    {
        public delegate void LogHandler(string message);
        public event LogHandler MessageLogged;

        private Connection connection;

        public MerchantData(Connection connection)
        {
            this.connection = connection;
        }

        public Dictionary<int, Dictionary<int, MerchantItem>> LoadMerchants()
        {
            Dictionary<int, Dictionary<int, MerchantItem>> merchants = new Dictionary<int, Dictionary<int, MerchantItem>>();
            SqlCommand command = new SqlCommand("SELECT * FROM MarketOrders;");
            DataTable list = connection.ExecuteSQLTable(command);

            if (list != null && list.Rows.Count > 0)
                foreach (DataRow itemData in list.Rows)
                {
                    int merchantId = (int)itemData["MerchantId"];
                    MerchantItem item = MerchantItem.Parse(itemData);
                    if (merchants.ContainsKey(merchantId))
                        merchants[merchantId].Add(item.Id, item);
                    else
                    {
                        // new merchants are loaded during NpcHelper.LoadNpcs, but add new one if it doesn't exist
                        merchants.Add(merchantId, new Dictionary<int, MerchantItem>());
                        merchants[merchantId].Add(item.Id, item);
                    }
                }

            return merchants;
        }

        public void RemoveItem(string databaseId, int count)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE MarketOrders SET Count = Count - @Count WHERE Id = @Id; DELETE FROM MarketOrders WHERE Count <= 0 AND Id = @Id;");
                command.Parameters.AddWithValue("@Id", databaseId);
                command.Parameters.AddWithValue("@Count", count);
                connection.ExecuteSQLNoReturn(command);
            }
            catch (Exception ex)
            {
                LogMessage("Error in Remove MarketOrder: " + ex.ToString());
            }
        }

        public void AddItem(MerchantItem item, bool stack)
        {
            try
            {
                if (stack)
                {
                    SqlCommand command = new SqlCommand("UPDATE MarketOrders SET Count = @Count WHERE ID = @Id;");
                    command.Parameters.AddWithValue("@Id", item.DatabaseId);
                    command.Parameters.AddWithValue("@Count", item.Item.Count);
                    connection.ExecuteSQLNoReturn(command);
                }
                else
                {

                    SqlCommand command = new SqlCommand("INSERT INTO MarketOrders (ID, MerchantId, ItemId, Endurance, Colour, SpecialEffect1, SpecialEffect2, SpecialEffect3, Stats, Count, Unlimited, Quality, Level, ColorType) " +
                        "VALUES (@Id, @MerchantId, @ItemId, @Endurance, @Colour, @SpecialEffect1, @SpecialEffect2, @SpecialEffect3, @Stats, @Count, @Unlimited, @Quality, @Level, @ColorType);");
                    command.Parameters.AddWithValue("@Id", item.DatabaseId);
                    command.Parameters.AddWithValue("@MerchantId", item.MerchantId);
                    command.Parameters.AddWithValue("@ItemId", item.Item.ItemId);
                    command.Parameters.AddWithValue("@Endurance", item.Item.Endurance);
                    command.Parameters.AddWithValue("@Colour", item.Item.Colour);
                    command.Parameters.AddWithValue("@SpecialEffect1", item.Item.SpecialEffect1);
                    command.Parameters.AddWithValue("@SpecialEffect2", item.Item.SpecialEffect2);
                    command.Parameters.AddWithValue("@SpecialEffect3", item.Item.SpecialEffect3);
                    string stats = string.Empty;
                    foreach (KeyValuePair<ItemStat, int> stat in item.Item.Stats)
                        stats += string.Format("{0} {1} ", (int)stat.Key, stat.Value);
                    command.Parameters.AddWithValue("@Stats", stats.Trim());
                    command.Parameters.AddWithValue("@Count", item.Item.Count);
                    command.Parameters.AddWithValue("@Unlimited", item.Unlimited);
                    command.Parameters.AddWithValue("@Quality", item.Item.Quality);
                    command.Parameters.AddWithValue("@Level", item.Item.Level);
                    command.Parameters.AddWithValue("@ColorType", item.Item.ColorType);
                    connection.ExecuteSQLNoReturn(command);
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error in Add MarketOrder: " + ex.ToString());
            }
        }

        #region LOGGING
        public void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message);
        }
        #endregion
    }
}
