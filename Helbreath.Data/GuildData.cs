﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets;

namespace Helbreath.Data
{
    public class GuildData
    {
        public delegate void LogHandler(string message);
        public event LogHandler MessageLogged;

        private Connection connection;

        public GuildData(Connection connection)
        {
            this.connection = connection;
        }

        public Dictionary<string, Guild> LoadGuilds()
        {
            Dictionary<string, Guild> guilds = new Dictionary<string, Guild>();
            SqlCommand command = new SqlCommand("SELECT ID, Name FROM Guilds;");
            DataTable list = connection.ExecuteSQLTable(command);

            if (list != null && list.Rows.Count > 0)
                foreach (DataRow guildData in list.Rows)
                {
                    Guild guild = new Guild(guildData["Name"].ToString(), guildData["ID"].ToString());
                    guilds.Add(guild.Name, guild);
                }

            return guilds;
        }

        public void LoadGuildMembers(Guild guild)
        {
            SqlCommand command = new SqlCommand("SELECT ID, Name, GuildRank FROM Characters WHERE ID = @ID");
            command.Parameters.AddWithValue("@ID", guild.ID);
            DataTable list = connection.ExecuteSQLTable(command);

            if (list != null && list.Rows.Count > 0)
                foreach (DataRow memberTable in list.Rows)
                {
                    GuildMember member = new GuildMember(memberTable["ID"].ToString(), memberTable["Name"].ToString(), memberTable["GuildRank"].ToString());
                    guild.Members.Add(member.Name, member);
                }
        }

        public bool GuildExists(string name)
        {
            SqlCommand command = new SqlCommand("SELECT Count(ID) FROM Guilds WHERE lower(Name) = lower(@Name)");
            command.Parameters.AddWithValue("@Name", name);

            string value = connection.ExecuteSQLScalar(command);

            int intParser;
            if (Int32.TryParse(value, out intParser))
                if (intParser > 0) return true;
                else return false;
            else return false;
        }

        public bool CreateGuild(Guild guild)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Guilds (ID, Name, FoundedOn, FoundedBy) VALUES " +
                                                "(@ID, @Name, GETDATE(), @Founder)");
            command.Parameters.AddWithValue("@ID", guild.ID);
            command.Parameters.AddWithValue("@Name", guild.Name);
            command.Parameters.AddWithValue("@Founder", guild.FoundedBy);

            if (connection.ExecuteSQLNoReturn(command)) return true;
            else return false;
        }

        public bool DeleteGuild(Guild guild)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Guilds WHERE Name = @Name");
            command.Parameters.AddWithValue("@Name", guild.Name);

            if (connection.ExecuteSQLNoReturn(command)) return true;
            else return false;
        }

        #region LOGGING
        public void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message);
        }
        #endregion
    }
}
