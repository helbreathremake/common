﻿

using Helbreath.Common.Assets;
using System;
using System.Data.SqlClient;

namespace Helbreath.Data
{
    public class AccountData
    {
        private Connection connection;

        public AccountData(Connection connection)
        {
            this.connection = connection;
        }

        public bool LoginExists(string username)
        {
            SqlCommand command = new SqlCommand("SELECT Count(ID) FROM Accounts WHERE lower(Name) = lower(@Name)");
            command.Parameters.AddWithValue("@Name", username);

            string value = connection.ExecuteSQLScalar(command);

            int intParser;
            if (Int32.TryParse(value, out intParser))
                if (intParser > 0) return true;
                else return false;
            else return false;
        }

        public int Login(string username, string password)
        {
            SqlCommand command = new SqlCommand("SELECT Count(ID) FROM Accounts WHERE lower(Name) = lower(@Name) AND Password = @Password");
            command.Parameters.AddWithValue("@Name", username);
            command.Parameters.AddWithValue("@Password", password);

            string value = connection.ExecuteSQLScalar(command);
            
            int intParser;
            if (Int32.TryParse(value, out intParser))
                return intParser;
            else if (value.StartsWith("ERR"))
                return -1;
            else return 0;
        }

        public String GetAccountID(string username)
        {
            SqlCommand command = new SqlCommand("SELECT ID FROM Accounts WHERE lower(Name) = lower(@Name)");
            command.Parameters.AddWithValue("@Name", username);

            return connection.ExecuteSQLScalar(command);
        }

        public Account LoadAccount(string username)
        {
            SqlCommand command = new SqlCommand("SELECT ID, Name FROM Accounts WHERE lower(Name) = lower(@Name)");
            command.Parameters.AddWithValue("@Name", username);

            Account account = new Account();

            if (account.Load(connection.ExecuteSQLTable(command).Rows[0]))
            {
                command = new SqlCommand("SELECT * FROM Characters WHERE AccountID = @AccountID");
                command.Parameters.AddWithValue("@AccountID", account.ID);

                if (account.LoadCharacters(connection.ExecuteSQLTable(command)))
                {
                    return account;
                }
                else return null;
            }
            else return null;
        }

        public void LoadCharacters(Account account)
        {         
            SqlCommand command = new SqlCommand("SELECT * FROM Characters WHERE AccountID = @AccountID");
            command.Parameters.AddWithValue("@AccountID", account.ID);
            account.LoadCharacters(connection.ExecuteSQLTable(command));
        }

        public bool CreateAccount(string username, string password, string email, string quiz, string answer)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Accounts (ID, Name, Password, Email, CreatedOn, Question, Answer) " +
                                                "VALUES (newid(), @Name, @Password, @Email, GETDATE(), @Question, @Answer)");
            command.Parameters.AddWithValue("@Name", username);
            command.Parameters.AddWithValue("@Password", password);
            command.Parameters.AddWithValue("@Email", email);
            command.Parameters.AddWithValue("@Question", quiz);
            command.Parameters.AddWithValue("@Answer", answer);

            if (connection.ExecuteSQLNoReturn(command)) return true;
            else return false;
        }

        public bool ChangePassword(string username, string password, string newPassword)
        {
            SqlCommand command = new SqlCommand("UPDATE Accounts SET Password = @NewPassword WHERE lower(Name) = @Name AND Password = @Password");
            command.Parameters.AddWithValue("@Name", username);
            command.Parameters.AddWithValue("@Password", password);
            command.Parameters.AddWithValue("@NewPassword", newPassword);

            if (connection.ExecuteSQLNoReturn(command)) return true;
            else return false;
        }
    }
}
