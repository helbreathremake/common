﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;

using Helbreath.Common;

namespace Helbreath.Data
{
    public class EventData
    {
        private Connection connection;

        public EventData(Connection connection)
        {
            this.connection = connection;
        }

        public Dictionary<WorldEventType, WorldEventResult> LoadEventResults()
        {
            Dictionary<WorldEventType, WorldEventResult> results = new Dictionary<WorldEventType, WorldEventResult>();

            SqlCommand command = new SqlCommand("SELECT WE.[Type], WE.StartTime, WE.EndTime, WE.Winner, WE.ID FROM WorldEventResults WE INNER JOIN (SELECT TOP 1 [Type], ID FROM WorldEventResults ORDER BY EndTime DESC) AS DR ON DR.ID = WE.ID;");
            DataTable table = connection.ExecuteSQLTable(command);

            if (table != null && table.Rows.Count > 0)
                foreach (DataRow row in table.Rows)
                {
                    WorldEventType typeParser;
                    if (Enum.TryParse<WorldEventType>(row["Type"].ToString(), out typeParser))
                    {
                        WorldEventResult r = new WorldEventResult(typeParser);
                        r.Parse(row);
                        results.Add(r.Type, r);
                    }
                }

            return results;
        }

        public bool SaveEventResult(WorldEventResult result)
        {
            SqlCommand command = new SqlCommand("INSERT INTO WorldEventResults (ID, Type, StartTime, EndTime, Winner) VALUES " +
                                                "(newid(), @Type, @StartTime, @EndTime, @Winner)");
            command.Parameters.AddWithValue("@Type", result.Type);
            command.Parameters.AddWithValue("@StartTime", result.StartDate);
            command.Parameters.AddWithValue("@EndTime", result.EndDate);
            command.Parameters.AddWithValue("@Winner", result.Winner);

            if (connection.ExecuteSQLNoReturn(command)) return true;
            else return false;
        }
    }
}
