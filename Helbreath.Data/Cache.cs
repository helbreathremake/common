﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Data
{
    // TODO - cache entire Account objects to speed up login server, relatively small so should be small memory footprint
    public static class AccountCache
    {
    }

    // TODO - cache character ids to speed up logins such as CharacterData.CharacterExists()
    public static class CharacterCache
    {
    }
}
