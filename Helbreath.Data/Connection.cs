﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Helbreath.Data
{
    public class Connection
    {
        public delegate void LogHandler(string message);
        public event LogHandler MessageLogged;

        private string connectionString;

        public Connection(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public bool Test()
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("SELECT count(*) FROM dbo.sysobjects", connection);

                    if (command.ExecuteNonQuery() > 0) return true;
                    else return true;
                }
            }
            catch(Exception e)
            {
                return false;
            }
        }


        public bool ExecuteSQLNoReturn(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    command.Connection = connection;

                    if (command.ExecuteNonQuery() > 0) return true;
                    else return false;
                }
            }
            catch (Exception ex)
            {
                if (MessageLogged != null) MessageLogged("SQL Error: " + command.CommandText + " - " + ex.ToString());
                return false;
            }
        }

        public DataTable ExecuteSQLTable(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    command.Connection = connection;

                    DataTable table = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    if (adapter.Fill(table) > 0)
                        return table;
                    else return null;
                }
            }
            catch (Exception ex)
            {
                if (MessageLogged != null) MessageLogged("SQL Error: " + ex.ToString());
                return null;
            }
        }

        public string ExecuteSQLScalar(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    command.Connection = connection;

                    object obj = command.ExecuteScalar();

                    if (obj != null) return obj.ToString();
                    else return null;
                }
            }
            catch (Exception ex)
            {
                if (MessageLogged != null) MessageLogged("SQL Error: " + ex.ToString());

                return "ERR Sql error: " + ex.Message;
            }
        }

        public string ExecuteSQLScalar(string query)
        {
            SqlCommand command = new SqlCommand(query);
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    command.Connection = connection;

                    object obj = command.ExecuteScalar();

                    if (obj != null) return obj.ToString();
                    else return null;
                }
            }
            catch (Exception ex)
            {
                if (MessageLogged != null) MessageLogged("SQL Error: " + ex.ToString());
                return "ERR Sql error: " + ex.Message;
            }
        }
    }
}